(function ($) {
    $.pkError = function (res) {
        BootstrapDialog.show({
            title: '<i class="glyphicon glyphicon-volume-up"></i>&nbsp;&nbsp;' + $.L('action_error'),
            message: res.hasOwnProperty('StateMsg') ? res.StateMsg : '',
            onhide: function () {
                console.log(typeof res);
                if (res.hasOwnProperty('actionType') && res.hasOwnProperty('url')) {
                    if (res.actionType !== 'alert') {
                        window.location.href = res.url;
                    }
                }
            },
            buttons: [{
                label: 'ok',
                action: function (dialog) {
                    dialog.close();
                }
            }]
        });
    };
    $.pkEvents = function (content, url) {
        BootstrapDialog.show({
            title: $.L('Events'),
            message: content,
            closable: false,
            buttons: [],
            onshow: function () {
                window.setTimeout('topage("' + url + '");', 1000);
            }
        });
    }

    $.pkLoadingPage = function (url, title) {
        url += '&_=' + new Date().getTime();
        BootstrapDialog.show({
            title: title,
            message: $('<div></div>').load(url),
            buttons: [
                {
                    label: 'ok',
                    action: function (dialog) {
                        $.pkFormPost($('#dialogform'));
                        dialog.close();
                    }
                }
            ]
        });
    }


})(jQuery);
$(function () {
    // 设置jQuery Ajax全局的参数
    $.ajaxSetup({
        //type: "POST",
        cache: false,
        error: function (jqXHR, textStatus, errorThrown) {
            var responseText = $.parseJSON(jqXHR.responseText);
            // console.log(jqXHR);
            // console.log(responseText);
            // console.log(jqXHR.status);
            switch (jqXHR.status) {
                case(500):
                    if (responseText.hasOwnProperty('StateMsg')) {
                        // if (typeof bootbox !== 'undefined') {
                        //     bootbox.alert(content);
                        // } else {
                        // }
                        var div_showTips = $('#show_tips');
                        if (div_showTips.length === 1) {
                            div_showTips.text(responseText.StateMsg)
                                .removeClass('alert-success').addClass('alert-warning');
                        } else {
                            pkcms.msg(responseText.StateMsg);
                        }
                    }
                    break;
                default:
                    break;
            }
        },
        success: function (data) {
            // console.log('data:', typeof data, data);
            var result;
            if (typeof data == "object") {
                result = data;
            } else if (typeof data == "string" && data.match("^\{(.+:.+,*){1,}\}$")) {
                result = jQuery.parseJSON(data);
            }
            // console.log('result:', typeof result, result);
            if (typeof result == "object" && result.hasOwnProperty('StateCode')) {
                // console.log('result:', typeof result, result);
                if ((result.StateCode === 200) && result.hasOwnProperty('actionType')) {
                    switch (result.actionType) {
                        case 'BackOff':
                            console.log($('#myModal').css('display') === 'block');
                            if ($('#myModal').css('display') === 'block') {
                                $('#myModal .modal-dialog .modal-content').empty();
                                $('#myModal').modal('hide');
                            }
                            if (result.hasOwnProperty('StateMsg')) {
                                pkcms.msg(result.StateMsg);
                            }
                            // console.log('result.url:', result.url !== null);
                            // if (result.hasOwnProperty('url') && (result.url !== '')) {
                            //     $('#main').data('src', result.url);
                            //     $('#main').load($('#main').data('src'));
                            // } else {
                            //     alert(mainSrc);
                            //     $('#main').load(mainSrc);
                            // }
                            if ($('#subMain').css('display') !== 'block') {
                                $('#backPage').click();
                            } else {
                                mainToggler();
                            }
                            break;
                        case 'openWin':
                            window.location.href = result.url;
                            break;
                    }
                } else if (result.hasOwnProperty('StateMsg')) {
                    pkcms.msg(result.StateMsg);
                }
            }
        }
    });
});

// ================== 异步加载页面 start
function mainToggler() {
    if ($('#subMain').css('display') === 'block') {
        $('#subMain').empty().hide();
        $('#main').show();
    } else {
        $('#main').hide();
        $('#subMain').show();
    }
}

function failurePaging(JSxml) {
    $('#myModal').modal('hide').removeClass('loading');
    var responseText = $.parseJSON(JSxml.responseText);
    // console.log(typeof responseText,responseText);
    if ((typeof responseText === "object") && (responseText.hasOwnProperty('StateMsg'))) {
        pkcms.msg(responseText.StateMsg);
    } else {
        pkcms.msg(responseText);
    }
}

function beginPaging() {
    $('#myModal').addClass('loading').empty().modal('show');
}

function successPaging() {
    $('#myModal').modal('hide').removeClass('loading');
    $("html,body").animate({scrollTop: 0}, 600);
    // console.log('scrollTop:',scrollTop);
    //A标签异步刷新，记录最后的内容列表的链接
    $('a[data-ajax-update="#main"]').on('click', function () {
        var url = $(this).attr('href');
        if ((url.indexOf('List') > 0) || (url.indexOf('del/AdminGetOptions') > 0)
            || (url.indexOf('AdminGetPage') > 0)) {
            // alert(url);
            $('#backPage').attr('href', $(this).attr('href'));
        }
    });
    if ($('button').length > 0) {
        $('button').on('click', function () {
            btnAction($(this));
        });
    }
    //如有多选操作的代码，则进行初始化
    if ($('#selectAll').length > 0) {
        $('#selectAll').die().on('click', function() {
            if ($(this).attr('checked') === 'checked') {
                $('input:checkbox').attr('checked', 'checked');
            } else {
                $('input:checkbox').removeAttr('checked');
            }
        });
    }
    //如有列表操作的代码，则进行初始化
    if ($('select[name="mobile"], select[name="attribute"]').length > 0) {
        $('select').change(function() {
            //列表操作
            var select_action = ['mobile', 'attribute'];
            if ($.inArray($(this).attr('name'), select_action) >= 0) {
                if ($(this).val() === '') {
                    return false;
                }
                var tableId = $(this).data('table');
                pkcms.listPost(tableId, $(this).attr('name'), $('#'+tableId).data('action'));
                return false;
            }
        });
    }
    if ($(".selectDate").length > 0) {
        $('.selectDate').datetimepicker({
            language:  'zh-CN',
            weekStart: 1,
            todayBtn:  1,
            autoclose: 1,
            todayHighlight: 1,
            startView: 2,
            minView: 2,
            forceParse: 0
        });
    }
}

// ================== 异步加载页面 end
