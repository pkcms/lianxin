// ==================== 新的 HTML5 上传
function html5_uploadFile(inputFileId, showInputId) {
    var file = document.getElementById(inputFileId).files[0];
    if (file) {
        var fd = new FormData();
        fd.append("fileToUpload", file);
        fd.append('uploadType', document.getElementById('uploadType').value)
        fd.append('showInputId', showInputId)
        var xhr = new XMLHttpRequest();
        xhr.upload.addEventListener("progress", html5_uploadProgress, false);
        xhr.addEventListener("load", html5_uploadComplete, false);
        xhr.addEventListener("error", html5_uploadFailed, false);
        xhr.addEventListener("abort", html5_uploadCanceled, false);
        xhr.open("POST", "/index.php/attachment/AdminHtml5Upload"); //修改成自己的接口
        xhr.send(fd);
    }
}

function html5_uploadProgress(evt) {
    if (evt.lengthComputable) {
        var percentComplete = Math.round(evt.loaded * 100 / evt.total);
        document.getElementById('progressNumber').innerHTML = percentComplete.toString() + '%';
    } else {
        document.getElementById('progressNumber').innerHTML = 'unable to compute';
    }
}

function html5_uploadComplete(evt) {
    /* 服务器端返回响应时候触发event事件*/
    var responseText = evt.target.responseText;
    if (responseText.match("^\{(.+:.+,*){1,}\}$")) {
        document.getElementById('tips').innerHTML = '';
        var json = $.parseJSON(responseText);
        console.log(json);
        $('#'+json.showFieldId).val(json.path);
        $('#progressNumber').html('');
    } else {
        document.getElementById('tips').innerHTML = responseText;
    }
}

function html5_uploadFailed(evt) {
    alert("There was an error attempting to upload the file.");
}

function html5_uploadCanceled(evt) {
    alert("The upload has been canceled by the user or the browser dropped the connection.");
}