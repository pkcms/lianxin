/**
 * 移除指定的 HTML 标签的 ID
 * 多文件（图片）上传的移除操作
 * @param id
 * @param class_name
 * @param field
 */
function remove_id(id, class_name, field) {
    $('#' + id).remove();
    if ($('.' + class_name).length < 2) {
        $('<input type="hidden" name="' + field + '" value="" />').appendTo('#' + class_name);
    }
}

function permutation(list_className) {
    //获取表格的行
    var x = document.querySelectorAll("." + list_className),
        //获取所有排序箭头
        y = document.querySelectorAll(".arrow_" + list_className),
        //获取表格
        w = document.getElementById(list_className);
    //点击箭头按键向上交换表格的行(li),交换之后之前的箭头按钮就不在鼠标的位置了,
    // 会再次触发排序函数02中的鼠标移出事件,运行排序函数01对i进行重新计数,
    // console.log('y:', y);
    for (var i = 0; i < x.length; i++) {
        (function (i) {
            y[2 * i].onclick = function () {
                w.insertBefore(x[i], x[i - 1]);
            };
            y[2 * i + 1].onclick = function () {
                if (x[i + 1] !== undefined) {
                    w.insertBefore(x[i + 1], x[i]);
                } else {
                    w.insertBefore(x[i], x[0]);
                }
            }
        })(i)
    }
}

//所有东西加载完之后就运行排序函数
function permutation_uploadList(htmlId) {
    //获取所有排序箭头
    var y = document.querySelectorAll(".arrow_" + htmlId);
    for (var j = 0; j < y.length; j++) {
        //鼠标移动到任意一个箭头上就能触发排序函数01,让排序函数中的i值重新计算,不使用之前闭包内保存的值
        (function (j) {
            y[j].onmouseover = function () {
                permutation(htmlId);
            }
        })(j);
    }
}