
//异步常规提交
if ($('.btn-primary').length > 0 || $('.btn-adminLogin').length > 0) {
    $('.btn-primary, .btn-adminLogin').die().live('click', function() {
        $.pkFormPost($('#' + $(this).data('form')));
        return false;
    });
}
//异步非常规提交--用于列表处理
if ($('button[data-action]').length > 0) {
    $('button[data-action]').die().live('click', function() {
        var question_action = ['revert', 'sort', 'del', 'remove', 'emptybin'];
        var messageStr = $.L('action_check') + $(this).text();
        var action = $(this).data('action');
        var formId = $(this).data('table');
        console.log('data-action:',action);
        if ($.inArray(action, question_action) >= 0) { //列表提交[删除和其他属性]
            BootstrapDialog.show({
                id: 'delDialog',
                title: $.L('question'),
                message: messageStr,
                onhide: function() {
                    $('#delDialog').remove();
                },
                buttons: [{
                        label: $.L('confirm'),
                        cssClass: 'btn-primary',
                        action: function(dialog) {
                            dialog.close();
                            pkcms.listPost(formId, action)
                        }
                    }, {
                        label: $.L('cancel'),
                        action: function(dialog) {
                            dialog.close();
                        }
                    }]
            });
        }
    });
}
//如有选项卡的代码，则进行初始化
if ($('.nav-tabs').length > 0) {
    $('.nav-tabs a:first').tab('show');
}
//工具提示
//A标签异步刷新，记录最后的内容列表的链接
$('a[data-ajax-update="#main"]').die().live('click', function() {
    $('#main').attr('src', $(this).attr('href'));
});
//下拉菜单
if ($('.dropdown-toggle').length > 0) {
    $('.dropdown-toggle').die().live('click', function() {
        $(this).dropdown('show');
    });
}
//控制 modal-body 的高度
$('.modal-body').slimScroll({
    height: (pkcms.pageHeight() - 200)
});
