//列表提交
pkcms.listPost = function (htmlId, btnAction, tableAction) {
    pkcms.msg($.L('action_do'));
    var data = {action: btnAction};
    if ($('input:hidden').length > 0) {
        $('input:hidden').each(function (index) {
            var hObj = $('input:hidden:eq(' + index + ')');
            data[hObj.attr('name')] = hObj.val();
        });
    }

    // revert:还原 del:删除 remove:彻底删除 attribute:属性 mobile:移动
    var postActionArray = ['revert', 'del', 'remove', 'attribute', 'mobile'];
    if ($.inArray(btnAction, postActionArray) >= 0) {
        if ($('#' + htmlId + ' tbody input:checkbox').length > 0) {
            data.selectData = new Array();
            for (var i = 0; i < $('#' + htmlId + ' tbody input:checkbox:checked').length; i++) {
                var checkbox_checked = $('#' + htmlId + ' tbody input:checkbox:checked:eq(' + i + ')');
                data.selectData.push(checkbox_checked.val());
            }
        }
        if (($('#' + htmlId + ' tbody input:radio:checked').length > 0)) {
            data.selectData = $('input:radio:checked').val();
        }
    }
    // //清空回收站
    // if (action == 'emptybin') {
    //     data += "&emptybin=" + $('input[name="emptybin"]').attr('id');
    // }
    if ($.inArray(btnAction, ['attribute','mobile']) >= 0) {
        data[btnAction] = $('select[name="' + btnAction + '"]').val();
    }
    if (btnAction == 'sort') {
        data.selectData = {};
        for (var i = 0; i < $('#' + htmlId + ' input:text').length; i++) {
            var sortObj = $('#' + htmlId + ' input:text:eq(' + i + ')');
            data.selectData[sortObj.data('id')] = sortObj.val();
        }
    }
    $.post('/index.php/' + tableAction, data);
};

function btnAction(_this) {
    pkcms.msg($.L('action_do'));
    let _btnAction = _this.data('action');
    console.log('btnAction:', _btnAction);
    let tableId = _this.data('table');
    let formId = _this.data('form');
    if (formId !== undefined) {
        let formObj = $('#' + formId);
        let formAction = '/index.php/' + formObj.attr('action');
        $.post(formAction, formObj.serialize());
    }
    if (tableId !== undefined) {
        let question_action = ['revert', 'sort', 'del', 'remove'];
        let tableObj = $('#' + tableId);
        let messageStr = $.L('action_check') + _this.text();
        let tableAction = tableObj.data('action');
        // alert(_btnAction);
        if ($.inArray(_btnAction, question_action) >= 0) {
            bootbox.confirm(messageStr, function (c) {
                console.log('c:', c);
                if (c) {
                    pkcms.listPost(tableId, _btnAction, tableAction)
                }
            });
        }
    }
}

function toOtherCategory(showField) {
    let selectData = '';
    let selectInput = $('#toOtherCategoryList input:checked');
    selectInput.each(function (index) {
        let value = $('#toOtherCategoryList input:checked:eq(' + index + ')').val().split('|');
//                console.log(value);
        selectData += '<li><input type="hidden" name="othors[]" value="' + value[0] + '" />' + value[1] + '</li>';
    });
    $('#' + showField).html(selectData);
}