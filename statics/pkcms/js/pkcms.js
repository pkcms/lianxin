var pkcms = {};
//检测浏览器的类型及版本
pkcms.checkBrowser = function () {
    var ug = navigator.userAgent.toLowerCase();
    var userAgent = document.getElementById("userAgent");

    //检测IE及版本
    var IE = ug.match(/msie\s*\d\.\d/); //提取浏览器类型及版本信息，注match()方法返回的是数组而不是字符串
    var isIE = check(/msie/);
    if (isIE) {
        pkcms.browserType = "Internet Explorer";
        pkcms.browserVer = IE.join(" ").match(/[0-9]/g).join("."); //先用join()方法转化为字符串，然后用match()方法匹配到版本信息，再用join()方法转化为字符串
    }

    //检测chrome及版本
    var chrome = ug.match(/chrome\/\d\.\d/gi);
    var isChrome = check(/chrome/);
    if (isChrome) {
        pkcms.browserType = "Chrome";
        pkcms.browserVer = chrome.join(" ").match(/[0-9]/g).join(".");
    }

    //检测firefox及版本
    var firefox = ug.match(/firefox\/\d\.\d/gi);
    var isFirefox = check(/firefox/);
    if (isFirefox) {
        pkcms.browserType = "Firefox";
        pkcms.browserVer = firefox.join(" ").match(/[0-9]/g).join(".");
    }
};
//复制到剪贴板
pkcms.copyToClipboard = function (txt) {
    if (window.clipboardData) {
        window.clipboardData.clearData();
        window.clipboardData.setData("Text", txt);
    } else if (navigator.userAgent.indexOf("Opera") !== -1) {
        window.location = txt;
    } else if (window.netscape) {
        try {
            netscape.security.PrivilegeManager.enablePrivilege("UniversalXPConnect");
        } catch (e) {
            alert($.L('copy_msg'));
        }
        var clip = Components.classes['@mozilla.org/widget/clipboard;1'].createInstance(Components.interfaces.nsIClipboard);
        if (!clip)
            return;
        var trans = Components.classes['@mozilla.org/widget/transferable;1'].createInstance(Components.interfaces.nsITransferable);
        if (!trans)
            return;
        trans.addDataFlavor('text/unicode');
        var str = new Object();
        var len = new Object();
        var str = Components.classes["@mozilla.org/supports-string;1"].createInstance(Components.interfaces.nsISupportsString);
        var copytext = txt;
        str.data = copytext;
        trans.setTransferData("text/unicode", str, copytext.length * 2);
        var clipid = Components.interfaces.nsIClipboard;
        if (!clip)
            return false;
        clip.setData(trans, null, clipid.kGlobalClipboard);
        alert($.L('copy_ok'));
    }
};
//动态加载 JavaScript 文件
pkcms.loadScript = function (file_name) {
    var url = location.protocol + '//' + location.hostname
        + (location.port == '' ? '' : ':' + location.port) + '/statics/js/' + file_name + ".js";
    document.write('<scr' + 'ipt type="text/javascript" src="' + url + '"><\/scr' + 'ipt>');
}
pkcms.msg = function (content) {
    Messenger().post(content);
}
//浏览计数（catid：栏目id,id:内容id)
pkcms.hits = function (catid, id) {
    if ($('#pk_hits').length > 0) {
        $('#pk_hits').html('loading_');
        $.get('/index.php/content/asynchronous/', 'actionType=hits&catid=' + catid + '&id=' + id, function (result) {
            $('#pk_hits').html(result);
        });
    }
};