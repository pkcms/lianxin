<?php
/**
 * 项目基本配置
 * User: Administrator
 * Date: 2019/5/27
 * Time: 11:52
 */

const LONG_RANGE_TIMEOUT = 180;
const DEFAULT_MODULE = 'content';
const DEFAULT_CONTROLLER = 'site';
const DEFAULT_SITE_ID = 1;
const BAIDU_MAP_KEY = 'kTVcrmGo2kycDlhxdjtA3Mqe';
const CAPTCHA_CODE_EXPIRE = 600;
const CONTENT_SHOW_PUT_TIME_CELL = 30;
const FORM_PUT_TIME_CELL = 180;

const API_GuestMsg = 'https://guestmsg.zj11.net/';