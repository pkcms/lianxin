<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<{if $isLoadJS}>
<link href="/statics/bootstrap-3.3.7-dist/css/bootstrap.css" rel="stylesheet" type="text/css" />
<link href="/statics/bootstrap-3.3.7-dist/css/bootstrap-theme.min.css" rel="stylesheet" type="text/css" />
<{/if}>

<div class="container-fluid">
    <div class="jumbotron">
        <h1>消息提示</h1>
        <p><{$msg}></p>
        <{if $url}>
        <p>
            <a href="<{$url}>" class="btn btn-primary btn-large">
                点这里进行跳转
            </a>
        </p>
        <{/if}>
    </div>
</div>
