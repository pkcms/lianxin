<?php

/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2019/5/25
 * Time: 11:56
 */

//namespace PKCore;

use PKCore\Request;
use PKCore\Route;

function tplTag_Template($file, $dir = NULL)
{
    $tpl = new \PKCore\Tpl();
    if (Request::controller() != 'AdminDoMakeHtml' && (Request::module() == 'Admin' || stristr(Request::controller(), 'Admin'))) {
        $dir = ucfirst($dir);
    }
    $Site_Themes = \PKApp\Content\Classes\BaseController::$siteEntity['setting']['Site_Themes'];
    empty($Site_Themes) ?: $tpl->SetTplThemes($Site_Themes);
    is_null($dir) ?: $tpl = $tpl->SetTplDir($dir);
    return $tpl->SetTplParamList(Request::param())->Display($file);
}

function tplTag_PK($module, $data)
{
    //转换成参数数组
    preg_match_all("/([a-z]+)\=[\"]?([^\"]+)[\"]?/i", stripslashes($data), $matches, PREG_SET_ORDER);
    $param = array('module' => $module);
    //可视化条件
    foreach ($matches as $v) {
        $param[$v[1]] = $v[1] == 'action' ? ucwords($v[2]) : $v[2];
    }
    $display_html = '$res = tplTag_Module(' . str_replace("'", '"', var_export($param, TRUE)) . ');'
        . 'if(!is_array($res)) {echo $res;} else {extract($res);}';
    return "<" . '?php ' . $display_html . '?' . '>';
}

/**
 * PK标签结束
 */
function tplTag_PKEnd()
{
    return '';
}

/**
 * 模板模块标签调用
 * @param $param
 * @return string
 */
function tplTag_Module(array $param = array())
{
    try {
        $module = ucwords($param['module']);
        unset($param['module']);
        $action = ucwords($param['action']);
        unset($param['action']);
        if (isset($action) == FALSE || empty($action))
            $action = 'Main';
        $class = '\PKApp\\' . $module . '\Classes\\' . $module . 'TemplateTag';
        $classObj = new $class();
        if (method_exists($classObj, $action)) {
            return $classObj->$action($param);
        } else {
            throw new \Exception(Route\language('Template_TagFunc_NoExists') . $module . ' / ' . $action);
        }
    } catch (\Exception $ex) {
        \PKCore\handlerException($ex);
    }
    return '';
}

/**
 * 转义 // 为 /
 * @param $var
 * @return mixed
 */
function tplTag_Addquote($var)
{
    return str_replace("\\\"", "\"", preg_replace("/\[([a-zA-Z0-9_\-\.\x7f-\xff]+)\]/s", "['\\1']", $var));
}
