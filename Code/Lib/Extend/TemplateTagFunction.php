<?php
/**
 * 模板函数标签
 */

use PKApp\Model\Classes\FormControl;
use PKCore\Converter;
use PKCore\PKController;
use PKCore\Request;
use PKCore\TMPSave;
use PKCore\Tpl;

/**
 * 单选选择器
 * @param array $selectList
 * @param int $value
 * @param string $inputName
 * @return string|null
 */
function RadioButton($selectList = array(), $value = 0, $inputName = '')
{
    return \PKApp\Model\Classes\FormControl::OptionByRadio(null, $inputName, $selectList, $value);
}

/**
 * 多选
 * @param array $selectList 数组
 * @param array $valueList
 * @param string $inputName 控件名称
 * @return string|null
 */
function CheckBoxButton($selectList, $valueList = array(), $inputName = '')
{
    return \PKApp\Model\Classes\FormControl::OptionByCheckBox(null, $inputName, $selectList, $valueList);
}

/**
 * 下拉菜单的选择器
 * @param $inputName
 * @param array $selectList 菜单数组
 * @param null $value 选择项
 * @param null $inputId 禁用选项
 * @param array $disabledLIst
 * @return string
 */
function optionBySelct($inputName, $selectList = array(), $value = null, $inputId = null, $disabledLIst = array())
{
    if (is_string($value) && stristr($value, ',')) {
        $value = explode(',', $value);
    }
    settype($value, 'array');
    return \PKApp\Model\Classes\FormControl::OptionBySelect($inputId, $inputName, $selectList, $value, $disabledLIst);
}

function form_editor($inputName = '', $inputFileId = '', $value = array())
{
    return \PKApp\Model\Classes\FormControl::editor($inputFileId, $inputName, $value, null);
}

function UploadFile($uploadType = '', $value = array(), $inputName = '', $inputTextId = null, $inputFileId = 'fileToUpload')
{
    return \PKApp\Model\Classes\FormControl::UploadFile($inputName, $uploadType, $value, $inputTextId, $inputFileId);
}

function UploadFileListEachByImage($legendName, $liId, $inputName, $value)
{
    return FormControl::UploadFile_ImageList($legendName, $liId, $inputName, $value);
}

function UploadFileListEachByImageItem($index, $liId, $inputName, $imagePath, $imageInfo, $imageLink = '')
{
    $tpl = new Tpl();
    $param = array('liId' => $liId, 'index' => $index, 'inputName' => $inputName, 'imagePath' => $imagePath,
        'imageInfo' => $imageInfo, 'imageLink' => $imageLink);
    return $tpl->SetTplParamList($param)->SetTplDir('Model')
        ->PhpDisplay('form_control_uploadList_imageEachItem');
}

function UploadFileListEachByFileItem($index, $liId, $inputName, $filePath, $fileInfo)
{
    $tpl = new Tpl();
    $param = array('liId' => $liId, 'index' => $index, 'inputName' => $inputName, 'filePath' => $filePath,
        'fileInfo' => $fileInfo);
    return $tpl->SetTplParamList($param)->SetTplDir('Model')
        ->PhpDisplay('form_control_uploadList_fileEachItem');
}

function UploadFileListBtn($showHtmlId = '', $modelId = '', $inputName = '')
{
    $getParam = 'showHtmlId=' . $showHtmlId . '&inputName=' . urlencode($inputName) . '&modelId=' . $modelId;
    return <<<html
<a class="btn btn-default" href="/index.php/Attachment/AdminFileList?$getParam"
 data-toggle="modal" data-target="#myModal">Upload File list</a>
<script type="text/javascript">
    permutation_uploadList('{$showHtmlId}');
</script>
html;
}

function admin_breadcrumb($page)
{
    return <<<eof
<div class="crumbs">
    <ul id="breadcrumbs" class="breadcrumb">
        <li>
            <i class="icon-home"></i>
            <a data-ajax="true" data-ajax-begin="beginPaging"
               data-ajax-failure="failurePaging" data-ajax-mode="replace"
               data-ajax-success="successPaging" data-ajax-update="#main"
               href="/index.php/admin/admin" title="后台首页">
                后台首页</a>
        </li>
        <li class="current">{$page}</li>
    </ul>
</div>
eof;
}

/**
 * 生成缩略图函数
 * @param string $image 图片路径
 * @param int $width 缩略图宽度
 * @param int $height 缩略图高度
 * @param int $narrow 是否等比例
 * @param null $exp 保存的扩展名
 * @param string $smallpic 无图片是默认图片路径
 * @return mixed
 */
function thumb($image, $width = 135, $height = 135, $narrow = 0, $exp = NULL, $smallpic = 'nopic.gif')
{
    if (empty($image)) {
        return '/statics/images/nopic.gif';
    }
    if (strpos($image, 'ttp:') > 0) {
        return $image;
    }
    $fileArr = explode('.', $image);
    if (strtolower($fileArr[count($fileArr) - 1]) == 'gif') {
        return $image;
    }
    $imagePath = PATH_ROOT . ltrim(str_replace('/', DS, $image), DS);
    if (!file_exists($imagePath)) {
        return $image;
    }
    $width = \PKCore\Formats::isNumeric($width);
    $height = \PKCore\Formats::isNumeric($height);

    $arr = explode('/', $image);
    $fileName = explode('.', $arr [count($arr) - 1]);
    unset($arr [count($arr) - 1]);
    $thumbImgFileName = $fileName[0] . '_' . $width . '-' . $height . '.' . (empty($exp) ? strtolower($fileName[1]) : $exp);
    $thumbPath = PATH_ROOT . 'thumb' . DS . $arr [count($arr) - 1] . DS . $thumbImgFileName;
    if (!file_exists($thumbPath)) {
        new \PKApp\Attachment\Classes\Thumb($image, $thumbPath, $width, $height, $narrow);
    }
    return str_replace(DS, '/', str_replace(PATH_ROOT, '/', $thumbPath));
}

/**
 * 字符截取 支持UTF8
 * @param $string
 * @param int $length
 * @param string $dot
 * @return mixed|string
 */
function str_cut($string, $length = 100, $dot = '...')
{
    $strLen = strlen($string);
    if ($strLen <= $length)
        return $string;
    $string = str_replace(array(' ', '&nbsp;', '&amp;', '&quot;', '&#039;',
        '&ldquo;', '&rdquo;', '&mdash;', '&lt;', '&gt;', '&middot;', '&hellip;'),
        array('∵', ' ', '&', '"', "'", '“', '”', '—', '<', '>', '·', '…'),
        $string);
    $strcut = '';
    $length = intval($length - strlen($dot) - $length / 3);
    $n = $tn = $noc = 0;
    while ($n < strlen($string)) {
        $t = ord($string[$n]);
        if ($t == 9 || $t == 10 || (32 <= $t && $t <= 126)) {
            $tn = 1;
            $n++;
            $noc++;
        } elseif (194 <= $t && $t <= 223) {
            $tn = 2;
            $n += 2;
            $noc += 2;
        } elseif (224 <= $t && $t <= 239) {
            $tn = 3;
            $n += 3;
            $noc += 2;
        } elseif (240 <= $t && $t <= 247) {
            $tn = 4;
            $n += 4;
            $noc += 2;
        } elseif (248 <= $t && $t <= 251) {
            $tn = 5;
            $n += 5;
            $noc += 2;
        } elseif ($t == 252 || $t == 253) {
            $tn = 6;
            $n += 6;
            $noc += 2;
        } else {
            $n++;
        }
        if ($noc >= $length) {
            break;
        }
    }
    if ($noc > $length) {
        $n -= $tn;
    }
    $strcut = substr($string, 0, $n);
    $strcut = str_replace(array('∵', '&', '"', "'", '“', '”', '—', '<', '>', '·', '…'),
        array(' ', '&amp;', '&quot;', '&#039;', '&ldquo;', '&rdquo;', '&mdash;', '&lt;', '&gt;', '&middot;', '&hellip;'),
        $strcut);
    return $strcut . $dot;
}

function download($path)
{
    (!empty($path)) ?: \PKCore\alert('DownLoad_FileNoExists');
    return Request::domain() . 'index.php/Tools/Download?file=/' . ltrim($path, '/');
}

function GuestMsg($isFloat = true)
{
    // 查询IP
    $apiUrl_ip = '/index.php/GuestMsg/PublicAjax';
    // 初始正文
    $style = Request::param('siteContact_guestmsgstyle');
    $site_id = Request::param('site_id');
    $tel = Converter::jsonEnCode(Request::param('siteContact_tel'));
    $qq = Converter::jsonEnCode(Request::param('siteContact_qq'));
    $weChat = Converter::jsonEnCode(Request::param('siteContact_wechat'));
    $float = (bool)$isFloat;
    $id = $float ? 'float' : '';
    $domain = Request::domain();
    $apiUrl = API_GuestMsg;
    $time = time();
    return <<<html
<div id="GuestMsg_{$id}"></div>
<link rel="stylesheet" href="/statics/helloweba_tncode/style.css" />
<script type="text/javascript" src="/statics/helloweba_tncode/tn_code.js"></script>
<script type="text/javascript">
    $(document).ready(function () {
        let params = {
          siteId: "{$site_id}",
          style: "{$style}",
          domain: "{$domain}",
          float: "{$float}",
          tel: {$tel},
          qq: {$qq},
          weChat: {$weChat}
      };
      $.get('{$apiUrl_ip}',function(res) {
          if ((typeof res === "string")) {
              res = JSON.parse(res);
          }
        if (res.hasOwnProperty('StateCode') && (res.StateCode === 200) && res.hasOwnProperty('Result')) {
            var result = JSON.parse(res.Result);
            if (result.hasOwnProperty('isHyperNumber')) {
                params['isHyperNumber'] = result.isHyperNumber;
                $.post('{$apiUrl}index.php?_={$time}', params, function(body) {
                    $('#GuestMsg_{$id}').html(body);
                });
            }
        }
      });
    });
</script>
html;
}

function listParamsSearchUrl($siteId = 0, $catId = 0, $paramName = '', $paramValue = '', $pageIndex = 0)
{
    $get = Request::get();
    $get['siteId'] = $siteId;
    $get['catId'] = $catId;
    unset($get['id']);
    if (!empty($paramName)) {
        $get[$paramName] = $paramValue;
        if (empty($paramValue)) {
            unset($get[$paramName]);
        }
    }
    if (empty($pageIndex)) {
        unset($get['pageIndex']);
    } else {
        $get['pageIndex'] = $pageIndex;
    }
    $paramsList = array();
    foreach ($get as $key => $value) {
        $paramsList[] = $key . '=' . $value;
    }
    return PKController::ToMeUrl('content/Search') . '?' . implode('&', $paramsList);
}