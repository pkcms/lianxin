<?php
/**
 * 底层语言包
 * User: Administrator
 * Date: 2019/5/24
 * Time: 9:08
 */

return array(
    'UserLogin_IdEmpty' => array(
        'zh-cn' => '用户登陆身份已经失效或过期',
        'en' => '',
    ),
    'UserLogin_Power_Error' => array(
        'zh-cn' => '您的没有这个功能模块的操作权限',
        'en' => '',
    ),
    'DataBase_ConfigEmpty' => array(
        'zh-cn' => '数据库的配置信息为空，或者是数组类型的配置信息',
        'en' => '',
    ),
    'DataBase_Config_IndexEmpty' => array(
        'zh-cn' => '数据库的配置信息的索引为空',
        'en' => '',
    ),
    'PHPExtension_MBString_NoExists' => array(
        'zh-cn' => '当前服务器的 PHP 运行库（版本）中的 mbstring 扩展没有开启，请联系服务器商开启，否则无法使用与该扩展有关的功能模块。',
        'en' => '',
    ),
    'PHPExtension_Sqlite_NoExists' => array(
        'zh-cn' => '当前服务器的 PHP 运行库（版本）中的 Sqlite 扩展没有开启，请联系服务器商开启，否则无法使用与该扩展有关的功能模块。',
        'en' => '',
    ),
    'PHPExtension_PDOSqlite_NoExists' => array(
        'zh-cn' => '当前服务器的 PHP 运行库（版本）中的 pdo_sqlite 扩展没有开启，请联系服务器商开启，否则无法使用与该扩展有关的功能模块。',
        'en' => '',
    ),
    'PHPExtension_zip_NoExists' => array(
        'zh-cn' => '当前服务器的 PHP 运行库（版本）中的 zip 扩展没有开启，请联系服务器商开启，否则无法使用与该扩展有关的功能模块。',
        'en' => '',
    ),
    'DataBase_SqliteFile_Empty' => array(
        'zh-cn' => '数据库配置信息中的 Sqlite 数据库文件参数不存在，或者为空',
        'en' => '',
    ),
    'DataBase_SqlitePrefix_Empty' => array(
        'zh-cn' => '数据库配置信息中的 Sqlite 数据表名的前缀参数不存在，或者为空',
        'en' => '',
    ),
    'DataBase_SqliteFile_NoExists' => array(
        'zh-cn' => '当前项目中的 Sqlite 数据库文件找不到或者不存在，请将数据库文件放在：',
        'en' => '',
    ),
    'DataBase_Sql_NumError' => array(
        'zh-cn' => 'SQL 执行出错',
        'en' => '',
    ),
    'SystemFile_IncludeError' => array(
        'zh-cn' => '系统在引用时找不到文件路径，路径是：',
        'en' => '',
    ),
    'TempLate_File_NoExists' => array(
        'zh-cn' => '模板文件找不到或不存在，路径是：',
        'en' => '',
    ),
    'Template_TagModule_NoExists' => array(
        'zh-cn' => '模板的模块标签找不到或不存在，模块标签名是：',
        'en' => '',
    ),
    'Template_TagFunc_NoExists' => array(
        'zh-cn' => '模板的操作标签找不到或不存在，操作标签名是：',
        'en' => '',
    ),
    'Data_Input_Success' => array(
        'zh-cn' => '数据入库成功',
        'en' => '',
    ),
    'Select' => array(
        'zh-cn' => '请选择……',
        'en' => '',
    ),
    'Dict_Empty' => array(
        'zh-cn' => '请检查字典数据文件（路径：……\Cache\Config\Dictionaries.php）是否已经更新到最新，现发现缺少关键查询：',
        'en' => ''
    ),
    'Module_InstallOk' => array(
        'zh-cn' => '扩展模块安装成功',
        'en' => '',
    ),
    'App_Error' => array(
        'zh-cn' => '没有正确安装，请重新安装',
        'en' => ''
    ),
);
