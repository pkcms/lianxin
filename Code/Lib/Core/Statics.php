<?php
/**
 * 静态处理
 * User: wz_zh
 * Date: 2017/7/13
 * Time: 9:46
 */

namespace PKCore;

use PKCore\Route;

Route\extendCore('Fsocketopen');

use PKCore\Extend\Fsocketopen;

class Statics
{

    /**
     * 将 HTML 转换成 ASCII 码
     * @param string $html_code HTML 代码
     * @return mixed|string
     */
    public static function toASCII($html_code = '')
    {
        if ($html_code != "") {
            $html_code = str_replace(">", ">", $html_code);
            $html_code = str_replace("<", "<", $html_code);
            $html_code = str_replace(" ", chr(32), $html_code);
            $html_code = str_replace("", chr(13), $html_code);
            $html_code = str_replace("<br>", chr(10) & chr(10), $html_code);
            $html_code = str_replace("<BR>", chr(10), $html_code);
        }
        return $html_code;
    }

    /**
     * 从文章正文中提取图片路径
     * @param string $html_code
     * @return bool|mixed
     */
    public static function imgsinarticle($html_code = '')
    {
        $temp = array();
        preg_match_all("/(src|SRC)=[\"|'| ]{0,}((.*).(gif|jpg|jpeg|bmp|png))/isU", $html_code, $temp);
        if (is_array($temp[0]) == FALSE) {
            return FALSE;
        }
        foreach ($temp[0] as $key => $value) {
            $temp[0][$key] = substr($value, 5);
        }
        return $temp[0];
    }

    /**
     * 对 HTML 代码进行压缩
     * @param string $data 要处理的内容信息
     * @return string
     */
    public static function zip($data)
    {
        if (strlen($data) > 2) {
            $data = preg_replace("/[\r|\n|\t|\r\n]/", "", $data);
            $data = preg_replace("/\/\/[\S\f\t\v ]*?;[\r|\n]/", "", $data); // js注释请以";"号结尾
            // $data=preg_replace("/\<\!\-\-[\s\S]*?\-\-\>/","",$data);//去掉html里的<!--注释-->
            $data = preg_replace("/\>[\s]+\</", "><", $data);
            $data = preg_replace("/;[\s]+/", ";", $data);
            $data = preg_replace("/[\s]+\}/", "}", $data);
            $data = preg_replace("/}[\s]+/", "}", $data);
            $data = preg_replace("/\{[\s]+/", "{", $data);
            $data = preg_replace("/([\s]){2,}/", "$1", $data);
            $data = preg_replace("/[\s]+\=[\s]+/", "=", $data);
            $data = str_replace('	', '', $data);
            return $data;
        } else {
            return "";
        }
    }

    /**
     * 分页的处理
     * @param int $data_count 总数据量
     * @param int $line_number 返回列的行数
     * @param int $page_code_index 当前的页码
     * @param string $link 翻页
     * @return array
     */
    public static function pages($data_count = 0, $line_number = 0, $page_code_index = 0, $link = '')
    {
        $page_code_index = max($page_code_index, 1);
        // 基本页码总数
        $base_page_size = 5;
        //最后页，也是总页数
        $page_count = ceil($data_count / $line_number);
        //上一页
        $pre_page_code = max($page_code_index - 1, 1);
        // 下一页
        $next_page_code = $page_code_index + 1;
        // 判断下一页是否为最后一页
        $next_page_code <= $page_count ?: $next_page_code = $page_count;
        // 产生页码区间
        // 产生最小和最大的值
        $min = max($page_code_index - 2, 1);
        $max = $page_code_index + 2;
        if ($page_count <= $base_page_size) {
            // 要显示页数 小等于 要求基本总数
            $new_min = $page_count - $base_page_size + 1;
            $min = $new_min < 1 ? $min : min($new_min, $min);
            $max = $page_count;
        } else {
            // 要显示页数 大于 要求基本总数
            $max <= $page_count ?: $max = min($max, $page_count);
        }
        $page_section = range($min, $max);
        // 产生 HTML
        if ($page_count == 0) return array();
        $result = array(
            'index' => $page_code_index,
            'pageCount' => $page_count,
            'dataCount' => $data_count,
            'start' => self::_replaceStartPageCode(1, $link, true),
            'pre' => self::_replaceStartPageCode($pre_page_code, $link));
        foreach ($page_section as $number) {
            $result['sizeList'][$number] = self::_replaceStartPageCode($number, $link);
        }
        $result['next'] = self::_replaceStartPageCode($next_page_code, $link);
        $result['end'] = self::_replaceStartPageCode($page_count, $link);
        return $result;
    }

    private static function _replaceStartPageCode($pageCode, $link, $isHome = false)
    {
        $pregStr = '(_\[page\]|-\[page\]|\/\[page\]|\[page\])';
        preg_match($pregStr, $link, $preg);
        if (count($preg) == 0) {
            return $link;
        }
        $parts = preg_split('#\?#i', $link, 2);
        if (count($parts) == 2) {
            return str_replace('[page]', $pageCode, $link);
        } else {
            if ($isHome && ($pageCode == 1)) {
                return preg_replace($pregStr, '', $link);
            } else {
                return str_replace('[page]', $pageCode, $link);
            }
        }
    }

    /**
     * 对http协议的状态设定，跳转页面中需要经常使用的函数
     * @param string $code 状态码
     */
    public static function headStatusCode($code = '')
    {
        static $_status = array(
            // Informational 1xx
            100 => 'Continue',
            101 => 'Switching Protocols',
            // Success 2xx
            200 => 'OK',
            201 => 'Created',
            202 => 'Accepted',
            203 => 'Non-Authoritative Information',
            204 => 'No Content',
            205 => 'Reset Content',
            206 => 'Partial Content',
            // Redirection 3xx
            300 => 'Multiple Choices',
            301 => 'Moved Permanently',
            302 => 'Moved Temporarily ', // 1.1
            303 => 'See Other',
            304 => 'Not Modified',
            305 => 'Use Proxy',
            // 306 is deprecated but reserved
            307 => 'Temporary Redirect',
            // Client Error 4xx
            400 => 'Bad Request',
            401 => 'Unauthorized',
            402 => 'Payment Required',
            403 => 'Forbidden',
            404 => 'Not Found',
            405 => 'Method Not Allowed',
            406 => 'Not Acceptable',
            407 => 'Proxy Authentication Required',
            408 => 'Request Timeout',
            409 => 'Conflict',
            410 => 'Gone',
            411 => 'Length Required',
            412 => 'Precondition Failed',
            413 => 'Request Entity Too Large',
            414 => 'Request-URI Too Long',
            415 => 'Unsupported Media Type',
            416 => 'Requested Range Not Satisfiable',
            417 => 'Expectation Failed',
            // Server Error 5xx
            500 => 'Internal Server Error',
            501 => 'Not Implemented',
            502 => 'Bad Gateway',
            503 => 'Service Unavailable',
            504 => 'Gateway Timeout',
            505 => 'HTTP Version Not Supported',
            509 => 'Bandwidth Limit Exceeded'
        );
        if (array_key_exists($code, $_status)) {
            header('HTTP/1.1 ' . $code . ' ' . $_status[$code]);
        }
    }

    public static function resultJsonModel($data)
    {
        header("Content-type:application/json; charset=UTF-8");
        die(Converter::jsonEnCode($data));
    }

    public static function resultWebPageModel($code)
    {
        header("Content-type:text/html;charset=utf-8");
        exit($code);
    }

    /**
     * 关闭窗口脚本
     */
    final protected static function closeWin()
    {
        header("Content-type:text/html;charset=utf-8");
        echo "<script>window.opener=null;window.open('','_self');window.close();</script>";
    }

    /**
     * 获取 WEB 页的代码
     * @param string $url URL 地址
     * @return bool|string
     */
    public static function getWebCode($url = '')
    {
        if (empty($url)) {
            return false;
        }
        $data = '';
        for ($i = 0; $i < 5 && empty($data); $i++) {
            if (function_exists('fsockopen')) {
                $data = Fsocketopen::getHttpCode($url);
            } elseif (function_exists('file_get_contents')) {
                $data = Files::getContents($url);
            } else {
                return false;
            }
        }
        return $data;
    }

}