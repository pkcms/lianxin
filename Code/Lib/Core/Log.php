<?php
/**
 * Created by PhpStorm.
 * User: wz_zh
 * Date: 2019/5/4
 * Time: 12:59
 */

namespace PKCore;

\PKCore\Route\PlugIn();
use Monolog\Handler\StreamHandler;
use Monolog\Logger;

class Log
{
    private static $_name = 'PKFrame';

    private static function _dirName()
    {
        return PATH_TMP . 'Log' . DS;
    }

    private static function _fileName()
    {
        return self::_dirName() . date('Ymd') . '.log';
    }

    public static function WARNING($message, $errNo, $file, $lineNum)
    {
        try {
            $log = new Logger(self::$_name);
            $log->pushHandler(new StreamHandler(self::_fileName(), Logger::WARNING));
            $log->addWarning($message, array('errNo' => $errNo, 'file' => $file, 'lineNum' => $lineNum));
        } catch (\Exception $ex) {
            exit('system WARNING');
        }
    }

    public static function ERROR($message, $errNo, $file, $lineNum)
    {
        try {
            $log = new Logger(self::$_name);
            $log->pushHandler(new StreamHandler(self::_fileName(), Logger::ERROR));
            $log->addError($message, array('errNo' => $errNo, 'file' => $file, 'lineNum' => $lineNum));
        } catch (\Exception $ex) {
            exit('system ERROR');
        }
    }

    public static function INFO($message, array $context = array())
    {
        try {
            $log = new Logger(self::$_name);
            $log->pushHandler(new StreamHandler(self::_fileName(), Logger::NOTICE));
            $log->addInfo($message, $context);
        } catch (\Exception $ex) {
            exit('system INFO');
        }
    }

    public static function LOGS($message, array $context = array())
    {
        try {
            $log = new Logger(self::$_name);
            $log->pushHandler(new StreamHandler(self::_fileName(), Logger::NOTICE));
            $log->addNotice($message, $context);
        } catch (\Exception $ex) {
            exit('system LOGS:'.$ex->getMessage());
        }
    }

}