<?php

namespace PKCore;

class Xml
{

    public function Create($strRoot, $data, $itemName = NULL, $chiRoot = NULL)
    {
        $dom = new \DOMDocument('1.0', 'utf-8');
        $root = $dom->createElement($strRoot);
        $dom->appendChild($root);

        if (empty($chiRoot) == FALSE) {
            $chiXml = $dom->createElement($chiRoot);
            $root->appendChild($chiXml);
        }

        if (Formats::isArray($data)) {
            foreach ($data as $key => $value) {
                if (empty($itemName) == FALSE) {
                    $itemXml = $dom->createElement($itemName);
                    if (isset($chiXml) && empty($chiRoot) == FALSE)
                        $chiXml->appendChild($itemXml);
                    else
                        $root->appendChild($itemXml);
                }

                if (Formats::isArray($value)) {
                    foreach ($value as $k => $v) {
                        $name = $dom->createElement($k);
                        if (isset($itemXml) && empty($itemName) == FALSE)
                            $itemXml->appendChild($name);
                        else
                            $root->appendChild($name);

                        $str = $dom->createTextNode($v);
                        $name->appendChild($str);
                    }
                }
            }
        }
        return $dom->saveXML();
    }

    function Read($xmlfile, $strRoot, $array, $chiRoot = NULL)
    {
        $dom = new \DOMDocument('1.0', 'utf-8');
        $result = array();
        $dom->load($xmlfile);
        $strData = $dom->getElementsByTagName($strRoot);
        if (empty($chiRoot) == FALSE && method_exists($strData, 'getElementsByTagName'))
            $strData = $strData->getElementsByTagName($strData);

        foreach ($strData as $value) {
            if (Formats::isArray($array) == FALSE)
                continue;
            foreach ($array as $v) {
                if (method_exists($strData, 'getElementsByTagName')) {
                    $kData = $strData->getElementsByTagName($v);
                    if (method_exists($kData, 'item')) {
                        $result[$v] = $kData->item(0)->nodeValue;
                    }
                }
            }
        }
        return $result;
    }

}


