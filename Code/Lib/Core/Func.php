<?php
/**
 * 函数集
 * User: wz_zh
 * Date: 2017/7/12
 * Time: 17:21
 */

namespace PKCore;

use PKCore\Route;

/**
 * 计算程序执行时间
 * @param $start_time - 开始时间
 * @return int 单位ms
 */
function executeTime($start_time)
{
    return round(microtime(true) - $start_time, 5);
}

/**
 * 字符串方式实现 preg_match("/(s1|s2|s3)/", $string, $match)
 * @param string $string 源字符串
 * @param array $arr 要查找的字符串 如array('s1', 's2', 's3')
 * @param bool $returnvalue 是否返回找到的值
 * @return bool
 */
function dstrpos($string = '', $arr = array(), $returnvalue = false)
{
    if (empty($string))
        return false;
    foreach ((array)$arr as $v) {
        if (strpos($string, $v) !== false) {
            $return = $returnvalue ? $v : true;
            return $return;
        }
    }
    return false;
}

/**
 * 产生随机数字字符串
 * @param int $length 输出长度
 * @param string $chars 可选的 ，默认为 0123456789
 * @return   string     字符串
 */
function random($length, $chars = '0123456789')
{
    $hash = '';
    $max = strlen($chars) - 1;
    for ($i = 0; $i < $length; $i++) {
        $hash .= $chars[mt_rand(0, $max)];
    }
    return $hash;
}

/**
 * 生成随机字符串
 * @param int $length
 * @param bool $add_length
 * @return string
 */
function randstr($length = 6, $add_length = FALSE)
{
    $string = '123456789abcdefghijklmnpqrstuvwxyzABCDEFGHIJKLMNPQRSTUVWXYZ';
    if ($add_length)
        $string .= '!@#$%^&*-_';
    return random($length, $string);
}

/**
 * 获取当前时间戳,精确到毫秒
 * @return float
 */
function microtimeFloat()
{
    list($usec, $sec) = explode(" ", microtime());
    return ((float)$usec + (float)$sec);
}

/**
 * 时间戳转日期格式（精确到毫秒，x代表毫秒）
 * @param $time
 * @return mixed
 */
function get_microtime_format($time)
{
    if (strstr($time, '.')) {
        sprintf("%01.3f", $time); //小数点。不足三位补0
        list($usec, $sec) = explode(".", $time);
        $sec = str_pad($sec, 3, "0", STR_PAD_RIGHT); //不足3位。右边补0
    } else {
        $usec = $time;
        $sec = "000";
    }
    $date = date("Y-m-d H:i:s.x", $usec);
    return str_replace('x', $sec, $date);
}

/**
 * 时间日期转时间戳格式（精确到毫秒）
 * @param $time
 * @return string
 */
function get_data_format($time)
{
    list($usec, $sec) = explode(".", $time);
    $date = strtotime($usec);
    $return_data = str_pad($date . $sec, 13, "0", STR_PAD_RIGHT); //不足13位。右边补0
    return $return_data;
}

/**
 * 返回本周的开始日期到结束日期
 * @return array
 */
function weekDates()
{
    //当前日期
    $default_date = date("Y-m-d");
    //$first =1 表示每周星期一为开始日期 0表示每周日为开始日期
    $first = 1;
    //获取当前周的第几天 周日是 0 周一到周六是 1 - 6
    $w = date('w', strtotime($default_date));
    //获取本周开始日期，如果$w是0，则表示周日，减去 6 天
    $week_start = date('Y-m-d 00:00:00', strtotime("$default_date -" . ($w ? $w - $first : 6) . ' days'));
    //本周结束日期
    $week_end = date('Y-m-d 23:59:59', strtotime("$week_start +6 days"));
    return array($week_start, $week_end);
}

/*
 * 返回本月的开始日期到结束日期
 * @return array
 */
function monthDates()
{
    $month_start = date('Y-m-01 00:00:00', TIMESTAMP);
    $month_end = date('Y-m-d 23:59:59', strtotime("$month_start +1 month -1 day"));
    return array($month_start, $month_end);
}

/**
 * 返回数组的维度
 * @param $arr
 * @return mixed
 */
function arrayLevel($arr = array())
{
    $al = array(0);
    function aL($arr, &$al, $level = 0)
    {
        if (is_array($arr)) {
            $level++;
            $al[] = $level;
            foreach ($arr as $v) {
                aL($v, $al, $level);
            }
        }
    }

    aL($arr, $al);
    return max($al);
}

/**
 * 检查 PHP 的要求基版本
 * @param string $base_ver 基版本号
 * @return bool
 */
function checkPhpVer($base_ver = '')
{
    return version_compare(phpversion(), $base_ver, '>=') ? true : false;
}

function msg($string, $url = '', $actionType = 'BackOff', $StateCode = 200)
{
    if ($StateCode == 200) {
        if (!is_array($string)) {
            $msgStr = Route\language($string);
            $string = '';
        } else {
            $msgStr = '';
            $string = Converter::jsonEnCode($string);
        }
    } else {
        $msgStr = $string;
        $string = '';
    }
    if (Request::isAjax()) {
        if ($StateCode == 500) {
            Statics::headStatusCode($StateCode);
        }
        Statics::resultJsonModel(array('StateCode' => $StateCode, 'Result' => $string,
            'StateMsg' => $msgStr, 'actionType' => $actionType, 'url' => $url));
    } else {
        $tpl = new Tpl();
        $tpl->Notice($msgStr, $url);
    }
}

function alert($langKey = '', $tips = null, $errCode = 500, $url = null)
{
    $str = empty($langKey) ? '' : Route\language($langKey);
    if (!is_null($tips)) {
        $str .= ' ' . $tips;
    }
    msg($str, $url, 'alert', $errCode);
}

/**
 * 自定义错误处理
 * @param $errNo
 * @param $errStr
 * @param $errFile
 * @param $errLine
 */
function handlerError($errNo, $errStr, $errFile, $errLine)
{
    switch ($errNo) {
        case E_ERROR:
        case E_PARSE:
        case E_CORE_ERROR:
        case E_COMPILE_ERROR:
        case E_USER_ERROR:
        case E_STRICT:
            ob_end_clean();
            Log::ERROR($errStr, $errNo, $errFile, $errLine);
            alert($errStr);
            break;
        case E_USER_WARNING:
        case E_USER_NOTICE:
            Log::WARNING($errStr, $errNo, $errFile, $errLine);
            break;
        default:
//            Log::ERROR($errStr, $errNo, $errFile, $errLine);
            break;
    }
}

/**
 * 致命错误捕获
 */
function handleShutdown()
{
    // 如果是因为严重错误(未被捕获)导致脚本退出, 则需要处理(作为对 set_error_handler的补充)
    if (!is_null($error = error_get_last()) && isFatal($error['type'])) {
        // handleException() 函数同时处理 set_exception_handler
        handlerException(new \ErrorException(
            $error['message'], $error['type'], 0, $error['file'], $error['line']
        ));
    }
}

function isFatal($type)
{
    // 以下错误无法被 set_error_handler 捕获: E_ERROR, E_PARSE, E_CORE_ERROR, E_CORE_WARNING, E_COMPILE_ERROR, E_COMPILE_WARNING
    return in_array($type, array(E_COMPILE_ERROR, E_CORE_ERROR, E_ERROR, E_PARSE));
}

/**
 * 程序运行时遇到的区域性错误
 * @param string|array $vars
 */
function fail($vars = '')
{
    $result = "";
    if (Formats::isArray($vars)) {
        foreach ($vars as $key => $value) {
            if (Request::isAjax()) {
                $result .= $key . ':' . (is_array($value) ? json_encode($value) : $value) . ', ';
            } else {
                $result .= '<b style="color: red;">' . $key . '</b>:' . (is_array($value) ? json_encode($value) : $value) . '<br/>';
            }
        }
    } else {
        $result .= $vars;
    }
    alert($result);
}

function handlerException(\Exception $ex)
{
    Log::WARNING($ex->getMessage(), $ex->getCode(), $ex->getFile(), $ex->getLine());
    alert($ex->getMessage());
}

function handlerThrowable(\Throwable $ex)
{
    Log::WARNING($ex->getMessage(), $ex->getCode(), $ex->getFile(), $ex->getLine());
    alert($ex->getMessage());
}

register_shutdown_function('\PKCore\handleShutdown');
set_error_handler('\PKCore\handlerError');
if (version_compare(phpversion(), '7.0.0', '<')) {
    set_exception_handler('\PKCore\handlerThrowable');
}
