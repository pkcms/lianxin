<?php


namespace PKCore;

use PKCore\Route;

class Curl
{
    private $_url;
    private $_isSSI;
    private $_is_SSLVERSION = true;
    private $_param;
    private $_method;
    private $_timeOut = 0;
    private $_init;
    private $_response;

    public function __construct($url, $method)
    {
        function_exists('curl_init') or \PKCore\fail('php extend curl no exists');
        $this->_url = $url;
        $this->_isSSI = stristr($url, 'https');
        $this->_method = $method;
    }

    public function Param($param = array())
    {
        switch (strtolower($this->_method)) {
            case 'get':
            case 'file':
                $this->_url .= '?' . Converter::arrayToJoinString($param);
                break;
            case 'post':
                $this->_param = Converter::arrayToJoinString($param);
                break;
            case 'json':
                $this->_param = Converter::jsonEnCode($param);
                break;
        }
        return $this;
    }

    /**
     * 超时
     * @param int $second
     * @return $this
     */
    public function TimeOut($second = 0)
    {
        $this->_timeOut = $second;
        return $this;
    }

    public function OutPutParam()
    {
        exit(var_dump($this));
    }

    public function Request()
    {
        $this->_init = curl_init();
        // 设置超时限制防止死循环
        if ($this->_timeOut > 0) {
            curl_setopt($this->_init, CURLOPT_TIMEOUT, $this->_timeOut);
            curl_setopt($this->_init, CURLOPT_CONNECTTIMEOUT, $this->_timeOut);
        }
        $actionName = 'request' . ucfirst($this->_method);
        try {
            if (method_exists($this, $actionName)) {
                if (version_compare(phpversion(), '7.0.0', '>=')) {
                    $this->{$actionName}();
                } else {
                    $this->$actionName();
                }
            } else {
                throw new \Exception('CURL class no Exists Function name: ' . $actionName);
            }
        } catch (\Exception $exception) {
            handlerException($exception);
        }
        if ($actionName != 'requestFILE') {
            curl_setopt($this->_init, CURLOPT_URL, $this->_url);

            if (version_compare(PHP_VERSION, '5.5.0', '>')) {
                $this->_multi();
            } else {
                $this->_exec();
            }
        }
        return $this->_response;
    }

    public function setIsSSLVERSION($is_version)
    {
        $this->_is_SSLVERSION = $is_version;
        return $this;
    }

    private function _common()
    {
        curl_setopt($this->_init, CURLOPT_HEADER, FALSE);
        curl_setopt($this->_init, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($this->_init, CURLOPT_FAILONERROR, true);
    }

    protected function requestGET()
    {
        $this->_common();
        if ($this->_isSSI) {
            // 对认证证书来源的检查
            curl_setopt($this->_init, CURLOPT_SSL_VERIFYPEER, 0);
            // 从证书中检查SSL加密算法是否存在
            curl_setopt($this->_init, CURLOPT_SSL_VERIFYHOST, 1);
//            if ($this->_is_SSLVERSION) {
//                //CURL_SSLVERSION_TLSv1
//                curl_setopt($this->_init, CURLOPT_SSLVERSION, 1);
//            }
        }
        // 模拟用户使用的浏览器
        curl_setopt($this->_init, CURLOPT_USERAGENT, $_SERVER['HTTP_USER_AGENT']);
    }

    protected function requestPOST()
    {
        if ($this->_isSSI) {
            // 对认证证书来源的检查
            curl_setopt($this->_init, CURLOPT_SSL_VERIFYPEER, 0);
            // 从证书中检查SSL加密算法是否存在
            curl_setopt($this->_init, CURLOPT_SSL_VERIFYHOST, 1);
            if ($this->_is_SSLVERSION) {
                //CURL_SSLVERSION_TLSv1
                curl_setopt($this->_init, CURLOPT_SSLVERSION, 3);
            }
        }
        self::_common();
        if (isset($_SERVER['HTTP_USER_AGENT'])) {
            curl_setopt($this->_init, CURLOPT_USERAGENT, $_SERVER['HTTP_USER_AGENT']);
        }
        curl_setopt($this->_init, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($this->_init, CURLOPT_AUTOREFERER, 1);
        !array_key_exists('cookie_file', $GLOBALS) ?:
            curl_setopt($this->_init, CURLOPT_COOKIEFILE, $GLOBALS['cookie_file']);
        curl_setopt($this->_init, CURLOPT_POST, true);
        curl_setopt($this->_init, CURLOPT_POSTFIELDS, $this->_param);
    }

    protected function requestJSON()
    {
        if ($this->_isSSI) {
            // 对认证证书来源的检查
            curl_setopt($this->_init, CURLOPT_SSL_VERIFYPEER, 0);
            // 从证书中检查SSL加密算法是否存在
            curl_setopt($this->_init, CURLOPT_SSL_VERIFYHOST, 1);
            if ($this->_is_SSLVERSION) {
                //CURL_SSLVERSION_TLSv1
                curl_setopt($this->_init, CURLOPT_SSLVERSION, TRUE);
            }
        }
        $this->_common();
        curl_setopt($this->_init, CURLOPT_POST, true);
        curl_setopt($this->_init, CURLOPT_POSTFIELDS, $this->_param);
        curl_setopt($this->_init, CURLOPT_VERBOSE, 1);
        curl_setopt($this->_init, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($this->_init, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/json',
                'Content-Length: ' . strlen($this->_param))
        );
    }

    protected function requestFILE()
    {
        curl_setopt($this->_init, CURLOPT_URL, $this->_url);
        curl_setopt($this->_init, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($this->_init, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($this->_init, CURLOPT_RETURNTRANSFER, 1);
        $this->_response = curl_exec($this->_init); // 已经获取到内容，没有输出到页面上。
        curl_close($this->_init);
    }

    /**
     * 只支持 5.5.0 以上的版本(批处理)
     */
    private function _multi()
    {
        try {
            $handle = curl_multi_init();
            curl_multi_add_handle($handle, $this->_init);
            $flag = null;
            do {
                $status = curl_multi_exec($handle, $flag);
                // Check for errors
                if ($status > 0) {
                    // Display error message
                    \PKCore\fail(Route\language(500102) . curl_multi_strerror($status));
                    break;
                }
            } while ($flag > 0);
            if (curl_errno($this->_init) > 0) {
                throw new \Exception(curl_error($this->_init));
            }
            if (curl_getinfo($this->_init, CURLINFO_HTTP_CODE) !== 200) {
                $msg = 'CURL HTTP State Code: ' . curl_getinfo($this->_init, CURLINFO_HTTP_CODE)
                    . ', Msg:' . curl_error($this->_init);
                throw new \Exception($msg);
            }
            $this->_response = curl_multi_getcontent($this->_init);
            curl_multi_remove_handle($handle, $this->_init);
            curl_multi_close($handle);
        } catch (\Exception $exception) {
            handlerException($exception);
        }
    }

    private function _exec()
    {
        try {
            $this->_response = curl_exec($this->_init);
            if (curl_errno($this->_init) > 0) {
                throw new \Exception(curl_error($this->_init));
            }
            if (curl_getinfo($this->_init, CURLINFO_HTTP_CODE) !== 200) {
                $msg = 'CURL HTTP State Code: ' . curl_getinfo($this->_init, CURLINFO_HTTP_CODE)
                    . ', Msg:' . curl_error($this->_init);
                throw new \Exception($msg);
            }
            curl_close($this->_init);
        } catch (\Exception $exception) {
            handlerException($exception);
        }
    }

}