<?php
/**
 * 数据模型驱动
 * User: wz_zh
 * Date: 2017/9/6
 * Time: 9:56
 */

namespace PKCore;

use PKCore\Route;
use PKDriver\PDO_Sqlite;
use PKDriver\Sqlite;

class DataBase
{
    private $_config = array();
    private $_sql = array();
    private $_from;
    private $_where;
    private $_order;
    private $_group;
    private $_limit;
    private $_toList = false;
    private $_isBatch;
    private $_dbInitClass, $_nowDBLinkIndex;

    /**
     * DataBase constructor.
     * @param $tableName - 数据表的表名
     * @param int $linkIndex
     */
    public function __construct($tableName = '', $linkIndex = 0)
    {
        empty($tableName) ? $this->_setDBLink($linkIndex) : $this->From($tableName, $linkIndex);
    }

    public function GetNowConfig($index = 0)
    {
        static $configList;
        $nowConfig = array();
        try {
            $this->_nowDBLinkIndex = $index;
            Request::param('NowDBLinkIndex', $index);
            !empty($configList) ?: $configList = Route\config('Database', true);
            if (empty($configList) || !is_array($configList)) {
                throw new \Exception(Route\language('DataBase_ConfigEmpty'));
            }
            if (array_key_exists($index, $configList) && is_array($configList[$index])) {
                $this->_config[$index] = $configList[$index];
                $nowConfig = $configList[$index];
            } else {
                throw new \Exception(Route\language('DataBase_Config_IndexEmpty'));
            }
        } catch (\Exception $exception) {
            \PKCore\handlerException($exception);
        }
        return $nowConfig;
    }

    /**
     * 多链接的索引设置
     * @param int $index
     * @return array|mixed
     */
    private function _setDBLink($index = 0)
    {
        try {
            $nowConfig = $this->GetNowConfig($index);
            if (is_array($this->_dbInitClass) && array_key_exists($index, $this->_dbInitClass)) {
                // 已经存在的初始化
                !method_exists($this->_dbInitClass[$index], 'checkPing') ?: $this->_dbInitClass[$index]->checkPing();
            } elseif (is_array($nowConfig) && array_key_exists('type', $nowConfig)) {
                is_array($this->_dbInitClass) ?: $this->_dbInitClass = array();
                switch ($nowConfig['type']) {
                    case 'sqlite':
                        if (array_key_exists('file', $nowConfig) && !empty($nowConfig['file'])) {
                            if (extension_loaded('pdo_sqlite')) {
                                $this->_dbInitClass[$index] = new PDO_Sqlite($nowConfig['file']);
                            } else {
                                $this->_dbInitClass[$index] = new Sqlite($nowConfig['file']);
                            }
                        } else {
                            throw new \Exception(Route\language('DataBase_SqliteFile_Empty'));
                        }
                        break;
                }
            }
        } catch (\Exception $exception) {
            \PKCore\handlerException($exception);
        }
        return $nowConfig;
    }

    /**
     * 数据表的联表查询
     * @param $tableName
     * @param $joinWhere
     * @param int $linkIndex
     * @param string $joinType 连接的类型有： left join（左连接）, right join（右连接）, inner join（内连接）
     * @return $this
     * @throws \Exception
     */
    public function TableJoin($tableName, $joinWhere, $linkIndex = 0, $joinType = 'left')
    {
        if (stristr($this->_from, 'FROM')) {
            $this->_from .= " {$joinType} join " . $this->_tableName($tableName, $linkIndex) . " on {$joinWhere}";
        } else {
            fail('SQL FROM param is Empty');
        }
        return $this;
    }

    public function GetTableName($tableName)
    {
        return $this->_tableName($tableName);
    }

    private function _tableName($tableName, $linkIndex = 0)
    {
        try {
            $config = $this->_setDBLink($linkIndex);
            if (is_array($config) && array_key_exists('type', $config)) {
                switch ($config['type']) {
                    case 'sqlite':
                        if (in_array($tableName, array('sqlite_master'))) {
                            return $tableName;
                        }
                        if (array_key_exists('prefix', $config) && !empty($config['prefix'])) {
                            return stristr($tableName, $config['prefix']) ? $tableName : "{$config['prefix']}{$tableName}";
                        } else {
                            throw new \Exception(Route\language('DataBase_SqlitePrefix_Empty'));
                        }
                        break;
                }
            }
        } catch (\Exception $exception) {
            \PKCore\handlerException($exception);
        }
        return null;
    }

    public function From($tableName, $linkIndex = 0)
    {
        $this->_from = ' FROM ' . $this->_tableName($tableName, $linkIndex);
        return $this;
    }

    /**
     * 组装条件SQL语句
     * @param array $options 选项数据
     * @param string $logic 关系词 And | Or
     * @return $this
     */
    public function Where($options = array(), $logic = 'and')
    {
        $this->_where = ' WHERE ';
        if (Formats::isArray($options)) {
            $temp = array();
            foreach ($options as $key => $value) {
                if (Formats::isArray($value)) {
                    $v_arr = array();
                    foreach ($value as $v) {
                        // 判断是否为数字,非数字则要带上引号
                        $v_arr[] = Formats::isNumeric($v) ? $v : '"' . $v . '"';
                    }
                    $value = Formats::isArray($v_arr) ? implode(',', $v_arr) : '';
                    $temp[] = $key . ' IN (' . $value . ')';
                } else {
                    if (is_string($key)) {
                        $temp[] = $this->_whereRelation($key, $value);
                    } else {
                        $temp[] = $value;
                    }
                }
            }
            $this->_where .= implode(' ' . strtoupper($logic) . ' ', $temp);
        }
        return $this;
    }

    /**
     * @return mixed
     */
    protected function getWhere()
    {
        return $this->_where;
    }


    private function _whereRelation($key, $value)
    {
        $config = $this->_config[$this->_nowDBLinkIndex];
        if (isset($config['type'])) {
            switch (\gettype($value)) {
                case 'boolean':
                    switch ($config['type']) {
                        case 'mysql':
                            return "{$key}=" . ($value ? "TRUE" : "FALSE");
                            break;
                            break;
                        case 'sqlite':
                            return "{$key}=" . ($value ? 1 : 0);
                            break;
                    }
                    break;
                default:
                    if (!is_array($value)) {
                        return $key . '=' . (!empty($value) && stristr('"', $value) ? $value : '"' . $value . '"');
                    }
                    break;
            }
        }
        return '';
    }

    /***
     * 设置查询数据时的分组字段
     * @param string $groupBy 按某个指定字段的数据进行分组
     * @return $this
     */
    public function GroupBy($groupBy)
    {
        $this->_group = " GROUP BY `{$groupBy}`";
        return $this;
    }

    /**
     * 排序
     * @param $orderByField
     * @param string $orderByMode (ASC | DESC)
     * @return $this
     */
    public function OrderBy($orderByField, $orderByMode = 'DESC')
    {
        empty($this->_order) ? $this->_order = " ORDER BY {$orderByField} {$orderByMode}" : $this->_order .= ",{$orderByField} {$orderByMode}";
        return $this;
    }

    /**
     * @return mixed
     */
    public function getOrder()
    {
        return $this->_order;
    }

    /**
     * 分页
     * @param $rowSize
     * @param $index
     * @return $this
     */
    public function Limit($rowSize, $index = 0)
    {
        $this->_limit = " LIMIT " . ($index == 0 ? $rowSize : ($index - 1) * $rowSize . ',' . $rowSize);
        return $this;
    }

    public function setLimitEmpty()
    {
        $this->_limit = "";
        return $this;
    }

    /**
     * 在数据表中查询数据的所有记录
     * @param mixed $param
     * @return $this
     */
    public function Select($param = null)
    {
        if (Formats::isArray($param)) {
            $select = implode(',', $param);
        } elseif (is_null($param)) {
            $select = '*';
        } else {
            $select = $param;
        }
        $sql = "SELECT {$select}{$this->_from}";
        empty($this->_where) ?: $sql .= $this->_where;
        empty($this->_order) ?: $sql .= $this->_order;
        empty($this->_group) ?: $sql .= $this->_group;
        empty($this->_limit) ?: $sql .= $this->_limit;
        $this->_sql[] = $sql;
        return $this;
    }

    public function First()
    {
        $this->_toList = false;
        if (!Formats::isArray($this->_sql)) {
            fail('Do Query SQL is Empty');
        }
        $sql_str = strtolower($this->_sql[0]);
        $class = $this->_dbInitClass[$this->_nowDBLinkIndex];
        $result = null;
        if (strstr($sql_str, 'select') && method_exists($class, 'fetchAssoc')) {
            $result = $class->fetchAssoc($this->_sql[0], $this->_toList);
            $this->_sql = array();
        }
        return $result;
    }

    public function ToList()
    {
        $this->_toList = true;
        if (!Formats::isArray($this->_sql)) {
            fail('Do Query SQL is Empty');
        }
        $sql_str = strtolower($this->_sql[0]);
        $class = $this->_dbInitClass[$this->_nowDBLinkIndex];
        $result = null;
        if (strstr($sql_str, 'select') && method_exists($class, 'fetchAssoc')) {
            $result = $class->fetchAssoc($this->_sql[0], $this->_toList);
            $this->_sql = array();
        }
        return $result;
    }

    /**
     * 数据表数据量的统计
     * @param string $fieldAS
     * @param string $field
     * @return int
     */
    public function Count($fieldAS = 'count', $field = 'id')
    {
        $sql = "SELECT COUNT({$field}) AS {$fieldAS}{$this->_from}";
        empty($this->_where) ?: $sql .= $this->_where;
        empty($this->_group) ?: $sql .= $this->_group;
        $this->_sql[] = $sql;
        $result = $this->First();
        return !empty($result) ? $result[$fieldAS] : 0;
    }

    // ============================ 数据设置

    public function Sql($sql)
    {
        $this->_sql[] = empty($this->_limit) ? $sql : $sql . $this->_limit;
        return $this;
    }

    /**
     * 检查数据表名是否已经存在
     * @param null $tableName
     * @return bool
     */
    public function CheckTableNameExists($tableName = null)
    {
        if (is_null($tableName)) {
            $view_tableName = str_replace(' FROM ', '', $this->_from);
        } else {
            $view_tableName = $this->GetTableName($tableName);
        }
        $this->_from = ' FROM ' . $this->GetTableName('sqlite_master');
        $result = $this->Where(array('type' => 'table', 'name' => $view_tableName))
            ->Count('count', '*');
        if (is_null($tableName)) {
            $this->_from = ' FROM ' . $this->GetTableName($view_tableName);
        }
        $this->_where = array();
        return (bool)$result;
    }

    /**
     * 将提交的数组转换为 insert SQL语句
     * @param array $data 被 INSERT 数组
     * @param bool $isBatch
     * @return $this
     */
    public function Insert($data = array(), $isBatch = false)
    {
        $this->_isBatch = $isBatch;
        $config = $this->_config[$this->_nowDBLinkIndex];
        $field = $value = '';
        if (is_object($data)) {
            $data = Converter::objectToArray($data);
        }
        if (Formats::isArray($data) && isset($data[0])) {
            $valueArr = array();
            foreach ($data as $item) {
                list($field, $valueArr[]) = self::_insertOrReplaceDataJoin($item);
            }
            if ($isBatch && $config['type'] == 'sqlite') {
                $value = $valueArr;
            } else {
                $value = Formats::isArray($valueArr) ? implode(',', $valueArr) : '';
            }
        } elseif (Formats::isArray($data)) {
            list($field, $value) = self::_insertOrReplaceDataJoin($data);
        }
        if (!empty($field) && !empty($value)) {
            $table = trim(str_replace('FROM', '', $this->_from));
            if ($isBatch && $config['type'] == 'sqlite' && is_array($value) && array_key_exists(0, $value)) {
                foreach ($value as $item) {
                    $this->_sql[] = "INSERT " . "INTO {$table} ({$field}) VALUES {$item}";
                }
            } else {
                $this->_sql[] = "INSERT " . "INTO {$table} ({$field}) VALUES {$value}";
            }
        }
        return $this;
    }

    /**
     * 将提交的数组转换成 UPDATE SQL 语句
     * @param array $data 被 UPDATE 的数组
     * @return $this
     */
    public function Update($data = array())
    {
        $setArr = array();
        if (is_object($data)) {
            $data = Converter::objectToArray($data);
        }
        $config = $this->_config[$this->_nowDBLinkIndex];
        foreach ($data as $key => $value) {
            if (isset($config['type'])) {
                switch ($config['type']) {
                    case 'mysql':
                        switch (\gettype($value)) {
                            case 'boolean':
                                $item = "`{$key}`=" . ($value ? "TRUE" : "FALSE");
                                break;
                            default:
                                $item = "`{$key}`=" . "'" . str_replace("'", "\'", $value) . "'";
                                break;
                        }
                        break;
                    case 'sqlite':
                        switch (\gettype($value)) {
                            case 'boolean':
                                $item = "{$key}=" . ($value ? "TRUE" : "FALSE");
                                break;
                            default:
                                $item = "{$key}=" . "'" . str_replace("'", "\'", $value) . "'";
                                break;
                        }
                        break;
                }
            }
            !isset($item) ?: $setArr[] = $item;
        }
        if (Formats::isArray($setArr)) {
            $set = implode(',', $setArr);
            $table = trim(str_replace('FROM', '', $this->_from));
            $sql = "UPDATE" . " {$table} SET {$set}";
            empty($this->_where) ?: $sql .= $this->_where;
            $this->_sql[] = $sql;
        }
        return $this;
    }

    /**
     * REPLACE 批量数据更新
     * @param array $data
     * @return $this
     */
    public function Replace($data = array())
    {
        $field = $value = '';
        if (is_object($data)) {
            $data = Converter::objectToArray($data);
        }
        if (Formats::isArray($data) && isset($data[0])) {
            $valueArr = array();
            foreach ($data as $item) {
                list($field, $valueArr[]) = self::_insertOrReplaceDataJoin($item);
            }
            $value = implode(',', $valueArr);
        } elseif (Formats::isArray($data)) {
            list($field, $value) = self::_insertOrReplaceDataJoin($data);
        }
        if (!empty($field) && !empty($value)) {
            $table = trim(str_replace('FROM', '', $this->_from));
            $this->_sql[] = "REPLACE" . " INTO {$table} ({$field}) VALUES {$value}";
        }
        return $this;
    }

    /**
     * 组合提交的数组为 INSERT SQL 组合
     * @param array $data 被 INSERT 数组
     * @return array
     */
    private function _insertOrReplaceDataJoin($data = array())
    {
        $fieldArr = $dataArr = array();
        $field = $value = '';
        $config = $this->_config[$this->_nowDBLinkIndex];
        if (Formats::isArray($data)) {
            foreach ($data as $key => $value) {
                if (empty($key)) {
                    continue;
                }
                $fieldArr[] = $key;
                $dataArr[] = str_replace("'", "\'", $value);
            }
            if (isset($config['type'])) {
                switch ($config['type']) {
                    case 'mysql':
                        $field = '`' . implode('`,`', $fieldArr) . '`';
                        $value = "('" . implode("','", $dataArr) . "')";
                        break;
                    case 'sqlite':
                        $field = '[' . implode('],[', $fieldArr) . ']';
                        $value = "('" . implode("','", $dataArr) . "')";
                        break;
                }
            }
        }
        return array($field, $value);
    }

    /**
     * 删除数据表中的指定数据
     * @return $this
     */
    public function Delete()
    {
        $sql = "DELETE {$this->_from}";
        empty($this->_where) ?: $sql .= $this->_where;
        $this->_sql[] = $sql;
        return $this;
    }

    /**
     * 清空数据表中的指定数据
     * @return $this
     */
    public function Truncate()
    {
        $this->_from = str_replace(' FROM ', '', $this->_from);
        $sql = "TRUNCATE {$this->_from}";
        $this->_sql[] = $sql;
        return $this;
    }

    /**
     * 执行 SQL 语句
     */
    public function Exec()
    {
        if (!Formats::isArray($this->_sql)) {
            fail('Do Query SQL is Empty');
        }
        $sql_str = strtolower($this->_sql[0]);
        $class = $this->_dbInitClass[$this->_nowDBLinkIndex];
        $result = null;
        if (strstr($sql_str, 'insert') && method_exists($class, 'insert')
            && method_exists($class, 'insertId')) {
            if ($this->_isBatch) {
                $class->insert($this->_sql);
            } else {
                $result = $class->insertId($this->_sql[0]);
            }
        } elseif (method_exists($class, 'sqlQuery')) {
            $result = $class->sqlQuery($this->_sql[0]);
        }
        $this->_sql = array();
        return $result;
    }

    public function BatchRun()
    {
        if (!Formats::isArray($this->_sql)) {
            fail('Do Query SQL is Empty');
        }
        $sql_str = implode(';', $this->_sql);
        $class = $this->_dbInitClass[$this->_nowDBLinkIndex];
        if (method_exists($class, 'sqlQuery')) {
            $class->sqlQuery($sql_str);
        }
    }

    /**
     * @return mixed
     */
    protected function getFrom()
    {
        return $this->_from;
    }

}

function DB($tableName, $linkIndex = 0)
{
    return new DataBase($tableName, $linkIndex);
}