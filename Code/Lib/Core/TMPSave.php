<?php
/**
 * 临时存储
 * User: wz_zh
 * Date: 2017/7/13
 * Time: 15:46
 */

namespace PKCore;

class TMPSave
{
    private static $_cache_path;

    public static function set($data = null, $name = '', $path = '')
    {
        self::_setPath(PATH_TMP . 'cache_' . strtolower($path) . DS);
        self::_setLocal($data, $name, $path);
    }

    public static function get($name = '', $path = '')
    {
        self::_setPath(PATH_TMP . 'cache_' . strtolower($path) . DS);
        return self::_getLocal($name, $path);
    }

    public static function del($name = '', $path = '')
    {
        self::_delLocal($name, $path);
    }

    /**
     * 设置本地缓存数据
     * @param null $data
     * @param string $name
     * @param string $path
     * @return bool
     */
    private static function _setLocal($data = null, $name = '', $path = '')
    {
        if (is_null($data) || is_null($path)) {
            return false;
        }
        // 如果是数组则为 true ， 否则为 false
        $is_top_write = (bool)$is_arr = Formats::isArray($data);
        if ($is_arr) {
            $content = Converter::jsonEnCode($data);
        } else {
            $file_path = self::$_cache_path . $name;
            if ($path == 'logs') {
                if (file_exists($file_path)) {
                    $max_size = 1024 * 1024 * 1;
                    $file_param = Files::baseProperty($file_path);
                    $file_param['size'] < $max_size ?: $is_top_write = true;
                }
                $data = Converter::date() . "\r\n" . $data . "\r\n\r\n";
            }
            $content = $data;
        }
        Files::putContents(self::$_cache_path, $name, $content, $is_top_write);
        return true;
    }

    /**
     * 本地缓存文件的读取
     * @param string $name 文件名
     * @param string $path 路径
     * @return array|bool|mixed|null|string
     */
    private static function _getLocal($name = '', $path = '')
    {
        if (is_null($path)) {
            return false;
        }
        $result = Files::getContents(self::$_cache_path . $name);
        if ($is_json = Formats::isJson($result)) {
            $converter = Converter::objectToArray($is_json);
        } else {
            $converter = Converter::Unserialize($result);
        }
        return empty($converter) ? $result : $converter;
    }

    /**
     * 本地缓存文件的删除
     * @param string $name 文件名
     * @param string $path 路径
     */
    private static function _delLocal($name = '', $path = '')
    {
        if (!empty($path)) {
            self::$_cache_path = rtrim(self::$_cache_path, DS) . DS . $path;
        }
        Files::unlink(self::$_cache_path . $name);
    }

    /**
     * 设置缓存的路径
     * @param mixed $cache_path
     */
    private static function _setPath($cache_path)
    {
        self::$_cache_path = $cache_path;
        file_exists(self::$_cache_path) ?: Files::mkdir(self::$_cache_path);
    }

}