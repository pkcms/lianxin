<?php

/**
 * PK框架 - 底层 路由
 * User: zhanghui - qq:366065186
 * Date: 2016/4/5
 * Time: 12:27
 */

namespace PKCore\Route;

use PKCore\Files;
use PKCore\Log;
use PKCore\Statics;
use PKCore\Request;

const FILE_EXT = '.php';

function gotoApps()
{
    $blacklist_ip = Files::getContents(PATH_TMP . 'blacklist_ip.log');
    $blacklist_ip = explode(',', $blacklist_ip);
    $guest_ip = Request::GuestIP();
    if (in_array($guest_ip,$blacklist_ip)) {
        die('blacklist');
    }
    Log::LOGS('request info',
        array(
            'request_url' => Request::url(),
            'request_ip' => $guest_ip,
            'method' => Request::method(),
            'post' => Request::post()
        ));
    Request::route();
    $module_name = Request::module();
    !empty($module_name) ?: $module_name = Request::module(DEFAULT_MODULE);
    $ctrl_name = Request::controller();
    !empty($ctrl_name) ?: $ctrl_name = Request::controller(DEFAULT_CONTROLLER);
    $ctrl_file_name = ucfirst($module_name) . DS . ucfirst($ctrl_name);
    isLoadingFile(PATH_APP . $ctrl_file_name) ?: \PKCore\fail('not loading ' . $ctrl_file_name . ' controller file');
    $class_name = '\PKApp\\' . $module_name . '\\' . $ctrl_name;
    class_exists($class_name) ?: \PKCore\fail('not ' . $ctrl_name . ' controller');
    $ctrl = new $class_name();
    $action = Request::action();
    is_callable(array($ctrl, $action)) ?: \PKCore\fail('controller no action ' . $action);
    $result = $ctrl->$action();
    Request::isAjax() && is_array($result) ? Statics::resultJsonModel($result) : Statics::resultWebPageModel($result);
}

/**
 * 加载配置文件
 * @param $file_name
 * @param bool $isReturnData
 * @return bool|mixed
 */
function config($file_name, $isReturnData = false)
{
    $path = PATH_CACHE . 'Config' . DS . ucfirst($file_name);
    return isLoadingFile($path, $isReturnData);
}

/**
 * 加载模块类
 * @param $class
 */
function moduleClasses($class)
{
    try {
        $path = PATH_APP . $class;
        if (!isLoadingFile($path)) {
            throw new \Exception(language('SystemFile_IncludeError') . $path);
        }
    } catch (\Exception $exception) {
        \PKCore\handlerException($exception);
    }
}

/**
 * 加载扩展
 * @param $module_name - 扩展的文件名
 * @return bool|mixed
 */
/**
 * @param $extend_name
 */
function extendCore($extend_name)
{
    try {
        $path = PATH_LIB . 'Extend' . DS . ucfirst($extend_name);
        if (!isLoadingFile($path)) {
            throw new \Exception(language('SystemFile_IncludeError') . $path);
        }
    } catch (\Exception $exception) {
        \PKCore\handlerException($exception);
    }
}

function PlugIn()
{
    try {
        $path = PATH_PK . 'PlugIn' . DS . 'vendor' . DS . 'autoload.php';
        if (!isLoadingFile($path)) {
            throw new \Exception(language('SystemFile_IncludeError') . $path);
        }
    } catch (\Exception $exception) {
        \PKCore\handlerException($exception);
    }
}

/**
 * 加载系统文件
 * @param $file_name
 */
function DbDriver($file_name)
{
    $dir = PATH_LIB . 'DbDriver' . DS;
    isLoadingFile($dir . ucfirst($file_name)) ?: \PKCore\fail('no loading ' . $dir . ucfirst($file_name) . ' driver');
}

/**
 * 语言文件处理
 * @param $code
 * @param string $app (取决于应用区域，如：system)
 * @param string $languageType (语言的类型，如： zh-cn、en)
 * @return mixed|string
 */
function language($code, $app = null, $languageType = 'zh-cn')
{
    static $languageList = array();
    // 底层共用语言包
    if (array_key_exists('Common', $languageList) && array_key_exists($code, $languageList['Common'])
        && array_key_exists($languageType, $languageList['Common'][$code])) {
        return $languageList['Common'][$code][$languageType];
    } elseif (!array_key_exists('Common', $languageList)) {
        $dir = PATH_LIB . 'Language';
        $data = isLoadingFile($dir, true);
        if (is_array($data)) {
            $languageList['Common'] = $data;
            if (array_key_exists($code, $data) && array_key_exists($languageType, $data[$code])) {
                return $data[$code][$languageType];
            }
        }
    }
    !is_null($app) ?: $app = Request::module();
    // 应用语言包
    if (array_key_exists($app, $languageList) && array_key_exists($code, $languageList[$app])
        && array_key_exists($languageType, $languageList[$app][$code])) {
        return $languageList[$app][$code][$languageType];
    } elseif (!array_key_exists($app, $languageList)) {
        $data = isLoadingFile(PATH_APP . $app . DS . 'Language', true);
        if (is_array($data)) {
            $languageList[$app] = $data;
            if (array_key_exists($code, $data) && array_key_exists($languageType, $data[$code])) {
                return $data[$code][$languageType];
            }
        }
    }
    return $code;
}

function languageApp($code, $languageType = 'zh-cn')
{
    return language($code, Request::module(), $languageType);
}

/**
 * 判断是否已经加载
 * @param $file_path - 加载文件的路径
 * @param bool $isReturnData
 * @return bool|mixed
 */
function isLoadingFile($file_path, $isReturnData = false)
{
    static $loadExistsList = array();
    is_file($file_path) ?: $file_path .= FILE_EXT;
    $file_md5 = md5($file_path);
    if ($isReturnData && file_exists($file_path)) {
        /** @noinspection PhpIncludeInspection */
        return strpos($file_path, FILE_EXT) ? include("$file_path") : Files::getContents($file_path);
    }
    if (array_key_exists($file_md5, $loadExistsList)) {
        return true;
    } elseif (pathinfo($file_path) && file_exists($file_path)) {
        $loadExistsList[$file_md5] = $file_path;
//        print_r($loadExistsList);
        /** @noinspection PhpIncludeInspection */
        include_once("$file_path");
        return true;
    }
    return false;
}
