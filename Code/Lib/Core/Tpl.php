<?php

namespace PKCore;

use PKCore\Route;

Route\extendCore('TplTag');
Route\extendCore('TemplateTagFunction');

/**
 * ===========================================
 * 模板引擎类
 * update: 2014-09-16
 * ============================================
 */
class Tpl
{
    // 用來保存所有的模板變量
    private $_tpl_param = array();
    //模板的主题
    private static $_tpl_themes = null;
    private $_tpl_dir = null;
    private $_tpl_isCommon = false;
    public $is_Model_output = false;
    public $is_Mobile = true;
    private $is_admin, $is_php;

    /**
     * 前台模板设置主题
     * @param $tpl_themes
     * @return $this
     */
    public function SetTplThemes($tpl_themes)
    {
        self::$_tpl_themes = $tpl_themes;
        return $this;
    }

    /**
     * 前台设置主题下的目录
     * @param $tpl_dir
     * @return $this
     */
    public function SetTplDir($tpl_dir = null)
    {
        $this->_tpl_dir = ((Request::module() == 'Admin') || stristr(Request::controller(), 'Admin')) ? ucfirst($tpl_dir) : $tpl_dir;
        return $this;
    }

    /**
     * @return null
     */
    private function _getTplDir()
    {
        return !empty($this->_tpl_dir) ? $this->_tpl_dir : Request::module();
    }

    /**
     * 模板编译路径
     */
    private function _getCompiledPath()
    {
        $path = PATH_TMP . 'Templates' . DS;
        if ($this->is_Model_output || $this->is_php || ($this->is_admin && Request::controller() != 'AdminDoMakeHtml')) {
            // 是后台，但非发布静态
            $path .= 'Admin' . DS;
        } else {
            $path .= self::$_tpl_themes . DS;
        }
        $path .= $this->_getTplDir() . DS;
        file_exists($path) ?: Files::mkdir($path);
        return $path;
    }

    /**
     * 模板路径
     * @param $fileName
     * @return string
     */
    private function _getTemplatePath($fileName = null)
    {
        defined('JS_PATH') ?: define('JS_PATH', '/statics/js/');
        defined('CSS_PATH') ?: define('CSS_PATH', '/statics/css/');
        defined('IMAGE_PATH') ?: define('IMAGE_PATH', '/statics/images/');
        $this->is_admin = Request::module() == 'Admin' || stristr(Request::controller(), 'Admin');
        $this->is_php = !is_null($fileName) && stristr($fileName, '.php');
        if ($this->is_Model_output || $this->is_php || ($this->is_admin && Request::controller() != 'AdminDoMakeHtml')) {
            // 是后台，但非发布静态
            $tpl_path = PATH_APP . $this->_getTplDir() . DS . 'Template' . DS;
        } else {
            // 非后台
            $tpl_path = PATH_ROOT . 'Templates' . DS . self::$_tpl_themes . DS . strtolower($this->_getTplDir()) . DS;
            $this->SetTplParam('template_style_path', '/Templates/' . self::$_tpl_themes . '/style');
        }
        !$this->_tpl_isCommon ?: $tpl_path = PATH_LIB . DS . 'Template' . DS;
        return $tpl_path;
    }

    /**
     * 检查模板的文件名
     * @param $fileName
     * @return string
     */
    private function _checkFile($fileName)
    {
        stristr($fileName, '.') ?: $fileName .= '.tpl';
        $path = $this->_getTemplatePath($fileName) . $fileName;
        try {
            if (!file_exists($path)) {
                throw new \Exception(Route\language('TempLate_File_NoExists') . $path);
            }
        } catch (\Exception $e) {
            handlerException($e);
        }
        return $path;
    }

    /**
     * 检查文件中是否带有BOM
     * @param string $fileName 被检查的文件名
     */
    private function _checkBOM($fileName)
    {
        $contents = file_get_contents($fileName);
        $charset[1] = substr($contents, 0, 1);
        $charset[2] = substr($contents, 1, 1);
        $charset[3] = substr($contents, 2, 1);
        if (ord($charset[1]) == 239 && ord($charset[2]) == 187 && ord($charset[3]) == 191) {
            $fileNum = fopen($fileName, "w");
            flock($fileNum, LOCK_EX);
            fwrite($fileNum, substr($contents, 3));
            fclose($fileNum);
        }
    }

    private function _readFile($file)
    {
        // 检查模板文件的修改时间
        $templateFile = $this->_checkFile($file);
        $fileInfo = Files::baseProperty($templateFile);
        $compiledFileName = $file . '.php';
        $compiledFilePath = $this->_getCompiledPath() . $compiledFileName;
        !file_exists($compiledFilePath) ?: $compiledFileInfo = Files::baseProperty($compiledFilePath);
        // 释放模板变量
        if (count($this->_tpl_param) > 0) {
            extract($this->_tpl_param);
        }
        if (isset($compiledFileInfo) && is_array($fileInfo)
            && array_key_exists('c_time', $fileInfo) && array_key_exists('m_time', $fileInfo)
            && is_array($compiledFileInfo) && array_key_exists('m_time', $compiledFileInfo)) {
            $isCreateCompiled = ($fileInfo['m_time'] > $compiledFileInfo['m_time'])
                || ($fileInfo['c_time'] > $compiledFileInfo['m_time']);
        } elseif (!isset($compiledFileInfo)) {
            $isCreateCompiled = true;
        }
        // 判断是否要重新创建编译
        if (isset($isCreateCompiled) && $isCreateCompiled) {
            ob_start();
            $this->_checkBOM($templateFile);
            include_once "{$templateFile}";
            $htmlCode_Template = ob_get_contents();
            ob_end_clean();
            Files::putContents($this->_getCompiledPath(), $compiledFileName, $this->_parseHandler($htmlCode_Template));
        }
        ob_start();
        if (Request::controller() == 'AdminDoMakeHtml') {
            /** @noinspection PhpIncludeInspection */
            require "{$compiledFilePath}";
        } else {
            /** @noinspection PhpIncludeInspection */
            require_once "{$compiledFilePath}";
        }
        $htmlCode_Template = ob_get_contents();
        ob_end_clean();
        return $htmlCode_Template;
    }

    public function PhpDisplay($file)
    {
        // 释放模板变量
        if (count($this->_tpl_param) > 0) {
            extract($this->_tpl_param);
        }
        $templateFile = $this->_checkFile($file . '.php');
        // 判断是否要重新创建编译
        ob_start();
        $this->_checkBOM($templateFile);
        require "{$templateFile}";
        $htmlCode_Template = ob_get_contents();
        ob_end_clean();
        return $htmlCode_Template;
    }

    /**
     * 通知显示
     * @param $msg
     * @param null $url
     */
    public function Notice($msg, $url = null)
    {
        $this->_tpl_isCommon = true;
        $isLoadJS = !(stristr(Request::controller(), 'Admin')) || (stristr(Request::controller(), 'Public')
                || stristr(Request::action(), 'Public'));
        $isLoadJS ?: $isLoadJS = !Request::isAjax();
        $this->SetTplParam('msg', $msg);
        $this->SetTplParam('url', $url);
        $this->SetTplParam('isLoadJS', $isLoadJS);
        exit($this->_readFile('notice'));
    }

    /**
     * 直接显示
     * @param $file
     * @return false|string
     */
    public function Display($file)
    {
        return $this->_readFile($file);
    }

    /**
     * 解析模板
     * @param $str
     * @return string|string[]|null
     */
    private function _parseHandler($str)
    {
        $str = preg_replace("/\<{template\s+(.+)\}>/", "<?php echo tplTag_Template(\\1); ?>", $str);
        $str = preg_replace("/\<{include\s+(.+)\}>/", "<?php include \\1; ?>", $str);
        $str = preg_replace("/\<{php\s+(.+)\}>/", "<?php \\1?>", $str);
        $str = preg_replace("/\<{if\s+(.+?)\}>/", "<?php if(\\1) { ?>", $str);
        $str = preg_replace("/\<{else\}>/", "<?php } else { ?>", $str);
        $str = preg_replace("/\<{elseif\s+(.+?)\}>/", "<?php } elseif (\\1) { ?>", $str);
        $str = preg_replace("/\<{\/if\}>/", "<?php } ?>", $str);
        //for 循环
        $str = preg_replace("/\<{for\s+(.+?)\}>/", "<?php for(\\1) { ?>", $str);
        $str = preg_replace("/\<{\/for\}>/", "<?php } ?>", $str);
        //++ --
        $str = preg_replace("/\<{\+\+(.+?)\}>/", "<?php ++\\1; ?>", $str);
        $str = preg_replace("/\<{\-\-(.+?)\}>/", "<?php ++\\1; ?>", $str);
        $str = preg_replace("/\<{(.+?)\+\+\}>/", "<?php \\1++; ?>", $str);
        $str = preg_replace("/\<{(.+?)\-\-\}>/", "<?php \\1--; ?>", $str);
        $str = preg_replace("/\<{loop\s+(\S+)\s+(\S+)\}>/", "<?php if(isset(\\1) && is_array(\\1)) foreach(\\1 AS \\2) { ?>", $str);
        $str = preg_replace("/\<{loop\s+(\S+)\s+(\S+)\s+(\S+)\}>/", "<?php if(isset(\\1) && is_array(\\1)) foreach(\\1 AS \\2 => \\3) { ?>", $str);
        $str = preg_replace("/\<{\/loop\}>/", "<?php } ?>", $str);
        $str = preg_replace("/\<{([a-zA-Z_\x7f-\xff][a-zA-Z0-9_\x7f-\xff:]*\(([^{}]*)\))\}>/", "<?php echo \\1;?>", $str);
        $str = preg_replace("/\<{\\$([a-zA-Z_\x7f-\xff][a-zA-Z0-9_\x7f-\xff:]*\(([^{}]*)\))\}>/", "<?php if(isset(\\1)) {echo @\\1;}?>", $str);
        $str = preg_replace("/\<{(\\$[a-zA-Z_\x7f-\xff][a-zA-Z0-9_\x7f-\xff]*)\}>/", "<?php if(isset(\\1)) {echo \\1;}?>", $str);
        $str = preg_replace("/\<{(\\$[a-zA-Z0-9_\[\]\'\"\$\x7f-\xff]+)\}>/es", "tplTag_Addquote('<?php if(isset(\\1)) {echo \\1;}?>')", $str);
        $str = preg_replace("/\<{([A-Z_\x7f-\xff][A-Z0-9_\x7f-\xff]*)\}>/s", "<?php echo \\1;?>", $str);
        $str = preg_replace("/\<{pk:(\w+)\s+([^}]+)\}>/ie", "tplTag_PK('$1','$2', '$0') ?>", $str);
        $str = preg_replace("/\<{\/pk\}>/ie", "tplTag_PKEnd()", $str);
        return $str;
    }

    /**
     * @param $key
     * @param null $value
     * @param bool $isDecode
     * @return $this
     */
    public function SetTplParam($key, $value = null, $isDecode = true)
    {
        Request::param($key, $value);
        $this->_tpl_param[$key] = $isDecode ? Fileter::htmlspecialchars_decode($value) : $value;
        return $this;
    }

    public function SetTplParamList(array $lists = null)
    {
        if (!is_null($lists) && Formats::isArray($lists)) {
            Request::param($lists);
            foreach ($lists as $index => $list) {
                $this->_tpl_param[$index] = Fileter::htmlspecialchars_decode($list);
            }
        }
        return $this;
    }
}
