<?php
/**
 * 通用控制器
 * User: Administrator
 * Date: 2018/12/26
 * Time: 10:15
 */

namespace PKCore;


use PKCore\Route;

abstract class PKController
{
    protected static $pageIndex, $pageSize;
    private static $_dictList;

    abstract public function Main();

    /**
     * 处理请求参数中带有翻页参数
     * @param string $indexField
     * @param string $rowsField
     */
    final protected function GetPages($indexField = 'pageIndex', $rowsField = 'pageSize')
    {
        self::$pageIndex = max(Formats::isNumeric(Request::get($indexField)), 1);
        self::$pageSize = max(Formats::isNumeric(Request::get($rowsField)), 10);
    }

    /**
     * 处理请求参数有时间区间参数，转为 SQL 查询参数
     * @param $dbViewStartField
     * @param null $dbViewEndField
     * @param string $startTimeField
     * @param string $endTimeField
     * @return array
     */
    final protected function postByDateIntervalToSQLParam($dbViewStartField, $dbViewEndField = null,
                                                          $startTimeField = 'StartTime', $endTimeField = 'EndTime')
    {
        $result = array();
        $startTimeFieldIsEmpty = Request::has($startTimeField, 'post');
        $endTimeFieldIsEmpty = Request::has($endTimeField, 'post');
        if ($startTimeFieldIsEmpty && $endTimeFieldIsEmpty) {
            $startTime = strtotime(Request::post($startTimeField));
            $endTime = strtotime(Request::post($endTimeField));
            $startTime < $endTime ?: alert(500481);
            // 如果数据表查询的字段为相同字段可为 null
            !empty($dbViewEndField) ?: $dbViewEndField = $dbViewStartField;
            $result[] = "unix_timestamp(`{$dbViewStartField}`) >= '{$startTime}'";
            $result[] = "unix_timestamp(`{$dbViewEndField}`) <= '{$endTime}'";
        }
        return $result;
    }

    final protected function checkPostFieldIsEmpty($field, $tips = null)
    {
        $isNotEmpty = Request::has($field, 'post');
        if (!$isNotEmpty && !is_null($tips)) {
            alert($tips);
        }
        if ($tips != null) {
            return is_string(Request::post($field)) ? trim(Request::post($field)) : Request::post($field);
        }
        return $isNotEmpty;
    }

    final protected function checkPostFieldIsExists($field)
    {
        return Request::has($field, 'post', false);
    }

    public static function GetArrayByKey($key, array $array = array(), $tips = null, $module = null)
    {
        if (array_key_exists($key, $array)) {
            if (!is_null($tips) && empty($array[$key])) {
                \PKCore\alert($tips);
            }
            return $array[$key];
        } elseif (!is_null($tips)) {
            \PKCore\alert($tips);
        }
        return '';
    }

    public static function ToMeUrl($path)
    {
        return Request::domain() . 'index.php/' . $path;
    }

    public static function getDict($name)
    {
        if (empty(self::$_dictList)) {
            self::$_dictList = Route\config('Dictionaries', true);
        }
        if (array_key_exists($name, self::$_dictList)) {
            return self::$_dictList[$name];
        } else {
            \PKCore\alert('Dict_Empty', $name);
        }
        return array();
    }

    protected function json($data, $msg = '', $isSuccess = true) {
        Statics::resultJsonModel(array(
            'isSuccess'=>$isSuccess,'data'=>$data,'msg'=>$msg
        ));
    }

}
