<?php
/**
 * 数据格式验证
 * User: wz_zh
 * Date: 2017/7/11
 * Time: 15:54
 */

namespace PKCore;

class Formats
{
    private static $list = array(
        'num' => '/^[0-9.-]+$/',
        'int' => '/^[0-9-]+$/',
        'price' => '/^[0-9.]+$/',
        'letter' => '/^[a-z]+$/i',
        'letterNum' => '/^[0-9a-z]+$/i',
        'letter_num' => '/^[0-9a-z_]+$/i',
        'letter_' => '/^[a-z_]+$/i',
        'file_name' => '/^[0-9a-z.]+$/i',
        'email' => '/^[\w\-\.]+@[\w\-\.]+(\.\w+)+$/',
        'qq' => '/^[0-9]{5,20}$/',
        'url' => '/^http:\/\//',
        'mobile' => '/^(1)[0-9]{10}$/',
        'tel' => '/^[0-9-]{6,13}$/',
        'zipCode' => '/^[0-9]{6}$/',
        'date' => '/^([0-9]{4})-([0-9]{2})-([0-9]{2})$/',
        'datetime' => '/^d{4}-d{2}-d{2} d{2}:d{2}:d{2}$/s',
        'ip' => '/^((?:(?:25[0-5]|2[0-4]\d|((1\d{2})|([1-9]?\d)))\.){3}(?:25[0-5]|2[0-4]\d|((1\d{2})|([1 -9]?\d))))$/'
    );


    /**
     * 是否为 IP
     * @param null $ip
     * @return bool
     */
    public static function isIP($ip = null)
    {
        return self::pregMatch('ip', $ip);
    }

    /**
     * 执行正则表达式
     * @param string $type
     * @param string $data
     * @return bool
     */
    public static function pregMatch($type = '', $data = '')
    {
        if (empty($data) || is_array($data)) {
            return false;
        }
        return preg_match(self::$list[$type], $data) ? true : false;
    }

    /**
     * URL 格式的检查
     *
     * @param string $url 文件在网络的链接地址
     * @return boolean
     */
    public static function isUrl($url = '')
    {
        if ($url != "") {
            $url_parts = parse_url($url);
            $scheme = $url_parts['scheme'];
            $host = $url_parts['host'];
            $path = $url_parts['path'];
            $port = !empty($url_parts['port']) ? ':' . $url_parts['port'] : '';
            $url = (!empty($scheme) ? $scheme . '://' . $host : (!empty($host) ? 'http://' . $host : 'http://' . $path)) . $port . '/';
            return $url;
        }
        return null;
    }


    /**
     * 纠正序列化字符串
     * @param null $value
     * @return null|bool
     */
    public static function isSerialize($value = null)
    {
        $value = trim($value);
        if (is_null($value)) {
            return false;
        }
        if ('N;' == $value) {
            return true;
        }
        if (!preg_match('/^([adObis]):/', $value, $badions)) {
            return false;
        }
        switch ($badions[1]) {
            case 'a' :
            case 'O' :
            case 's' :
                if (preg_match("/^{$badions[1]}:[0-9]+:.*[;}]\$/s", $value)) {
                    return true;
                }
                break;
            case 'b' :
            case 'i' :
            case 'd' :
                if (preg_match("/^{$badions[1]}:[0-9.E-]+;\$/", $value)) {
                    return true;
                }
                break;
        }
        return false;
    }

    public static function isBool($value = null)
    {
        return $value ? True : False;
    }

    /**
     * 检查变量是否为整形
     * @param $expression -要检查的变量
     * @return int|string
     */
    public static function isNumeric($expression)
    {
        return (empty($expression) || !\is_numeric($expression)) ? 0 : $expression;
    }

    /**
     * 检查变量是否为浮点型
     * @param float $expression 要检查的变量
     * @return boolean
     */
    public static function isFloat($expression)
    {
        $expression = (\is_float($expression) || \is_numeric($expression)) ? \number_format($expression, 2) : number_format(0, 2);
        return str_replace(',', '', $expression);
    }

    /**
     * 检查变量是否为数组
     * @param $expression - 要检查的变量
     * @return boolean
     */
    public static function isArray($expression)
    {
        return (!\is_array($expression) || !count($expression)) ? false : count($expression);
    }

    /**
     * 判断是否是一维数据
     * @param $expression
     * @return bool
     */
    public static function isOneArray($expression)
    {
        return count($expression) == count($expression, 1);
    }

    /**
     * 检查是否为 JSON 格式
     * @param null $vars
     * @return bool
     */
    public static function isJson($vars = null)
    {
        if (self::isArray($vars)) {
            return false;
        }
        $json_data = json_decode($vars);
        return json_last_error() == JSON_ERROR_NONE ? $json_data : false;
    }

}