<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2019/5/27
 * Time: 15:27
 */

namespace PKDriver;

use PKCore\Formats;
use PKCore\Log;
use PKCore\Route;

class Sqlite
{
    private $_fileName;

    public function __construct($fileName)
    {
        try {
            if (!extension_loaded('sqlite3')) {
                throw new \Exception(Route\language('PHPExtension_Sqlite_NoExists'));
            }
            $this->_fileName = $fileName;
        } catch (\Exception $e) {
            \PKCore\handlerException($e);
        }
    }

    private function _conn()
    {
        try {
            $path = PATH_CACHE . 'Config' . DS . $this->_fileName . '.db';
            if (!file_exists($path)) {
                throw new \Exception(Route\language('DataBase_SqliteFile_NoExists') . $path);
            }
            return new \SQLite3($path);
        } catch (\Exception $exception) {
            \PKCore\handlerException($exception);
        }
        return null;
    }

    private function _errorLog(\SQLite3 $conn, $sqlStr)
    {
        Log::LOGS($conn->lastErrorMsg(), array('sql' => $sqlStr, 'ErrorCode' => $conn->lastErrorCode()));
    }

    public function sqlQuery($sqlStr)
    {
        try {
            $conn = $this->_conn();
            $conn->busyTimeout(10);
            $conn->exec($sqlStr);
            if ($conn->lastErrorCode() != 0) {
                $this->_errorLog($conn, $sqlStr);
                throw new \Exception(Route\language('DataBase_Sql_NumError'));
            }
            $conn->close();
        } catch (\Exception $exception) {
            \PKCore\handlerException($exception);
        }
    }

    public function fetchAssoc($sqlStr, $toList = false)
    {
        $result = array();
        try {
            $conn = $this->_conn();
            if ($toList) {
                $sth = $conn->query($sqlStr);
                if ($conn->lastErrorCode() != 0) {
                    $this->_errorLog($conn, $sqlStr);
                    throw new \Exception(Route\language('DataBase_Sql_NumError'));
                }
                while ($row = $sth->fetchArray(SQLITE3_ASSOC)) {
                    array_push($result, $row);
//                    $result[] = $row;
                }
            } else {
                $result = $conn->querySingle($sqlStr, TRUE);
                if ($conn->lastErrorCode() != 0) {
                    $this->_errorLog($conn, $sqlStr);
                    throw new \Exception(Route\language('DataBase_Sql_NumError'));
                }
            }
        } catch (\Exception $exception) {
            \PKCore\handlerException($exception);
        }
        return $result;
    }

    public function insert($sql) {
        try {
            $conn = $this->_conn();
            if (Formats::isArray($sql)) {
                $conn->exec(implode(';',$sql));
            }
            if ($conn->lastErrorCode() != 0) {
                $this->_errorLog($conn, $sql);
                throw new \Exception(Route\language('DataBase_Sql_NumError'));
            }
            $conn->close();
        } catch (\Exception $exception) {
            \PKCore\handlerException($exception);
        }
    }

    public function insertId($sqlStr) {
        try {
            $conn = $this->_conn();
            $conn->busyTimeout(10);
            $conn->exec($sqlStr);
            if ($conn->lastErrorCode() != 0) {
                $this->_errorLog($conn, $sqlStr);
                throw new \Exception(Route\language('DataBase_Sql_NumError'));
            }
            $id = $conn->lastInsertRowID();
            $conn->close();
            return $id;
        } catch (\Exception $exception) {
            \PKCore\handlerException($exception);
        }
    }

    public function __destruct()
    {
        $this->_conn()->close();
    }

}