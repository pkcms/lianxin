<?php


namespace PKDriver;

use PKCore\Converter;
use PKCore\Formats;
use PKCore\Log;
use PKCore\Route;

class PDO_Sqlite
{

    private $_fileName;

    public function __construct($fileName)
    {
        try {
            if (!extension_loaded('pdo_sqlite')) {
                throw new \Exception(Route\language('PHPExtension_PDOSqlite_NoExists'));
            }
            $this->_fileName = $fileName;
        } catch (\Exception $e) {
            \PKCore\handlerException($e);
        }
    }

    private function _conn()
    {
        try {
            $path = PATH_CACHE . 'Config' . DS . $this->_fileName . '.db';
            if (!file_exists($path)) {
                throw new \Exception(Route\language('DataBase_SqliteFile_NoExists') . $path);
            }
            $conn = new \PDO('sqlite:' . $path);
            $conn->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
            return $conn;
        } catch (\Exception $exception) {
            \PKCore\handlerException($exception);
        }
        return null;
    }

    private function _errorLog(\PDO $conn, $sqlStr)
    {
        Log::LOGS(Converter::jsonEnCode($conn->errorInfo()), array('sql' => $sqlStr, 'ErrorCode' => $conn->errorCode()));
    }

    public function sqlQuery($sqlStr)
    {
        $conn = $this->_conn();
        try {
            $conn->exec($sqlStr);
        } catch (\PDOException $exception) {
            $this->_errorLog($conn,$sqlStr);
            \PKCore\handlerException($exception);
        }
    }

    public function fetchAssoc($sqlStr, $toList = false)
    {
        $result = '';
        $conn = $this->_conn();
        try {
            if ($toList) {
                $sth = $conn->query($sqlStr);
                $result = $sth->fetchAll(\PDO::FETCH_ASSOC);
            } else {
                $sth = $conn->query($sqlStr);
                $result = $sth->fetch(\PDO::FETCH_ASSOC);
            }
        } catch (\PDOException $exception) {
            $this->_errorLog($conn,$sqlStr);
            \PKCore\handlerException($exception);
        }
        return $result;
    }

    public function insert($sql)
    {
        $conn = $this->_conn();
        try {
            if (Formats::isArray($sql)) {
                $conn->exec(implode(';', $sql));
            }
        } catch (\PDOException $exception) {
            $this->_errorLog($conn,$sqlStr);
            \PKCore\handlerException($exception);
        }
    }

    public function insertId($sqlStr)
    {
        $id = 0;
        $conn = $this->_conn();
        try {
            $conn->exec($sqlStr);
            $id = $conn->lastInsertId();
        } catch (\PDOException $exception) {
            $this->_errorLog($conn,$sqlStr);
            \PKCore\handlerException($exception);
        }
        return $id;
    }

}