<?php


namespace PKApp\Antispam;


use PKApp\Admin\Classes\AdminController;
use PKApp\Antispam\Classes\Antispam;
use PKApp\Content\Classes\CategoryDataBase;
use PKApp\Content\Classes\ContentsDataBase;
use PKApp\Model\Classes\ModelExtend;
use PKCore\Request;

class AdminCheckCategory extends AdminController
{

    public function Main()
    {
        $result = array(
            'count' => $this->_count(),
            'result' => '',
        );
        $result['count'] == 0 ?: $result['result'] = $this->_eachIndex();
        return $result;
    }

    private function _db()
    {
        static $db;
        !empty($db) ?: $db = new CategoryDataBase();
        return $db;
    }

    private function _count()
    {
        return $this->_db()->Count();
    }

    private function _eachIndex()
    {
        $eachIndex = Request::get('eachIndex');
        $entity = $this->_db()->GetCategoryByNext($eachIndex);
        $antispam = new Antispam();
        $tips = $antispam->CheckCategory($entity);
        if (empty($tips)) {
            return '';
        }
        return array('id' => $entity['id'], 'name' => $entity['name'], 'catId' => $entity['catid'], 'tips' => $tips);
    }

}