<?php
// 综合搜索

namespace PKApp\Antispam;


use PKApp\Admin\Classes\AdminController;
use PKApp\Model\Classes\ModelDatabase;
use PKCore\Converter;
use PKCore\Tpl;

class AdminAntispam extends AdminController
{

    public function Main()
    {
        extension_loaded('mbstring') ?: \PKCore\alert('PHPExtension_MBString_NoExists');
        $db_model = new ModelDatabase();
        $modelList = $db_model->GetLists('content',null,array('name','id'));
        if (is_array($modelList)) {
            $modelList = Converter::arrayColumn($modelList,'name','id');
        } else {
            \PKCore\alert('Model_Empty');
        }
        $tpl = new Tpl();
        return $tpl->SetTplParam('modelList',$modelList)
            ->Display('init');
    }

}