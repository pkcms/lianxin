<?php


namespace PKApp\Antispam\Classes;

use PKCore\Converter;
use PKCore\Fileter;
use PKCore\Formats;
use PKCore\PKController;
use PKCore\Route;
use PKCore\Curl;
use PKCore\Statics;

class Antispam
{
    private $_apiCheckStrLen = 10000;

    public function __construct()
    {
        extension_loaded('mbstring') ?: Statics::resultJsonModel(
            array('msg' => Route\language('PHPExtension_MBString_NoExists', 'Antispam')));
    }

    public function CheckSite($entity)
    {
        $checkFields = array('Site_Name', 'Site_Title', 'Site_Keyword', 'Site_Description');
        $contentList = array();
        foreach ($checkFields as $checkField) {
            if (is_array($entity) && array_key_exists($checkField, $entity) && !empty($entity[$checkField])) {
                $str = Fileter::filterValue(Fileter::htmlspecialchars_decode($entity[$checkField]), true);
                $str_len = mb_strlen($str, 'utf8');
                if ($str_len < $this->_apiCheckStrLen) {
                    $contentList[] = $str;
                } else {
                    $tmp = $this->_mbStrSplit($str, $this->_apiCheckStrLen);
                    $contentList = array_merge($contentList, $tmp);
                }
            }
        }
        return $this->_service($contentList);
    }

    public function CheckCategory($entity)
    {
        $seo = PKController::GetArrayByKey('seo', $entity);
        unset($entity['seo']);
        $seo = Converter::Unserialize($seo);
        empty($seo) ?: $entity = array_merge($entity, $seo);
        $contentList = array();
        foreach ($entity as $value) {
            if (!empty($value)) {
                $str = Fileter::filterValue(Fileter::htmlspecialchars_decode($value), true);
                $str_len = mb_strlen($str, 'utf8');
                if ($str_len < $this->_apiCheckStrLen) {
                    $contentList[] = $str;
                } else {
                    $tmp = $this->_mbStrSplit($str, $this->_apiCheckStrLen);
                    $contentList = array_merge($contentList, $tmp);
                }
            }
        }
        return $this->_service($contentList);
    }

    public function CheckContent($entity)
    {
        if (is_array($entity) && array_key_exists('data', $entity)) {
            $entity = array_merge($entity, $entity['data']);
            unset($entity['data']);
        }
        $checkFields = array('title', 'content', 'seotitle', 'seokeywords', 'seodescription');
        $contentList = array();
        foreach ($checkFields as $checkField) {
            if (is_array($entity) && array_key_exists($checkField, $entity) && !empty($entity[$checkField])) {
                $str = Fileter::filterValue(Fileter::htmlspecialchars_decode($entity[$checkField]), true);
                $str_len = mb_strlen($str, 'utf8');
                if ($str_len < $this->_apiCheckStrLen) {
                    $contentList[] = $str;
                } else {
                    $tmp = $this->_mbStrSplit($str, $this->_apiCheckStrLen);
                    $contentList = array_merge($contentList, $tmp);
                }
            }
        }
        return $this->_service($contentList);
    }

    private function _service($contentList)
    {
        $tips = $this->_funcAntispam($contentList);
        if (empty($tips)) {
            return '';
        }
        return $tips;
    }

    /**
     * 支持中文的PHP按字符串长度分割成数组
     * @param $string
     * @param int $len
     * @return array
     */
    private function _mbStrSplit($string, $len = 1)
    {
        $start = 0;
        $strlen = mb_strlen($string, 'utf8');
        $array = array();
        while ($strlen) {
            $array[] = mb_substr($string, $start, $len, "utf8");
            $string = mb_substr($string, $len, $strlen, "utf8");
            $strlen = mb_strlen($string);
        }
        return $array;
    }

    // 内容安全
    private function _funcAntispam($string)
    {
        $apiUrl = 'http://antispam.zj11.net/';
        $url = new Curl($apiUrl, 'json');
        $resultJson = $url->Param(array(
            'content' => $string
        ))->TimeOut(180)->Request();
        $res = json_decode($resultJson);
        $tips = '';
        $count = 0;
        if (isset($res->Code) && $res->Code == 200) {
            if (isset($res->results) && is_array($res->results)) {
                foreach ($res->results as $result) {
                    if (isset($result->Label) && isset($result->LabelName)) {
                        switch ($result->Label) {
                            case 'customized':
                                if (isset($result->List) && is_object($result->List) && count($result->List) > 0) {
                                    $tips .= $result->LabelName . ': <Br/>';
                                    foreach ($result->List as $key => $value) {
                                        $tips .= $key . ': ' . $value . '<Br/>';
                                    }
                                }
                                break;
                            case 'normal':
                                break;
                            default:
                                if (isset($result->List) && is_array($result->List) && count($result->List) > 0) {
                                    $tips .= $result->LabelName . ': ' . implode('、', $result->List) . '<Br/>';
                                }
                                break;
                        }
                    }
                }
                empty($tips) ?: $tips = Route\language('AntispamApi_Tips', 'Antispam') . '<Br/>' . $tips;
            }
        } elseif (isset($result->Msg)) {
            \PKCore\alert('AntispamApi_Error', $res->Msg);
        }
        return $tips;
    }

}