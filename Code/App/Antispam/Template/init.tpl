<{admin_breadcrumb('敏感词检测')}>

<!-- Title -->
<div class="page-header">
    <div class="page-title">
        <h3>
            敏感词检测
        </h3>
    </div>
</div>
<!-- /Title -->
<!-- Row -->
<div class="row">
    <div class="col-md-12">
        <div class="widget">
            <div class="widget-content row">
                <form method="get" class="form-inline" action="/index.php/content/AdminSearch/Search"
                      data-ajax="true" data-ajax-mode="replace" data-ajax-update="#contentList">
                    <div class="form-group col-md-6 col-sm-6 col-xs-12">
                        <label class="control-label">
                            第1步：选择检测的内容模型
                        </label>
                        <{optionBySelct('modelId', $modelList)}>
                    </div>
                    <div class="form-group col-md-6 col-sm-6 col-xs-12">
                        <label class="control-label">
                            第2步：选择性操作
                        </label>
                        <div>
                            <input type="button" class="btn btn-info" id="antispamByContent" value="内容检测">
                            <input type="button" class="btn btn-info" id="antispamByCategory" value="栏目检测">
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="widget box">
            <div class="widget-header">
                <h4>
                    <i class="icon-reorder"></i>
                    列表
                </h4>
            </div>
            <div class="widget-content">
                <div id="tips" class="alert alert-info fade in">
                    <i class="icon-info-sign"></i>
                    <span></span>
                </div>
                <div id="contentList">
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /Row -->
<script type="text/javascript">
    var isGoToAntispam = false;
    var tipsId = $('#tips span');
    $('#antispamByCategory').die().on('click', function () {
        isGoToAntispam = !isGoToAntispam;
        if (isGoToAntispam) {
            var html = '<table class="table table-striped table-condensed table-hover table-bordered table-responsive"></table>';
            $('#contentList').html(html);
            $(this).removeClass('btn-info').addClass('btn-danger');
            $(this).attr('value', '停止栏目检测');
            antispamByCategory(1);
        } else {
            $(this).removeClass('btn-danger').addClass('btn-info').attr('value', '重新开始栏目检测');
        }
        return false;
    });

    $('#antispamByContent').die().on('click', function () {
        var modelId = $('select').val();
        if (modelId == '' || modelId == null) {
            tips.html('请选择检测的模型');
            return false;
        }
        isGoToAntispam = !isGoToAntispam;
        if (isGoToAntispam) {
            var html = '<table class="table table-striped table-condensed table-hover table-bordered table-responsive"></table>';
            $('#contentList').html(html);
            $(this).removeClass('btn-info').addClass('btn-danger');
            $(this).attr('value', '停止内容检测');
            antispamByContent(modelId, 1);
        } else {
            $(this).removeClass('btn-danger').addClass('btn-info').attr('value', '重新开始内容检测');
        }
        return false;
    });

    function antispamByCategory(eachIndex) {
        var data = {total:0,eachIndex};
        $.get('/index.php/Antispam/AdminCheckCategory', data,
            function (res) {
                if (typeof res === "object") {
                    data.total = res.count;
                    tipsId.html("检测总数：" + data.total + '，已完成检测数：' + data.eachIndex);
                    if (res.result !== '') {
                        var html = '<tr><th><a data-toggle="modal" data-target="#editorDialog"\n' +
                            ' href="/index.php/content/AdminGetCategory?id=' + res.result.id + '"\n' +
                            ' ><i class="icon-pencil" />' +
                            res.result.name + '</a></th><td>' + res.result.tips + '</td></tr>';
                        $(html).appendTo('#contentList table');
                    }
                    if (isGoToAntispam && (data.eachIndex <= data.total)) {
                        antispamByCategory(eachIndex + 1);
                    } else {
                        tipsId.html('已完成，或者已停止检测');
                        $('#antispamByCategory').removeClass('btn-danger').addClass('btn-info').attr('value', '重新开始栏目检测');
                    }
                } else {
                    tipsId.html(res);
                }
            });
    }

    function antispamByContent(modelId, eachIndex) {
        var data = {total:0,modelId,eachIndex};
        $.get('/index.php/Antispam/AdminCheckContent', data,
            function (res) {
                data.total = res.count;
                tipsId.html("检测总数：" + data.total + '，已完成检测数：' + data.eachIndex);
                if (res.result !== '') {
                    var html = '<tr><th><a onclick="mainToggler();" data-ajax="true"' +
                        ' data-ajax-begin="beginPaging"\n' +
                        ' data-ajax-failure="failurePaging"\n' +
                        ' data-ajax-mode="replace" data-ajax-success="successPaging" data-ajax-update="#subMain"\n' +
                        ' href="/index.php/content/AdminGetContent?catId=' + res.result.catId + '&id=' + res.result.id + '"\n' +
                        ' ><i class="icon-pencil" />' +
                        res.result.title + '</a></th><td>' + res.result.tips + '</td></tr>';
                    $(html).appendTo('#contentList table');
                }
                if (isGoToAntispam && (data.eachIndex <= data.total)) {
                    antispamByContent(modelId, eachIndex + 1);
                } else {
                    tipsId.html('已完成，或者已停止内容检测');
                    $('#antispamByContent').removeClass('btn-danger').addClass('btn-info').attr('value', '重新开始内容检测');
                }
            });
    }
</script>
