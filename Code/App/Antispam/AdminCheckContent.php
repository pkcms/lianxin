<?php


namespace PKApp\Antispam;


use PKApp\Admin\Classes\AdminController;
use PKApp\Antispam\Classes\Antispam;
use PKApp\Content\Classes\ContentsDataBase;
use PKApp\Model\Classes\ModelExtend;
use PKCore\Request;

class AdminCheckContent extends AdminController
{

    private $_modelId;

    public function Main()
    {
        $this->_modelId = Request::get('modelId');
        !empty($this->_modelId) ?: \PKCore\alert('Search_Error');
        $result = array(
            'count' => $this->_count(),
            'result' => '',
        );
        $result['count'] == 0 ?: $result['result'] = $this->_eachIndex();
        return $result;
    }

    private function _dbContents()
    {
        static $db;
        !empty($db) ?: $db = new ContentsDataBase($this->_modelId);
        return $db;
    }

    private function _count()
    {
        return $this->_dbContents()->GetCountContent(array('status' => 1));
    }

    private function _eachIndex()
    {
        $eachIndex = Request::get('eachIndex');
        $entity = $this->_dbContents()->GetContentDetailByNext($eachIndex);
        $antispam = new Antispam();
        $tips = $antispam->CheckContent($entity);
        if (empty($tips)) {
            return '';
        }
        return array('id' => $entity['id'], 'title' => $entity['title'], 'catId' => $entity['catid'], 'tips' => $tips);
    }

}