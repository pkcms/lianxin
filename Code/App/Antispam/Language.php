<?php
/**
 * 模块语言包
 * User: Administrator
 * Date: 2019/5/27
 * Time: 9:28
 */

return array(
    'AntispamApi_Tips' => array(
        'zh-cn' => '您提交的信息中存在敏感词信息，检查结果：',
        'en' => ''
    ),
    'AntispamApi_Error' => array(
        'zh-cn' => '内容安全远程接口出错，提示：',
        'en' => ''
    ),
);