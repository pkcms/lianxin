<?php


namespace PKApp\Links;


use PKApp\Admin\Classes\AdminController;
use PKApp\Links\Classes\LinksDataBase;
use PKCore\Converter;
use PKCore\Formats;
use PKCore\Request;
use PKCore\Tpl;

class AdminSetLinks extends AdminController
{

    public function Main()
    {
        $action = Request::post('action');
        if (method_exists($this, $action)) {
            $this->$action();
        }
    }

    public function GetDetail()
    {
        $id = Formats::isNumeric(Request::get('id'));
        $tpl = new Tpl();
        if (!empty($id)) {
            $entity = LinksDataBase::GetLinkDetail($id);
            empty($entity) || !is_array($entity) ?: $tpl->SetTplParamList($entity);
        } else {
            $parent = Formats::isNumeric(Request::get('parent'));
            $tpl->SetTplParam('parentid', $parent)->SetTplParam('is_index', 1);
        }
        $parent = LinksDataBase::GetLinkParentList();
        $parent_select = !empty($parent) && is_array($parent) ? Converter::arrayColumn($parent, 'name','id') : array();
        $tpl->SetTplParam('parent_select', $parent_select)
            ->SetTplParam('IsOrNot',self::getDict('IsOrNot'));
        return $tpl->Display('data_view');
    }

    public function SetDetail()
    {
        $this->checkPostFieldIsEmpty('sitename', 'Links_SiteNameEmpty');
        $id = Request::post('id');
        $post = Request::post();
        empty($id) ? LinksDataBase::AddLink($post) : LinksDataBase::UpdateLink($post, $id);
        \PKCore\msg('Data_Input_Success');
    }

    protected function del()
    {
        $set_id = Request::post('selectData');
        if (!empty($set_id)) {
            LinksDataBase::DelLink($set_id);
        }
        \PKCore\msg('Data_Input_Success');
    }

    protected function sort()
    {
        $sort = Request::post('selectData');
        if (Formats::isArray($sort)) {
            foreach ($sort as $key => $value) {
                $data = array('listsort' => Formats::isNumeric($value));
                LinksDataBase::UpdateLink($data, Formats::isNumeric($key));
            }
        }
        \PKCore\msg('Data_Input_Success');
    }
}