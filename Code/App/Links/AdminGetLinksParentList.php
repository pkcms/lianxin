<?php


namespace PKApp\Links;


use PKApp\Admin\Classes\AdminController;
use PKApp\Links\Classes\LinksDataBase;
use PKCore\Tpl;

class AdminGetLinksParentList extends AdminController
{

    public function Main()
    {
        $tpl = new Tpl();
        return $tpl->SetTplParam('parent_lists', LinksDataBase::GetLinkParentList())
            ->Display('parent_lists');
    }
}