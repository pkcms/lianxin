<{admin_breadcrumb('链接管理')}>

<!-- Title -->
<div class="page-header">
    <div class="page-title">
        <h3>
            链接 站点编辑
        </h3>
    </div>
</div>
<!-- /Title -->
<!-- Row -->
<div class="row">
    <div class="col-md-12">
        <div class="widget box">
            <div class="widget-header">
                <h4>基本填写</h4>
            </div>
            <div class="widget-content">
                <form id="linkForm" class="form-horizontal row-border" action="links/AdminSetLinks/SetDetail">
                    <div class="form-group">
                        <label class="col-md-2 control-label">
                            所属分类:
                        </label>
                        <div class="col-md-10">
                            <{optionBySelct('parentid', $parent_select, $parentid)}>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label">
                            链接名称:
                        </label>
                        <div class="col-md-10">
                            <input type="text" name="sitename" class="form-control" value="<{$sitename}>" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label">
                            logo地址:
                        </label>
                        <div class="col-md-10">
                            <{UploadFile('image',$sitelogo,'sitelogo')}>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label">
                            链接地址:
                        </label>
                        <div class="col-md-10">
                            <input type="text" name="siteurl" class="form-control" value="<{$siteurl}>" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label">
                            链接描述:
                        </label>
                        <div class="col-md-10">
                            <textarea name="siteinfo" class="form-control"><{$siteinfo}></textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label">
                            是否在首页显示:
                        </label>
                        <div class="col-md-10">
                            <{optionBySelct('is_index', $IsOrNot, $is_index)}>
                        </div>
                    </div>
                    <div class="form-actions">
                        <input type="hidden" name="id" value="<{$id}>" />
                        <button type="button" data-form="linkForm" class="btn btn-primary pull-left">
                            提交保存
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- /Row -->
