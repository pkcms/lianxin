<{admin_breadcrumb('链接管理')}>

<!-- Title -->
<div class="page-header">
    <div class="page-title">
        <h3>
            <{$parentName}> 链接列表
        </h3>
    </div>
</div>
<!-- /Title -->
<!-- Row -->
<div class="row">
    <div class="col-md-12">
        <div class="widget">
            <div class="widget-content">
                <div class="btn-toolbar btn-toolbar-demo">
                    <a class="btn btn-success" data-ajax="true" data-ajax-begin="beginPaging" data-ajax-failure="failurePaging"
                       data-ajax-mode="replace" data-ajax-success="successPaging" data-ajax-update="#main"
                       href="/index.php/links/AdminSetLinks/GetDetail?parent=<{$parentid}>">
                        <i class="icon-plus-sign"></i>&nbsp;添加新的链接</a>
                    <button class="btn btn-danger"  data-action="del" data-table="LinkList">
                        <i class="icon-remove-circle"></i>
                        删除
                    </button>
                    <button class="btn btn-default" data-action="sort" data-table="LinkList">
                        排序
                    </button>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-12">
        <div class="widget box">
            <div class="widget-header">
                <h4>
                    <i class="icon-reorder"></i>
                    列表
                </h4>
            </div>
            <div class="widget-content">
                <table id="LinkList" data-action="links/AdminSetLinks"
                       class="table table-bordered table-striped table-hover mb-0">
                    <thead class="thead-secondary">
                    <tr>
                        <th width="30">&nbsp;</th>
                        <th width="65">排序</th>
                        <th width="65">操作</th>
                        <th>分类</th>
                        <th>网站名称</th>
                        <th>网站链接</th>
                    </tr>
                    </thead>
                    <tbody>
                    <{loop $lists $item}>
                        <tr>
                            <td><input type="checkbox" value="<{$item[id]}>" name="set_id[]" /></td>
                            <td>
                                <input type="text" value="<{$item[listsort]}>" name="sort[]" data-id="<{$item[id]}>"
                                       class="form-control" />
                            </td>
                            <td>
                                <a data-ajax="true" data-ajax-begin="beginPaging" data-ajax-failure="failurePaging"
                                   data-ajax-mode="replace" data-ajax-success="successPaging" data-ajax-update="#main"
                                   href="/index.php/links/AdminSetLinks/GetDetail?id=<{$item[id]}>">
                                    <i class="icon-edit"></i>编辑</a>
                            </td>
                            <td><{$parentName}></td>
                            <td><{$item[sitename]}></td>
                            <td><{$item[siteurl]}></td>
                        </tr>
                        <{/loop}>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<!-- /Row -->
