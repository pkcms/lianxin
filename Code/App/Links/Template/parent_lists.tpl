<{admin_breadcrumb('链接管理')}>

<!-- Title -->
<div class="page-header">
    <div class="page-title">
        <h3>
            链接 分类管理
        </h3>
    </div>
</div>
<!-- /Title -->
<!-- Row -->
<div class="row">
    <div class="col-md-12">
        <div class="widget">
            <div class="widget-content">
                <div class="btn-toolbar btn-toolbar-demo">
                    <a class="btn btn-success" data-ajax="true" data-ajax-begin="beginPaging" data-ajax-failure="failurePaging"
                       data-ajax-mode="replace" data-ajax-success="successPaging" data-ajax-update="#main"
                       href="/index.php/links/AdminSetLinksParent/GetDetail">
                        <i class="icon-plus-sign"></i>&nbsp;添加分类</a>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-12">
        <div class="widget box">
            <div class="widget-header">
                <h4>
                    <i class="icon-reorder"></i>
                    列表
                </h4>
            </div>
            <div class="widget-content">
                <table class="table table-bordered table-striped table-hover mb-0">
                    <thead class="thead-secondary">
                    <tr>
                        <th width="30">&nbsp;</th>
                        <th width="250">操作</th>
                        <th width="30">ID</th>
                        <th>分类名</th>
                    </tr>
                    </thead>
                    <tbody>
                    <{loop $parent_lists $item}>
                        <tr>
                            <td><input type="radio" value="<{$item[id]}>" name="parent" /></td>
                            <td>
                                <a href="/index.php/links/AdminSetLinksParent/GetDetail?id=<{$item[id]}>"
                                   data-ajax="true" data-ajax-begin="beginPaging" data-ajax-failure="failurePaging"
                                   data-ajax-mode="replace" data-ajax-success="successPaging" data-ajax-update="#main">
                                    <i class="icon-edit"></i>
                                    编辑分类
                                </a>
                                <span>|</span>
                                <a data-ajax="true" data-ajax-begin="beginPaging" data-ajax-failure="failurePaging"
                                   data-ajax-mode="replace" data-ajax-success="successPaging" data-ajax-update="#main"
                                   href="/index.php/links/AdminGetLinksList?parent=<{$item[id]}>">链接列表</a>
                            </td>
                            <td><{$item[id]}></td>
                            <td><{$item[name]}></td>
                        </tr>
                        <{/loop}>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<!-- /Row -->
