<{admin_breadcrumb('链接管理')}>

<!-- Title -->
<div class="page-header">
    <div class="page-title">
        <h3>
            链接 分类编辑
        </h3>
    </div>
</div>
<!-- /Title -->
<!-- Row -->
<div class="row">
    <div class="col-md-12">
        <div class="widget box">
            <div class="widget-header">
                <h4>基本填写</h4>
            </div>
            <div class="widget-content">
                <form id="linkForm" class="form-horizontal row-border" action="links/AdminSetLinksParent/SetDetail">
                    <div class="form-group">
                        <label class="col-md-2 control-label">
                            分类名:
                        </label>
                        <div class="col-md-10">
                            <input type="text" name="name" class="form-control" value="<{$name}>" />
                        </div>
                    </div>
                    <div class="form-actions">
                        <input type="hidden" name="id"  value="<{$id}>" />
                        <button type="button" data-form="linkForm" class="btn btn-primary pull-left">
                            提交保存
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- /Row -->
