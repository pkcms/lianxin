<?php


namespace PKApp\Links;


use PKApp\Admin\Classes\AdminController;
use PKApp\Links\Classes\LinksDataBase;
use PKCore\Formats;
use PKCore\Request;
use PKCore\Tpl;

class AdminSetLinksParent extends AdminController
{

    public function Main()
    {
        $action = Request::get('action');
        if (method_exists($this,$action)) {
            $this->$action();
        }
    }

    public function GetDetail()
    {
        $id = Formats::isNumeric(Request::get('id'));
        $tpl = new Tpl();
        if (!empty($id)) {
            $entity = LinksDataBase::GetLinkParentDetail($id);
            empty($entity) || !is_array($entity) ?: $tpl->SetTplParamList($entity);
        }
        return $tpl->Display('parent_view');
    }

    public function SetDetail()
    {
        $this->checkPostFieldIsEmpty('name', 'Links_NameEmpty');
        $id = Request::post('id');
        $post = Request::post();
        empty($id) ? LinksDataBase::AddLinkParent($post) : LinksDataBase::UpdateLinkParent($post, $id);
        \PKCore\msg('Data_Input_Success');
    }

    protected function del()
    {
        $set_id = Request::post('set_id');
        if (!empty($set_id)) {
            LinksDataBase::DelLinkParent($set_id);
        }
        \PKCore\msg('Data_Input_Success');
    }
}