<?php
/**
 * 模块语言包
 * User: Administrator
 * Date: 2019/5/27
 * Time: 9:28
 */

return array(
    'Links_NameEmpty' => array(
        'zh-cn' => '链接分类名称不能为空',
        'en' => ''
    ),
    'Links_SiteNameEmpty' => array(
        'zh-cn' => '链接站点名称不能为空',
        'en' => ''
    ),
);