<?php


namespace PKApp\Links;


use PKApp\Admin\Classes\AdminController;
use PKApp\Links\Classes\LinksDataBase;
use PKCore\Formats;
use PKCore\Request;
use PKCore\Tpl;

class AdminGetLinksList extends AdminController
{

    public function Main()
    {
        $parentId = Formats::isNumeric(Request::get('parent'));
        $parentEntity = LinksDataBase::GetLinkParentDetail($parentId);
        $tpl = new Tpl();
        return $tpl->SetTplParam('parentid', $parentId)
            ->SetTplParam('parentName', $parentEntity['name'])
            ->SetTplParam('lists', LinksDataBase::GetLinkList($parentId))
            ->Display('data_lists');
    }
}