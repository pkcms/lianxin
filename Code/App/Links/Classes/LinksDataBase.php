<?php


namespace PKApp\Links\Classes;


use PKCore\DataBase;

class LinksDataBase
{

    private static $_links = 'links';
    private static $_links_parent = 'links_parent';

    public static function GetLinkParentList()
    {
        $db = new DataBase(self::$_links_parent);
        return $db->Select()->ToList();
    }

    public static function GetLinkParentDetail($id)
    {
        $db = new DataBase(self::$_links_parent);
        return $db->Where(array('id' => $id))->Select()->First();
    }

    public static function AddLinkParent($data)
    {
        if (is_array($data) && array_key_exists('id', $data)) {
            unset($data['id']);
        }
        $db = new DataBase(self::$_links_parent);
        return $db->Insert($data)->Exec();
    }

    public static function UpdateLinkParent($data, $id)
    {
        $db = new DataBase(self::$_links_parent);
        return $db->Where(array('id' => $id))->Update($data)->Exec();
    }

    public static function DelLinkParent($id)
    {
        $db = new DataBase(self::$_links_parent);
        return $db->Where(array('id' => $id))->Delete()->Exec();
    }

    public static function GetLinkList($parentId)
    {
        $db = new DataBase(self::$_links);
        return $db->Where(array('parentid' => $parentId))->Select()->OrderBy('listsort','asc')->ToList();
    }

    public static function GetLinkDetail($id)
    {
        $db = new DataBase(self::$_links);
        return $db->Where(array('id' => $id))->Select()->First();
    }

    public static function AddLink($data)
    {
        if (is_array($data) && array_key_exists('id', $data)) {
            unset($data['id']);
        }
        $db = new DataBase(self::$_links);
        return $db->Insert($data)->Exec();
    }

    public static function UpdateLink($data, $id)
    {
        $db = new DataBase(self::$_links);
        return $db->Where(array('id' => $id))->Update($data)->Exec();
    }

    public static function DelLink($id)
    {
        $db = new DataBase(self::$_links);
        return $db->Where(array('id' => $id))->Delete()->Exec();
    }

}