<?php

namespace PKApp\Links\Classes;

use PKApp\Links\Classes\LinksDataBase;
use PKCore\Formats;
use PKCore\Request;

class LinksTemplateTag {

    function Main($param) {
        $parentId = Formats::isNumeric($param['parent']);
        $result = array('linkList'=>LinksDataBase::GetLinkList($parentId));
        return $result;
    }

}

?>
