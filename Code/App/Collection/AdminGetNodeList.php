<?php


namespace PKApp\Collection;


use PKApp\Admin\Classes\AdminController;
use PKApp\Collection\Classes\CollectionDataBase;
use PKCore\Tpl;

class AdminGetNodeList extends AdminController
{

    public function Main()
    {
        $tpl = new Tpl();
        $db_collection = new CollectionDataBase();
        $tpl->SetTplParam('lists', $db_collection->GetNodeList($this->loginUser()->SiteId));
        return $tpl->Display('node_list');
    }
}