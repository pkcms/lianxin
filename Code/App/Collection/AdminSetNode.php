<?php


namespace PKApp\Collection;


use PKApp\Admin\Classes\AdminController;
use PKApp\Collection\Classes\CollectionDataBase;
use PKCore\Request;

class AdminSetNode extends AdminController
{

    public function Main()
    {
        $post = Request::post();
        $post['webSiteUrlConfig'] = serialize($post['webSiteUrlConfig']);
        $post['rule'] = serialize($post['rule']);
        $post['siteId'] = $this->loginUser()->SiteId;
        $db_collection = new CollectionDataBase();
        $db_collection->SetNode($post);
        \PKCore\msg('Data_Input_Success');
    }
}