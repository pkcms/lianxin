<?php


namespace PKApp\Collection;


use PKApp\Admin\Classes\AdminController;
use PKApp\Collection\Classes\Collection;
use PKApp\Collection\Classes\CollectionContentDataBase;
use PKApp\Collection\Classes\CollectionExtend;
use PKCore\Converter;
use PKCore\Fileter;
use PKCore\Formats;
use PKCore\PKController;
use PKCore\Request;
use PKCore\Tpl;

class AdminCollectionUrlLists extends AdminController
{

    public function Main()
    {
        Request::has('nodeId', 'get') ?: \PKCore\alert('Node_IdEmpty');
        $id = Formats::isNumeric(Request::get('nodeId'));
        $entity = CollectionExtend::GetNode($id);
        $tpl = new Tpl();
        $siteUrlType = self::GetArrayByKey('siteUrlType', $entity);
        $webSiteUrlConfig = PKController::GetArrayByKey('webSiteUrlConfig', $entity, 'Node_WebSiteUrlConfigEmpty');
        $pageIndex = Formats::isNumeric(Request::get('pageIndex'));
        if (Request::has('pageIndex', 'get')) {
            $col = new Collection();
            $urlList = array();
            switch ($siteUrlType) {
                case 'pages':
                    $webSiteUrlConfig = PKController::GetArrayByKey('webSiteUrlConfig', $entity, 'Node_WebSiteUrlConfigEmpty');
                    $siteUrlList = explode("\r\n", PKController::GetArrayByKey('siteUrlList', $webSiteUrlConfig, 'Node_UrlPageEmpty'));
                    $url = $siteUrlList[$pageIndex - 1];
                    $urlList = $col->GetSiteUrlTypeByPages($entity, $pageIndex);
                    break;
                case 'lists':
                    $url = str_replace('(*)', $pageIndex,
                        PKController::GetArrayByKey('urlPage', $webSiteUrlConfig, 'Node_UrlPageEmpty'));
                    $urlList = $col->GetSiteUrlTypeByLists($entity, $pageIndex);
                    break;
            }
            list($count_exists, $count_input) = $this->_checkExists($urlList, $id);
            $tpl->SetTplParam('urlLists', $urlList)->SetTplParam('url', $url)
                ->SetTplParam('total', count($urlList))->SetTplParam('count_exists', $count_exists)
                ->SetTplParam('count_input', $count_input);
            return $tpl->PhpDisplay('url_info');
        } else {
            $pageEnd = 0;
            switch ($siteUrlType) {
                case 'pages':
                    $webSiteUrlConfig = PKController::GetArrayByKey('webSiteUrlConfig', $entity, 'Node_WebSiteUrlConfigEmpty');
                    $siteUrlList = explode("\r\n", PKController::GetArrayByKey('siteUrlList', $webSiteUrlConfig, 'Node_UrlPageEmpty'));
                    $pageEnd = count($siteUrlList);
                    $pageIndex = 1;
                    break;
                case 'lists':
                    $url = str_replace('(*)', $pageIndex,
                        PKController::GetArrayByKey('urlPage', $webSiteUrlConfig, 'Node_UrlPageEmpty'));
                    $pageIndex = $webSiteUrlConfig['pageSize_start'];
                    $pageEnd = $webSiteUrlConfig['pageSize_end'];
                    break;
            }
            $tpl->SetTplParam('nodeId', $id)
                ->SetTplParam('pageIndex',$pageIndex)
                ->SetTplParam('pageEnd',$pageEnd);
            return $tpl->Display('url_lists');
        }
    }

    private function _checkExists($urlLists, $nodeId)
    {
        if (Formats::isArray($urlLists)) {
            $urls = Converter::arrayGetByKey($urlLists, 'url');
            $col_CD = new CollectionContentDataBase();
            $urlLists_exists = $col_CD->Where(array('url' => $urls, 'nodeId' => $nodeId,
                'siteId' => $this->loginUser()->SiteId))->Select(array('id', 'url'))->ToList();
            $input_batch = array();
            if (is_array($urlLists_exists)) {
                $urlLists_exists = Converter::arrayColumn($urlLists_exists, 'url', 'id');
                $count_exists = count($urlLists_exists);
                foreach ($urlLists as $item) {
                    if (!in_array($item['url'], $urlLists_exists)) {
                        $item['title'] = Fileter::htmlspecialchars($item['title']);
                        $item['status'] = 'await';
                        $item['siteId'] = $this->loginUser()->SiteId;
                        $item['nodeId'] = $nodeId;
                        $input_batch[] = $item;
                    }
                }
                !Formats::isArray($input_batch) ?: $col_CD->Insert($input_batch, true)->Exec();
            } else {
                $count_exists = 0;
            }
            $count_input = count($input_batch);
            return array($count_exists, $count_input);
        }
        return array(0, 0);
    }

}