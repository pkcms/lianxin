<?php


namespace PKApp\Collection;


use PKApp\Admin\Classes\AdminController;
use PKApp\Collection\Classes\CollectionContentDataBase;
use PKApp\Content\Classes\CategoryDataBase;
use PKApp\Content\Classes\ContentsDataBase;
use PKApp\Model\Classes\ModelExtend;
use PKCore\Converter;
use PKCore\Files;
use PKCore\Formats;
use PKCore\Request;
use PKCore\Statics;

class AdminImportContent extends AdminController
{

    private $_actionType;
    private $_field_content_isIndex;
    private $_modelId;
    private $_import_total;
    private $_import_pageTotal = 0;
    private $_isSavePicToLocal;

    public function Main()
    {
        $post = Request::post();
        $this->_actionType = self::GetArrayByKey('actionType', $post, 'import_actionType_empty');
        $content_idList = self::GetArrayByKey('idList', $post,
            $this->_actionType == 'importSelect' ? 'import_idList_empty' : null);
        $categoryId = self::GetArrayByKey('categoryId', $post, 'import_categoryId_empty');
        $nodeId = self::GetArrayByKey('nodeId', $post, 'import_nodeId_empty');
        $this->_isSavePicToLocal = (bool)self::GetArrayByKey('isSavePicToLocal', $post);
        $this->_getCategory($categoryId);
        $importList = $this->_getContentList($nodeId, $categoryId, $content_idList);
        $this->_importContent($importList);
        $this->_setStatus($content_idList);
        return array('pageTotal' => $this->_import_pageTotal, 'pageIndex' => self::$pageIndex);

    }

    private function _getCategory($categoryId)
    {
        $db_category = new CategoryDataBase();
        $entity_category = $db_category->Where(array('id' => $categoryId))->Select(array('modelid'))->First();
        $this->_modelId = self::GetArrayByKey('modelid', $entity_category);
        $entity_modelField = ModelExtend::GetModelFieldEntity($this->_modelId);
        $fieldList = self::GetArrayByKey('fieldList', $entity_modelField);
        $entity_modelField_content = self::GetArrayByKey('content', $fieldList, 'import_field_content');
        $this->_field_content_isIndex = (bool)self::GetArrayByKey('isindex', $entity_modelField_content);
    }

    private function _getContentList($nodeId, $categoryId, $idList)
    {
        $where = array('nodeId' => $nodeId, 'siteId' => $this->loginUser()->SiteId);
        $this->_actionType == 'importSelect' ? $where['id'] = $idList : $where['status'] = 'content';
        $db_content = new CollectionContentDataBase();
        $db_content->Where($where);
        if ($this->_actionType == 'importAll') {
            $entity_count = $db_content->Count();
            self::GetPages();
            $db_content->Limit(self::$pageSize, self::$pageIndex);
            $this->_import_pageTotal = ceil($entity_count / self::$pageSize);
        }
        $listEntity = $db_content->Select()->ToList();
        $statusListEntity = Converter::arrayGetByKey($listEntity, 'status');
        !in_array('await', $statusListEntity) ?: \PKCore\alert('import_status_await');
        !in_array('import', $statusListEntity) ?: \PKCore\alert('import_status_import');
        $importList = array();
        if (Formats::isArray($listEntity)) {
            foreach ($listEntity as $item) {
                $tmp_data = Converter::objectToArray(json_decode($item['data']));
                $tmp_data['content'] = $this->_replaceImageToLocal($tmp_data['content']);
                $tmp_time = strtotime($tmp_data['time']);
                if ($this->_field_content_isIndex) {
                    $importList[] = array('title' => $tmp_data['title'], 'content' => $tmp_data['content'],
                        'siteid' => $this->loginUser()->SiteId, 'catid' => $categoryId, 'status' => 1,
                        'modelid' => $this->_modelId, 'seotitle' => $tmp_data['title'],
                        'inputtime' => $tmp_time, 'updatetime' => $tmp_time,
                        'data' => array());
                } else {
                    $importList[] = array(
                        'title' => $tmp_data['title'], 'inputtime' => $tmp_time, 'updatetime' => $tmp_time,
                        'siteid' => $this->loginUser()->SiteId, 'catid' => $categoryId, 'status' => 1,
                        'modelid' => $this->_modelId, 'seotitle' => $tmp_data['title'],
                        'data' => array('content' => $tmp_data['content'])
                    );
                }
            }
        }
        return $importList;
    }

    private function _replaceImageToLocal($htmlCode)
    {
        if ($this->_isSavePicToLocal) {
            $imageList = Statics::imgsinarticle($htmlCode);
            $tmp_replace_img = array();
            if (Formats::isArray($imageList)) {
                $tmp_savePath = 'upload' . DS . date('Ymd') . DS;
                foreach ($imageList as $image) {
                    $tmp_fileExt = Files::GetFileExtension($image);
                    $imageUrl = (stristr($image, 'http:') ? '' : 'http:') . $image;
                    $tmp_img = Files::getContents($imageUrl, 60);
                    if (!empty($tmp_img)) {
                        $fileName = SYS_TIME . \PKCore\random(6) . '.' . $tmp_fileExt;
                        Files::mkdir(PATH_ROOT . $tmp_savePath);
                        file_put_contents(PATH_ROOT . $tmp_savePath . $fileName, $tmp_img);
                        $tmp_replace_img[] = '/' . str_replace(DS, '/', $tmp_savePath) . $fileName;
                    }
                }
            }
            if (Formats::isArray($tmp_replace_img)) {
                $htmlCode = str_replace($imageList, $tmp_replace_img, $htmlCode);
            }
        }
        return $htmlCode;
    }

    private function _importContent($importList)
    {
        if (Formats::isArray($importList)) {
            foreach ($importList as $item) {
                $this->_db_content()->AddContent($item);
            }
        }
    }

    private function _setStatus($idList)
    {
        $db_cc = new CollectionContentDataBase();
        $db_cc->Where(array('id' => $idList))->Update(array('status' => 'import'))->Exec();
    }

    private function _db_content()
    {
        static $db;
        !empty($db) ?: $db = new ContentsDataBase($this->_modelId);
        return $db;
    }
}