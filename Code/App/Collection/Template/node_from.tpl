<{admin_breadcrumb('采集管理')}>

<!-- Title -->
<div class="page-header">
    <div class="page-title">
        <h3>
            采集节点编辑
        </h3>
    </div>
</div>
<!-- /Title -->
<!-- Row -->
<div class="row">
    <div class="col-md-12">
        <form id="collectionNodeForm" class="form-horizontal row-border" action="Collection/AdminSetNode">
            <div class="widget box">
                <div class="widget-header">
                    <h4>
                        <i class="icon-reorder"></i>
                        基本填写
                    </h4>
                </div>
                <div class="widget-content">
                    <div class="form-group">
                        <label class="col-md-2 control-label">
                            节点名称:
                        </label>
                        <div class="col-md-10">
                            <input type="text" class="form-control" name="name" value="<{$name}>"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label">
                            采集页面编码:
                        </label>
                        <div class="col-md-10">
                            <{RadioButton($sourceCharsetList, $sourceCharset, 'sourceCharset')}>
                        </div>
                    </div>
                </div>
            </div>
            <div class="widget box">
                <div class="widget-header">
                    <h4>
                        <i class="icon-reorder"></i>
                        网址采集
                    </h4>
                </div>
                <div class="widget-content">
                    <div class="form-group">
                        <label class="col-md-2 control-label">
                            网址类型:
                        </label>
                        <div id="siteUrlTypeSelect" class="col-md-10">
                            <{RadioButton($siteUrlTypeList, $siteUrlType ? $siteUrlType : 'lists', 'siteUrlType')}>
                        </div>
                    </div>
                    <div id="siteUrlType_lists" class="form-group siteUrlConfig">
                        <label class="col-md-2 control-label">
                            网址配置:
                        </label>
                        <div class="col-md-10">
                            <input type="text" id="name" class="form-control" name="webSiteUrlConfig[urlPage]"
                                   value="<{$webSiteUrlConfig[urlPage]}>"/>
                            <p class="help-block">如：http://……/help_(*).htm,页码使用(*)做为通配符。</p>
                            <div class="form-group">
                                <label class="col-md-2 control-label">
                                    页码范围(从):
                                </label>
                                <div class="col-md-4">
                                    <input type="text" name="webSiteUrlConfig[pageSize_start]"
                                           value="<{if $webSiteUrlConfig[pageSize_start]}><{$webSiteUrlConfig[pageSize_start]}><{else}>1<{/if}>"
                                           class="form-control">
                                </div>
                                <label class="col-md-2 control-label">
                                    页码范围(到):
                                </label>
                                <div class="col-md-4">
                                    <input type="text" name="webSiteUrlConfig[pageSize_end]"
                                           value="<{if $webSiteUrlConfig[pageSize_end]}><{$webSiteUrlConfig[pageSize_end]}><{else}>10<{/if}>"
                                           class="form-control">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="siteUrlType_pages" class="form-group siteUrlConfig" style="display: none">
                        <label class="col-md-2 control-label">
                            网址配置:
                        </label>
                        <div class="col-md-10">
                            <textarea class="form-control" name="webSiteUrlConfig[siteUrlList]"><{if $webSiteUrlConfig[siteUrlList]}><{$webSiteUrlConfig[siteUrlList]}><{/if}></textarea>
                            <p class="help-block">每行一条</p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label">
                            获取网址(从):
                        </label>
                        <div class="col-md-4">
                            <textarea class="form-control" name="webSiteUrlConfig[url_start]"><{if $webSiteUrlConfig[url_start]}><{$webSiteUrlConfig[url_start]}><{/if}></textarea>
                        </div>
                        <label class="col-md-2 control-label">
                            获取网址(到):
                        </label>
                        <div class="col-md-4">
                            <textarea class="form-control" name="webSiteUrlConfig[url_end]"><{if $webSiteUrlConfig[url_end]}><{$webSiteUrlConfig[url_end]}><{/if}></textarea>
                        </div>
                    </div>
                </div>
            </div>
            <div class="widget box">
                <div class="widget-header">
                    <h4>
                        <i class="icon-reorder"></i>
                        内容采集
                    </h4>
                </div>
                <div class="widget-content">
                    <div class="form-group">
                        <label class="col-md-2 control-label">
                            标题匹配规则:
                        </label>
                        <div class="col-md-4">
                                    <textarea class="form-control" name="rule[title_rule]"
                                              id="title_rule"><{if $rule[title_rule]}><{$rule[title_rule]}><{else}>&lt;title&gt;^&lt;/title&gt;<{/if}></textarea>
                            <p class="help-block">使用"^"作为通配符</p>
                        </div>
                        <label class="col-md-2 control-label">
                            过滤选项:
                        </label>
                        <div class="col-md-4">
                                    <textarea class="form-control" name="rule[title_html_rule]"
                                              id="title_html_rule"><{if $rule[title_html_rule]}><{$rule[title_html_rule]}><{/if}></textarea>
                            <!-- Large modal -->
                            <button type="button" class="btn btn-default" data-toggle="modal"
                                    data-target=".bs-example-modal-lg" onclick="html_role('title_html_rule')">
                                选择
                            </button>
                            <p class="help-block">该过滤选项是HTML标签的过滤</p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label">
                            时间匹配规则:
                        </label>
                        <div class="col-md-4">
                                    <textarea class="form-control" name="rule[time_rule]"
                                              id="time_rule"><{if $rule[time_rule]}><{$rule[time_rule]}><{/if}></textarea>
                            <p class="help-block">使用"^"作为通配符</p>
                        </div>
                        <label class="col-md-2 control-label">
                            过滤选项:
                        </label>
                        <div class="col-md-4">
                                    <textarea class="form-control" name="rule[time_html_rule]"
                                              id="time_html_rule"><{if $rule[time_html_rule]}><{$rule[time_html_rule]}><{/if}></textarea>
                            <!-- Large modal -->
                            <button type="button" class="btn btn-default" data-toggle="modal"
                                    data-target=".bs-example-modal-lg" onclick="html_role('time_html_rule')">
                                选择
                            </button>
                            <p class="help-block">该过滤选项是HTML标签的过滤</p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label">
                            内容匹配规则:
                        </label>
                        <div class="col-md-4">
                                    <textarea class="form-control" name="rule[content_rule]"
                                              id="content_rule"><{if $rule[content_rule]}><{$rule[content_rule]}><{/if}></textarea>
                            <p class="help-block">使用"^"作为通配符</p>
                        </div>
                        <label class="col-md-2 control-label">
                            过滤选项:
                        </label>
                        <div class="col-md-4">
                                    <textarea class="form-control" name="rule[content_html_rule]"
                                              id="content_html_rule"><{if $rule[content_html_rule]}><{$rule[content_html_rule]}><{/if}></textarea>
                            <!-- Large modal -->
                            <button type="button" class="btn btn-default" data-toggle="modal"
                                    data-target=".bs-example-modal-lg" onclick="html_role('content_html_rule')">
                                选择
                            </button>
                            <p class="help-block">该过滤选项是HTML标签的过滤</p>
                        </div>
                    </div>
                </div>
            </div>
            <input type="hidden" name="id" value="<{$id}>"/>
            <div class="form-actions">
                <button type="button" data-form="collectionNodeForm" class="btn btn-primary pull-left">
                    提交保存
                </button>
            </div>
        </form>
    </div>
</div>
<!-- /Row -->

<div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        &times;
                    </button>
                    <h4 class="modal-title">
                        选择过滤的选项
                    </h4>
                </div>
                <div id="roleList" class="modal-body row">
                    <{loop $htmlTagList $key $item}>
                    <div class="col-xs-3">
                        <input type="checkbox" value="<{$key}>"><{$item}>
                    </div>
                    <{/loop}>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">
                        取消
                    </button>
                    <button type="button" class="btn btn-primary" data-dismiss="modal" onclick="html_tags()">
                        选好
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    var htmlRoleId;

    function html_role(htmlId) {
        htmlRoleId = htmlId;
    }

    function html_tags() {
        $('#roleList input:checked').each(function (index, item) {
            var html = $(item).val() + "\r\n";
            var value = $('#' + htmlRoleId).val() + html;
            $('#' + htmlRoleId).val(value);
        });
        $('.bs-example-modal-lg').modal('toggle');
    }

    // 采集网址的类型
    function checkSiteUrlType(index) {
        $('.siteUrlConfig').hide();
        $('#siteUrlType_' + index).show();
    }

    $('#siteUrlTypeSelect input:radio').click(function () {
        checkSiteUrlType($(this).val());
    });
    checkSiteUrlType('<{if $siteUrlType}><{$siteUrlType}><{else}>lists<{/if}>');
</script>
