<?php echo admin_breadcrumb('采集管理'); ?>

<!-- Title -->
<div class="page-header">
    <div class="page-title">
        <h3>
            采集内容
        </h3>
    </div>
</div>
<!-- /Title -->
<!-- Row -->
<div class="row">
    <div class="col-md-12">
        <div class="widget box">
            <div class="widget-content">
                <div id="show_url_div" style="font-size: 16px">
                    采集点：&nbsp;<?php echo isset($node) && is_array($node) ? $node['name'] : '' ?>&nbsp;
                    正在进行采集内容
                    <br><br>
                    采集进度：<b id="GoOn">……</b>&nbsp;/&nbsp;<b><?php echo isset($count_total) ? $count_total : 0 ?></b>
                    <br><br>
                    <span id="tips"></span>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /Row -->

<script type="text/javascript">
    var data = {
        total: <?php echo isset($count_total) ? $count_total : 0 ?>,
        index: 0,
        nodeId:<?php echo isset($node) && is_array($node) ? $node['id'] : 0 ?>
    };

    function col_content() {
        if (data.index >= data.total) {
            $('#tips').html('没有寻找需要采集的内容');
            return false;
        }
        $.ajax({
            url: '/index.php/Collection/AdminCollectionContent/Collection',
            type: 'get',
            data: data,
            cache: false,
            success: function (result) {
                if (typeof result == "object") {
                    data.index += 1;
                    $('#GoOn').html(data.index);
                    $('#tips').html(result.msg);
                    if (data.index < data.total) {
                        col_content();
                    } else {
                        $('#tips').html('完成内容的采集');
                    }
                } else {
                    $('#tips').html(result);
                }
            }
        });
    }

    col_content();
</script>
