<{admin_breadcrumb('采集管理')}>

<!-- Title -->
<div class="page-header">
    <div class="page-title">
        <h3>
            采集节点管理
        </h3>
        <p>
            采集节点就是作业流程的转折点或关键点，采集节点管理对这些关键点的事前计划、事中措施、事后总结的管理方式。
        </p>
    </div>
</div>
<!-- /Title -->
<!-- Row -->
<div class="row">
    <div class="col-md-12">
        <div class="widget">
            <div class="widget-content">
                <div class="btn-toolbar btn-toolbar-demo">
                    <a class="btn btn-success" data-ajax="true" data-ajax-begin="beginPaging"
                       data-ajax-failure="failurePaging"
                       data-ajax-mode="replace" data-ajax-success="successPaging"
                       data-ajax-update="#main"
                       href="/index.php/Collection/AdminGetNode">
                        <i class="icon-plus-sign"></i>&nbsp;添加节点</a>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-12">
        <div class="widget box">
            <div class="widget-header">
                <h4>
                    <i class="icon-reorder"></i>
                    列表
                </h4>
            </div>
            <div class="widget-content">
                <table class="table table-striped table-condensed table-hover table-bordered table-responsive">
                    <thead>
                    <tr>
                        <th width="500">操作</th>
                        <th>节点名称</th>
                    </tr>
                    </thead>
                    <tbody>
                    <{loop $lists $item}>
                        <tr class="<{$item[id]}>">
                            <td>
                                <a data-ajax="true" data-ajax-begin="beginPaging"
                                   data-ajax-failure="failurePaging"
                                   data-ajax-mode="replace" data-ajax-success="successPaging"
                                   data-ajax-update="#main"
                                   href="/index.php/Collection/AdminGetNode?id=<{$item[id]}>">修改</a>
                                &nbsp;|&nbsp;
                                <a data-ajax="true" data-ajax-begin="beginPaging"
                                   data-ajax-failure="failurePaging"
                                   data-ajax-mode="replace" data-ajax-success="successPaging"
                                   data-ajax-update="#main"
                                   href="/index.php/Collection/AdminTestNode?nodeId=<{$item[id]}>">测试</a>
                                &nbsp;|&nbsp;
                                <a data-ajax="true" data-ajax-begin="beginPaging"
                                   data-ajax-failure="failurePaging"
                                   data-ajax-mode="replace" data-ajax-success="successPaging"
                                   data-ajax-update="#main"
                                   href="/index.php/Collection/AdminCollectionUrlLists?nodeId=<{$item[id]}>">采集网址</a>
                                &nbsp;|&nbsp;
                                <a data-ajax="true" data-ajax-begin="beginPaging"
                                   data-ajax-failure="failurePaging"
                                   data-ajax-mode="replace" data-ajax-success="successPaging"
                                   data-ajax-update="#main"
                                   href="/index.php/Collection/AdminCollectionContent?nodeId=<{$item[id]}>">采集内容</a>
                                &nbsp;|&nbsp;
                                <a data-ajax="true" data-ajax-begin="beginPaging"
                                   data-ajax-failure="failurePaging"
                                   data-ajax-mode="replace" data-ajax-success="successPaging"
                                   data-ajax-update="#main"
                                   href="/index.php/Collection/AdminPublishContentList?nodeId=<{$item[id]}>">内容发布</a>
                            </td>
                            <td>
                                <{$item[name]}>
                            </td>
                        </tr>
                        <{/loop}>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<!-- /Row -->
