<{admin_breadcrumb('采集管理')}>

<!-- Title -->
<div class="page-header">
    <div class="page-title">
        <h3>
            测试采集
        </h3>
    </div>
</div>
<!-- /Title -->
<!-- Row -->
<div class="row">
    <div class="col-md-12">
        <div class="widget box">
            <div class="widget-header">
                <h4>
                    <i class="icon-reorder"></i>
                    列表
                </h4>
            </div>
            <div class="widget-content">
                <table class="table table-striped table-condensed table-hover table-bordered table-responsive">
                    <tbody>
                    <{loop $urlLists $item}>
                        <tr>
                            <td>
                                <{$item[title]}>&nbsp;->&nbsp;
                                <a  data-toggle="modal" data-target="#myModal"
                                    href="/index.php/Collection/AdminTestNode?nodeId=<{$nodeId}>&url=<{urlencode($item[url])}>">查看</a>
                                <br>
                                <{$item[url]}>
                            </td>
                        </tr>
                        <{/loop}>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<!-- /Row -->
