<{admin_breadcrumb('采集管理')}>

<!-- Title -->
<div class="page-header">
    <div class="page-title">
        <h3>
            <{$nodeName}>&nbsp;-&nbsp;文章列表
        </h3>
    </div>
</div>
<!-- /Title -->
<!-- Row -->
<div class="row">
    <div class="col-md-12">
        <div class="widget">
            <div class="widget-content">
                <div class="btn-toolbar btn-toolbar-demo">
                    <button class="btn btn-danger" data-action="del">
                        <i class="icon-remove-circle"></i>
                        删除
                    </button>
                    <input type="button" value="导入选中" class="btn btn-default" <{if $status == 'import'}> disabled="disabled"<{/if}> onclick="importSelect()">
                    <input type="button" value="导入全部" class="btn btn-default" <{if $status == 'import'}> disabled="disabled"<{/if}> onclick="importAll()">
                    <input type="button" value="删除导入历史" class="btn btn-default" <{if $status != 'import'}> disabled="disabled"<{/if}> onclick="deleteImportLog()">
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-12">
        <div class="widget box">
            <div class="widget-header">
                <h4>
                    <i class="icon-reorder"></i>
                    列表
                </h4>
                <div class="toolbar no-padding">
                    <div class="btn-group">
                        <!-列表筛选方式 start-->
                        <span class="btn btn-xs dropdown-toggle" data-toggle="dropdown">
                          筛选
                          <i class="icon-angle-down"></i>
                        </span>
                        <ul class="dropdown-menu pull-right">
                            <li>
                                <a data-ajax="true" data-ajax-begin="beginPaging"
                                   data-ajax-failure="failurePaging"
                                   data-ajax-mode="replace" data-ajax-success="successPaging"
                                   data-ajax-update="#main"
                                   href="/index.php/Collection/AdminPublishContentList?nodeId=<{$nodeId}>">
                                    全部
                                </a>
                            </li>
                            <{loop $CollectionContentStatus $key $item}>
                            <li><a data-ajax="true" data-ajax-begin="beginPaging"
                                   data-ajax-failure="failurePaging"
                                   data-ajax-mode="replace" data-ajax-success="successPaging"
                                   data-ajax-update="#main"
                                   href="/index.php/Collection/AdminPublishContentList?nodeId=<{$nodeId}>&status=<{$key}>"><{$item}></a></li>
                            <{/loop}>
                        </ul>
                        <!-列表筛选方式 end-->
                    </div>
                </div>
            </div>
            <div class="widget-content no-padding">
                <div class="dataTables_header clearfix">
                    <div class="col-md-4 col-md-offset-8">
                        <form class="form-vertical row-border" method="get"
                              action="/index.php/Collection/AdminPublishContentList"
                              data-ajax="true" data-ajax-mode="replace" data-ajax-update="#main">
                            <div class="form-group">
                                <label class="control-label">
                                    本节点搜索:
                                </label>
                                <div class="row no-padding">
                                    <div class="col-xs-10" style="padding-left: 0; padding-right: 0;">
                                        <input type="text" name="query" class="form-control" maxlength="10"
                                               placeholder="请输入标题的搜索关键词"/>
                                    </div>
                                    <div class="col-xs-2" style="padding-left: 0;">
                                        <input type="hidden" name="nodeId" value="<{$nodeId}>" />
                                        <input type="hidden" name="status" value="<{$status}>">
                                        <button class="btn btn-info" name="sort">
                                            <i class="icon-search"></i>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <table class="table table-striped table-condensed table-hover table-bordered table-responsive">
                    <thead>
                    <tr>
                        <th width="35">&nbsp;</th>
                        <th width="55">操作</th>
                        <th width="70">状态</th>
                        <th>标题</th>
                        <th>网址</th>
                    </tr>
                    </thead>
                    <{loop $lists $item}>
                    <tbody>
                    <tr class="<{$item[id]}>">
                        <td><input type="checkbox" name="set_id[]" value="<{$item[id]}>"<{if ($item[status] != 'content') && ($status != 'import')}> disabled="disabled"<{/if}> /></td>
                        <td>
                            <a href="javascript:void(0)" onclick="$('#tab_<{$item['id']}>').toggle()">
                                查看
                            </a>
                        </td>
                        <td>
                            <{$CollectionContentStatus[$item[status]]}>
                        </td>
                        <td>
                            <{$item[title]}>
                        </td>
                        <td>
                            <{$item[url]}>
                        </td>
                    </tr>
                    <tr id="tab_<{$item['id']}>" style="display: none;">
                        <td colspan="5">
                            <textarea class="form-control" style="width:98%;height:300px;"><{$item[data]}></textarea>
                        </td>
                    </tr>
                    </tbody>
                    <{/loop}>
                </table>
                <div class="row">
                    <div class="dataTables_footer clearfix">
                        <div class="col-md-6">
                            <div class="dataTables_info" id="DataTables_Table_4_info">
                                第&nbsp;<{$pageList['index']}>/<{$pageList['pageCount']}>&nbsp;页，
                                总&nbsp;<{$pageList['dataCount']}>&nbsp;条。
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="dataTables_paginate paging_bootstrap">
                                <ul class="pagination">
                                    <{loop $pageList $key $pageItem}>
                                    <{if $key == 'pre'}>
                                    <li class="prev<{if $pageList['index'] == 1}> disabled<{/if}>">
                                        <a data-ajax="true" data-ajax-begin="beginPaging" data-ajax-failure="failurePaging"
                                           data-ajax-mode="replace" data-ajax-success="successPaging" data-ajax-update="#main"
                                           href="<{$pageItem}>">← Previous</a>
                                    </li>
                                    <{elseif $key == 'sizeList'}>
                                    <{loop $pageItem $i $sizeUrl}>
                                <li<{if $pageList['index'] == $i}> class="active"<{/if}>>
                                    <a data-ajax="true" data-ajax-begin="beginPaging" data-ajax-failure="failurePaging"
                                       data-ajax-mode="replace" data-ajax-success="successPaging" data-ajax-update="#main"
                                       href="<{$sizeUrl}>"><{$i}></a>
                                    </li>
                                    <{/loop}>
                                    <{elseif $key == 'next'}>
                                    <li class="next<{if $pageList['index'] == $pageList['pageCount']}>  disabled<{/if}>">
                                        <a data-ajax="true" data-ajax-begin="beginPaging" data-ajax-failure="failurePaging"
                                           data-ajax-mode="replace" data-ajax-success="successPaging" data-ajax-update="#main"
                                           href="<{$pageItem}>">Next → </a>
                                    </li>
                                    <{/if}>
                                    <{/loop}>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /Row -->

<div class="modal fade bs-example-modal-step-selectCategory" id="step-selectCategory" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        &times;
                    </button>
                    <h4 class="modal-title">
                        导入设置
                    </h4>
                </div>
                <div id="roleList" class="modal-body">
                    <div class="form-group row">
                        <label class="col-md-4 control-label">
                            选择要导入的栏目:
                        </label>
                        <div class="col-md-8">
                            <{$categoryListHtml}>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-md-4 control-label">
                            是否保存图片到本地:
                        </label>
                        <div class="col-md-8">
                            <{RadioButton($IsOrNot, 0, 'isSavePicToLocal')}>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-md-4 control-label">
                            执行结果:
                        </label>
                        <div id="tips" class="col-md-8">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">
                        取消
                    </button>
                    <button type="button" class="btn btn-primary" data-dismiss="modal" onclick="_import(1)">
                        选好
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javaScript">
    var data = {
        actionType: '',
        idList: [],
        categoryId: 0,
        isSavePicToLocal: 0,
        nodeId: <{$nodeId}>
    };
    function importSelect() {
        var selectId = new Array();
        data.actionType = 'importSelect';
        $('input:checkbox:checked').each(function (index, item) {
            var value = $(item).val();
            selectId.push(value);
        });
        if (selectId.length == 0) {
            alert('请选择要导入的内容……');
        } else {
            data.idList = selectId;
            console.log(selectId);
            $('#step-selectCategory').modal('toggle');
        }
    }
    function importAll() {
        data.actionType = 'importAll';
        $('#step-selectCategory').modal('toggle');
    }
    function deleteImportLog() {
        var selectId = new Array();
        data.actionType = 'deleteImportLog';
        $('input:checkbox:checked').each(function (index, item) {
            var value = $(item).val();
            selectId.push(value);
        });
        if (selectId.length == 0) {
            pkcms.msg('请选择要删除导入历史的内容……');
        } else {
            data.idList = selectId;
            $.ajax({
                url: '/index.php/Collection/AdminSetContentOther?action=deleteImportLog',
                type: 'post',
                data: data,
                success: function (result) {
                    pkcms.msg(result);
                    $('#backPage').click();
                }
            });
        }
    }

    function _import(pageIndex) {
        data.categoryId = $('#set_category:Checked').val();
        data.isSavePicToLocal = $('input[name="isSavePicToLocal"]:checked').val();
        if (data.categoryId == undefined) {
            pkcms.msg('请选择要导入到哪个栏目……');
        } else {
            $.ajax({
                url: '/index.php/Collection/AdminImportContent?pageIndex='+pageIndex,
                type: 'post',
                data: data,
                success: function (result) {
                    if (typeof result == "object") {
                        console.log(result);
                        if (result.pageIndex < result.pageTotal) {
                            _import(parseInt(result.pageIndex) + 1);
                        } else {
                            $('#tips').html('导入成功');
                            $('#backPage').click();
                            $('.modal-backdrop').hide();
                            $('body').removeClass('modal-open');
                        }
                    } else {
                        $('#tips').html(result);
                    }
                }
            });
        }
        console.log(data);
    }

</script>