<?php
function htmlForeachCollection($lists, $level = 0)
{
    if (is_array($lists)) {
        foreach ($lists as $item) {
            ?>
            <label>
                <input type="radio" id="set_category" name="set_category"
                       value="<?php echo $item['id'] ?>"<?php if ($item['child']) { ?> disabled="disabled"<?php } ?> />
                <?php for ($i = 0; $i < $level; $i++) {
                    echo '&nbsp&nbsp;&nbsp&nbsp;';
                } ?>
                <?php echo $item['name'] ?>
            </label>
            <br/>
            <?php if ($item['child']) {
                htmlForeachCollection($item['child'], $level + 1);
            }
        }
    }
}

if (isset($categoryList)) {
    htmlForeachCollection($categoryList);
}
?>