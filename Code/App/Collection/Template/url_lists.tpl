<{admin_breadcrumb('采集管理')}>

<!-- Title -->
<div class="page-header">
    <div class="page-title">
        <h3>
            采集网址列表
        </h3>
    </div>
</div>
<!-- /Title -->
<!-- Row -->
<div class="row">
    <div class="col-md-12">
        <div class="widget box">
            <div class="widget-header">
                <h4>
                    <i class="icon-reorder"></i>
                    网址列表
                </h4>
            </div>
            <div id="show_url_div" class="widget-content">
            </div>
        </div>
    </div>
</div>
<!-- /Row -->

<script type="text/javascript">
    var url = '/index.php/Collection/AdminCollectionUrlLists?';
    var pageIndex = <{$pageIndex}>;
    var pageEnd = <{$pageEnd}>;
    function col_urlLists() {
        $.get(url, 'nodeId=<{$nodeId}>&pageIndex=' + pageIndex, function (result) {
            $('#show_url_div').html(result);
            if (pageIndex < pageEnd) {
                pageIndex += 1;
                col_urlLists();
            }
        });
    }

    col_urlLists();
</script>
