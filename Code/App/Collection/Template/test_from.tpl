<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                &times;
            </button>
            <h4 class="modal-title">
                测试采集信息
            </h4>
        </div>
        <div class="modal-body">
            <table class="table table-striped table-condensed table-hover table-bordered table-responsive">
                <{loop $lists $key $item}>
                <tr>
                    <th><{$key}>：</th>
                    <td><{$item}></td>
                </tr>
                <{/loop}>
            </table>
        </div>
    </div>
</div>

