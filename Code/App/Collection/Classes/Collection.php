<?php


namespace PKApp\Collection\Classes;

use PKCore\Converter;
use PKCore\Files;
use PKCore\Fileter;
use PKCore\PKController;
use PKCore\Route;
use PKCore\Statics;

class Collection
{

    public function GetSiteUrlTypeByLists($entity, $pageIndex)
    {
        $webSiteUrlConfig = PKController::GetArrayByKey('webSiteUrlConfig', $entity, 'Node_WebSiteUrlConfigEmpty');
        $url = str_replace('(*)', $pageIndex,
            PKController::GetArrayByKey('urlPage', $webSiteUrlConfig, 'Node_UrlPageEmpty'));
        return $this->_getSourceCode($url, $entity, $webSiteUrlConfig);
    }

    public function GetSiteUrlTypeByPages($entity, $index)
    {
        $webSiteUrlConfig = PKController::GetArrayByKey('webSiteUrlConfig', $entity, 'Node_WebSiteUrlConfigEmpty');
        $siteUrlList = explode("\r\n", PKController::GetArrayByKey('siteUrlList', $webSiteUrlConfig, 'Node_UrlPageEmpty'));
        return $this->_getSourceCode($siteUrlList[$index - 1], $entity, $webSiteUrlConfig);
    }

    private function _getSourceCode($url, $entity, $webSiteUrlConfig)
    {
        $sourceCode = $this->_getHtmlSourceSiteCode($url,
            PKController::GetArrayByKey('sourceCharset', $entity, 'Node_SourceCharsetEmpty'));
        $url_start = Fileter::htmlspecialchars_decode(PKController::GetArrayByKey('url_start', $webSiteUrlConfig));
        $url_end = Fileter::htmlspecialchars_decode(PKController::GetArrayByKey('url_end', $webSiteUrlConfig));
        $cutHtmlCode = $this->_cutHtmlCode($sourceCode, $url_start, $url_end);
        $urlLists = $this->getUrlLists($url, $cutHtmlCode, $webSiteUrlConfig);
        return $urlLists;
    }

    /**
     * 获取文章网址
     * @param string $url 采集地址
     * @param string $html
     * @param array $config 配置
     * @return array
     */
    protected function getUrlLists($url, $html, &$config)
    {
        $html = str_replace(array("\r", "\n"), '', $html);
        $html = str_replace(array("</a>", "</A>"), "</a>\n", $html);
        preg_match_all('/<a ([^>]*)>([^\/a>].*)<\/a>/i', $html, $out);
        //$out[1] = array_unique($out[1]);
        //$out[2] = array_unique($out[2]);
        $data = array();
        foreach ($out[1] as $k => $v) {
            if (preg_match('/href=[\'"]?([^\'" ]*)[\'"]?/i', $v, $match_out)) {
                if ($config['url_contain']) {
                    if (strpos($match_out[1], $config['url_contain']) === false) {
                        continue;
                    }
                }
                if ($config['url_except']) {
                    if (strpos($match_out[1], $config['url_except']) !== false) {
                        continue;
                    }
                }
                $url2 = $match_out[1];
                $url2 = $this->_checkUrl($url2, $url, $config);
                $data[$k]['url'] = $url2;
                $data[$k]['title'] = strip_tags($out[2][$k]);
            }
        }
        return $data;
    }

    /**
     * 采集内容
     * @param string $url 采集地址
     * @param $sourceCode
     * @param array $config 配置参数
     * @param integer $page 分页采集模式
     * @return array
     */
    public function GetContent($url, $sourceCode, $config, $page = 0)
    {
        set_time_limit(300);
        static $oldUrl = array();
        $page = intval($page) ? intval($page) : 0;
        $data = array();
        if ($html = $this->_getHtmlSourceSiteCode($url, $sourceCode)) {
            if (empty($page)) {
                //获取标题
                if ($config['title_rule']) {
                    $title_rule = $this->_replace_sg(Fileter::htmlspecialchars_decode($config['title_rule']));
                    $data['title'] = $this->_replace_item($this->_cutHtmlCode($html, $title_rule[0], $title_rule[1]), $config['title_html_rule']);
                }

                //获取时间
                if ($config['time_rule']) {
                    $time_rule = $this->_replace_sg(Fileter::htmlspecialchars_decode($config['time_rule']));
                    $data['time'] = $this->_replace_item($this->_cutHtmlCode($html, $time_rule[0], $time_rule[1]), $config['time_html_rule']);
                }

                if (empty($data['time'])) $data['time'] = TIMESTAMP;

            }

            //获取内容
            if ($config['content_rule']) {
                $content_rule = $this->_replace_sg(Fileter::htmlspecialchars_decode($config['content_rule']));
                $data['content'] = $this->_replace_item($this->_cutHtmlCode($html, $content_rule[0], $content_rule[1]), $config['content_html_rule']);
            }

            //处理分页
            if (in_array($page, array(0, 2)) && !empty($config['content_page_start']) && !empty($config['content_page_end'])) {
                $oldurl[] = $url;
                $tmp[] = $data['content'];
                $page_html = $this->_cutHtmlCode($html, $config['content_page_start'], $config['content_page_end']);

                //全部罗列模式
                if ($page == 0 && $page_html) {
                    preg_match_all('/<a [^>]*href=[\'"]?([^>\'" ]*)[\'"]?/i', $page_html, $out);
                    if (is_array($out[1]) && !empty($out[1])) {
                        $out = array_unique($out[1]);
                        foreach ($out as $k => $v) {
                            if ($out[1][$k] == '#') {
                                continue;
                            }
                            $v = $this->_checkUrl($v, $url, $config);
                            $results = $this->GetContent($v, $sourceCode, $config, 1);
                            if (!in_array($results['content'], $tmp) && $results['content'] != "") {
                                $tmp[] = $results['content'];
                            }
                        }
                    }

                }
                $data['content'] = $config['content_page'] == 1 ? implode('[page]', $tmp) : implode('', $tmp);
            }
            return $data;
        }
    }

    /**
     * 获取远程HTML
     * @param string $url 获取地址
     * @param string $sourceCode 源字符集
     * @return bool|false|string|null
     */
    private function _getHtmlSourceSiteCode($url, $sourceCode)
    {
        if (!empty($url)) {
            $html = Files::getContents(Fileter::htmlspecialchars_decode($url), 180);
            if (CHARSET != $sourceCode) {
                $html = iconv($sourceCode, CHARSET . '//TRANSLIT//IGNORE', $html);
            }
            return $html;
        } else {
            return false;
        }
    }

    /**
     * 替换采集内容
     * @param string $html 采集规则
     * @return array
     */
    private function _replace_sg($html)
    {
        $list = explode('^', $html);
        if (is_array($list)) {
            foreach ($list as $k => $v) {
                $list[$k] = str_replace(array("\r", "\n"), '', trim($v));
            }
        }
        return $list;
    }

    /**
     * 过滤代码
     * @param string $html HTML代码
     * @param string $filter 过滤配置
     * @return bool|string|string[]|null
     */
    private function _replace_item($html, $filter = null)
    {
        if (empty($filter)) {
            return $html;
        }
        $filter = Fileter::htmlspecialchars_decode($filter);
        $filterArr = explode("\n", $filter);
        $htmlTagList = PKController::getDict('html_tag');
        $patterns = $replace = array();
        $p = 0;
        foreach ($filterArr as $k => $tagName) {
            $tagName = trim($tagName);
            if (empty($tagName) || !array_key_exists($tagName, $htmlTagList)) continue;
            $replace_str = $htmlTagList[$tagName];
            $c = explode('[|]', $replace_str);
            $patterns[$k] = '/' . str_replace('/', '\/', $c[0]) . '/i';
            $replace[$k] = $c[1];
            $p = 1;
        }
        return $p ? @preg_replace($patterns, $replace, $html) : false;
    }

    /**
     * HTML切取
     * @param string $htmlCode 要进入切取的HTML代码
     * @param string $start 开始
     * @param string $end 结束
     * @return bool|string
     */
    private function _cutHtmlCode($htmlCode = '', $start = '', $end = '')
    {
        if (empty($htmlCode)) {
            return false;
        }
        $htmlCode = Statics::zip($htmlCode);
        $start = Statics::zip($start);
        $end = Statics::zip($end);
        $htmlCode = explode(trim($start), $htmlCode);
        if (is_array($htmlCode)) $htmlCode = explode(trim($end), $htmlCode[1]);
        return trim($htmlCode[0]);
    }

    /**
     * URL地址检查
     * @param string $url 需要检查的URL
     * @param string $baseUrl 基本URL
     * @param array $config 配置信息
     * @return string
     */
    private function _checkUrl($url = '', $baseUrl = '', $config = array())
    {
        $urlInfo = parse_url($baseUrl);
        $baseUrl = $urlInfo['scheme'] . '://' . $urlInfo['host'] .
            (substr($urlInfo['path'], -1, 1) === '/' ? substr($urlInfo['path'], 0, -1) : str_replace('\\', '/', dirname($urlInfo['path']))) . '/';
        if (strpos($url, '://') === false) {
            if ($url[0] == '/') {
                $url = $urlInfo['scheme'] . '://' . $urlInfo['host'] . $url;
            } else {
                if ($config['page_base']) {
                    $url = $config['page_base'] . $url;
                } else {
                    $url = $baseUrl . $url;
                }
            }
        }
        return $url;
    }
}