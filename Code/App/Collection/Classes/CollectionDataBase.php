<?php


namespace PKApp\Collection\Classes;


use PKCore\DataBase;
use PKCore\Route;
use PKCore\Tpl;

class CollectionDataBase extends DataBase
{

    public function __construct()
    {
        parent::__construct('collection_node');
        $isExists = $this->CheckTableNameExists();
        $tpl = new Tpl();
        $isExists ?: $tpl->Notice(Route\language('App_Name') . Route\language('App_Error'));
    }

    public function SetNode($data)
    {
        $id = $data['id'];
        unset($data['id']);
        if (empty($id)) {
            return $this->Insert($data)->Exec();
        } else {
            $this->Where(array('id' => $id))->Update($data)->Exec();
            return $id;
        }
    }

    public function GetNode($id)
    {
        return $this->Where(array('id' => $id))->Select()->First();
    }

    public function GetNodeList($siteId)
    {
        return $this->Where(array('siteId' => $siteId))
            ->Select(array('id', 'name'))->ToList();
    }

}