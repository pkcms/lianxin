<?php


namespace PKApp\Collection\Classes;

use PKCore\Converter;

class CollectionExtend
{

    public static function GetNode($id)
    {
        $db_collection = new CollectionDataBase();
        $entity = $db_collection->GetNode($id);
        if (!empty($entity) && is_array($entity)) {
            $entity['webSiteUrlConfig'] = Converter::Unserialize($entity['webSiteUrlConfig']);
            $entity['rule'] = Converter::Unserialize($entity['rule']);
        } else {
            \PKCore\alert('Node_Empty');
        }
        return $entity;
    }

}