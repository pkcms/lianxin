<?php


namespace PKApp\Collection\Classes;

use PKCore\Route;
use PKCore\DataBase;

class CollectionContentDataBase extends DataBase
{

    public function __construct()
    {
        parent::__construct('collection_content');
        $isExists = $this->CheckTableNameExists();
        $isExists ?: \PKCore\alert('App_Name',Route\language('App_Error'));
    }


}