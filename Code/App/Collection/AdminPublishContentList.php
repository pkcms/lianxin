<?php


namespace PKApp\Collection;


use PKApp\Admin\Classes\AdminController;
use PKApp\Collection\Classes\CollectionContentDataBase;
use PKApp\Collection\Classes\CollectionExtend;
use PKApp\Content\Classes\CategoryDataBase;
use PKApp\Content\Classes\ContentExtend;
use PKCore\Converter;
use PKCore\Fileter;
use PKCore\Formats;
use PKCore\Request;
use PKCore\Statics;
use PKCore\Tpl;

class AdminPublishContentList extends AdminController
{

    private $_nodeId;

    public function Main()
    {
        $entity = $this->_getNode();
        $status = null;
        if (Request::has('status', 'get')) {
            $status = Request::get('status');
        }
        self::GetPages();
        list($count, $lists) = $this->_getContentLists($status);
        $pageNav = Statics::pages($count, self::$pageSize, self::$pageIndex,
            '/index.php/Collection/AdminPublishContentList?nodeId=' . $this->_nodeId . '&pageIndex=[page]');
        $tpl = new Tpl();
        return $tpl->SetTplParam('lists', $lists)->SetTplParam('pageList', $pageNav)
            ->SetTplParam('CollectionContentStatus', self::getDict('CollectionContentStatus'))
            ->SetTplParam('categoryListHtml', $this->_getCategoryList())
            ->SetTplParam('nodeId', $this->_nodeId)
            ->SetTplParam('IsOrNot', self::getDict('IsOrNot'))
            ->SetTplParam('status', $status)
            ->SetTplParam('nodeName', self::GetArrayByKey('name', $entity['name']))
            ->Display('publish_content');
    }

    private function _getNode()
    {
        Request::has('nodeId', 'get') ?: \PKCore\alert('Node_IdEmpty');
        $this->_nodeId = Formats::isNumeric(Request::get('nodeId'));
        return CollectionExtend::GetNode($this->_nodeId);
    }

    private function _getContentLists($status = null)
    {
        $options = array('siteId' => $this->loginUser()->SiteId, 'nodeId' => $this->_nodeId);
        if (!is_null($status)) {
            $options['status'] = $status;
        }
        if (Request::has('query', 'get')) {
            $query = trim(Request::get('query'));
            $options[] = 'title LIKE "%' . $query . '%"';
        }
        $db_content = new CollectionContentDataBase();
        $db_content->Where($options);
        $count = $db_content->Count();
        $list = $db_content->Limit(self::$pageSize, self::$pageIndex)->OrderBy('id')
            ->Select(array('id', 'title', 'url', 'status', 'data'))->ToList();
        if (Formats::isArray($list)) {
            foreach ($list as $index => $item) {
                $item['data'] = Fileter::htmlspecialchars(print_r(Converter::objectToArray(json_decode($item['data'])), true));
                $list[$index] = $item;
            }
        }
        return array($count, $list);
    }

    private function _getCategoryList()
    {
        $db_category = new CategoryDataBase();
        $lists = $db_category->Where(array('siteid' => $this->loginUser()->SiteId, 'type' => 2, 'status <> 4'))
            ->Select(array('id', 'parentid', 'name'))->ToList();
        $lists = ContentExtend::TreeCategoryList($lists, 0);
        $tpl = new Tpl();
        return $tpl->SetTplParam('categoryList', $lists)->PhpDisplay('category_select');
    }

}