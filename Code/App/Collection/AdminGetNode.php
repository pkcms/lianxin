<?php


namespace PKApp\Collection;


use PKApp\Admin\Classes\AdminController;
use PKApp\Collection\Classes\CollectionExtend;
use PKCore\Fileter;
use PKCore\Formats;
use PKCore\Request;
use PKCore\Tpl;

class AdminGetNode extends AdminController
{

    public function Main()
    {
        $tpl = new Tpl();
        if (Request::has('id','get')) {
            $id = Formats::isNumeric(Request::get('id'));
            $tpl->SetTplParamList(CollectionExtend::GetNode($id));
        }
        $htmlTagList = self::getDict('html_tag');
        $htmlTag = array();
        if (Formats::isArray($htmlTagList)) {
            foreach ($htmlTagList as $index => $item) {
                $htmlTag[Fileter::htmlspecialchars($index)] = Fileter::htmlspecialchars($index);
            }
        }
        $tpl->SetTplParam('sourceCharsetList', self::getDict('sourceCharset'))
            ->SetTplParam('siteUrlTypeList', self::getDict('siteUrlType'))
            ->SetTplParam('htmlTagList', $htmlTag);
        return $tpl->Display('node_from');
    }
}