<?php
/**
 * 模块语言包
 * User: Administrator
 * Date: 2019/5/27
 * Time: 9:28
 */

return array(
    'App_Name' => array(
        'zh-cn' => '采集器',
        'en' => ''
    ),
    'Node_Empty' => array(
        'zh-cn' => '采集器节点的信息找不到，或者不存在',
        'en' => ''
    ),
    'Node_IdEmpty' => array(
        'zh-cn' => '采集器节点的Id找不到，或者不存在',
        'en' => ''
    ),
    'Node_WebSiteUrlConfigEmpty' => array(
        'zh-cn' => '采集器节点的网址采集的配置信息（webSiteUrlConfig）找不到，或者不存在',
        'en' => ''
    ),
    'Node_UrlPageEmpty' => array(
        'zh-cn' => '采集器节点的网址配置找不到，且不为空',
        'en' => ''
    ),
    'Node_SourceCharsetEmpty' => array(
        'zh-cn' => '采集器节点的页面编码找不到，且不为空',
        'en' => ''
    ),
    'fp_error' => array(
        'zh-cn' => '连接失败',
        'en' => ''
    ),
    '[content]' => array(
        'zh-cn' => '[内容]',
        'en' => ''
    ),
    'Collection_UrlEmpty' => array(
        'zh-cn' => '被采集的内容 URL 不能为空',
        'en' => ''
    ),
    'del_idList_empty' => array(
        'zh-cn' => '请选择要删除的内容',
        'en' => ''
    ),
    'import_idList_empty' => array(
        'zh-cn' => '请选择要导入的内容',
        'en' => ''
    ),
    'import_categoryId_empty' => array(
        'zh-cn' => '请选择要导入到哪个栏目',
        'en' => ''
    ),
    'import_nodeId_empty' => array(
        'zh-cn' => '要执行导入的节点 ID 不能为空',
        'en' => ''
    ),
    'import_status_await' => array(
        'zh-cn' => '执行导入的内容中有存在未采集的内容',
        'en' => ''
    ),
    'import_status_import' => array(
        'zh-cn' => '执行导入的内容中有存在已导入的内容',
        'en' => ''
    ),
    'import_field_content' => array(
        'zh-cn' => '执行导入到栏目的数据模型中没有存在 content 字段名',
        'en' => ''
    ),
);