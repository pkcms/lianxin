<?php


namespace PKApp\Collection;


use PKApp\Admin\Classes\AdminController;
use PKApp\Collection\Classes\Collection;
use PKApp\Collection\Classes\CollectionContentDataBase;
use PKApp\Collection\Classes\CollectionExtend;
use PKCore\Formats;
use PKCore\Request;
use PKCore\Tpl;

class AdminCollectionContent extends AdminController
{
    private $_nodeId;

    public function Main()
    {
        $entity = $this->_getNode();
        // 要采集的内容列表
        $count_content = $this->_getContentLists();
        $tpl = new Tpl();
        return $tpl->SetTplParam('count_total', $count_content)->SetTplParam('node', $entity)
            ->PhpDisplay('collection');
    }

    public function Collection()
    {
        $nodeEntity = $this->_getNode();
        $contentEntity = $this->_getContent();
        $col = new Collection();
        $collection_content = $col->GetContent(
            self::GetArrayByKey('url', $contentEntity),
            self::GetArrayByKey('sourceCharset', $nodeEntity),
            self::GetArrayByKey('rule', $nodeEntity));
        $data = array('status' => 'content', 'data' => json_encode($collection_content));
        $this->_setContent($data,
            self::GetArrayByKey('id', $contentEntity));
        \PKCore\msg(self::GetArrayByKey('title', $contentEntity));
    }

    private function _getNode()
    {
        Request::has('nodeId', 'get') ?: \PKCore\alert('Node_IdEmpty');
        $this->_nodeId = Formats::isNumeric(Request::get('nodeId'));
        return CollectionExtend::GetNode($this->_nodeId);
    }

    private function _getContentLists()
    {
        $db_content = new CollectionContentDataBase();
        return $db_content->Where(array('siteId' => $this->loginUser()->SiteId,
            'nodeId' => $this->_nodeId, 'status' => 'await'))->Count();
    }

    private function _getContent()
    {
        $db_content = new CollectionContentDataBase();
        return $db_content->Where(array('siteId' => $this->loginUser()->SiteId,
            'nodeId' => $this->_nodeId, 'status' => 'await'))->Select()->First();
    }

    private function _setContent($data, $id)
    {
        $db_content = new CollectionContentDataBase();
        return $db_content->Where(array('id' => Formats::isNumeric($id)))->Update($data)->Exec();
    }
}