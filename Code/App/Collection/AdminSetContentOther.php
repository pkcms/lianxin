<?php


namespace PKApp\Collection;


use PKApp\Admin\Classes\AdminController;
use PKApp\Collection\Classes\CollectionContentDataBase;
use PKCore\Request;
use PKCore\Route;

class AdminSetContentOther extends AdminController
{

    public function Main()
    {
        $action = Request::get('action');
        return !method_exists($this, $action) ?: $this->$action();
    }

    protected function deleteImportLog()
    {
        $post = Request::post();
        $content_idList = self::GetArrayByKey('idList', $post, 'import_idList_empty');
        $db_content = new CollectionContentDataBase();
        $db_content->Where(array('id' => $content_idList))->Update(array('status' => 'content'))->Exec();
        return Route\language('Data_Input_Success');
    }

    protected function del()
    {
        $post = Request::post();
        $content_idList = self::GetArrayByKey('set_id', $post, 'del_idList_empty');
        $db_content = new CollectionContentDataBase();
        $db_content->Where(array('id' => $content_idList))->Delete()->Exec();
        \PKCore\msg('Data_Input_Success');
    }

}