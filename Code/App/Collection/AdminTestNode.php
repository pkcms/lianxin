<?php
/**
 * 采集器的测试
 */

namespace PKApp\Collection;


use PKApp\Admin\Classes\AdminController;
use PKApp\Collection\Classes\Collection;
use PKApp\Collection\Classes\CollectionExtend;
use PKCore\Files;
use PKCore\Fileter;
use PKCore\Formats;
use PKCore\Request;
use PKCore\Tpl;

class AdminTestNode extends AdminController
{

    public function Main()
    {
        Request::has('nodeId', 'get') ?: \PKCore\alert('Node_IdEmpty');
        $id = Formats::isNumeric(Request::get('nodeId'));
        $entity = CollectionExtend::GetNode($id);
        $tpl = new Tpl();
        $col = new Collection();
        if (Request::has('url', 'get')) {
            $url = Request::get('url');
            $data = $col->GetContent($url,
                self::GetArrayByKey('sourceCharset', $entity, 'Node_SourceCharsetEmpty'), $entity['rule']);
            $tpl->SetTplParam('lists', $data);
            return $tpl->Display('test_from');
        } else {
            // 测试列表
            $actionName = 'GetSiteUrlTypeBy' . ucfirst($entity['siteUrlType']);
            $tpl->SetTplParam('nodeId', $id);
            if ($actionName == 'GetSiteUrlTypeByPages') {
                $testIndex = 1;
            } else {
                $webSiteUrlConfig = self::GetArrayByKey('webSiteUrlConfig', $entity);
                $pageSize_start = self::GetArrayByKey('pageSize_start', $webSiteUrlConfig);
                $testIndex = $pageSize_start;
            }
            !method_exists($col, $actionName) ?: $tpl->SetTplParam('urlLists', $col->$actionName($entity, $testIndex));
            return $tpl->Display('test_lists');
        }
    }

}