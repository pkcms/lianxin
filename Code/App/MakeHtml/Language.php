<?php
/**
 * 模块语言包
 * User: Administrator
 * Date: 2019/5/27
 * Time: 9:28
 */

return array(
    'Content_ModelError' => array(
        'zh-cn' => '正文内容在经过数据模型转换后变空，请检查一下模型数据',
        'en' => ''
    ),
);