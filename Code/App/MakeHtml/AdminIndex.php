<?php


namespace PKApp\MakeHtml;


use PKApp\Content\Classes\CategoryDataBase;
use PKApp\Content\Classes\ContentExtend;
use PKApp\Content\Classes\ContentsDataBase;
use PKApp\MakeHtml\Classes\ContentMakeHtml;
use PKApp\MakeHtml\Classes\CountContentData;
use PKCore\Converter;
use PKCore\Files;
use PKCore\Formats;
use PKCore\Log;
use PKCore\Request;
use PKCore\Statics;
use PKCore\Tpl;

class AdminIndex extends CountContentData
{
    public function Main()
    {
        $tpl = new Tpl();
        $tpl->SetTplParam('tableHtml', $this->_getTableList());
        return $tpl->Display('start');
    }

    private function _getTableList()
    {
        $tpl = new Tpl();
        return $tpl->SetTplParam('lists', ContentExtend::AdminGetCategoryList($this->loginUser()->SiteId, null, true))
            ->PhpDisplay('category_table');
    }

}
