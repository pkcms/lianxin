<?php

namespace PKApp\MakeHtml\Classes;

use PKApp\Admin\Classes\AdminController;
use PKApp\Content\Classes\CategoryDataBase;
use PKApp\Content\Classes\ContentDataBase;
use PKApp\Content\Classes\ContentExtend;
use PKApp\Content\Classes\ContentsDataBase;
use PKApp\Model\Classes\ModelExtend;
use PKCore\Converter;
use PKCore\Formats;
use PKCore\Request;

abstract class CountContentData extends AdminController
{

    protected $categoryListTotal, $contentTotal, $theContentTotal;
    // 当前栏目的总列表数
    protected $theListTotal;

    // 统计所有栏目的列表翻页及单页总数
    protected function countCategoryByAll()
    {
        $this->categoryListTotal = $this->contentTotal = 0;
        $db_category = new CategoryDataBase();
        $categoryListEntity = ContentExtend::GetCategoryList(
            $this->loginUser()->SiteId, null, false, true);
        if (Formats::isArray($categoryListEntity)) {
            $this->_foreachCountCategory($categoryListEntity);
        }
    }

    // 统计指定栏目的列表翻页及单页总数
    public function countCategoryByFirst($catId = null)
    {
        $this->categoryListTotal = $this->contentTotal = 0;
        $db_category = new CategoryDataBase();
        $categoryEntity = $db_category->GetCategoryDetail($catId);
        if (!empty($categoryEntity)) {
//            print_r($categoryEntity);
            $this->countCategoryContentData($categoryEntity);
        }
    }

    private function _foreachCountCategory($categoryListEntity)
    {
        foreach ($categoryListEntity as $item) {
            if (!empty($item['type'])) {
                $item['setting'] = Converter::Unserialize($item['setting']);
            }
            $this->countCategoryContentData($item);
        }
    }

    /**
     * 统计当前栏目的内容总数据及栏目页总数
     * @param $category
     */
    protected function countCategoryContentData($category)
    {
        static $db_category;
        switch ($category['type']) {
            case 2:
                $catId = !empty($category['arrchildid']) ? $category['arrchildid'] : $category['id'];
                $where = array('status' => array(0, 1), 'siteid' => $this->loginUser()->SiteId, 'catid in (' . $catId . ')');
                $db_content = new ContentsDataBase($category['modelid']);
                $this->theContentTotal = $db_content->GetCountContent($where);
                !empty($category['arrchildid']) ?: $this->contentTotal += $this->theContentTotal;
                !empty($db_category) ?: $db_category = new CategoryDataBase();
                $db_category->SetCategoryDetailById(array('total' => $this->theContentTotal), $category['id']);
                if (($category['status'] == 1) || (
                    ($category['status'] == 0) && ($category['parentId'] == 0)
                    )) {
                    $this->theListTotal = max(ceil($this->theContentTotal / $category['setting']['listDataSize']), 1);
                    $this->categoryListTotal += $this->theListTotal;
                }
                //                echo $category['name'] . ' ' . $this->theContentTotal . "\r\n";
                break;
            case 0;
            case 1:
                $this->categoryListTotal += 1;
                //                echo $category['name'] . ' ' . $this->categoryListTotal . ' ';
                break;
        }
    }
}
