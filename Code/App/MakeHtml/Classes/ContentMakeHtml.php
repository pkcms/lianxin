<?php

namespace PKApp\MakeHtml\Classes;

use PKApp\Admin\Classes\AdminExtend;
use PKApp\Content\Classes\CategoryDataBase;
use PKApp\Content\Classes\ContentDetail;
use PKApp\Content\Classes\ContentExtend;
use PKApp\Content\Classes\ContentPageDataBase;
use PKApp\Content\Classes\ContentsDataBase;
use PKApp\Content\Classes\ContentTemplateExists;
use PKApp\Content\Classes\ContentUrlFormat;
use PKApp\Model\Classes\ModelDatabase;
use PKCore\Converter;
use PKCore\Files;
use PKCore\Formats;
use PKCore\Request;
use PKCore\Route;
use PKCore\Tpl;

class ContentMakeHtml
{

    private $sitePath, $sitePathByUrl, $siteId, $siteDomain;
    private $mobilePath, $IsMobileStatics;
    private $themes;
    private $tpl, $tpl_m;

    function __construct($siteId)
    {
        $this->siteId = $siteId;
        $siteEntity = AdminExtend::GetSiteInfo($siteId);
        $this->sitePathByUrl = $sitePath = $siteEntity['Site_Path'];
        $this->sitePath = PATH_ROOT . (empty($sitePath) ? NULL : trim($sitePath, '/')) . DS;
        $this->siteDomain = $siteEntity['Site_Domain'];
        $setting = $siteEntity['setting'];
        try {
            if (empty($setting['Site_Themes'])) {
                throw new \Exception(Route\language('Site_ThemesEmpty', 'Admin'));
            }
        } catch (\Exception $exception) {
            \PKCore\handlerException($exception);
        }
        $this->IsMobileStatics = $setting['IsMobileStatics'];
        $this->mobilePath = PATH_ROOT . (empty($setting['MobileStaticsPath']) ? NULL : trim($setting['MobileStaticsPath'], '/')) . DS;
        $this->tpl = new Tpl();
        $this->tpl->SetTplThemes($setting['Site_Themes']);
        $this->tpl->SetTplDir('content');
        $this->setSiteTemplates($siteEntity);
    }

    protected function setSiteTemplates($siteEntity)
    {
        $siteEntity['Site_Id'] = $siteEntity['id'];
        unset($siteEntity['id']);
        $notSetTemplateTagKey = array('Site_statisticsCode', 'setting', 'id');
        if (Formats::isArray($siteEntity)) {
            foreach ($siteEntity as $key => $value) {
                if ($key == 'Site_Contact') {
                    $siteContact = $siteEntity[$key];
                    foreach ($siteContact as $index => $item) {
                        $new_index = 'siteContact_' . strtolower($index);
                        $this->tpl->SetTplParam($new_index,
                            (($index == 'qq') ? explode(',', $item) : $item));
                    }
                } elseif ($key == 'info') {
                    $siteInfo = $siteEntity[$key];
                    foreach ($siteInfo as $index => $item) {
                        $new_index = 'site_' . strtolower($index);
                        $this->tpl->SetTplParam($new_index,
                            (($index == 'qq') ? explode(',', $item) : $item));
                    }
                } else {
                    in_array($key, $notSetTemplateTagKey) ?: $this->tpl->SetTplParam(strtolower($key), $value);
                }
            }
        }
    }

    private function _createhtml($dir, $file, $data)
    {
        Files::putContents($dir, $file, $data);
    }

    function baiduMapMakeHtml()
    {
        $db_category = new CategoryDataBase();
        $categoryListEntity = $db_category->GetList($this->siteId, null, true);
        $html_category = array();
        $html_model = array();
        $oldModelId = array();
        if (Formats::isArray($categoryListEntity)) {
            $html_category = ContentExtend::TreeCategoryList($categoryListEntity, 0, $this->sitePathByUrl);
            $categoryListEntity = $this->_convert_arr_key($categoryListEntity, 'id');
        }
        $db_model = new ModelDatabase();
        $modelListEntity = $db_model->GetLists('content', array(), array('id', 'name'));
        !Formats::isArray($modelListEntity) ?: $modelListEntity = Converter::arrayColumn($modelListEntity, 'name', 'id');
        foreach ($modelListEntity as $modelId => $modelName) {
            $html_model[$modelId] = array(
                'name' => $modelName,
                'lists' => array()
            );
            $db_content = new ContentsDataBase($modelId);
            $contentListEntity = $db_content->GetListByNotData(array('siteid' => $this->siteId, 'status' => 1),
                array('id', 'catid', 'title', 'url'));
            if (Formats::isArray($contentListEntity)) {
                foreach ($contentListEntity as $content) {
                    $categoryEntity = $categoryListEntity[$content['catid']];
                    $templateConfig = $categoryEntity['template'];
                    $html_model[$modelId]['lists'][] = array(
                        'title' => $content['title'],
                        'url' => ContentUrlFormat::Content($categoryEntity, $templateConfig, $content, $this->sitePathByUrl),
                    );
                }
            }
        }
        $this->tpl->SetTplParam('categoryList', $html_category)
            ->SetTplParam('modelList', $html_model);
        Files::putContents($this->sitePath, 'sitemap.htm',
            $this->tpl->PhpDisplay('site_map'));
    }

    private function _convert_arr_key($arr, $key_name)
    {
        $arr2 = array();
        foreach ($arr as $key => $val) {
            $val['template'] = Converter::Unserialize($val['template']);
            $arr2[$val[$key_name]] = $val;
        }
        return $arr2;
    }

    function contentHomeMakeHtml()
    {
        $array = array('id' => $this->siteId);
        $this->_createhtml(
            $this->sitePath, 'index.htm',
            $this->tpl->Display('index')
        );
        if ($this->IsMobileStatics) {
            $this->_createhtml(
                $this->mobilePath, 'index.htm',
                $this->tpl->Display('index_m')
            );
        }
    }

    function contentCategoryMakeHtml($category, $pageIndex)
    {
        $template = is_array($category['template']) ? $category['template'] : Converter::Unserialize($category['template']);
        if (!$template['categoryHtml']) {
            return false;
        }
        is_array($category['seo']) ?: $category['seo'] = Converter::Unserialize($category['seo']);
        $this->_categroyParent($category);
        Request::get('id', $category['id']);
        Request::get('pageIndex', $pageIndex);
        $tplFile = $template['categoryTpl'];
        $url_tplParam = ContentUrlFormat::Category($category, $template);
        $category['url'] = $url_tplParam;
        $this->tpl->SetTplParamList($category);
        $url = ContentUrlFormat::Category($category, $template, null, true);
        $path = $this->pathResolve($url, $pageIndex);
        $array = array('id' => $category['id'], 'pageIndex' => $pageIndex, 'siteId' => $this->siteId);
        $html = $this->tpl->Display(substr($tplFile, 0, strlen($tplFile) - 4));
        if ($this->IsMobileStatics && !empty($this->mobilePath)) {
            $html_m = $this->tpl->Display(substr($tplFile, 0, strlen($tplFile) - 4) . '_m');
        }
        if ($pageIndex == 1) {
            // 栏目为第一页时
            $path_once = $this->pathResolve($url, $pageIndex, false);
            $fileName_once = preg_replace('(-\[page\]|_\[page\]|/\[page\]|\[page\])', '', $path_once['file']);
            $dir_once = preg_replace('(-\[page\]|_\[page\]|/\[page\]|\[page\])', '', $path_once['dir']);
            $this->_createhtml($this->sitePath . $dir_once, $fileName_once, $html);
            if ($this->IsMobileStatics && !empty($this->mobilePath) && isset($html_m)) {
                $this->_createhtml($this->mobilePath . $dir_once, $fileName_once, $html_m);
            }
        }
        $this->_createhtml($this->sitePath . $path['dir'], $path['file'], $html);
        if ($this->IsMobileStatics && isset($html_m)) {
            $dirPath = $this->mobilePath . $path['dir'];
            $this->_createhtml($dirPath, $path['file'], $html_m);
        }
    }

    function contentPageMakeHtml($category)
    {
        $template = is_array($category['template']) ? $category['template'] : Converter::Unserialize($category['template']);
        if (!$template['pageHtml']) {
            return false;
        }
        $this->_categroyParent($category);
        Request::get('id', $category['id']);
        $tplFile = $template['pageTpl'];
        // pageEntity
        $db_page = new ContentPageDataBase();
        $pageEntity = $db_page->GetPageDetail($category['id']);
        is_array($pageEntity) ?: $this->tpl->Notice(Route\language('Page_Empty'));
        // do create
        $url = ContentUrlFormat::Category($category, $template);
        $category['url'] = $url;
        $this->tpl->SetTplParam('category', $category)->SetTplParamList($pageEntity);
        $path = $this->pathResolve($url);
        $this->_createhtml($this->sitePath . $path['dir'], $path['file'],
            $this->tpl->Display(substr($tplFile, 0, strlen($tplFile) - 4)));
        if ($this->IsMobileStatics) {
            $this->_createhtml($this->mobilePath . $path['dir'], $path['file'],
                $this->tpl->Display(substr($tplFile, 0, strlen($tplFile) - 4) . '_m'));
        }
    }

    private function _categroyParent($categoryEntity)
    {
        $this->tpl->SetTplParam('categoryParent',
            ContentTemplateExists::GetParentIdList($categoryEntity['siteId'],
                $categoryEntity['arrparentid'], $this->sitePathByUrl));
    }

    function contentContentMakeHtml($category, $content = NULL)
    {
        Request::get('id', $content['id']);
        Request::get('siteId', $content['siteId']);
        $template = is_array($category['template']) ? $category['template'] : ($category['template'] = Converter::Unserialize($category['template']));
        if (!$template['contentHtml']) {
            return false;
        }
        $url = ContentUrlFormat::Content($category, $template, $content);
        $contentDetail = new ContentDetail(
            $this->sitePathByUrl,
            $category,
            $this->tpl
        );
        list($categoryEntity, $templateSetting) = $contentDetail->Category();
        $content = $contentDetail->GetContent($content);
        if (empty($content)) {
            \PKCore\alert('Content_ModelError');
        }
        $contentDetail->GetUpOrNext($category, $template, $content);
        //模板
        if (empty($content['template'])) {
            $tplFile = $template['contentTpl'];
        } else {
            $tplFile = $content['template'];
        }
        // create
        $path = $this->pathResolve($url);
        $this->_createhtml($this->sitePath . $path['dir'], $path['file'],
            $this->tpl->Display(substr($tplFile, 0, strlen($tplFile) - 4)));
        if ($this->IsMobileStatics) {
            $contentDetail_m = new ContentDetail(
                $this->mobilePath,
                $category,
                $this->tpl
            );
            $contentDetail_m->GetContent($content);
            $this->_createhtml($this->mobilePath . $path['dir'], $path['file'],
                $this->tpl->Display(substr($tplFile, 0, strlen($tplFile) - 4) . '_m'));
        }
    }

    function pathResolve($path, $pageIndex = null, $isReplacePage = true)
    {
        $path = ltrim($path, '/');
        $result = array('dir' => '', 'file' => '');
        $files_arr = explode('/', $path);
        if ($isReplacePage && !is_null($pageIndex)) {
            $files_arr = str_replace('[page]', $pageIndex, $files_arr);
        }
        while (count($files_arr) > 1) {
            $result['dir'] .= array_shift($files_arr) . DS;
        }
        $result['file'] = array_shift($files_arr);
        return $result;
    }

}
