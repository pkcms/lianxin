<?php


namespace PKApp\MakeHtml;

use PKApp\MakeHtml\Classes\CountContentData;
use PKCore\Request;
use PKCore\Statics;

class AdminCount extends CountContentData
{

    /**
     * 统计更新的数据
     */
    public function Main()
    {
        $result = array();
        switch (Request::post('type')) {
            case 'baiDuMaps':
            case 'home':
                $result['countTotal'] = 1;
                break;
            case 'select':
                $selectList = Request::post('selectData');
                $result['countContent'] = 0;
                $result['countCategory'] = 0;
                $result['countTotal'] = 0;
                foreach ($selectList as $item) {
                    $this->countCategoryByFirst($item);
                    $result['countContent'] += $this->contentTotal;
                    $result['countCategory'] += $this->categoryListTotal;
                    $result['countTotal'] += ($this->contentTotal + $this->categoryListTotal);
                }
                break;
            case 'all':
                $this->countCategoryByAll();
                $result['countContent'] = $this->contentTotal;
                $result['countCategory'] = $this->categoryListTotal;
                $result['countTotal'] = $this->contentTotal + $this->categoryListTotal;
                break;
        }
        \PKCore\msg($result);
    }
}