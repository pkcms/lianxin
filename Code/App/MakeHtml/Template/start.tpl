<{admin_breadcrumb('发布静态')}>

<!-- Title -->
<div class="page-header">
    <div class="page-title">
        <h3>
            发布内容/生成静态
        </h3>
    </div>
</div>
<!-- /Title -->
<!-- Row -->
<div class="row">
    <div class="col-md-6 col-sm-12 col-xs-12">
        <div class="widget box">
            <div class="widget-header">
                <h4>
                    <i class="icon-check"></i>
                    选择性发布中的栏目选择
                </h4>
            </div>
            <div class="widget-content">
                <table id="selectCategory"
                       class="table table-striped table-condensed table-hover table-bordered table-responsive">
                    <tbody>
                    <{$tableHtml}>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="col-md-6 col-sm-12 col-xs-12">
        <div class="widget box">
            <div class="widget-header">
                <h4>
                    <i class="icon-wrench"></i>
                    操作选项
                </h4>
            </div>
            <div class="widget-content">
                <button class="MakeHtmlButton btn btn-primary btn-block" data-type="baiDuMaps">
                    生成百度站点地图
                </button>
                <button class="MakeHtmlButton btn btn-primary btn-block" data-type="home">
                    生成首页
                </button>
                <button class="MakeHtmlButton btn btn-primary btn-block" data-type="select">
                    生成所选定的栏目（及内容）
                </button>
                <button class="MakeHtmlButton btn btn-primary btn-block" data-type="all">
                    生成所有的栏目（及内容）
                </button>
            </div>
        </div>
        <div class="widget box">
            <div class="widget-header">
                <h4>
                    <i class="icon-cogs"></i>
                    生成的进度
                </h4>
            </div>
            <div class="widget-content">
                <div id="step_count" style="display: none">
                    <div id="step_count_tips" class="alert alert-warning f20">
                        正在统计要更新的数据
                    </div>
                    <div id="step_count_res" class="alert alert-success f20" style="display: none">
                        总量：<strong id="countTotal">0</strong>，&nbsp;
                        栏目量：<strong id="countCategory">0</strong>，&nbsp;
                        内容量：<strong id="countContent">0</strong>
                    </div>
                </div>
                <div id="step_do" style="display: none;">
                    <div class="m20">
                        <div class="progress">
                            <div class="progress-bar" role="progressbar" aria-valuenow="60" aria-valuemin="0"
                                 aria-valuemax="100" style="width: 0%;">
                                0%
                            </div>
                        </div>
                    </div>
                    <div class="m20 red f20">
                        正在更新栏目：<b><span id="actionCategory"></span></b>，用时:<b><span id="actionTime"></span></b>秒
                    </div>
                </div>
            </div>
        </div>
        <div class="widget box">
            <div class="widget-header">
                <h4>
                    <i class="icon-info-sign"></i>
                    生成的提示
                </h4>
            </div>
            <div class="widget-content">
                <div  id="show_tips" class="alert"></div>
            </div>
        </div>
    </div>
</div>
<!-- /Row -->

<script type="text/javascript">
    var params;

    function setParams() {
        params = {
            total: 0, categoryIndex: 1, nowCategoryPage: 0, nowContentPage: 0,
            finishTotal: 0, finishCategory: 0, finishContent: 0, type: '', selectData: []
        };
    }

    function doMakeHtml() {
        $.post('/index.php/makeHtml/AdminDoMakeHtml', params, function (res) {
            if ((typeof res == "object") && res.hasOwnProperty('Result')) {
                res = $.parseJSON(res.Result);
                console.log('do res:', res);
                if (typeof res == "object") {
                    if (res.hasOwnProperty('name')) {
                        $('#actionCategory').text(res.name);
                    }
                    if (res.hasOwnProperty('htmlTime')) {
                        $('#actionTime').text(res.htmlTime);
                    }
                    if (res.hasOwnProperty('finishCategory')) {
                        params.finishCategory = res.finishCategory;
                    }
                    if (res.hasOwnProperty('finishContent')) {
                        params.finishContent = res.finishContent;
                    }
                    if (res.hasOwnProperty('finishTotal')) {
                        params.finishTotal = res.finishTotal;
                    }
                    if (res.hasOwnProperty('categoryIndex')) {
                        params.categoryIndex = res.categoryIndex;
                    }
                    if (res.hasOwnProperty('nowCategoryPage')) {
                        params.nowCategoryPage = res.nowCategoryPage;
                    }
                    if (res.hasOwnProperty('nowContentPage')) {
                        params.nowContentPage = res.nowContentPage;
                    }
                    if (res.hasOwnProperty('schedule')) {
                        $('.progress-bar').text(res.schedule + '%').width(res.schedule + '%');
                        if (res.schedule === 100) {
                            $('#step_do').hide();
                            show_stepDone();
                        } else {
                            doMakeHtml();
                        }
                    }
                }
            }
        });
    }

    function show_stepDone() {
        $('#show_tips').removeClass('alert-warning').addClass('alert-success').text('全部更新完成');
    }

    function hide_stepDone() {
        $('#show_tips').removeClass('alert-warning').removeClass('alert-success').text('');
    }

    $('.MakeHtmlButton').die().on('click', function () {
        hide_stepDone();
        setParams();
        $('#step_count, #step_count_tips').show();
        $('#step_count_res').hide();
        params.type = $(this).data('type');
        if (params.type === 'select') {
            var selectLen = $('#selectCategory input:checked').length;
            if (selectLen <= 0) {
                $('#step_count_tips').hide();
                $('#show_tips').text('请选择要生成的栏目！')
                    .removeClass('alert-success').addClass('alert-warning');
                return false;
            } else {
                for (var i = 0; i < selectLen; i++) {
                    var elem = $('#selectCategory input:checked:eq(' + i + ')');
                    params.selectData.push($(elem).val());
                }
            }
        }
        console.log('type:', params.type);
        $.post('/index.php/makeHtml/AdminCount', params, function (res) {
            if ((typeof res == "object") && res.hasOwnProperty('Result')) {
                res = $.parseJSON(res.Result);
                console.log('count res:', res);
                if (typeof res == "object") {
                    if (res.hasOwnProperty('countCategory')) {
                        $('#countCategory').html(res.countCategory);
                    } else {
                        $('#countCategory').html(0);
                    }
                    if (res.hasOwnProperty('countContent')) {
                        $('#countContent').html(res.countContent);
                    } else {
                        $('#countContent').html(0);
                    }
                    if (res.hasOwnProperty('countTotal')) {
                        $('#countTotal').html(res.countTotal);
                        params.total = res.countTotal;
                        if (res.countTotal === 0) {
                            $('#step_do').hide();
                            show_stepDone();
                            return false;
                        }
                        console.log('params:', params);
                        doMakeHtml();
                    }
                    $('#step_count_res').show();
                }
            }
            $('#step_count_tips').hide();
            $('#step_do').show();
        });
    });
</script>