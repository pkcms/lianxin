<?php
function htmlForeachTable($lists, $level)
{
    if (is_array($lists)) {
        foreach ($lists as $item) {
            ?>
            <tr>
                <td>
                    <input type="checkbox" value="<?php echo $item['id'] ?>" name="set_id"/>
                </td>
                <td><?php echo $item['id'] ?></td>
                <td>
                    <?php for ($i = 0; $i < $level; $i++) {
                        echo '&nbsp&nbsp;&nbsp&nbsp;';
                    } ?>
                    <?php echo $item['name'] ?>
                </td>
            </tr>
            <?php if (is_array($item['child']) && count($item['child']) > 0) {
                htmlForeachTable($item['child'], $level + 1);
            }
        }
    }
}

if (isset($lists)) {
    htmlForeachTable($lists, 0);
}
?>