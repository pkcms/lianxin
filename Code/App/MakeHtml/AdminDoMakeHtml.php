<?php


namespace PKApp\MakeHtml;


use PKApp\Content\Classes\CategoryDataBase;
use PKApp\Content\Classes\ContentsDataBase;
use PKApp\MakeHtml\Classes\ContentMakeHtml;
use PKApp\MakeHtml\Classes\CountContentData;
use PKCore\Converter;
use PKCore\Files;
use PKCore\Formats;
use PKCore\Request;

class AdminDoMakeHtml extends CountContentData
{
    private $vars;

    /**
     * 正在更新
     */
    public function Main()
    {
        $this->vars = array();
        $path = PATH_TMP . 'createLogs' . DS;
        $fileName = date('Ymd');
        $log_arr = \PKCore\Route\isLoadingFile($path . $fileName, true);
        if (empty($log_arr)) {
            $log_arr = array();
        }
        $this->_makeHtmlGet();
        switch (Request::post('type')) {
            case 'baiDuMaps':
                $this->_makeBaiDuMapHtml();
                break;
            case 'home':
                $this->_makeHomeHtml();
                break;
            case 'select':
                $this->_selectMakeHtml();
                break;
            case 'all':
                $this->_allMakeHtml();
                break;
        }
        $this->vars['htmlTime'] = \PKCore\executeTime(SYS_TIME);
        //更新进度
        $this->vars['schedule'] = $this->vars['total'] > 0 ?
            (round($this->vars['finishTotal'] / $this->vars['total'], 5) * 100) : 100;
//        $log_arr[] = $this->vars;
//        Files::putContents($path, $fileName . '.php', Converter::arrayToSaveString($log_arr));
        \PKCore\msg($this->vars);
    }

    private function _makeHtmlGet()
    {
        //总数
        $this->vars['total'] = Formats::isNumeric(Request::post('total'));
        $this->vars['categoryIndex'] = Formats::isNumeric(Request::post('categoryIndex'));
        $this->vars['nowCategoryPage'] = Formats::isNumeric(Request::post('nowCategoryPage'));
        $this->vars['nowContentPage'] = Formats::isNumeric(Request::post('nowContentPage'));
        $this->vars['finishTotal'] = Formats::isNumeric(Request::post('finishTotal'));
        $this->vars['finishCategory'] = Formats::isNumeric(Request::post('finishCategory'));
        $this->vars['finishContent'] = Formats::isNumeric(Request::post('finishContent'));
    }

    private function _contentMakeHtml()
    {
        static $initClass;
        !empty($initClass) ?: $initClass = new ContentMakeHtml($this->loginUser()->SiteId);
        return $initClass;
    }

    private function _makeBaiDuMapHtml()
    {
        $this->vars['name'] = 'BaiDuMaps';
        $this->_contentMakeHtml()->baiduMapMakeHtml();
        $this->vars['finishTotal'] += 1;
    }

    //首页静态
    private function _makeHomeHtml()
    {
        $this->vars['name'] = 'home';
        $this->_contentMakeHtml()->contentHomeMakeHtml();
        $this->vars['finishTotal'] += 1;
    }

    private function _selectMakeHtml()
    {
        $selectList = Request::post('selectData');
        $categoryId = $selectList[$this->vars['categoryIndex'] - 1];
        $db_category = new CategoryDataBase();
        $category = $db_category->GetCategoryDetail($categoryId);
        if (is_array($category)) {
            $this->_makeCategoryGroup($category);
        }
    }

    //全部更新
    private function _allMakeHtml()
    {
        $db_category = new CategoryDataBase();
        $category = $db_category->GetCategoryByMakeHtml(
            $this->loginUser()->SiteId, $this->vars['categoryIndex']);
        if (is_array($category)) {
            $category['setting'] = Converter::Unserialize($category['setting']);
            $this->_makeCategoryGroup($category);
        }
    }

    //栏目更新分组执行
    private function _makeCategoryGroup($category)
    {
        $this->vars['name'] = $category['name'];
        switch ($category['type']) {
            case 2:
                $this->countCategoryContentData($category);
                if (!empty($category['arrchildid']) && ($category['status'] == 1)) {
                    // 父级栏目
                    $this->_makeCategoryHtml($category);
                    if (($this->vars['nowCategoryPage'] == $this->theListTotal)) {
                        $this->vars['categoryIndex'] += 1;
                        $this->vars['nowCategoryPage'] = 0;
                    }
                } else {
                    // 子级栏目
                    // 有一级栏目（无子栏目）的隐蔽状态
                    if (($this->vars['nowCategoryPage'] < $this->theListTotal) && (($category['status'] == 1) || (
                                ($category['status'] == 0) && ($category['parentId'] == 0)
                            ))) {
                        $this->_makeCategoryHtml($category);
                    }
                    if ($this->vars['nowContentPage'] < $this->theContentTotal) {
                        $this->_makeContentHtml($category);
                    }
                    if (($this->vars['nowCategoryPage'] == $this->theListTotal)
                        && ($this->vars['nowContentPage'] == $this->theContentTotal)) {
                        $this->vars['categoryIndex'] += 1;
                        $this->vars['nowCategoryPage'] = 0;
                        $this->vars['nowContentPage'] = 0;
                    }
                }
                break;
            case 1:
                $this->_makePageHtml($category);
                $this->vars['categoryIndex'] += 1;
                $this->vars['finishCategory'] += 1;
                $this->vars['finishTotal'] += 1;
                break;
            case 0;
                $this->vars['finishCategory'] += 1;
                $this->vars['categoryIndex'] += 1;
                $this->vars['finishTotal'] += 1;
                break;
        }
    }

    //单页面静态
    private function _makePageHtml($category)
    {
        $this->_contentMakeHtml()->contentPageMakeHtml($category);
    }

    //列表页更新
    private function _makeCategoryHtml($category)
    {
        $line = 1;
        $pageIndex = $this->vars['nowCategoryPage'] += $line;
        $this->_contentMakeHtml()->contentCategoryMakeHtml($category, $pageIndex);
        $this->vars['finishCategory'] += $line;
        $this->vars['finishTotal'] += $line;
    }

    //内容页更新
    private function _makeContentHtml($category)
    {
        $display = 1;
        $pageIndex = $this->vars['nowContentPage'] += $display;
        $where = array(
            'catid' => $category['id'], 'status' => 1,
            'siteid' => $category['siteId'] ? $category['siteId'] : $this->loginUser()->SiteId
        );
        $db_content = new ContentsDataBase($category['modelid']);
        $db_res = $db_content->GetList($where, array('id'), $pageIndex, $display);
        $contentCount = $db_res['count'];
        $contentList = $db_res['lists'];
        $theListCount = count($contentList);
        foreach ($contentList as $value) {
            $this->vars['contentId']= $value['id'];
            $contentEntity = $db_content->GetContentDetail($value['id']);
            $this->_contentMakeHtml()->contentContentMakeHtml($category, $contentEntity);
        }
        $this->vars['finishContent'] += $theListCount;
        $this->vars['finishTotal'] += $theListCount;
    }
}