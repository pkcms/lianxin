<?php


namespace PKApp\Poster;


use PKApp\Admin\Classes\AdminController;
use PKApp\Poster\Classes\PosterDataBase;
use PKCore\Tpl;

class AdminGetSpaceList extends AdminController
{

    public function Main()
    {
        $tpl = new Tpl();
        return $tpl->SetTplParam('adminGroup',$this->loginUser()->GroupId)
            ->SetTplParam('lists', PosterDataBase::GetSpaceList())
            ->Display('space_init');
    }
}