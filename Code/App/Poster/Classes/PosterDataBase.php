<?php


namespace PKApp\Poster\Classes;


use PKCore\DataBase;

class PosterDataBase
{

    private static $_table = 'poster';

    public static function GetSpaceList()
    {
        $db = new DataBase(self::$_table);
        return $db->Select(array('id', 'name'))->ToList();
    }

    public static function GetSpaceDetail($id)
    {
        $db = new DataBase(self::$_table);
        return $db->Where(array('id' => $id))->Select(array('id', 'name'))->First();
    }

    public static function GetPosterDetail($id)
    {
        $db = new DataBase(self::$_table);
        return $db->Where(array('id' => $id))->Select()->First();
    }

    public static function AddSpace($data)
    {
        $db = new DataBase(self::$_table);
        return $db->Insert($data)->Exec();
    }

    public static function UpdateSpace($data, $id)
    {
        $db = new DataBase(self::$_table);
        return $db->Where(array('id' => $id))->Update($data)->Exec();
    }

    public static function DelSpace($id)
    {
        $db = new DataBase(self::$_table);
        return $db->Where(array('id' => $id))->Delete()->Exec();
    }

}