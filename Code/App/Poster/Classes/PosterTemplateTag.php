<?php

namespace PKApp\Poster\Classes;

use PKCore\Converter;
use PKCore\Formats;
use PKCore\Request;
use PKCore\Tpl;

class PosterTemplateTag
{

    function Main($param)
    {
        $siteId = max((is_array($param) && array_key_exists('siteId', $param) ? $param['siteId'] : 0), 1);
        $id = Formats::isNumeric($param['id']);
        if (!empty($id)) {
            $entity = PosterDataBase::GetPosterDetail($id);
            if (!empty($entity) && is_array($entity) && array_key_exists('adsList', $entity)) {
                $entity['adsList'] = Converter::Unserialize($entity['adsList']);
                return array('adsList' => $entity['adsList'][$siteId]);
            }
        }
        return array();
    }

}

?>
