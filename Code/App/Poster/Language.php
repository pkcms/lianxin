<?php
/**
 * 模块语言包
 * User: Administrator
 * Date: 2019/5/27
 * Time: 9:28
 */

return array(
    'Space_NotExists' => array(
        'zh-cn' => '广告位不存在',
        'en' => ''
    ),
    'Space_NameEmpty' => array(
        'zh-cn' => '广告位名称不能为空',
        'en' => ''
    ),
);