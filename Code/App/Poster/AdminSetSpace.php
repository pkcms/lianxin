<?php


namespace PKApp\Poster;


use PKApp\Admin\Classes\AdminController;
use PKApp\Poster\Classes\PosterDataBase;
use PKCore\Formats;
use PKCore\Request;
use PKCore\Tpl;

class AdminSetSpace extends AdminController
{

    public function Main()
    {
        $action = Request::get('action');
        if (method_exists($this,$action)) {
            $this->$action();
        }
    }

    public function GetDetail()
    {
        $id = Formats::isNumeric(Request::get('id'));
        $tpl = new Tpl();
        if (!empty($id)) {
            $entity = PosterDataBase::GetSpaceDetail($id);
            empty($entity) || !is_array($entity) ?: $tpl->SetTplParamList($entity);
        }
        return $tpl->Display('space_form');
    }

    public function SetDetail()
    {
        $this->checkPostFieldIsEmpty('name', 'Space_NameEmpty');
        $id = Request::post('id');
        $post = Request::post();
        if (empty($id)) {
            unset($post['id']);
            PosterDataBase::AddSpace($post);
        } else {
            PosterDataBase::UpdateSpace($post, $id);
        }
        \PKCore\msg('Data_Input_Success');
    }

    protected function del()
    {
        $set_id = Request::post('set_id');
        if (!empty($set_id)) {
            PosterDataBase::DelSpace($set_id);
        }
        \PKCore\msg('Data_Input_Success');
    }
}