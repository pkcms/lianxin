<?php


namespace PKApp\Poster;


use PKApp\Admin\Classes\AdminController;
use PKApp\Poster\Classes\PosterDataBase;
use PKCore\Converter;
use PKCore\Formats;
use PKCore\Request;
use PKCore\Tpl;

class AdminSetPoster extends AdminController
{

    public function Main()
    {
    }

    public function GetPoster()
    {
        $id = Formats::isNumeric(Request::get('id'));
        $tpl = new Tpl();
        $tpl->SetTplParam('id', $id);
        if (!empty($id)) {
            $entity = PosterDataBase::GetPosterDetail($id);
            if (!empty($entity) && is_array($entity) && array_key_exists('adsList', $entity)) {
                $adsList = Converter::Unserialize($entity['adsList']);
                $entity['adsList'] = $adsList[$this->loginUser()->SiteId];
                $tpl->SetTplParamList($entity);
            }
        }
        return $tpl->Display('poster_form');
    }

    public function SetPoster()
    {
        $id = Request::post('id');
        $post = Request::post();
        if (is_array($post) && array_key_exists('data', $post)) {
            $data = $post['data']['image'];
            $entity = PosterDataBase::GetPosterDetail($id);
            if (!empty($entity) && is_array($entity) && array_key_exists('adsList', $entity)) {
                if (empty($entity['adsList'])) {
                    $adsList = array($this->loginUser()->SiteId => $data);
                    $post['adsList'] = serialize($adsList);
                } else {
                    $adsList = Converter::Unserialize($entity['adsList']);
                    $adsList[$this->loginUser()->SiteId] = $data;
                    $post['adsList'] = serialize($adsList);
                }
            } else {
                \PKCore\alert('Space_NotExists');
            }
            unset($post['data']);
        }
        if (!empty($id)) {
            PosterDataBase::UpdateSpace($post, $id);
        }
        \PKCore\msg('Data_Input_Success');
    }

}