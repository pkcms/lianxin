<{admin_breadcrumb('广告管理')}>

<!-- Title -->
<div class="page-header">
    <div class="page-title">
        <h3>
            广告 版位编辑
        </h3>
    </div>
</div>
<!-- /Title -->
<!-- Row -->
<div class="row">
    <div class="col-md-12">
        <div class="widget box">
            <div class="widget-header">
                <h4>基本填写</h4>
            </div>
            <div class="widget-content">
                <form id="SpaceForm" class="form-horizontal row-border" action="poster/adminSetSpace/SetDetail">
                    <div class="form-group">
                        <label class="col-md-2 control-label">
                            版位名称:
                        </label>
                        <div class="col-md-10">
                            <input type="text" name="name" value="<{$name}>" class="form-control" />
                        </div>
                    </div>
                    <div class="form-actions">
                        <input type="hidden" name="id"  value="<{$id}>" />
                        <button type="button" data-form="SpaceForm" class="btn btn-primary pull-left">
                            提交保存
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- /Row -->
