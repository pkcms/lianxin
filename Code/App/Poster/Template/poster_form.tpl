<{admin_breadcrumb('广告管理')}>

<!-- Title -->
<div class="page-header">
    <div class="page-title">
        <h3>
            广告 版位 所属内容编辑
        </h3>
    </div>
</div>
<!-- /Title -->
<!-- Row -->
<div class="row">
    <div class="col-md-12">
        <div class="widget box">
            <div class="widget-header">
                <h4>基本填写</h4>
            </div>
            <div class="widget-content">
                <form id="PosterForm" class="form-horizontal row-border" action="poster/adminSetPoster/SetPoster">
                    <div class="form-group">
                        <label class="col-md-2 control-label">
                            广告版位名:
                        </label>
                        <div class="col-md-10">
                            <input type="text" name="name" value="<{$name}>" class="form-control" disabled="disabled" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label">
                            内容列表:
                        </label>
                        <div class="col-md-10">
                            <{UploadFileListEachByImage('', 'adsList', 'data[image]', $adsList)}>
                            <{UploadFileListBtn('adsList','','data[image]')}>
                        </div>
                    </div>
                    <div class="form-actions">
                        <input type="hidden" name="id" value="<{$id}>"/>
                        <button type="button" data-form="PosterForm" class="btn btn-primary pull-left">
                            提交保存
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- /Row -->
