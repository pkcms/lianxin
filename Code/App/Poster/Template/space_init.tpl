<{admin_breadcrumb('广告管理')}>

<!-- Title -->
<div class="page-header">
    <div class="page-title">
        <h3>
            广告 版位管理
        </h3>
    </div>
</div>
<!-- /Title -->
<!-- Row -->
<div class="row">
    <div class="col-md-12">
        <div class="widget">
            <div class="widget-content">
                <div class="btn-toolbar btn-toolbar-demo">
                    <a class="btn btn-success" data-ajax="true" data-ajax-begin="beginPaging" data-ajax-failure="failurePaging"
                       data-ajax-mode="replace" data-ajax-success="successPaging" data-ajax-update="#main"
                       href="/index.php/poster/adminSetSpace/GetDetail">
                        <i class="icon-plus-sign"></i>&nbsp;添加广告位</a>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-12">
        <div class="widget box">
            <div class="widget-header">
                <h4>
                    <i class="icon-reorder"></i>
                    列表
                </h4>
            </div>
            <div class="widget-content">
                <table class="table table-bordered table-striped table-hover mb-0">
                    <thead class="thead-secondary">
                    <tr>
                        <th width="30">选</th>
                        <th width="200">操作</th>
                        <th width="30">ID</th>
                        <th>广告版位名</th>
                    </tr>
                    </thead>
                    <tbody>
                    <{loop $lists $item}>
                        <tr>
                            <td><input type="radio" value="<{$item[id]}>" name="set_id" /></td>
                            <td>
                                <a data-ajax="true" data-ajax-begin="beginPaging" data-ajax-failure="failurePaging"
                                   data-ajax-mode="replace" data-ajax-success="successPaging" data-ajax-update="#main"
                                   href="/index.php/poster/adminSetSpace/GetDetail?id=<{$item[id]}>">
                                    <i class="icon-edit"></i>编辑牌位
                                </a>&nbsp;|&nbsp;
                                <a data-ajax="true" data-ajax-begin="beginPaging" data-ajax-failure="failurePaging"
                                   data-ajax-mode="replace" data-ajax-success="successPaging" data-ajax-update="#main"
                                   href="/index.php/poster/adminSetPoster/GetPoster?id=<{$item[id]}>">
                                    <i class="icon-list"></i>其下内容列表
                                </a>
                            </td>
                            <td><{$item[id]}></td>
                            <td><{$item[name]}></td>
                        </tr>
                        <{/loop}>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<!-- /Row -->
