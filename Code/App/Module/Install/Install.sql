DELETE FROM [pk_menu] WHERE [id] IN (25,28,31);
DELETE FROM [pk_menu] WHERE [name] IN ('在线留言');
UPDATE [pk_menu] SET [name] = '发布内容' WHERE [id] = 26;
INSERT INTO [pk_menu] ([name],[parentid],[app],[c],[type],[orderIndex]) VALUES ('在线客服留言',13,'LianChat','Admin','system',0);
INSERT INTO [pk_menu] ([name],[parentid],[app],[c],[type],[orderIndex]) VALUES ('导入数据',21,'content','AdminImport','system',0);
CREATE TABLE [pk_lian_chat] (
[id] INTEGER  NOT NULL PRIMARY KEY AUTOINCREMENT,
[username] TEXT  NULL,
[phone] TEXT  NULL,
[content] TEXT NULL,
[from_domain] TEXT  NULL,
[page_title] TEXT  NULL,
[page_url] TEXT  NULL,
[ip] TEXT  NULL,
[ipArea] TEXT NULL,
[add_time] INTEGER DEFAULT '0' NULL,
[status] INTEGER DEFAULT '0' NULL
);