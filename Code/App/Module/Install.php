<?php


namespace PKApp\Module;


use PKCore\DataBase;
use PKCore\Files;
use PKCore\Tpl;
use PKCore\Route;

class Install
{

    public function Main()
    {
        if ($this->_isInstall()) {
            $this->_del();
            $this->_sql();
            $tpl = new Tpl();
            $tpl->Notice(Route\language('Install_done'));
        }
    }

    private function _sql()
    {
        $path = PATH_APP . 'Module' . DS . 'Install' . DS.'install.sql';
        if (!file_exists($path)) {
            return false;
        }
        $sql = file_get_contents($path);
        $sqlLists = explode(';', $sql);
        $db = new DataBase();
        foreach ($sqlLists as $sqlStr) {
            if (!empty($sqlStr)) {
                $db->Sql($sqlStr);
            }
        }
        $db->BatchRun();
    }

    private function _del()
    {
        $lists = array(PATH_APP . 'GuestMsg');
        foreach ($lists as $item) {
            Files::unlink($item);
        }
    }

    private function _isInstall()
    {
        $version_cacheFile = PATH_CACHE . 'version.log';
        $version_nowFile = PATH_PK . 'version.log';
        file_exists($version_nowFile) ?: die('version file not exists');
        $isInstall = false;
        file_exists($version_cacheFile) ?: $isInstall = true;
        if (!$isInstall) {
            $version_cache = ltrim(file_get_contents($version_cacheFile), 'v');
            $version_now = ltrim(file_get_contents($version_nowFile), 'v');
            $isInstall = $version_now > $version_cache;
        }
        return $isInstall;
    }

}