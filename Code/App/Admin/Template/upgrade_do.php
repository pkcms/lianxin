<style>
    .upgrade_color_blue {
        color: blue;
    }

    .upgrade_color_green {
        color: green;
    }

    .upgrade_color_red {
        color: red;
    }
</style>
<?php echo admin_breadcrumb('系统设置'); ?>

<!-- Title -->
<div class="page-header">
    <div class="page-title">
        <h3>
            版本升级历程
        </h3>
        <p><b>升级过程中，请注意:</b></p>
        <ol>
            <li>在使用自动升级前请保证站点所在服务器的磁盘（或虚拟主机的空间）有大于 200MB 的空闲容量。否则有可能会导致升级失败。</li>
            <li>不要在后台进行其他的操作，否则有可能会使升级中断，导致程序不可用的问题产生！</li>
            <li>升级还没有完成时请不要手动删除、或移动站点根目录下的 Tmp （临时）目录中的任意文件（或文件夹）</li>
            <li>升级完毕后，请手动清理在站点根目录下的 Tmp （临时）目录中的 Upgrade 中的文件，Tmp （临时）目录中的 Backup 下的是备份，请谨慎删除！</li>
            <li>升级过程中会因服务器或网络等的因素，会产生缓慢、中断或失败等的影响。这时可采取手动更新，或是恢复备份。</li>
        </ol>
        <p>升级过程中，图标的作用。</p>
        <div class="row">
            <div class="col-md-3 col-sm-6 col-xs-12">
                <i class="icon-circle-arrow-right"></i>&nbsp;等待中
            </div>
            <div class="col-md-3 col-sm-6 col-xs-12">
                <i class="icon-circle-arrow-right upgrade_color_blue"></i>&nbsp;进行中
            </div>
            <div class="col-md-3 col-sm-6 col-xs-12">
                <i class="icon-ok-sign upgrade_color_green"></i>&nbsp;已完成
            </div>
            <div class="col-md-3 col-sm-6 col-xs-12">
                <i class="icon-remove-sign upgrade_color_red"></i>&nbsp;执行出错
            </div>
        </div>
    </div>
</div>
<!-- /Title -->
<!-- Row -->
<div class="row">
    <div class="col-md-12">
        <div class="widget">
            <div class="widget-content">
                <div class="btn-toolbar btn-toolbar-demo">
                    <input type="button"<?php if (isset($upgradeLists) && empty($upgradeLists)) { ?> disabled="disabled"<?php } ?> id="btnUpgrade" value="执行自动升级" onclick="doUpgrade(0);" class="btn btn-success"/>
                </div>
            </div>
        </div>
    </div>
    <?php
    if (isset($upgradeLists) && is_array($upgradeLists)) {
        foreach ($upgradeLists as $index => $upgrade) {
            ?>
            <div class="col-md-12">
                <div class="widget box">
                    <div class="widget-header">
                        <h4>
                            <i id="version_<?php echo $index ?>" class="icon-circle-arrow-right"></i>
                            版本号：<?php echo $upgrade['versionNum'] ?>
                        </h4>
                    </div>
                    <div class="widget-content">
                        <p>版本描述：<?php echo $upgrade['versionInfo'] ?></p>
                        <div class="row">
                            <div class="col-md-2 col-sm-6 col-xs-12">
                                <i id="backup_<?php echo $index ?>" class="icon-circle-arrow-right"></i>
                                <span>备份程序与数据</span>
                            </div>
                            <div class="col-md-2 col-sm-6 col-xs-12">
                                <i id="download_<?php echo $index ?>" class="icon-circle-arrow-right"></i>
                                <span>下载更新包</span>
                            </div>
                            <div class="col-md-2 col-sm-6 col-xs-12">
                                <i id="unZip_<?php echo $index ?>" class="icon-circle-arrow-right"></i>
                                <span>解压更新包</span>
                            </div>
                            <div class="col-md-2 col-sm-6 col-xs-12">
                                <i id="updateCode_<?php echo $index ?>" class="icon-circle-arrow-right"></i>
                                <span>更新程序</span>
                            </div>
                            <div class="col-md-2 col-sm-6 col-xs-12">
                                <i id="updateDB_<?php echo $index ?>" class="icon-circle-arrow-right"></i>
                                <span>更新数据库结构</span>
                            </div>
                            <div class="col-md-2 col-sm-6 col-xs-12">
                                <i id="delFile_<?php echo $index ?>" class="icon-circle-arrow-right"></i>
                                <span>删除无用文件</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php
        }
    }
    ?>
</div>
<!-- /Row -->

<script type="text/javascript">
    var total = <?php echo count($upgradeLists) ?>;
    var step = new Array('backup', 'download', 'unZip', 'updateCode', 'updateDB', 'delFile');
    var step_index = 0;

    function workDo(htmlId) {
        $('#' + htmlId).addClass('upgrade_color_blue');
    }

    function workRight(htmlId) {
        $('#' + htmlId).removeClass('icon-circle-arrow-right').removeClass('upgrade_color_blue')
            .addClass('icon-ok-sign').addClass('upgrade_color_green');
    }

    function workError(htmlId) {
        $('#' + htmlId).removeClass('icon-circle-arrow-right').removeClass('upgrade_color_blue')
            .addClass('icon-remove-sign').addClass('upgrade_color_red');
    }

    function doUpgrade_step(task, callback) {
        $.get('/index.php/Admin/AdminUpgrade?step=' + task, function (res) {
            console.log(res);
            if (res.success) {
                workRight(task);
            }
            callback(res.success);
        });
    }

    function doUpgrade(task_index) {
        $('#btnUpgrade').attr('disabled','disabled');
        if (task_index < total) {
            if (step_index < step.length) {
                var htmlId = step[step_index] + '_' + task_index;
                workDo(htmlId);
                doUpgrade_step(htmlId, function (success) {
                    if (success) {
                        workRight(htmlId);
                        step_index += 1;
                        doUpgrade(task_index);
                    } else {
                        workError(htmlId);
                        return false;
                    }
                });
            } else {
                task_index += 1;
                step_index = 0;
                doUpgrade(task_index);
            }
        }
    }
</script>
