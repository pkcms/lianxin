<!DOCTYPE html>
<html lang="zh-cn">
<head>
    <meta charset="utf-8">
    <meta name="viewport"
          content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0"/>
    <title>网站后台管理系统</title>
    <link href="/statics/meAdmin/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
    <link href="/statics/bootstrap-3.3.7-dist/css/bootstrap-theme.min.css" rel="stylesheet" type="text/css"/>
    <link rel="stylesheet" type="text/css"
          href="/statics/bootstrap-datetimepicker-master/css/bootstrap-datetimepicker.min.css"/>
    <!--[if lt IE 9]>
    <link rel="stylesheet" type="text/css" href="/statics/meAdmin/plugins/jquery-ui/jquery.ui.1.10.2.ie.css"/>
    <![endif]-->
    <link href="/statics/meAdmin/assets/css/main.css" rel="stylesheet" type="text/css"/>
    <link href="/statics/meAdmin/assets/css/plugins.css" rel="stylesheet" type="text/css"/>
    <link href="/statics/meAdmin/assets/css/responsive.css" rel="stylesheet" type="text/css"/>
    <link href="/statics/meAdmin/assets/css/icons.css" rel="stylesheet" type="text/css"/>
    <link rel="stylesheet" href="/statics/meAdmin/assets/css/fontawesome/font-awesome.min.css">
    <!--[if IE 7]>
    <link rel="stylesheet" href="/statics/meAdmin/assets/css/fontawesome/font-awesome-ie7.min.css">
    <![endif]-->
    <!--[if IE 8]>
    <link href="/statics/meAdmin/assets/css/ie8.css" rel="stylesheet" type="text/css"/>
    <![endif]-->
    <script type="text/javascript" src="/statics/jquery/jquery-1.8.3.min.js"></script>
    <!--    <script type="text/javascript" src="/statics/meAdmin/assets/js/libs/jquery-1.10.2.min.js"></script>-->
    <script type="text/javascript" src="/statics/meAdmin/plugins/jquery-ui/jquery-ui-1.10.2.custom.min.js"></script>
    <script type="text/javascript" src="/statics/meAdmin/bootstrap/js/bootstrap.min.js"></script>
    <script type="text/javascript"
            src="/statics/bootstrap-datetimepicker-master/js/bootstrap-datetimepicker.min.js"></script>
    <script type="text/javascript"
            src="/statics/bootstrap-datetimepicker-master/js/locales/bootstrap-datetimepicker.zh-CN.js"></script>
    <script type="text/javascript" src="/statics/meAdmin/assets/js/libs/lodash.compat.min.js"></script>
    <!--[if lt IE 9]>
    <script src="/statics/meAdmin/assets/js/libs/html5shiv.js"></script>
    <![endif]-->
    <script type="text/javascript" src="/statics/meAdmin/plugins/touchpunch/jquery.ui.touch-punch.min.js"></script>
    <script type="text/javascript" src="/statics/meAdmin/plugins/event.swipe/jquery.event.move.js"></script>
    <script type="text/javascript" src="/statics/meAdmin/plugins/event.swipe/jquery.event.swipe.js"></script>
    <script type="text/javascript" src="/statics/meAdmin/assets/js/libs/breakpoints.js"></script>
    <script type="text/javascript" src="/statics/meAdmin/plugins/respond/respond.min.js"></script>
    <script type="text/javascript" src="/statics/meAdmin/plugins/cookie/jquery.cookie.min.js"></script>
    <script type="text/javascript" src="/statics/meAdmin/plugins/slimscroll/jquery.slimscroll.min.js"></script>
    <script type="text/javascript"
            src="/statics/meAdmin/plugins/slimscroll/jquery.slimscroll.horizontal.min.js"></script>
    <!--[if lt IE 9]>
    <script type="text/javascript" src="/statics/meAdmin/plugins/flot/excanvas.min.js"></script>
    <![endif]-->
    <script type="text/javascript" src="/statics/meAdmin/plugins/sparkline/jquery.sparkline.min.js"></script>
    <script type="text/javascript" src="/statics/meAdmin/plugins/easy-pie-chart/jquery.easy-pie-chart.min.js"></script>
    <script type="text/javascript" src="/statics/meAdmin/plugins/blockui/jquery.blockUI.min.js"></script>
    <script type="text/javascript" src="/statics/meAdmin/plugins/fullcalendar/fullcalendar.min.js"></script>
    <script type="text/javascript" src="/statics/meAdmin/plugins/uniform/jquery.uniform.min.js"></script>
    <script type="text/javascript" src="/statics/meAdmin/plugins/select2/select2.min.js"></script>
    <script type="text/javascript" src="/statics/meAdmin/plugins/bootbox/bootbox.min.js"></script>
    <script type="text/javascript" src="/statics/meAdmin/assets/js/app.js"></script>
    <script type="text/javascript" src="/statics/meAdmin/assets/js/plugins.js"></script>
    <script type="text/javascript" src="/statics/meAdmin/assets/js/plugins.form-components.js"></script>
    <script src="/statics/messenger/js/messenger.min.js"></script>
    <script src="/statics/messenger/js/messenger-theme-future.js"></script>
    <link href="/statics/messenger/css/messenger.css" rel="stylesheet" type="text/css"/>
    <link href="/statics/messenger/css/messenger-theme-future.css" rel="stylesheet" type="text/css"/>
    <script type="text/javaScript" src="/statics/pkcms/js/pkcms.js"></script>
    <script type="text/javaScript" src="/statics/pkcms/js/pkcms-admin-func.js"></script>
    <script type="text/javaScript" src="/statics/pkcms/js/pkcms.bootstrap.modal.js"></script>
    <script type="text/javaScript" src="/statics/pkcms/js/pkcms.lang.js"></script>
    <script type="text/javascript" src="/statics/pkcms/js/admin-action.js"></script>
    <script>
        $(document).ready(function () {
            App.init();
            Plugins.init();
            FormComponents.init()
            $('body').on('hidden.bs.modal', '.modal', function () {
                $('.modal').modal('hide');
                $(this).removeData('bs.modal');
            });
            $('#backHome').click();
        });
    </script>
    <script type="text/javascript" src="/statics/meAdmin/assets/js/custom.js">
    </script>
    <script type="text/javascript" src="/statics/meAdmin/assets/js/demo/pages_calendar.js">
    </script>
    <style>
        .loading {
            background-image: url("/statics/pkcms/image/loading.gif");
            background-repeat: no-repeat;
            background-size: contain;
            background-position: center;
        }
    </style>
</head>

<body class="theme-dark">
<header class="header navbar navbar-fixed-top" role="banner">
    <div class="container">
        <ul class="nav navbar-nav">
            <li class="nav-toggle">
                <a href="javascript:void(0);" title="">
                    <i class="icon-reorder">
                    </i>
                </a>
            </li>
        </ul>
        <a class="navbar-brand" href="#">
            <img src="/statics/images/LXLogo.png" alt="logo"/>
        </a>
        <a href="#" class="toggle-sidebar bs-tooltip" data-placement="bottom"
           data-original-title="Toggle navigation">
            <i class="icon-reorder"></i>
        </a>
        <ul class="nav navbar-nav navbar-right">
            <li>
                <a id="backHome" data-ajax="true" data-ajax-begin="beginPaging"
                   data-ajax-failure="failurePaging" data-ajax-mode="replace"
                   data-ajax-success="successPaging" data-ajax-update="#main"
                   href="/index.php/admin/admin">
                    <i class="icon-home"></i>
                    <span>后台首页</span>
                </a>
            </li>
            <li style="display: none;">
                <a id="backPage" data-ajax="true" data-ajax-begin="beginPaging"
                   data-ajax-failure="failurePaging" data-ajax-mode="replace"
                   data-ajax-success="successPaging" data-ajax-update="#main"
                   href="/index.php/admin/admin">
                    <i class="icon-arrow-left"></i>
                    <span>后退</span>
                </a>
            </li>
            <?php if (isset($home_url)) { ?>
                <li>
                    <a href="<?php echo $home_url; ?>&perView=1" target="_blank">
                        <i class="icon-eye-open"></i>
                        <span>预览前台</span>
                    </a>
                </li>
            <?php } ?>
            <li>
                <a href="https://www.kancloud.cn/pkcms_cn/pkcms" target="_blank">
                    <i class="icon-book"></i>
                    <span>帮助文档</span>
                </a>
            </li>
            <li class="dropdown user">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                    <i class="icon-user"></i>
                    <span class="username"><?php echo isset($userName) ? $userName : '' ?></span>
                    <i class="icon-caret-down small"></i>
                </a>
                <ul class="dropdown-menu">
                    <li>
                        <a href="/index.php/member/AdminPublicLogin/out">
                            <i class="icon-key"></i>
                            退出
                        </a>
                    </li>
                </ul>
            </li>
        </ul>
    </div>
</header>
<div id="container">
    <div id="sidebar" class="sidebar-fixed">
        <div id="sidebar-content">
            <ul id="nav">
                <?php
                if (isset($menu) && is_array($menu)) {
                    foreach ($menu as $menu_key => $menu_item) {
                        ?>
                        <li<?php if ($menu_key == 0) { ?> class="current"<?php } ?>>
                            <a href="javascript:void(0);">
                                <i class="icon-list-alt"></i>
                                <?php echo $menu_item['name']; ?>
                            </a>
                            <?php
                            if (is_array($menu_item['children'])) {
                                ?>
                                <ul class="sub-menu">
                                    <?php
                                    foreach ($menu_item['children'] as $child_index => $child) {
                                        ?>
                                        <li>
                                            <a href="<?php echo $child['url']; ?>"
                                               data-ajax="true" data-ajax-begin="beginPaging"
                                               data-ajax-failure="failurePaging"
                                               data-ajax-mode="replace" data-ajax-success="successPaging"
                                               data-ajax-update="#main">
                                                <?php echo $child['name']; ?>
                                            </a>
                                        </li>
                                        <?php
                                        if (($menu_item['id'] == 21) && isset($categoryListHtml)
                                            && (($child_index + 1) == count($menu_item['children']))) {
                                            echo $categoryListHtml;
                                        }
                                    }
                                    ?>
                                </ul>
                                <?php
                            }
                            ?>
                        </li>
                        <?php
                    }
                }
                ?>
            </ul>
        </div>
        <div id="divider" class="resizeable">
        </div>
    </div>
    <div id="content">
        <div id="main" class="container">
        </div>
        <div id="subMain" class="container" style="display: none">
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog"
     aria-labelledby="exampleModalLabel" data-backdrop='static' aria-hidden="true">
</div>

<script type="text/javaScript" src="/statics/jquery/jquery.unobtrusive-ajax.js"></script>
</body>

</html>