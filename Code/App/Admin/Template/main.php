<?php

if ($_GET['act'] == "phpinfo") {
    phpinfo();
    exit();
}

//检测PHP设置参数
function show($varName)
{
    switch ($result = get_cfg_var($varName)) {
        case 0:
            return '<font color="red">×</font>';
            break;
        case 1:
            return '<font color="green">√</font>';
            break;
        default:
            return $result;
            break;
    }
}

echo admin_breadcrumb('详细')
?>

<!-- Title -->
<div class="page-header">
    <div class="page-title">
        <h3>后台首页</h3>
    </div>
</div>
<!-- /Title -->
<!-- Row -->
<div class="row">
    <div class="col-md-12">
        <h5 class="widget-title">
            <i class="icon-unchecked"></i>
            程序相关
        </h5>
    </div>
    <div class="col-md-12">
        <ol class="row">
            <dt class="col-md-3 col-sm-3 col-xs-12">当前版本：</dt>
            <dd class="col-md-9 col-sm-9 col-xs-12">
                <span id="LocalVersion"></span>
                (最新版本：<span id="ServiceVersion"></span>)
                <a id="btnUpgrade" style="display: none;" data-ajax="true" data-ajax-begin="beginPaging"
                   data-ajax-failure="failurePaging"
                   data-ajax-mode="replace" data-ajax-success="successPaging" data-ajax-update="#main"
                   href="/index.php/admin/AdminUpgrade" target="main" class="btn btn-default">现在升级</a>
            </dd>
        </ol>
        <ol class="row">
            <dt class="col-md-3 col-sm-3 col-xs-12">PHP运行库：</dt>
            <dd class="col-md-9 col-sm-9 col-xs-12">
                版本：<?php echo PHP_VERSION; ?> ，
                运行方式：<?php echo strtoupper(php_sapi_name()); ?> ，
                信息：<?php
                $phpSelf = $_SERVER['PHP_SELF'] ? $_SERVER['PHP_SELF'] : $_SERVER['SCRIPT_NAME'];
                $disFuns = get_cfg_var("disable_functions");
                echo (false !== eregi("phpinfo", $disFuns)) ? '<font color="red">×</font>' : "<a href='$phpSelf?act=phpinfo' target='_blank'>PHPINFO</a>";
                ?> 。
            </dd>
        </ol>
        <ol class="row">
            <dt class="col-md-3 col-sm-3 col-xs-12">资源限制信息：</dt>
            <dd class="col-md-9 col-sm-9 col-xs-12">
                脚本占用最大内存：<?php echo show("memory_limit"); ?> ，
                POST方法提交最大限制：<?php echo show("post_max_size"); ?> ，
                上传文件最大限制：<?php echo show("upload_max_filesize"); ?> 。
            </dd>
        </ol>
        <ol class="row">
            <dt class="col-md-3 col-sm-3 col-xs-12">绝对路径：</dt>
            <dd class="col-md-9 col-sm-9 col-xs-12">
                <?php echo $_SERVER['DOCUMENT_ROOT'] ? str_replace('\\', '/', $_SERVER['DOCUMENT_ROOT']) : str_replace('\\', '/', dirname(__FILE__)); ?>
            </dd>
        </ol>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <h5 class="widget-title">
            <i class="icon-unchecked"></i>
            服务器参数
        </h5>
        <div class="col-md-12">
            <ol class="row">
                <dt class="col-md-3 col-sm-3 col-xs-12">域名/IP地址：</dt>
                <dd class="col-md-9 col-sm-9 col-xs-12">
                    <?php echo @get_current_user(); ?>&nbsp;-&nbsp;<?php echo $_SERVER['SERVER_NAME']; ?>
                    (<?php if ('/' == DIRECTORY_SEPARATOR) {
                        echo $_SERVER['SERVER_ADDR'];
                    } else {
                        echo @gethostbyname($_SERVER['SERVER_NAME']);
                    } ?>)&nbsp;&nbsp;你的IP地址是：<?php echo @$_SERVER['REMOTE_ADDR']; ?>
                </dd>
            </ol>
            <ol class="row">
                <dt class="col-md-3 col-sm-3 col-xs-12">服务器标识：</dt>
                <dd class="col-md-9 col-sm-9 col-xs-12"><?php echo @php_uname();; ?></dd>
            </ol>
            <ol class="row">
                <dt class="col-md-3 col-sm-3 col-xs-12">操作系统：</dt>
                <dd class="col-md-9 col-sm-9 col-xs-12">
                    <?php $os = explode(" ", php_uname());
                    echo $os[0]; ?> &nbsp;内核版本：<?php if ('/' == DIRECTORY_SEPARATOR) {
                        echo $os[2];
                    } else {
                        echo $os[1];
                    } ?>
                </dd>
            </ol>
            <ol class="row">
                <dt class="col-md-3 col-sm-3 col-xs-12">解译引擎：</dt>
                <dd class="col-md-9 col-sm-9 col-xs-12"><?php echo $_SERVER['SERVER_SOFTWARE']; ?></dd>
            </ol>
            <ol class="row">
                <dt class="col-md-3 col-sm-3 col-xs-12">服务器语言：</dt>
                <dd class="col-md-9 col-sm-9 col-xs-12"><?php echo getenv("HTTP_ACCEPT_LANGUAGE"); ?></dd>
            </ol>
            <ol class="row">
                <dt class="col-md-3 col-sm-3 col-xs-12">服务器端口：</dt>
                <dd class="col-md-9 col-sm-9 col-xs-12"><?php echo $_SERVER['SERVER_PORT']; ?></dd>
            </ol>
            <ol class="row">
                <dt class="col-md-3 col-sm-3 col-xs-12">服务器主机名：</dt>
                <dd class="col-md-9 col-sm-9 col-xs-12">
                    <?php if ('/' == DS) {
                        echo $os[1];
                    } else {
                        echo $os[2];
                    } ?>
                </dd>
            </ol>
        </div>
    </div>
</div>
<!-- /Row -->
<script type="text/javascript">
    $.get('/index.php/Admin/AdminUpgrade/GetVersion', function (res) {
        if (res.hasOwnProperty('msg') && res.msg != null) {
            $('#ServiceVersion').html(res.msg);
        } else if (res.hasOwnProperty('version_service') && res.hasOwnProperty('version_local')) {
            let version_service = parseInt(res.version_service.replace('v', ''));
            let version_local = parseInt(res.version_local.replace('v', ''));
            if (typeof version_service == "number" && typeof version_local == "number") {
                if (version_service > version_local) {
                    $('#btnUpgrade').show();
                }
            }
            $('#ServiceVersion').html(res.version_service);
            $('#LocalVersion').html(res.version_local ? res.version_local : '因没有升级过，没有到版本号');
        }
    });
</script>