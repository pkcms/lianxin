<{admin_breadcrumb('系统设置')}>

<!-- Title -->
<div class="page-header">
    <div class="page-title">
        <h3>
            菜单编辑
        </h3>
    </div>
</div>
<!-- /Title -->
<!-- Row -->
<div class="row">
    <div class="col-md-12">
        <div class="widget box">
            <div class="widget-header">
                <h4>基本填写</h4>
            </div>
            <div class="widget-content">
                <form id="menuForm" class="form-horizontal row-border" action="Admin/SetMenuDetail/Post">
                    <div class="form-group">
                        <label class="col-md-2 control-label">
                            父级:
                        </label>
                        <div class="col-md-10">
                            <{optionBySelct('parentid', $parentList, $parentid)}>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label">
                            菜单名:
                        </label>
                        <div class="col-md-10">
                            <input type="text" name="name" class="form-control" value="<{$name}>" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label">
                            模块名:
                        </label>
                        <div class="col-md-10">
                            <input type="text" name="app" class="form-control" value="<{$app}>" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label">
                            文件名:
                        </label>
                        <div class="col-md-10">
                            <input type="text" name="c" class="form-control" value="<{$c}>" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label">
                            方法名:
                        </label>
                        <div class="col-md-10">
                            <input type="text" name="action" class="form-control" value="<{$action}>" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label">
                            附加参数:
                        </label>
                        <div class="col-md-10">
                            <input type="text" name="data" class="form-control" value="<{$data}>" />
                        </div>
                    </div>
                    <div class="form-actions">
                        <input type="hidden" name="id" value="<{$id}>" />
                        <input type="hidden" name="type" value="<{$type}>" />
                        <button type="button" data-form="menuForm" class="btn btn-primary pull-left">
                            提交保存
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- /Row -->
