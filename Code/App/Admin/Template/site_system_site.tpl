<{admin_breadcrumb('系统设置')}>


<!-- Title -->
<div class="page-header">
    <div class="page-title">
        <h3>
            站点设置
        </h3>
    </div>
</div>
<!-- /Title -->
<!-- Row -->
<div class="row">
    <div class="col-md-12">
        <form id="siteForm" class="form-horizontal row-border" action="Admin/SetSiteDetail/Post">
            <div class="widget box">
                <div class="widget-header">
                    <h4>
                        <i class="icon-reorder"></i>
                        站点基本信息
                    </h4>
                </div>
                <div class="widget-content">
                    <div class="form-group">
                        <label class="col-md-2 control-label">
                            站点名:
                        </label>
                        <div class="col-md-10">
                            <input type="text" class="form-control" name="Site_Name" value="<{$site[Site_Name]}>">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label">
                            站点域名:
                        </label>
                        <div class="col-md-10">
                            <input type="text" class="form-control" name="Site_Domain" value="<{$site[Site_Domain]}>">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label">
                            备案号:
                        </label>
                        <div class="col-md-10">
                            <input type="text" class="form-control" name="Site_Tcp" value="<{$site[Site_Tcp]}>">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label">
                            网站 LOGO:
                        </label>
                        <div class="col-md-10">
                            <{UploadFile('image',$site[info][logo],'info[logo]', 'logo')}>
                        </div>
                    </div>
                </div>
            </div>
            <div class="widget box">
                <div class="widget-header">
                    <h4>
                        <i class="icon-reorder"></i>
                        SEO 设置
                    </h4>
                </div>
                <div class="widget-content">
                    <div class="form-group">
                        <label class="col-md-2 control-label">
                            SEO 标题:
                        </label>
                        <div class="col-md-10">
                            <input type="text" class="form-control" name="Site_Title" value="<{$site[Site_Title]}>">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label">
                            SEO 关键字:
                        </label>
                        <div class="col-md-10">
                        <textarea class="form-control" name="Site_Keyword"><{$site[Site_Keyword]}></textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label">
                            SEO 简述:
                        </label>
                        <div class="col-md-10">
                        <textarea class="form-control" name="Site_Description"><{$site[Site_Description]}></textarea>
                        </div>
                    </div>
                </div>
            </div>
            <div class="widget box">
                <div class="widget-header">
                    <h4>
                        <i class="icon-reorder"></i>
                        企业客服联系信息
                    </h4>
                </div>
                <div class="widget-content">
                    <div class="form-group">
                        <label class="col-md-2 control-label">
                            站点模板主题:
                        </label>
                        <div class="col-md-10">
                            <{optionBySelct('Site_Contact[GuestMsgStyle]', $guestMsgStyleList, $site['Site_Contact']['GuestMsgStyle'])}>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label">
                            客服电话:
                        </label>
                        <div class="col-md-10">
                            <input type="text" class="form-control" name="Site_Contact[tel]" value="<{$site[Site_Contact][tel]}>"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label">
                            QQ 客服:
                        </label>
                        <div class="col-md-10">
                            <input type="text" class="form-control" name="Site_Contact[qq]" value="<{$site[Site_Contact][qq]}>"/>
                            <p class="help-block">
                                多个QQ号码之间用英文逗号（,）分隔
                            </p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label">
                            微信客服二维码:
                        </label>
                        <div class="col-md-10">
                            <{UploadFileListEachByImage('', 'WeChat', 'Site_Contact[WeChat]', $site[Site_Contact][WeChat])}>
                            <{UploadFileListBtn('WeChat','','Site_Contact[WeChat]')}>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label">
                            联系方式:
                        </label>
                        <div class="col-md-10">
                            <{form_editor("Site_Contact[detailes]","editor",$site[Site_Contact][detailes])}>
                        </div>
                    </div>
                </div>
            </div>
            <{if $groupid == 1}>
            <div class="widget box">
                <div class="widget-header">
                    <h4>
                        <i class="icon-reorder"></i>
                        扩展属性
                    </h4>
                </div>
                <div class="widget-content">
                    <div class="form-group">
                        <label class="col-md-2 control-label">
                            诚信通地址:
                        </label>
                        <div class="col-md-10">
                            <input type="text" class="form-control" name="info[Sincerity]" placeholder="请输入诚信通地址"
                                   value="<{$site[info][Sincerity]}>"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label">
                            是否启用静默禁词检测:
                        </label>
                        <div class="col-md-10">
                            <{RadioButton($switch, $site['setting']['isAntispam'], 'setting[isAntispam]')}>
                            <p class="help-block">
                                需考虑网络延时拖累执行效率因素
                            </p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label">
                            站点语言包:
                        </label>
                        <div class="col-md-10">
                            <{optionBySelct('setting[Default_Lang]', $language_select, $site[setting][Default_Lang])}>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label">
                            搜索页返回每页结果数:
                        </label>
                        <div class="col-md-10">
                            <input type="number" name="setting[searchItemSize]" class="form-control" value="<{if $site[setting][searchItemSize]}><{$site[setting][searchItemSize]}><{else}>10<{/if}>">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label">
                            站点模板主题:
                        </label>
                        <div class="col-md-10">
                            <{optionBySelct('setting[Site_Themes]', $themesList, $site['setting']['Site_Themes'])}>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label">
                            电脑版静态页路径:
                        </label>
                        <div class="col-md-10">
                            <input type="text" name="Site_Path" class="form-control" value="<{$site[Site_Path]}>">
                            <p class="help-block">
                                留空，则默认将静态页保存到站点的根目录
                            </p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label">
                            是否启用手机版:
                        </label>
                        <div class="col-md-10">
                            <{RadioButton($switch, $site['setting']['IsMobileStatics'], 'setting[IsMobileStatics]')}>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label">
                            移动版静态页路径:
                        </label>
                        <div class="col-md-10">
                            <input type="text" name="setting[MobileStaticsPath]" class="form-control"
                                   value="<{$site['setting']['MobileStaticsPath']}>">
                            <p class="help-block">
                                手机版关闭，无需设置
                            </p>
                        </div>
                    </div>
                </div>
            </div>
            <{/if}>
            <div class="form-actions">
                <input type="hidden" name="id" value="<{$site[id]}>"/>
                <button type="button" data-form="siteForm" class="btn btn-primary pull-left">
                    提交保存
                </button>
            </div>
        </form>
    </div>
</div>
<!-- /Row -->
