<{admin_breadcrumb('系统设置')}>

<!-- Title -->
<div class="page-header">
    <div class="page-title">
        <h3>
            导航菜单管理
        </h3>
    </div>
</div>
<!-- /Title -->
<!-- Row -->
<div class="row">
    <div class="col-md-12">
        <div class="widget">
            <div class="widget-content">
                <div class="btn-toolbar btn-toolbar-demo">
                    <a class="btn btn-success" data-ajax="true" data-ajax-begin="beginPaging"
                       data-ajax-failure="failurePaging"
                       data-ajax-mode="replace" data-ajax-success="successPaging"
                       data-ajax-update="#main"
                       href="/index.php/Admin/SetMenuDetail?type=<{$type}>">
                        <i class="icon-plus-sign"></i>&nbsp;添加菜单</a>
                    <button class="btn btn-danger" data-action="del" data-table="menuList">
                        <i class="icon-remove-circle"></i>
                        删除
                    </button>
                    <button class="btn btn-default" data-action="sort" data-table="menuList">
                        排序
                    </button>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-12">
        <div class="widget box">
            <div class="widget-header">
                <h4>
                    <i class="icon-reorder"></i>
                    列表
                </h4>
            </div>
            <div class="widget-content">
                <table id="menuList" data-action="Admin/SetMenuOther"
                       class="table table-bordered table-striped table-hover mb-0">
                    <thead class="thead-secondary">
                    <tr>
                        <th width="30"></th>
                        <th width="80">排序</th>
                        <th width="100">id</th>
                        <th>菜单名称</th>
                    </tr>
                    </thead>
                    <tbody>
                    <{loop $lists $item}>
                        <tr>
                            <td><input type="radio" value="<{$item[id]}>" name="set_id"/></td>
                            <td><input type="text" value="<{$item[orderIndex]}>" name="sort[]" data-id="<{$item[id]}>" size="3" maxlength="3"/>
                            </td>
                            <td><{$item[id]}></td>
                            <td>
                                <a class="f14" data-toggle="modal" data-target="#editorDialog" href="SetMenuDetail?id=<{$item[id]}>"
                                   data-toggle="tooltip" title="点击编辑">
                                    <i class="glyphicon glyphicon-edit"></i>&nbsp;<{$item[name]}>
                                </a>
                                <a class="f14 btn" data-toggle="modal" data-target="#editorDialog"
                                   href="SetMenuDetail?parentid=<{$item[id]}>" class="ajaxlink" data-toggle="tooltip"
                                   title="点击添加子导航">
                                    <i class="glyphicon glyphicon-plus-sign"></i>
                                </a>
                            </td>
                        </tr>
                        <{loop $item[children] $one}>
                        <tr>
                            <td><input type="radio" value="<{$one[id]}>" name="set_id"/></td>
                            <td>
                                <input type="text" value="<{$one[orderindex]}>" name="sort[]" data-id="<{$item[id]}>" size="3" maxlength="3"/>
                            </td>
                            <td><{$one[id]}></td>
                            <td>
                                &nbsp;&nbsp;&nbsp;&nbsp;
                                <a class="f14" data-toggle="modal" data-target="#editorDialog" href="SetMenuDetail?id=<{$one[id]}>"
                                   data-toggle="tooltip" title="点击编辑">
                                    <i class="glyphicon glyphicon-edit"></i>&nbsp;<{$one[name]}>
                                </a>
                            </td>
                        </tr>
                        <{/loop}>
                        <{/loop}>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<!-- /Row -->
