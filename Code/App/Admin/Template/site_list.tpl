<{admin_breadcrumb('系统设置')}>

<!-- Title -->
<div class="page-header">
    <div class="page-title">
        <h3>
            站点管理
        </h3>
    </div>
</div>
<!-- /Title -->
<!-- Row -->
<div class="row">
    <{if $groupId == 1}>
    <div class="col-md-12">
        <div class="widget">
            <div class="widget-content">
                <div class="btn-toolbar btn-toolbar-demo">
                    <a class="btn btn-success" data-ajax="true" data-ajax-begin="beginPaging"
                       data-ajax-failure="failurePaging"
                       data-ajax-mode="replace" data-ajax-success="successPaging"
                       data-ajax-update="#main"
                       href="/index.php/Admin/SetSiteDetail">
                        <i class="icon-plus-sign"></i>&nbsp;添加站点</a>
                </div>
            </div>
        </div>
    </div>
    <{/if}>
    <div class="col-md-12">
        <div class="widget box">
            <div class="widget-header">
                <h4>
                    <i class="icon-reorder"></i>
                    列表
                </h4>
            </div>
            <div class="widget-content" id="formDataList" data-action="form/AdminSetDataOther">
                <input type="hidden" name="modelId" value="<{$modelId}>">
                <table class="table table-bordered table-striped table-hover mb-0">
                    <thead class="thead-secondary">
                    <tr>
                        <th>id</th>
                        <th>站点名称</th>
                        <th>关联站点管理员</th>
                    </tr>
                    </thead>
                    <tbody>
                    <{loop $lists $item}>
                        <tr class="<{$item[id]}>">
                            <td><{$item[id]}></td>
                            <td>
                                <a data-ajax="true" data-ajax-begin="beginPaging"
                                   data-ajax-failure="failurePaging"
                                   data-ajax-mode="replace" data-ajax-success="successPaging"
                                   data-ajax-update="#main"
                                   href="/index.php/Admin/SetSiteDetail?id=<{$item[id]}>" title="点击编辑">
                                    <i class="icon-edit"></i>&nbsp;<{$item[Site_Name]}>
                                </a>
                            </td>
                            <td><{$item[admin]}></td>
                        </tr>
                        <{/loop}>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<!-- /Row -->
