<?php


namespace PKApp\Admin;


use PKApp\Admin\Classes\AdminController;
use PKApp\Admin\Classes\AdminDataBase;
use PKApp\Admin\Classes\AdminExtend;
use PKCore\Request;
use PKCore\Tpl;

class GetMenuList extends AdminController
{

    public function Main()
    {
        $type = Request::get('type');
        if (!empty($type)) {
            $typeList = self::getDict('menu_type');
            array_key_exists($type, $typeList) ?: $type = 'admin';
        } else {
            $type = 'admin';
        }
        $list = AdminExtend::GetMenu($type);
        $tpl = new Tpl();
        $tpl->SetTplParam('lists', $list)
            ->SetTplParam('type', $type);
        return $tpl->Display('menu_init');
    }
}