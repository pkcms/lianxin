<?php


namespace PKApp\Admin;


use PKApp\Admin\Classes\AdminController;
use PKApp\Admin\Classes\AdminDataBase;
use PKApp\Model\Classes\ModelDatabase;
use PKCore\Converter;
use PKCore\Formats;
use PKCore\Request;
use PKCore\Route;
use PKCore\Tpl;

class SetMenuDetail extends AdminController
{

    public function Main()
    {
        $id = Request::get('id');
        $tpl = new Tpl();
        $isNew = true;
        if (!empty($id)) {
            $entity = AdminDataBase::GetMenuById($id);
            if (is_array($entity)) {
                $tpl->SetTplParamList($entity);
                $isNew = false;
            }
        }
        if ($isNew) {
            $type = Request::get('type');
            if (!empty($type)) {
                $typeList = self::getDict('menu_type');
                array_key_exists($type, $typeList) ?: $type = 'admin';
            } else {
                $type = 'admin';
            }
            $tpl->SetTplParam('parentid', Formats::isNumeric(Request::get('parentid')))
                ->SetTplParam('type', $type);
        }
        $parentList = AdminDataBase::GetMenuList(array('parentid' => 0, 'type' => 'admin'), array('id', 'name'));
        if (!empty($parentList) && is_array($parentList)) {
            $parentList = Converter::arrayColumn($parentList, 'name', 'id');
        } else {
            $parentList = array();
        }
        $tpl->SetTplParam('parentList', $parentList);
        return $tpl->Display('menu_form');
    }

    public function Post()
    {
        $this->checkPostFieldIsEmpty('name', 'Menu_NameEmpty');
        $post = Request::post();
        $id = Request::post('id');
        if (empty($id)) {
            unset($post['id']);
            AdminDataBase::AddMenu($post);
        } else {
            AdminDataBase::UpdateMenu($post, $id);
        }
        \PKCore\msg('Data_Input_Success');
    }

    /**
     * 将表单的数据管理添加到后台管理导航中
     */
    public function byFormData()
    {
        $modelId = Formats::isNumeric(Request::get('modelId'));
        $db_model = new ModelDatabase();
        $modelEntity = $db_model->GetById($modelId);
        $data = array('app' => 'form', 'c' => 'AdminGetDataList', 'type' => 'admin',
            'name' => $modelEntity['name'], 'data' => 'modelId=' . $modelId);
        $menuEntity = AdminDataBase::GetMenuByName($data['name']);
        if (empty($menuEntity)) {
            // 还没有添加到导航
            // 检查父级是否已经存在
            $parentName = Route\language('Menu_Form_ParentName', 'admin', 'zh-cn');
            $parentEntity = AdminDataBase::GetMenuByName($parentName);
            if (empty($parentEntity)) {
                // 父级导航不存在
                $parentData = array('name' => $parentName, 'icon' => 'list-alt', 'type' => 'admin');
                $parentId = AdminDataBase::AddMenu($parentData);
            } else {
                $parentId = $parentEntity['id'];
            }
            $data['parentid'] = $parentId;
            AdminDataBase::AddMenu($data);
            \PKCore\msg('Data_Input_Success');
        } else {
            // 已经添加到导航
//            \PKCore\msg('Data_Input_Success');
            \PKCore\alert('Menu_NameExists');
        }
    }

}