<?php


namespace PKApp\Admin;


use PKApp\Admin\Classes\AdminController;
use PKApp\Admin\Classes\AdminDataBase;
use PKApp\Member\Classes\MemberDataBase;
use PKCore\Converter;
use PKCore\Formats;
use PKCore\Tpl;

class GetSiteList extends AdminController
{

    public function Main()
    {
        $tpl = new Tpl();
        $where = $this->loginUser()->GroupId == 1 ? array() : array('id' => $this->loginUser()->SiteId);
        $siteList = AdminDataBase::GetSiteList(array('id', 'Site_Name'), $where);
        $siteIdList = $this->_getSiteId($siteList);
        $userList = MemberDataBase::GetUserLists(array('siteId' => $siteIdList), array('siteId', 'username'));
        $result = $this->_handler($siteList, $userList);
        $tpl->SetTplParam('lists', $result)->SetTplParam('groupId', $this->loginUser()->GroupId);
        return $tpl->Display('site_list');
    }

    private function _getSiteId($siteList)
    {
        $result = array();
        if (Formats::isArray($siteList)) {
            foreach ($siteList as $item) {
                if (($this->loginUser()->GroupId == 1) || ($this->loginUser()->SiteId == $item['id'])) {
                    $result[] = $item['id'];
                }
            }
        }
        return $result;
    }

    private function _handler($siteList, $userList)
    {
        if (Formats::isArray($siteList)) {
            foreach ($siteList as $index => $item) {
                if (is_array($item) && array_key_exists('id', $item)) {
                    $siteId = $item['id'];
                    $nowUserList = array_filter($userList, function ($user) use ($siteId) {
                        if (isset($user['siteId'])) {
                            return $user['siteId'] == $siteId ? $user : array();
                        }
                        return array();
                    });
                    if (Formats::isArray($nowUserList)) {
                        $nowUserList = Converter::arrayGetByKey($nowUserList, 'username');
                    }
                    $item['admin'] = isset($nowUserList) && is_array($nowUserList) ? implode(',', $nowUserList) : '';
                }
                $siteList[$index] = $item;
            }
        }
        return $siteList;
    }
}