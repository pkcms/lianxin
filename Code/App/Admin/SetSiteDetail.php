<?php


namespace PKApp\Admin;


use PKApp\Admin\Classes\AdminController;
use PKApp\Admin\Classes\AdminDataBase;
use PKApp\Admin\Classes\AdminExtend;
use PKApp\Antispam\Classes\Antispam;
use PKCore\Converter;
use PKCore\Files;
use PKCore\PKController;
use PKCore\Request;
use PKCore\Tpl;

class SetSiteDetail extends AdminController
{

    public function Main()
    {
        $siteId = Request::get('id');
        if (empty($siteId) && $this->loginUser()->GroupId > 1 && $this->loginUser()->SiteId) {
            $siteId = $this->loginUser()->SiteId;
        }
        $tpl = new Tpl();
        $tpl->SetTplParam('groupid', $this->loginUser()->GroupId);
        $db_site = AdminExtend::GetSiteInfo($siteId);
        if (empty($db_site)) {
            $db_site = array('setting' => array('Site_nopic' => '', 'Default_Lang' => '', 'Site_Path' => '',
                'replacenum' => 0, 'searchItemSize' => '', 'Site_Themes' => '',
                'IsMobileStatics' => 0, 'MobileStaticsPath' => '', 'isAntispam' => 1));
        } else {
            if (is_array($db_site) && array_key_exists('setting', $db_site)
                && !array_key_exists('isAntispam', $db_site['setting'])) {
                $db_site['setting']['isAntispam'] = 1;
            }
        }
        $guest_style = Files::getContents(API_GuestMsg . 'style_list.json', 60);
        if (!empty($guest_style)) {
            $guest_style = Converter::objectToArray(json_decode($guest_style));
            $tpl->SetTplParam('guestMsgStyleList', self::GetArrayByKey('Lists', $guest_style));
        } else {
            $tpl->SetTplParam('guestMsgStyleList', array());
        }
        $tpl->SetTplParam('site', $db_site);
        $tpl->SetTplParam('switch', self::getDict('switch'));
        $tpl->SetTplParam('language_select', self::getDict('language_select'));
        //主题模板
        $themesPath = PATH_ROOT . 'Templates';
        $themesDirList = Files::opendir($themesPath);
        $tpl->SetTplParam('themesList', $this->getItemByArrayKey($themesDirList));
        return $tpl->Display('site_system_site');
    }

    public function Post()
    {
        $post = Request::post();
//        print_r($post); exit();
        $isNew = false;
        $setting = array();
        if (is_array($post)) {
            !array_key_exists('info', $post) ?: ($post['info'] = !is_array($post['info']) ? $post['info'] : serialize($post['info']));
            !array_key_exists('Site_Domain', $post) ?: ($post['Site_Domain'] = rtrim($post['Site_Domain'], '/') . '/');
            !array_key_exists('id', $post) ?: ($isNew = empty($post['id']));
            if (array_key_exists('setting', $post) && is_array($post['setting'])) {
                (array_key_exists('Default_Lang', $post['setting']) && !empty($post['setting']['Default_Lang'])) ?: \PKCore\alert('Site_LanguageEmpty');
                (array_key_exists('Site_Themes', $post['setting']) && !empty($post['setting']['Site_Themes'])) ?: \PKCore\alert('Site_ThemesEmpty');
                $setting = $post['setting'];
                $post['setting'] = serialize($post['setting']);
            } elseif ($siteId = PKController::GetArrayByKey('id', $post)) {
                $db_site = AdminExtend::GetSiteInfo($siteId);
                $setting = PKController::GetArrayByKey('setting', $db_site);
            }
        }
        $post['Site_Contact'] = serialize($post['Site_Contact']);
        if ((bool)PKController::GetArrayByKey('isAntispam', $setting)) {
            $antispam = new Antispam();
            $tips = $antispam->CheckSite($post);
            empty($tips) ?: \PKCore\alert('', $tips);
        }
        if ($isNew) {
            $siteId = AdminDataBase::AddSite($post);
        } else {
            $siteId = $post['id'];
            AdminDataBase::UpdateSite($post, $siteId);
        }
        $post['info'] = Converter::Unserialize($post['info']);
        $post['setting'] = Converter::Unserialize($post['setting']);
        Files::putContents(PATH_DATA, 'site_' . $siteId . '.php', Converter::arrayToSaveString($post));
        \PKCore\msg('Data_Input_Success');
    }

}