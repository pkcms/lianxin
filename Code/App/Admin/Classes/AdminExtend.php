<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2019/5/27
 * Time: 17:30
 */

namespace PKApp\Admin\Classes;


use PKCore\Converter;
use PKCore\Formats;

class AdminExtend
{

    /**
     * 对用户的密码进行加密
     * @param $password
     * @param $encrypt //传入加密串，在修改密码时做认证
     * @return array/password
     */
    public static function PasswordMD5($password, $encrypt = NULL)
    {
        return md5(md5(trim($password)) . $encrypt);
    }

    public static function GetMenu($type, $idList = null)
    {
        $option = array('type' => $type);
        is_null($idList) ?: $option['id'] = explode(',', $idList);
        $list = AdminDataBase::GetMenuList($option);
        return AdminExtend::_treeMenuChildren($list);
    }


    private static function _treeMenuChildren($menuList, $parentId = 0)
    {
        $result = array();
        $nowChildrenList = array_filter($menuList, function ($item) use ($parentId) {
            if (isset($item['parentid'])) {
                return $item['parentid'] == $parentId ? $item : array();
            }
            return array();
        });
        if (Formats::isArray($nowChildrenList)) {
            foreach ($nowChildrenList as $index => $item) {
                $path = '';
                if (is_array($item) && array_key_exists('app', $item) && array_key_exists('c', $item)) {
                    $path = $item['app'] . '/' . $item['c'];
                }
                if (is_array($item) && array_key_exists('action', $item) && !empty($item['action'])) {
                    $path .= '/' . $item['action'];
                }
                if (is_array($item) && array_key_exists('data', $item) && !empty($item['data'])) {
                    $path .= '?' . $item['data'];
                }
                $url = AdminController::ToMeUrl($path);
                if (isset($item['id'])) {
                    $children = self::_treeMenuChildren($menuList, $item['id']);
                }
                $result[] = array('icon' => $item['icon'], 'id' => $item['id'], 'name' => $item['name'], 'url' => $url,
                    'orderIndex' => $item['orderIndex'], 'children' => isset($children) ? $children : array());
            }
        }
        return $result;
    }

    public static function GetSiteInfo($siteId)
    {
        $siteEntity = AdminDataBase::GetSite(array('id' => $siteId), '*');
        if (is_array($siteEntity) && array_key_exists('setting', $siteEntity)) {
            $siteEntity['setting'] = Converter::Unserialize($siteEntity['setting']);
        }
        if (is_array($siteEntity) && array_key_exists('info', $siteEntity)) {
            $siteEntity['info'] = Converter::Unserialize($siteEntity['info']);
        }
        if (is_array($siteEntity) && array_key_exists('Site_Contact', $siteEntity)) {
            $siteEntity['Site_Contact'] = Converter::Unserialize($siteEntity['Site_Contact']);
            if (empty($siteEntity['Site_Contact'])) {
                $arr = array('detailes' => $siteEntity['Site_Contact']);
                $siteEntity['Site_Contact'] = $arr;
            }
        }
        return $siteEntity;
    }
}