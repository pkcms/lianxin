<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2019/5/27
 * Time: 10:37
 */

namespace PKApp\Admin\Classes;


use PKCore\DataBase;
use PKCore\Formats;

class AdminDataBase
{

    private static $_table_menu = 'menu';
    private static $_table_site = 'site';

    public static function GetMenuList($option, $field = '*')
    {
        $db = new DataBase(self::$_table_menu);
        return $db->Where($option)->OrderBy('orderIndex', 'ASC')
            ->OrderBy('id', 'ASC')->Select($field)->ToList();
    }

    public static function GetMenuById($id)
    {
        $db = new DataBase(self::$_table_menu);
        return $db->Where(array('id' => $id))->Select()->First();
    }

    public static function GetMenuByName($name)
    {
        $db = new DataBase(self::$_table_menu);
        return $db->Where(array('name' => $name))->Select()->First();
    }

    public static function AddMenu($data)
    {
        $db = new DataBase(self::$_table_menu);
        return $db->Insert($data)->Exec();
    }

    public static function UpdateMenu($data, $id)
    {
        $db = new DataBase(self::$_table_menu);
        return $db->Where(array('id' => $id))->Update($data)->Exec();
    }

    public static function DelMenu($id)
    {
        $db = new DataBase(self::$_table_menu);
        $db->Where(array('id' => $id))->Delete()->Exec();
    }

    public static function GetSite($options, $field)
    {
        $db = new DataBase(self::$_table_site);
        $siteInfo = $db->Where($options)->Select($field)->First();
        return $siteInfo;
    }

    public static function GetSiteList($field = '*', $where = array())
    {
        $db = new DataBase(self::$_table_site);
        if (Formats::isArray($where)) {
            $db->Where($where);
        }
        return $db->Select($field)->ToList();
    }

    public static function AddSite($data)
    {
        $db = new DataBase(self::$_table_site);
        return $db->Insert($data)->Exec();
    }

    public static function UpdateSite($data, $id)
    {
        $db = new DataBase(self::$_table_site);
        return $db->Where(array('id' => $id))->Update($data)->Exec();
    }

}