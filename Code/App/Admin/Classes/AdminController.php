<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2019/5/29
 * Time: 9:43
 */

namespace PKApp\Admin\Classes;

use PKCore\Formats;
use PKCore\Route;
use PKApp\Admin\Model\LoginUserInfo;
use PKCore\PKController;
use PKCore\Request;

abstract class AdminController extends PKController
{

    public function __construct()
    {
        $modulePower_isRoot = in_array(Request::controller(), array('Install','AdminModule'));
        if (stristr(Request::controller(), 'Public')
            || stristr(Request::action(), 'Public')) {
            // 不用检查 TOKEN 通道
        } elseif (stristr(Request::module(), 'Admin')
            || stristr(Request::controller(), 'Admin')
            || $modulePower_isRoot) {
            $admin = Request::session(SESSION_AdminKey);
            !empty($admin) && is_array($admin) ?: \PKCore\alert('UserLogin_IdEmpty',null, 500,'/index.php/member/AdminPublicLogin');
            $model = $this->loginUser($admin);
            if ($modulePower_isRoot && $this->loginUser()->GroupId != 1) {
                \PKCore\alert('UserLogin_Power_Error');
            }
        }
    }

    /**
     * @param null $loginUser
     * @return LoginUserInfo
     */
    protected function loginUser($loginUser = null)
    {
        static $m;
        if (!is_null($loginUser)) {
            $m = new LoginUserInfo();
            $m->Id = self::GetArrayByKey('uid', $loginUser);
            $m->SiteId = self::GetArrayByKey('siteId', $loginUser);
            $m->UserName = self::GetArrayByKey('username', $loginUser);
            $m->GroupId = self::GetArrayByKey('groupid', $loginUser);
        }
        return $m;
    }

    protected function getItemByArrayKey(array $array)
    {
        $result = array();
        if (Formats::isArray($array)) {
            foreach ($array as $item) {
                $result[$item] = $item;
            }
        }
        return $result;
    }

}