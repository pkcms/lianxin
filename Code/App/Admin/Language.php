<?php
/**
 * 模块语言包
 * User: Administrator
 * Date: 2019/5/27
 * Time: 9:28
 */

return array(
    'Menu_NameEmpty' => array(
        'zh-cn' => '菜单名称不能为空',
        'en' => ''
    ),
    'Menu_NameExists' => array(
        'zh-cn' => '菜单名已经存在',
        'en' => ''
    ),
    'Menu_Form_ParentName' => array(
        'zh-cn' => '表单管理',
        'en' => ''
    ),
    'Menu_Other_ParentName' => array(
        'zh-cn' => '其他管理',
        'en' => ''
    ),
    'Menu_Other_ParentName_NoExists' => array(
        'zh-cn' => '系统级菜单名【其他管理】找不到了',
        'en' => ''
    ),
    'Site_LanguageEmpty' => array(
        'zh-cn' => '站点语言包不能为空',
        'en' => ''
    ),
    'Site_ThemesEmpty' => array(
        'zh-cn' => '站点模板主题不能为空',
        'en' => ''
    ),
    'Module_App_UpdateOK' => array(
        'zh-cn' => '系统的扩展模块更新完毕',
        'en' => ''
    ),
    'Upgrade_version_empty' => array(
        'zh-cn' => '没有检测到新的版本',
        'en' => ''
    ),
);