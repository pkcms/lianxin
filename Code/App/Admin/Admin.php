<?php


namespace PKApp\Admin;


use PKApp\Admin\Classes\AdminController;
use PKCore\Tpl;

class Admin extends AdminController
{

    public function Main()
    {
        $tpl = new Tpl();
        return $tpl->PhpDisplay('main');
    }
}