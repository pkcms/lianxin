<?php


namespace PKApp\Admin;


use PKApp\Admin\Classes\AdminController;
use PKApp\Admin\Classes\AdminUpgradeDataBase;
use PKCore\Converter;
use PKCore\Curl;
use PKCore\Files;
use PKCore\Formats;
use PKCore\Route;
use PKCore\Request;
use PKCore\Statics;
use PKCore\Tpl;
use PKCore\Zip;

class AdminUpgrade extends AdminController
{

    private $_savePath;

    public function Main()
    {
        $listArr = $this->_getUpgradeFileByNow();
        if (Request::has('step', 'get')) {
            list($step, $index) = explode('_', Request::get('step'));
            $result = array('step' => $step, 'index' => $index);
            if (method_exists($this, $step) && Formats::isArray($listArr)) {
                $item = self::GetArrayByKey($index, $listArr);
                $steps = json_decode(self::GetArrayByKey('step', $item));
                if (isset($steps->$step)) {
                    $res = $this->$step($steps->$step);
                    if (Formats::isArray($res)) {
                        $result = array_merge($result, $res);
                    }
                    ($step != 'delFile') ?: Files::putContents(PATH_DATA, 'version.log', self::GetArrayByKey('versionNum', $item));
                }
            }
            Statics::resultJsonModel($result);
        } else {
            $tpl = new Tpl();
            $tpl->SetTplParam('upgradeLists', $listArr);
            return $tpl->PhpDisplay('upgrade_do');
        }
    }

    /**
     * 获取服务器上的最新版本信息
     */
    public function GetVersion()
    {
        $version_service = '';
        $localVersion = \PKCore\Route\isLoadingFile(PATH_PK . 'version.log', true);
        try {
            // 如果没有生成今天的更新信息，则执行以下
            $curl = new Curl('http://cmsapi.zj11.net/Version/GetVersionList', 'get');
            if (!empty($localVersion)) {
                $curl->Param(array('version' => $localVersion));
            }
            $res = json_decode($curl->Request());
            if (isset($res->State) && $res->State) {
                if (isset($res->Result) && count($res->Result) > 0) {
                    Files::putContents($this->_getDir(), $this->_getJsonFile(), Converter::jsonEnCode($res));
                    $info = $res->Result[count($res->Result) - 1];
                    $version_service = (is_object($info) && isset($info->versionNum)) ? $info->versionNum : '';
                } else {
                    $version_service = Route\language('Upgrade_version_empty');
                }
            } else {
                throw new \Exception('get Version fail:' . var_export($res));
            }
            Statics::resultJsonModel(array(
                'version_service' => $version_service,
                'version_local' => $localVersion,
            ));
        } catch (\Exception $exception) {
            \PKCore\handlerException($exception);
        }
    }

    private function _getUpgradeFileByNow()
    {
        $res = Files::getContents($this->_getDir() . $this->_getJsonFile());
        if (empty($res)) {
            return array();
        } else {
            $arr = Converter::objectToArray(json_decode($res));
            return self::GetArrayByKey('Result', $arr);
        }
    }

    private function _getDir()
    {
        return PATH_TMP . 'Upgrade' . DS;
    }

    private function _getJsonFile()
    {
        return Converter::date('Ymd') . '.json';
    }

    protected function backup($task)
    {
        if (is_array($task)) {
            $path = PATH_TMP . 'Backup' . DS;
            $zip = new Zip();
            $date = Converter::date('YmdHis');
            foreach ($task as $task_path) {
                $task_path = str_replace('/', DS, $task_path);
                $fileName = str_replace(DS, '_', $task_path) . '_' . $date;
                $zip->dozip(PATH_ROOT . $task_path, $path, $fileName);
            }
        }
        return array('success' => true);
    }

    protected function download($path)
    {
        $url = 'http://cmsfile.zj11.net/' . $path;
        //创建保存目录
        Files::mkdir($this->_getDir());
        $arr = parse_url($url);
        $fileName = basename($arr['path']);
        //获取远程文件所采用的方法
        if (extension_loaded('curl')) {
            $ch = curl_init();
            $timeout = 5;
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
            $content = curl_exec($ch);
            curl_close($ch);
        } else {
            ob_start();
            readfile($url);
            $content = ob_get_contents();
            ob_end_clean();
        }
        //echo $content;
        $size = strlen($content);
        //文件大小
        $fp2 = @fopen($this->_getDir() . $fileName, 'a');
        fwrite($fp2, $content);
        fclose($fp2);
        unset($content, $url);
        return array(
            'success' => true,
            'file_name' => $fileName,
            'save_path' => $this->_getDir() . $fileName,
            'file_size' => $size
        );
    }

    protected function unZip($zipFileName)
    {
        $outPath = $this->_getDir() . $zipFileName;
        $zipFileName = $outPath . '.zip';
        $zip = new Zip();
        $zip->unZip($zipFileName, $outPath);
        return array('success' => true);
    }

    protected function updateCode($task)
    {
        if (Formats::isArray($task)) {
            foreach ($task as $item) {
                $arr = explode('=>', $item);
                $source = $this->_getDir() . str_replace('/', DS, $arr[0]);
                $dest = PATH_ROOT . str_replace('/', DS, $arr[1]);
                Files::copyDir($source, $dest);
            }
        }
        return array('success' => true);
    }

    protected function updateDB($task)
    {
        if (Formats::isArray($task)) {
            $db = new AdminUpgradeDataBase();
            foreach ($task as $item) {
                $path = $this->_getDir() . str_replace('/', DS, $item);
                $sql = Files::getContents($path);
                $sqlLists = explode(';', $sql);
                foreach ($sqlLists as $sqlStr) {
                    if (!empty($sqlStr)) {
                        $db->Sql($sqlStr);
                    }
                }
            }
            $db->BatchRun();
        }
        return array('success' => true);
    }

    protected function delFile($task)
    {
        if (Formats::isArray($task)) {
            foreach ($task as $item) {
                $path = PATH_ROOT . str_replace('/', DS, $item);
                Files::unlink($path);
            }
        }
        return array('success' => true);
    }

}