<?php


namespace PKApp\Admin;


use PKApp\Admin\Classes\AdminController;
use PKApp\Admin\Classes\AdminDataBase;
use PKCore\Formats;
use PKCore\Request;

class SetMenuOther extends AdminController
{

    public function Main()
    {
        $action = Request::post('action');
        if (method_exists($this, $action)) {
            $this->$action();
        }
    }

    protected function sort()
    {
        $post = Request::post();
        if (Formats::isArray($post)) {
            foreach ($post['selectData'] as $key => $value) {
                $data = array('orderIndex' => Formats::isNumeric($value));
                AdminDataBase::UpdateMenu($data, Formats::isNumeric($key));
            }
        }
        \PKCore\msg('Data_Input_Success');
    }

    protected function del()
    {
        $id = Formats::isNumeric(Request::post('selectData'));
        if (!empty($id)) {
            AdminDataBase::DelMenu($id);
        }
        \PKCore\msg('Data_Input_Success');
    }
}