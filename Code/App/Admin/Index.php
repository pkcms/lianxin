<?php

namespace PKApp\Admin;

use PKApp\Admin\Classes\AdminController;
use PKApp\Admin\Classes\AdminDataBase;
use PKApp\Admin\Classes\AdminExtend;
use PKApp\Content\Classes\ContentExtend;
use PKApp\Member\Classes\MemberDataBase;
use PKCore\Formats;
use PKCore\Tpl;

class Index extends AdminController
{

    public function Main()
    {
        $tpl = new Tpl();
        $siteId = $this->loginUser()->SiteId;
        $tpl->SetTplParam('userName', $this->loginUser()->UserName);
        $tpl->SetTplParam('groupid', $this->loginUser()->GroupId);
        if ($siteId > 0) {
            $site = AdminDataBase::GetSite(array('id' => $siteId), array('Site_Name'));
            $tpl->SetTplParam('home_url', self::ToMeUrl('content/site?id=' . $siteId));
            $tpl->SetTplParam('categoryListHtml',$this->_getCategoryList());
        }
        $tpl->SetTplParam('menu', $this->_getMenuList());
        return $tpl->PhpDisplay('index');
    }

    private function _getMenuList()
    {
        $groupEntity = MemberDataBase::GetGroup($this->loginUser()->GroupId);
        $menuList = AdminExtend::GetMenu(array('system', 'admin'), $groupEntity['menuRole']);
//        print_r($menuList); exit();
        return $menuList;
    }

    private function _getCategoryList()
    {
        $tpl = new Tpl();
        return $tpl->SetTplParam('lists', ContentExtend::AdminGetCategoryList($this->loginUser()->SiteId, null, true))
            ->SetTplDir('content')->PhpDisplay('category_sidenav');
    }
}
