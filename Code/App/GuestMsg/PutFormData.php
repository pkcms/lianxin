<?php


namespace PKApp\GuestMsg;

use PKApp\Content\Classes\BaseController;
use PKApp\GuestMsg\Classes\GuestMsgDataBase;
use PKApp\Model\Classes\FieldDateBase;
use PKCore\Converter;
use PKCore\Curl;
use PKCore\Fileter;
use PKCore\Formats;
use PKCore\Request;
use PKCore\Route;
use PKCore\TMPSave;

class PutFormData extends BaseController
{

    private $_isHyperNumber = false;
    private $_hyperNumber;

    /**
     * {
     * "userName":"abc",
     * "userPhone":"13012345678",
     * "content":"test"
     * }
     */
    public function Main()
    {
        $this->_checkIP();
        if ($this->_hyperNumber > 3) {
            $tnCode_check = (Request::session('tncode_check'));
            if (empty($tnCode_check) || ($tnCode_check != 'ok')) {
                \PKCore\msg(array('isHyperNumber' => $this->_isHyperNumber,
                    'msg' => Route\language('Auth_Error')));
            }
        }
        $post = $this->_checkPostData();
        $siteId = self::GetArrayByKey('siteId', $post);
        unset($post['siteId']);
        $this->_sendLX($post);
        $this->_sendEmail($post, $siteId);
        $this->_insetDB($post);
        \PKCore\msg(array('isHyperNumber' => $this->_isHyperNumber, 'msg' => 'ok'));
    }

    private function _checkPostData()
    {
        $post = Request::post();
        $checkList = array('userName', 'userPhone', 'content', 'pageTitle', 'pageUrl');
        foreach ($checkList as $item) {
            $value = self::GetArrayByKey($item, $post);
            if (empty($value)) {
                \PKCore\msg(array('isHyperNumber' => $this->_isHyperNumber,
                    'msg' => Route\language('Input') . Route\language($item)));
            }
        }
        return $post;
    }

    /**
     * 将提交的数据录入到数据库中
     * @param $input
     */
    private function _insetDB($input)
    {
        unset($input['pageTitle'], $input['pageUrl']);
        $db = new GuestMsgDataBase();
        $db->Insert($input)->Exec();
    }

    private function _sendEmail($input, $siteId)
    {
        if (isset($siteId)) {
            $this->getSiteTemplates($siteId);
        } else {
            return false;
        }
        $site_Contact = self::GetArrayByKey('Site_Contact', self::$siteEntity);
        if (empty($site_Contact)) {
            return false;
        }
        $toEmail = self::GetArrayByKey('email', $site_Contact);
        if (empty($toEmail)) {
            return false;
        }
        $field_GuestMsg = self::getDict('field_GuestMsg');
        $html = '';
        foreach ($input as $key => $value) {
            $field_name = self::GetArrayByKey($key, $field_GuestMsg);
            if (!empty($field_name)) {
                $html .= '<p>' . $field_name . ':' . htmlspecialchars_decode($value) . '</p>';
            }
        }
        $this->_mailBox($toEmail, Route\language('emailTitle'), $html);
    }

    private function _sendLX($input)
    {
        $input['domainName'] = Request::domain();
        $input['ip'] = Request::GuestIP();
        $url = 'https://func.zj11.net/msg';
        $curl = new Curl($url, 'json');
        $curl->TimeOut(180);
        $res = $curl->Param($input)->Request();
        $res_json = json_decode($res);
        if (isset($res_json->StateCode) && ($res_json->StateCode != 1) && isset($res_json->StateMsg)) {
            \PKCore\alert($res_json->StateMsg);
        }
    }

    /**
     * 邮件发送
     * @param $toEmail
     * @param $title
     * @param $body
     */
    private function _mailBox($toEmail, $title, $body)
    {
        $serverInfo = Request::server('COMPUTERNAME') . '|'
            . Request::server('SERVER_NAME') . ':' . Request::server('SERVER_PORT')
            . '|' . Request::server('PATH_TRANSLATED');
        $postArray = array('title' => $title, 'toMail' => $toEmail, 'mailBody' => $body,
            'fromDomain' => Request::domain(),
            'key' => Fileter::base64Encode($serverInfo, true, 'PKCMSCODE'));
        $curl = new Curl('https://func.zj11.net/mail', 'json');
        $result = $curl->Param($postArray)->Request();
//        print_r($result); exit();
    }

    public function _checkIP()
    {
        $ip = Request::GuestIP();
        $dirName = 'GuestMsg_IP';
        $fileName = date('Ymd') . '.json';
        $nowTime = time();
        $nowLog = TMPSave::get($fileName, $dirName);
        if (empty($nowLog) || !is_array($nowLog)) {
            $nowLog = array($ip => array($nowTime));
        } elseif (is_array($nowLog)) {
            $nowLog[$ip][] = $nowTime;
        }
        $this->_hyperNumber = count($nowLog[$ip]);
        $this->_isHyperNumber = $this->_hyperNumber >= 3;
        TMPSave::set($nowLog, $fileName, $dirName);
    }

}