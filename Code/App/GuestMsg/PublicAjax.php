<?php


namespace PKApp\GuestMsg;


use PKCore\Converter;
use PKCore\Request;
use PKCore\TMPSave;

class PublicAjax
{

    public function Main()
    {
        // 查询IP
        $ip = Request::GuestIP();
        $nowLog = TMPSave::get(date('Ymd') . '.json', 'GuestMsg_IP');
        if (!empty($nowLog) && is_array($nowLog) && array_key_exists($ip, $nowLog)) {
            $isHyperNumber = count($nowLog[$ip]) >= 3;
        } else {
            $isHyperNumber = false;
        }
        \PKCore\msg(array('isHyperNumber' => $isHyperNumber));
    }

}