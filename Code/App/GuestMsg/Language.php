<?php
/**
 * 模块语言包
 * User: Administrator
 * Date: 2019/5/27
 * Time: 9:28
 */

return array(
    'App_Error' => array(
        'zh-cn' => '访客留言没有正确安装，请重新安装',
        'en' => ''
    ),
    'Auth_Error' => array(
        'zh-cn' => '未通过验证',
        'en' => ''
    ),
    'Guest' => array(
        'zh-cn' => '未知访客',
        'en' => ''
    ),
    'PostError' => array(
        'zh-cn' => '您当前的提交频率太过勤快，请休息一下！',
        'en' => ''
    ),
    'PostOk' => array(
        'zh-cn' => '提交成功',
        'en' => ''
    ),
    'Input' => array(
        'zh-cn' => '请输入',
        'en' => ''
    ),
    'userName' => array(
        'zh-cn' => '您的称呼！',
        'en' => ''
    ),
    'userPhone' => array(
        'zh-cn' => '您的联系号码！',
        'en' => ''
    ),
    'content' => array(
        'zh-cn' => '您的询盘内容！',
        'en' => ''
    ),
    'emailTitle' => array(
        'zh-cn' => '您的公司网站上有访客提交询盘，请尽快查阅',
        'en' => ''
    ),
);