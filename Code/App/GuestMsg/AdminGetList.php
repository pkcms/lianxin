<?php


namespace PKApp\GuestMsg;

use PKApp\Admin\Classes\AdminController;
use PKApp\GuestMsg\Classes\GuestMsgDataBase;
use PKCore\Statics;
use PKCore\Tpl;

class AdminGetList extends AdminController
{

    public function Main()
    {
        $db = new GuestMsgDataBase();
        self::GetPages();
        $db_res = $db->Limit(self::$pageSize, self::$pageIndex)->Select()->ToList();
        $db_count = $db->Count();
        $tpl = new Tpl();
        $pageNav = Statics::pages($db_count, self::$pageSize, self::$pageIndex,
            '/index.php/GuestMsg/AdminGetList?pageIndex=[page]');
        return $tpl->SetTplParam('pageList', $pageNav)->SetTplParam('lists', $db_res)->Display('list');
    }
}