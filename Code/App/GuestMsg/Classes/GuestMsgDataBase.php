<?php


namespace PKApp\GuestMsg\Classes;


use PKCore\DataBase;
use PKCore\Route;

class GuestMsgDataBase extends DataBase
{

    public function __construct()
    {
        parent::__construct('pk_guest_msg');
        try {
            $isExists = $this->CheckTableNameExists();
            if (!$isExists) {
                throw new \Exception(Route\language('App_Error'));
            }
        } catch (\Exception $exception) {
            \PKCore\handlerException($exception);
        }
    }

}