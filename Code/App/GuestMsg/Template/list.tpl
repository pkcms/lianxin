<div class="container-fluid row">
    <ol class="breadcrumb">
        <li class="glyphicon glyphicon-edit">访客留言列表</li>
    </ol>
</div>

<div class="container-fluid">
    <table class="table table-striped table-condensed table-hover table-bordered table-responsive">
        <{loop $lists $item}>
        <tbody>
        <tr>
            <th>访客称呼：<{$item[userName]}></th>
            <td rowspan="2">
                留言内容：<{$item[content]}>
            </td>
        </tr>
        <tr>
            <th>联系方式：<{$item[userPhone]}></th>
        </tr>
        </tbody>
        <{/loop}>
    </table>
</div>
<div>

    <ul class="pagination mp0 fr">
        <{loop $pageList $key $pageItem}>
        <{if $key == 'dataCount'}>
        <li>
            <a href="#">总条数：<{$pageItem}></a>
        </li>
        <{elseif $key == 'pageCount'}>
        <li>
            <a href="#">总页数：<{$pageItem}></a>
        </li>
        <{elseif $key == 'index'}>
        <li>
            <a href="#">当前第 <b><{$pageItem}></b></a>
        </li>
        <{elseif $key == 'sizeList'}>
        <{loop $pageItem $i $sizeUrl}>
        <li>
            <a href="<{$sizeUrl}>" data-ajax="true" data-ajax-mode="replace"
               data-ajax-update="#main"><{$i}></a>
        </li>
        <{/loop}>
        <{else}>
        <li>
            <a href="<{$pageItem}>" data-ajax="true" data-ajax-mode="replace"
               data-ajax-update="#main"><{$key}></a>
        </li>
        <{/if}>
        <{/loop}>
    </ul>
</div>