<?php


namespace PKApp\Tools;


use PKApp\Content\Classes\BaseController;
use PKCore\Files;
use PKCore\Request;

class DownLoad extends BaseController
{

    public function Main()
    {
        $filePath = rtrim(PATH_ROOT, DS) . Request::get('file');
        file_exists($filePath) ?: \PKCore\alert('DownLoad_FileError');
        Files::download(str_replace('/', DS, $filePath));
    }

}