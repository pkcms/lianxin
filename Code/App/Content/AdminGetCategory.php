<?php
/**
 * 栏目的编辑（与添加）表单的处理
 */

namespace PKApp\Content;


use PKApp\Admin\Classes\AdminController;
use PKApp\Content\Classes\CategoryDataBase;
use PKApp\Content\Classes\ContentDataBase;
use PKApp\Content\Classes\ContentExtend;
use PKApp\Member\Classes\MemberDataBase;
use PKApp\Model\Classes\ModelDatabase;
use PKApp\Template\Classes\TemplateExists;
use PKCore\Converter;
use PKCore\Formats;
use PKCore\Request;
use PKCore\Tpl;

class AdminGetCategory extends AdminController
{
    private $_templateDir = 'content';

    public function Main()
    {
        if (Request::has('id', 'get', false)) {
            $id = Formats::isNumeric(Request::get('id'));
            return $this->_getDetail($id);
        } elseif (Request::has('parent', 'get', false)) {
            $parentId = Formats::isNumeric(Request::get('parent'));
            return $this->_getParent($parentId);
        } elseif (Request::has('type', 'get', false)) {
            $type = Formats::isNumeric(Request::get('type'));
            return $this->_handlerForm($type);
        }
        return '';
    }

    private function _getDetail($id)
    {
        $entity = $this->_db()->GetCategoryDetail($id);
        return $this->_handlerForm($entity['type'], $entity);
    }

    private function _getParent($parentId)
    {
        $entityParent = $this->_db()->GetCategoryDetail($parentId);
        $entity = array('parentid' => $parentId);
        $delFieldArr = array('id', 'name', 'subname', 'seo', 'url', 'listsort', 'total', 'arrparentid', 'arrchildid',
            'image', 'parentid');
        foreach ($delFieldArr as $item) {
            if (is_array($entityParent) && array_key_exists($item, $entityParent)) {
                unset($entityParent[$item]);
            }
        }
        !Formats::isArray($entityParent) ?: $entity = array_merge($entity, $entityParent);
        return $this->_handlerForm($entity['type'], $entity);
    }

    private function _db()
    {
        static $db;
        !empty($db) ?: $db = new CategoryDataBase();
        return $db;
    }

    /**
     * 根据栏目的不同类型呈现不同的表单
     * @param $type
     * @param null $entity
     * @return false|string
     */
    private function _handlerForm($type, $entity = null)
    {
        $tpl = new Tpl();
        switch ($type) {
            case 1:
                $tplList = TemplateExists::GetFileList($this->loginUser()->SiteId, $this->_templateDir, 'page');
                $pageTplList = $this->getItemByArrayKey($tplList);
                $tpl->SetTplParam('pageTplList', $pageTplList);
                break;
            case 2:
                // 列表类型
                $db_model = new ModelDatabase();
                $modelEntityList = $db_model->GetLists(0);
                if (Formats::isArray($modelEntityList)) {
                    $modelEntityList = Converter::arrayColumn($modelEntityList, 'name', 'id');
                } else {
                    $modelEntityList = array();
                }
                $userGroup = MemberDataBase::GetGroupLists(array('id', 'name'), array('modelid' => 2));
                if (Formats::isArray($userGroup)) {
                    $userGroup = Converter::arrayColumn($userGroup, 'name', 'id');
                } else {
                    $userGroup = array();
                }
                $orderList = self::getDict('category_content_orderList');
                if (Formats::isArray($orderList)) {
                    $orderList = Converter::arrayColumn($orderList, 'name', 'contrary');
                }
                $categoryTplList = TemplateExists::GetFileList($this->loginUser()->SiteId, $this->_templateDir, 'category');
                $showTplList = TemplateExists::GetFileList($this->loginUser()->SiteId, $this->_templateDir, 'show');
                $tpl->SetTplParam('modelList', $modelEntityList)
                    ->SetTplParam('UserGroup', $userGroup)
                    ->SetTplParam('categoryTplList', $this->getItemByArrayKey($categoryTplList))
                    ->SetTplParam('showTplList', $this->getItemByArrayKey($showTplList))
                    ->SetTplParam('category_content_orderList', $orderList)
                    ->SetTplParam('contentList_showMode', self::getDict('contentList_showMode'));
                break;
        }
        return $tpl->SetTplParamList($entity)->SetTplParam('type', $type)
            ->SetTplParam('groupid', $this->loginUser()->GroupId)
            ->SetTplParam('selectParentHtml', ContentExtend::GetCategoryOptionList($this->loginUser()->SiteId, empty($entity) ? null : $entity['parentid']))
            ->SetTplParam('IsOrNot', self::getDict('IsOrNot'))
            ->SetTplParam('contentCategory_typeList', self::getDict('contentCategory_typeList'))
            ->Display('category_form_' . $type);
    }

}