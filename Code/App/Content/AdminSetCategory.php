<?php


namespace PKApp\Content;


use PKApp\Admin\Classes\AdminController;
use PKApp\Content\Classes\CategoryDataBase;
use PKApp\Content\Classes\ContentDataBase;
use PKApp\Content\Classes\ContentExtend;
use PKCore\Formats;
use PKCore\Request;

class AdminSetCategory extends AdminController
{
    private $_entityList;

    public function Main()
    {
        $type = Request::post('type');
        $this->checkPostFieldIsEmpty('name', 'Category_NameEmpty');
        $post = Request::post();
        $this->_entityList = $this->_db()->GetListByAdmin($this->loginUser()->SiteId, null,
            !empty($post['id']) ? array($post['id']) : null);
        switch ($type) {
            case 0:
                $this->_typeBYLink($post);
                break;
            case 1:
                $this->_typeBYPage($post);
                break;
            case 2:
                $this->_typeBYList($post);
                break;
        }
    }

    private function _typeBYLink($post)
    {
        $this->_postHandler($post);
    }

    private function _typeBYPage($post)
    {
        if (is_array($post)) {
            if (array_key_exists('template', $post) && is_array($post['template'])) {
                $post['template'] = serialize($post['template']);
            }
        }
        $this->_postHandler($post);
    }

    private function _typeBYList($post)
    {
        $this->checkPostFieldIsEmpty('modelid', 'Model_idEmpty');
        if (is_array($post)) {
            if (array_key_exists('seo', $post) && is_array($post['seo'])) {
                $post['seo'] = serialize($post['seo']);
            }
            if (array_key_exists('template', $post) && is_array($post['template'])) {
                $post['template'] = serialize($post['template']);
            }
            if (array_key_exists('setting', $post) && is_array($post['setting'])) {
                $post['setting'] = serialize($post['setting']);
            }
            if (array_key_exists('inherit', $post)) {
                unset($post['inherit']);
            }
        }
        $this->_postHandler($post);
    }

    private function _postHandler($post)
    {
        $id = Formats::isNumeric(Request::post('id'));
        $fromParentId = $post['fromParentId'];
        unset($post['fromParentId']);
        if (!empty($post['parentid'])) {
            $arrParentId = $this->_treeParent($post['parentid']);
            $post['arrparentid'] = Formats::isArray($arrParentId) ? implode(',', $arrParentId) : '';
        } else {
            $post['arrparentid'] = null;
        }
        if (empty($id)) {
            $post['siteId'] = $this->loginUser()->SiteId;
        } else {
            $arrChildId = $this->_treeChild($id);
            $post['arrchildid'] = Formats::isArray($arrChildId) ? implode(',', $arrChildId) : '';
        }
        $post['id'] = $this->_db()->SetCategoryDetail($post);
        // 将新修改的栏目信息追加到现在的列表中
        $this->_entityList[] = $post;
        if (!empty($post['parentid'])) {
            // 父级栏目更新其下的子栏目
            $parentArrChildId = $this->_treeChild($post['parentid']);
            $parentPost = array('arrchildid' => Formats::isArray($parentArrChildId) ? implode(',', $parentArrChildId) : '');
            $this->_db()->SetCategoryDetailById($parentPost, $post['parentid']);
            // 父级栏目的上上级栏目
            $parentArrParent = $this->_treeParent($post['parentid']);
            if (count($parentArrParent) > 0) {
                foreach ($parentArrParent as $item) {
                    if ($item != $post['parentid']) {
                        $parentArrChildId = $this->_treeChild($item);
                        $parentPost = array('arrchildid' => Formats::isArray($parentArrChildId) ? implode(',', $parentArrChildId) : '');
                        $this->_db()->SetCategoryDetailById($parentPost, $item);
                    }
                }
            }
        }
        if (!empty($fromParentId) && ($fromParentId != $post['parentid'])) {
            // 原来父级栏目更新其下的子栏目
            $fromParentArrChildId = $this->_treeChild($fromParentId);
            $fromParentPost = array('arrchildid' => Formats::isArray($fromParentArrChildId) ? implode(',', $fromParentArrChildId) : '');
            $this->_db()->SetCategoryDetailById($fromParentPost, $fromParentId);
            // 原来父级栏目的上上级栏目
            $parentArrParent = $this->_treeParent($fromParentId);
            if (count($parentArrParent) > 0) {
                foreach ($parentArrParent as $item) {
                    if ($item != $fromParentId) {
                        $parentArrChildId = $this->_treeChild($item);
                        $parentPost = array('arrchildid' => Formats::isArray($parentArrChildId) ? implode(',', $parentArrChildId) : '');
                        $this->_db()->SetCategoryDetailById($parentPost, $item);
                    }
                }
            }
        }
        \PKCore\msg('Data_Input_Success');
    }

    private function _db()
    {
        static $db;
        !empty($db) ?: $db = new CategoryDataBase();
        return $db;
    }

    private function _treeParent($id)
    {
        $result = array();
        if ($id == 0) {
            return $result;
        }
        $nowChild = array_filter($this->_entityList, function ($entity) use ($id) {
            return $entity['id'] == $id ? $entity : array();
        });
        if (Formats::isArray($nowChild)) {
            foreach ($nowChild as $item) {
                $tmp = $this->_treeParent($item['parentid']);
                !Formats::isArray($tmp) ?: $result = array_merge($result, $tmp);
                $result[] = $item['id'];
            }
        }
        return $result;
    }

    private function _treeChild($parentId)
    {
        $result = array();
        $nowChild = array_filter($this->_entityList, function ($entity) use ($parentId) {
            return $entity['parentid'] == $parentId ? $entity : array();
        });
        if (Formats::isArray($nowChild)) {
            foreach ($nowChild as $item) {
                $tmp = $this->_treeChild($item['id']);
                !Formats::isArray($tmp) ?: $result = array_merge($result, $tmp);
                $result[] = $item['id'];
            }
        }
        return $result;
    }
}