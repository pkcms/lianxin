<?php


namespace PKApp\Content;


use PKApp\Admin\Classes\AdminController;
use PKApp\Admin\Classes\AdminDataBase;
use PKApp\Admin\Classes\AdminExtend;
use PKApp\Antispam\Classes\Antispam;
use PKApp\Content\Classes\ContentExtend;
use PKApp\Content\Classes\ContentsDataBase;
use PKApp\Content\Classes\ContentUrlFormat;
use PKApp\Model\Classes\ModelExtend;
use PKApp\Model\Classes\ToInsertData;
use PKCore\Fileter;
use PKCore\Formats;
use PKCore\PKController;
use PKCore\Request;
use PKCore\Statics;

class AdminSetContent extends AdminController
{

    public function Main()
    {
        $this->checkPostFieldIsEmpty('title', 'Content_titleEmpty');
        $this->checkPostFieldIsEmpty('catid', 'Category_idEmpty');
        $post = Request::post();
        $this->_antispam($post);
        $catId = Request::post('catid');
        $categoryEntity = ContentExtend::GetCategory($catId);
        $tableName = ModelExtend::GetTableName($categoryEntity['modelid']);
        $insertHandler = new ToInsertData();
        $post = $insertHandler->Get($post, $categoryEntity['modelid']);
        $id = Request::post('id');
        // 同时发布到其他栏目
        if (isset($post['othors'])) {
            $othors = $post['othors'];
            unset($post['othors']);
        }
        !empty($post['seotitle']) ?: $post['seotitle'] = $post['title'];
        !empty($post['seokeywords']) ?: $post['seokeywords'] = $post['title'];
        if (empty($post['seodescription'])) {
            if (isset($post['content'])) {
                $content = $post['content'];
            } elseif (isset($post['data']['content'])) {
                $content = $post['data']['content'];
            } else {
                $content = $post['title'];
            }
            $post['seodescription'] = mb_substr(Statics::zip(Fileter::filterValue(
                Fileter::htmlspecialchars_decode($content), true
            )), 0, 70, 'utf-8').'……';
        }
        $post['modelid'] = $categoryEntity['modelid'];
        $post['inputtime'] = empty($post['inputtime']) ? time() : strtotime($post['inputtime']);
        $post['updatetime'] = time();
        $post['modelid'] = $categoryEntity['modelid'];
        $post['status'] = 1;
        $post['siteid'] = $this->loginUser()->SiteId;
        $db_content = new ContentsDataBase($categoryEntity['modelid']);
        empty($id) ? $id = $db_content->AddContent($post) : $db_content->UpdateContentAndData($post, $id);
        //同时发布到其他栏目
        if (isset($othors)) {
            $this->_addOtherCategroy($othors, $categoryEntity, $post, $id);
        }
        \PKCore\msg('Data_Input_Success', '/index.php/content/AdminGetContentList?catId=' . $catId);
    }

    private function _antispam($post)
    {
        $db_site = AdminExtend::GetSiteInfo($this->loginUser()->SiteId);
        $setting = PKController::GetArrayByKey('setting', $db_site);
        if ((bool)PKController::GetArrayByKey('isAntispam', $setting)) {
            $antispam = new Antispam();
            $tips = $antispam->CheckContent($post);
            empty($tips) ?: \PKCore\alert('', $tips);
        }
    }

    /**
     * 将动态内容同时发布到其他栏目
     * @param $categoryIdList
     * @param $categoryEntity
     * @param $contentEntity
     * @param $contentId
     * @return string
     */
    private function _addOtherCategroy($categoryIdList, $categoryEntity, $contentEntity, $contentId)
    {
        if (!Formats::isArray($categoryIdList)) {
            return false;
        }

        $siteEntity = AdminDataBase::GetSite(array('id' => $this->loginUser()->SiteId), array('Site_Path'));
        $contentEntity['url'] =  ContentUrlFormat::Content($categoryEntity, $categoryEntity['template'], $contentEntity, $siteEntity['Site_Path']);

        if (isset($contentEntity['data'])) {
            unset($contentEntity['data']);
        }
        foreach ($categoryIdList as $catId) {
            $contentEntity['catid'] = Formats::isNumeric($catId);
            $categoryEntity = ContentExtend::GetCategory($catId);
            $contentEntity['modelid'] = $categoryEntity['modelid'];
            $db_content = new ContentsDataBase($categoryEntity['modelid']);
            $db_content->AddContent($contentEntity);
        }
        return true;
    }

}