<?php


namespace PKApp\Content;


use PKApp\Content\Classes\BaseController;
use PKCore\Formats;
use PKCore\Request;

class Site extends BaseController
{

    public function Main()
    {
        $siteId = Formats::isNumeric(Request::get('id'));
        $this->getSiteTemplates($siteId);
        if (!Request::has('perView','get') && file_exists(PATH_ROOT.'index.htm')) {
            header('location:index.htm');
        }
        return $this->tplDisplay('index');
    }
}