<{admin_breadcrumb('内容管理')}>

<!-- Title -->
<div class="page-header">
    <div class="page-title">
        <h3>单页面-内容编辑</h3>
    </div>
</div>
<!-- /Title -->
<!-- Row -->
<div class="row">
    <div class="col-md-12">
        <form id="pageForm" class="form-horizontal row-border" action="content/AdminSetPage">
            <div class="widget box">
                <div class="widget-header">
                    <h4>
                        <i class="icon-reorder"></i>
                        基本填写
                    </h4>
                </div>
                <div class="widget-content">
                    <div class="form-group">
                        <label class="col-md-2 control-label">
                            页面标题:
                        </label>
                        <div class="col-md-10">
                            <input type="text" class="form-control" id="title" name="title" value="<{$title}>"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label">
                            内容:
                        </label>
                        <div class="col-md-10">
                            <{form_editor("content","editor",$content)}>
                        </div>
                    </div>
                </div>
            </div>
            <div class="widget box">
                <div class="widget-header">
                    <h4>
                        <i class="icon-reorder"></i>
                        SEO 设置
                    </h4>
                </div>
                <div class="widget-content">
                    <tr>
                        <th></th>
                        <td>
                        </td>
                    </tr>
                    <div class="form-group">
                        <label class="col-md-2 control-label">
                            SEO 标题:
                        </label>
                        <div class="col-md-10">
                            <input type="text" name="seotitle" class="form-control" value="<{$seotitle}>"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label">
                            SEO 关键词:
                        </label>
                        <div class="col-md-10">
                            <textarea name="seokeywords" class="form-control"><{$seokeywords}></textarea>
                            <p class="help-block">
                                多词之间使用英文逗号（,）分隔
                            </p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label">
                            SEO 简述:
                        </label>
                        <div class="col-md-10">
                            <textarea name="seodescription" class="form-control"><{$seodescription}></textarea>
                        </div>
                    </div>
                </div>
            </div>
            <input type="hidden" name="id" value="<{$catId}>"/>
            <input type="hidden" name="type" value="page"/>
            <div class="form-actions">
                <button type="button" data-form="pageForm" class="btn btn-primary pull-left">
                    提交保存
                </button>
            </div>
        </form>
    </div>
</div>
<!-- /Row -->
