<?php
if (!function_exists('htmlForeachOption')) {
    function htmlForeachOption($lists, $valueId, $level = 0)
    {
        if (is_array($lists) && (count($lists) > 0)) {
            foreach ($lists as $item) {
                ?>
                <option value="<?php echo $item['id'] ?>"
                    <?php if ($item['id'] == $valueId) { ?> selected="selected"<?php } ?>>
                    <?php for ($i = 0; $i < $level; $i++) {
                        echo '&nbsp&nbsp;';
                    } ?><?php echo($item['name']) ?></option>
                <?php if (is_array($item['child']) && (count($item['child']) > 0)) {
                    htmlForeachOption($item['child'], $valueId, $level + 1);
                }
            }
        }
    }
}

if (isset($lists)) {
    htmlForeachOption($lists, isset($valueId) ? $valueId : null);
}
?>