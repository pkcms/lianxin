<?php
function eachCategory($categoryList)
{
    if (is_array($categoryList)) {
        foreach ($categoryList as $index => $category) {
            ?>
            <li>
                <a href="<?php echo $category['url']; ?>" title="<?php echo $category['name']; ?>">
                    <?php echo $category['name']; ?></a>
                <?php
                if (isset($category['child'])) {
                    ?>
                    <ul>
                        <?php
                        eachCategory($category['child']);
                        ?>
                    </ul>
                    <?php
                }
                ?>
            </li>
            <?php
        }
    }
}

?>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>站点地图</title>
    <link href="/statics/bootstrap-3.3.7-dist/css/bootstrap.css" rel="stylesheet" type="text/css"/>
    <link href="/statics/bootstrap-3.3.7-dist/css/bootstrap-theme.min.css" rel="stylesheet" type="text/css"/>
</head>
<body>
<div class="container-fluid">
    <p>&nbsp;</p>
    <div class="panel panel-primary">
        <div class="panel-heading">
            栏目：
        </div>
        <div class="panel-body">
            <ul class="list-unstyled">
                <?php eachCategory(isset($categoryList) ? $categoryList : array()); ?>
            </ul>
        </div>
    </div>
    <?php
    if (isset($modelList) && is_array($modelList)) {
        foreach ($modelList as $model) {
            ?>
            <div class="panel panel-info">
                <div class="panel-heading">
                    <?php echo $model['name'] ?>：
                </div>
                <div class="panel-body">
                    <table class="table table-striped table-hover">
                        <?php
                        if (isset($model['lists']) && is_array($model['lists'])) {
                            foreach ($model['lists'] as $content) {
                                ?>
                                <tr>
                                    <td>
                                        <a href="<?php echo $content['url']; ?>"
                                           title="<?php echo $content['title']; ?>">
                                            <?php echo $content['title']; ?></a>
                                    </td>
                                </tr>
                                <?php
                            }
                        }
                        ?>
                    </table>
                </div>
            </div>
            <?php
        }
    }
    ?>
</div>
</body>
</html>