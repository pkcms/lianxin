<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                &times;
            </button>
            <h4 class="modal-title">
                选择要同时发布到哪些栏目
            </h4>
        </div>
        <div class="modal-body">
            <table id="toOtherCategoryList" class="table table-striped table-condensed table-hover table-bordered table-responsive">
                <tbody>
                <{$tplHtml}>
                </tbody>
            </table>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">
                取消
            </button>
            <button type="button" class="btn btn-primary" data-dismiss="modal" onclick="toOtherCategory('<{$showField}>');">
                选好
            </button>
        </div>
    </div>
</div>
