<{admin_breadcrumb('内容管理')}>


<!-- Title -->
<div class="page-header">
    <div class="page-title">
        <h3>
            内容编辑
        </h3>
    </div>
</div>
<!-- /Title -->
<!-- Row -->
<div class="row">
    <div class="col-md-12">
        <form id="contentForm" class="form-horizontal row-border" action="content/AdminSetContent">

            <div class="tabbable tabbable-custom">
                    <ul class="nav nav-tabs">
                        <li class="active">
                            <a href="#tab_1_1" data-toggle="tab">
                                基本填写
                            </a>
                        </li>
                        <li>
                            <a href="#tab_1_2" data-toggle="tab">
                                SEO 设置
                            </a>
                        </li>
                        <{if $groupid < 3}>
                        <li>
                            <a href="#tab_1_3" data-toggle="tab">
                                可选设置
                            </a>
                        </li>
                        <{/if}>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active" id="tab_1_1">
                            <div class="widget box">
                                <div class="widget-header">
                                    <h4>
                                        <i class="icon-reorder"></i>
                                        基本填写
                                    </h4>
                                </div>
                                <div class="widget-content">
                                    <div class="form-group">
                                        <label class="col-md-2 control-label">
                                            所属栏目:
                                        </label>
                                        <div class="col-md-4">
                                            <{$category['name']}>
                                            <input type="hidden" name="catid" value="<{$category['id']}>">
                                        </div>
                                        <label class="col-md-2 control-label">
                                            <a class="btn btn-success btn-sm" data-toggle="modal" data-target="#myModal"
                                               href="/index.php/content/AdminGetCategoryList/selectCategory?catid=<{$category['id']}>&showField=addto">
                                                <i class="icon-plus-sign-alt"></i>
                                                其他栏目
                                            </a>
                                        </label>
                                        <div class="col-md-4">
                                            <ul id="addto"></ul>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-2 control-label">
                                            标题:
                                        </label>
                                        <div class="col-md-10">
                                            <input type="text" name="title" class="form-control" value="<{$title}>"/>
                                        </div>
                                    </div>
                                    <{loop $fieldList $name $item}>
                                    <div class="form-group">
                                        <label class="col-md-2 control-label">
                                            <{$item[name]}>:
                                        </label>
                                        <div class="col-md-10">
                                            <{$item[htmlcode]}>
                                        </div>
                                    </div>
                                    <{/loop}>
                                    <div class="form-group">
                                        <label class="col-md-2 control-label">
                                            缩略图:
                                        </label>
                                        <div class="col-md-10">
                                            <{UploadFile('image',$thumb,'thumb')}>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="tab_1_2">
                            <div class="widget box">
                                <div class="widget-header">
                                    <h4>
                                        <i class="icon-reorder"></i>
                                        SEO 设置
                                    </h4>
                                </div>
                                <div class="widget-content">
                                    <div class="form-group">
                                        <label class="col-md-2 control-label">
                                            SEO 标题:
                                        </label>
                                        <div class="col-md-10">
                                            <input type="text" name="seotitle" class="form-control" value="<{$seotitle}>"/>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-2 control-label">
                                            SEO 关键字:
                                        </label>
                                        <div class="col-md-10">
                                            <textarea name="seokeywords" class="form-control"><{$seokeywords}></textarea>
                                            <p class="help-block">
                                                多词之间使用英文逗号（,）分隔
                                            </p>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-2 control-label">
                                            SEO 简述:
                                        </label>
                                        <div class="col-md-10">
                                            <textarea name="seodescription" class="form-control"><{$seodescription}></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <{if $groupid < 3}>
                        <div class="tab-pane" id="tab_1_3">
                            <div class="widget box">
                                <div class="widget-header">
                                    <h4>
                                        <i class="icon-reorder"></i>
                                        扩展属性
                                    </h4>
                                </div>
                                <div class="widget-content">
                                    <div class="form-group">
                                        <label class="col-md-2 control-label">
                                            录入时间:
                                        </label>
                                        <div class="col-md-10">
                                            <input type="text" id="inputtime" name="inputtime" class="form-control selectDate" value="<{$inputtime}>"/>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-2 control-label">
                                            模板:
                                        </label>
                                        <div class="col-md-10">
                                            <{optionBySelct('template', $showTplList, $template)}>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-2 control-label">
                                            URL规则:
                                        </label>
                                        <div class="col-md-10">
                                            <input type="text" name="url" class="form-control" value="<{$url}>"/>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <{/if}>
                    </div>
                </div>

            <input type="hidden" name="id" value="<{$id}>"/>
            <div class="form-actions">
                <button type="button" data-form="contentForm" class="btn btn-primary pull-left">
                    提交保存
                </button>
            </div>
        </form>
    </div>
</div>
<!-- /Row -->
