<?php
function htmlForeach($lists)
{

    if (is_array($lists)) {
        ?>
        <?php
        foreach ($lists as $item) {
            if ($item['type'] == 1) {
                $href = '/index.php/content/AdminGetPage?id=' . $item['id'];
                $dataHtml = 'data-ajax="true" data-ajax-begin="beginPaging" data-ajax-failure="failurePaging"
                       data-ajax-mode="replace" data-ajax-success="successPaging" data-ajax-update="#main""';
            } elseif ($item['type'] == 2) {
                if (!(is_array($item['child']) && count($item['child']) > 0)) {
                    $href = '/index.php/content/AdminGetContentList?catId=' . $item['id'];
                } else {
                    $href = '/index.php/content/AdminGetCategoryList?parentId=' . $item['id'];
                }
                $dataHtml = 'data-ajax="true" data-ajax-begin="beginPaging" data-ajax-failure="failurePaging" data-ajax-mode="replace" data-ajax-success="successPaging" data-ajax-update="#main"';
            } else {
                if (!(is_array($item['child']) && count($item['child']) > 0)) {
                    $href = '#';
                    $dataHtml = '';
                } else {
                    $href = '/index.php/content/AdminGetCategoryList?parentId=' . $item['id'];
                    $dataHtml = 'data-ajax="true" data-ajax-begin="beginPaging" data-ajax-failure="failurePaging" data-ajax-mode="replace" data-ajax-success="successPaging" data-ajax-update="#main"';
                }
            }
            if (is_array($item['child'] || (count($item['child']) > 0))) {
                $href = '/index.php/content/AdminGetCategoryList?parentId=' . $item['id'];
            }
            ?>
            <li>
                <a <?php echo $dataHtml; ?> href="<?php echo $href; ?>">
                    <?php echo($item['name']);
                    if ($item['total']) { ?>
                        <span class="label label-info pull-right"><?php echo $item['total'] ?></span>
                    <?php } ?>
                </a>
            </li>
        <?php } ?>
        <?php
    }
}

if (isset($lists)) {
    htmlForeach($lists);
}
?>
