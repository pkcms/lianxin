<{admin_breadcrumb('内容管理')}>

<!-- Title -->
<div class="page-header">
    <div class="page-title">
        <h3>
            <{if $parent}>
            <{loop $parent $item}>
            <{$item[name]}>&nbsp;-&nbsp;
            <{/loop}>
            <{/if}>
            <{$category[name]}>&nbsp;-&nbsp;内容列表
        </h3>
    </div>
</div>
<!-- /Title -->
<!-- Row -->
<div class="row">
    <div class="col-md-12">
        <div class="widget">
            <div class="widget-content">
                <div class="btn-toolbar btn-toolbar-demo">
                    <a class="btn btn-success" data-ajax="true" data-ajax-begin="beginPaging"
                       data-ajax-failure="failurePaging"
                       data-ajax-mode="replace" data-ajax-success="successPaging" data-ajax-update="#main"
                       href="/index.php/content/AdminGetContent?catId=<{$category[id]}>">
                        <i class="icon-plus-sign"></i>&nbsp;添加内容
                    </a>
                    <button class="btn btn-danger" data-action="del" data-table="contentList">
                        <i class="icon-remove-circle"></i>
                        删除
                    </button>
                    <button class="btn btn-default" data-action="sort" data-table="contentList">
                        排序
                    </button>
                </div>
            </div>
        </div>
    </div>

    <div class="col-md-12">
        <div class="widget box">
            <div class="widget-header">
                <h4>
                    <i class="icon-reorder"></i>
                    内容列表
                </h4>
                <div class="toolbar no-padding">
                    <div class="btn-group">
                        <!-列表筛选方式 start-->
                        <span class="btn btn-xs dropdown-toggle" data-toggle="dropdown">
                          筛选
                          <i class="icon-angle-down"></i>
                        </span>
                        <ul class="dropdown-menu pull-right">
                            <li>
                                <a data-ajax="true" data-ajax-begin="beginPaging"
                                   data-ajax-failure="failurePaging"
                                   data-ajax-mode="replace" data-ajax-success="successPaging"
                                   data-ajax-update="#main"
                                   href="/index.php/content/AdminGetContentList?catId=<{$category[id]}>&filter=all">
                                    全部
                                </a>
                            </li>
                            <{loop $attributeList $key $item}>
                            <li>
                                <a data-ajax="true" data-ajax-begin="beginPaging"
                                   data-ajax-failure="failurePaging"
                                   data-ajax-mode="replace" data-ajax-success="successPaging"
                                   data-ajax-update="#main"
                                   href="/index.php/content/AdminGetContentList?catId=<{$category[id]}>&filter=<{$key}>">
                                    <{$item}>
                                </a>
                            </li>
                            <{/loop}>
                            <li>
                                <a data-ajax="true" data-ajax-begin="beginPaging"
                                   data-ajax-failure="failurePaging"
                                   data-ajax-mode="replace" data-ajax-success="successPaging"
                                   data-ajax-update="#main"
                                   href="/index.php/content/AdminGetContentList?catId=<{$category[id]}>&filter=recovery">
                                    回收
                                </a>
                            </li>
                        </ul>
                        <!-列表筛选方式 end-->
                    </div>
                </div>
            </div>
            <div class="widget-content no-padding">
                <div class="dataTables_header clearfix">
                    <div class="col-md-8">
                        <div class="row">
                            <div class="col-sm-6">
                                <form class="form-vertical row-border">
                                    <div class="form-group">
                                        <label class="control-label">
                                            选择内容的属性操作:
                                        </label>
                                        <select name="attribute" data-table="contentList" class="form-control">
                                            <option value="">请选择...</option>
                                            <{loop $attributeList $key $item}>
                                            <option value="<{$key}>"><{$item}></option>
                                            <{/loop}>
                                        </select>
                                    </div>
                                </form>
                            </div>
                            <div class="col-sm-6">
                                <form class="form-vertical row-border">
                                    <div class="form-group">
                                        <label class="control-label">
                                            选择内容移动到哪个栏目:
                                        </label>
                                        <select name="mobile" data-table="contentList" class="form-control">
                                            <option value="">移动到…</option>
                                            <{$selectCategoryHtml}>
                                        </select>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <form class="form-vertical row-border" method="get"
                              action="/index.php/content/AdminGetContentList?catId=<{$category[id]}>"
                              data-ajax="true" data-ajax-begin="beginPaging" data-ajax-failure="failurePaging"
                              data-ajax-mode="replace" data-ajax-success="successPaging" data-ajax-update="#main">
                            <div class="form-group">
                                <label class="control-label">
                                    本栏目搜索:
                                </label>
                                <div class="row no-padding">
                                    <div class="col-xs-10" style="padding-left: 0; padding-right: 0;">
                                        <input type="text" name="query" class="form-control" maxlength="10"
                                               placeholder="请输入标题的搜索关键词"/>
                                    </div>
                                    <div class="col-xs-2" style="padding-left: 0;">
                                        <button class="btn btn-info" name="sort">
                                            <i class="icon-search"></i>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <table class="table table-bordered table-striped table-hover mb-0"
                       id="contentList" data-action="content/AdminSetContentOther">
                    <thead class="thead-secondary">
                    <tr>
                        <th width="30"><input type="checkbox" id="selectAll"/></th>
                        <th width="100">排序</th>
                        <th>
                            标题
                            <input type="hidden" name="catId" value="<{$category[id]}>"/>
                        </th>
                        <th width="180">文件名</th>
                    </tr>
                    </thead>
                    <tbody>
                    <{loop $lists $item}>
                        <tr class="<{$item[category_id]}>">
                            <td>
                                <input type="checkbox" value="<{$item[id]}>" name="set_id[]"/>
                            </td>
                            <td style="text-align:center;">
                                <input type="text" value="<{$item[listsort]}>" name="sort[]" data-id="<{$item[id]}>"
                                       class="form-control" />
                            </td>
                            <td>
                                <a href="/index.php/content/content?siteId=<{$category[siteId]}>&catId=<{$category[id]}>&id=<{$item[id]}>"
                                   title="点击预览内容" target="_blank">
                                    <i class="icon-eye-open"></i>
                                </a>
                                <a data-ajax="true" data-ajax-begin="beginPaging"
                                   data-ajax-failure="failurePaging"
                                   data-ajax-mode="replace" data-ajax-success="successPaging" data-ajax-update="#main"
                                   href="/index.php/content/AdminGetContent?catId=<{$item[catid]}>&id=<{$item[id]}>"
                                   title="点击编辑内容">
                                    <i class="icon-pencil"></i>
                                    <{$item[title]}>
                                </a>
                                <{if $item[thumb]}>
                                <i class="icon-picture"></i>
                                <{/if}>
                                <{loop $attributeList $k $name}>
                                <{if $item[$k]}>
                                <span class="label label-info"><{$name}></span>
                                <{/if}>
                                <{/loop}>
                                <{if $item[status] == 4}>
                                <span class="label label-danger">已删除</span>
                                <{/if}>
                            </td>
                            <td>
                                <a href="<{$item[url]}>" target="_blank"><{$item[url]}></a>
                            </td>
                        </tr>
                        <{/loop}>
                    </tbody>
                </table>
                <div class="row">
                    <div class="dataTables_footer clearfix">
                        <div class="col-md-6">
                            <div class="dataTables_info" id="DataTables_Table_4_info">
                                第&nbsp;<{$pageList['index']}>/<{$pageList['pageCount']}>&nbsp;页，
                                总&nbsp;<{$pageList['dataCount']}>&nbsp;条。
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="dataTables_paginate paging_bootstrap">
                                <ul class="pagination">
                                    <{loop $pageList $key $pageItem}>
                                    <{if $key == 'pre'}>
                                    <li class="prev<{if $pageList['index'] == 1}> disabled<{/if}>">
                                        <a data-ajax="true" data-ajax-begin="beginPaging" data-ajax-failure="failurePaging"
                                           data-ajax-mode="replace" data-ajax-success="successPaging" data-ajax-update="#main"
                                           href="<{$pageItem}>">← Previous</a>
                                    </li>
                                    <{elseif $key == 'sizeList'}>
                                    <{loop $pageItem $i $sizeUrl}>
                                <li<{if $pageList['index'] == $i}> class="active"<{/if}>>
                                    <a data-ajax="true" data-ajax-begin="beginPaging" data-ajax-failure="failurePaging"
                                       data-ajax-mode="replace" data-ajax-success="successPaging" data-ajax-update="#main"
                                       href="<{$sizeUrl}>"><{$i}></a>
                                    </li>
                                    <{/loop}>
                                    <{elseif $key == 'next'}>
                                    <li class="next<{if $pageList['index'] == $pageList['pageCount']}>  disabled<{/if}>">
                                        <a data-ajax="true" data-ajax-begin="beginPaging" data-ajax-failure="failurePaging"
                                           data-ajax-mode="replace" data-ajax-success="successPaging" data-ajax-update="#main"
                                           href="<{$pageItem}>">Next → </a>
                                    </li>
                                    <{/if}>
                                    <{/loop}>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /Row -->
