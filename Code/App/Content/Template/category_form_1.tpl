<{admin_breadcrumb('栏目管理')}>

<!-- Title -->
<div class="page-header">
    <div class="page-title">
        <h3>单页面形式-栏目编辑</h3>
    </div>
</div>
<!-- /Title -->
<!-- Row -->
<div class="row">
    <div class="col-md-12">
        <form id="categoryLinkForm" class="form-horizontal row-border" action="content/AdminSetCategory">
            <div class="widget box">
                <div class="widget-header">
                    <h4>
                        <i class="icon-reorder"></i>
                        基本设置
                    </h4>
                </div>
                <div class="widget-content">
                    <div class="form-group">
                        <label class="col-md-2 control-label">
                            上级栏目:
                        </label>
                        <div class="col-md-10">
                            <select id="parent" class="form-control" name="parentid"
                            <{if $groupid == 3}>disabled="disabled"<{/if}>>
                            <option value="0">作为一级栏目</option>
                            <{$selectParentHtml}>
                            </select>
                            <input type="hidden" name="fromParentId" value="<{$parentid}>"/>
                            <{if $groupid == 3}>
                        <input type="hidden" name="parentid" value="<{$parentid}>"/>
                            <{/if}>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label">
                            栏目名称:
                        </label>
                        <div class="col-md-10">
                            <input type="text" id="name" class="form-control" name="name"
                                   value="<{$name}>"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label">
                            副栏目名称:
                        </label>
                        <div class="col-md-10">
                            <input type="text" id="subname" class="form-control" name="subname"
                                   value="<{$subname}>"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label">
                            栏目图片:
                        </label>
                        <div class="col-md-10">
                            <{UploadFile('image',$image,'image')}>
                        </div>
                    </div>
                </div>
            </div>
            <{if $groupid < 4}>
            <div class="widget box">
                <div class="widget-header">
                    <h4>
                        <i class="icon-reorder"></i>
                        模板设置
                    </h4>
                </div>
                <div class="widget-content">
                    <div class="form-group">
                        <label class="col-md-2 control-label">
                            单网页模板:
                        </label>
                        <div class="col-md-10">
                            <{optionBySelct('template[pageTpl]', $pageTplList, $template[pageTpl])}>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label">
                            是否显示在导航栏:
                        </label>
                        <div class="col-md-10">
                            <{RadioButton($IsOrNot, $status, 'status')}>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label">
                            是否启用伪静态:
                        </label>
                        <div class="col-md-10">
                            <{RadioButton($IsOrNot, $template[isRewrite], 'template[isRewrite]')}>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label">
                            是否生成静态:
                        </label>
                        <div class="col-md-10">
                            <{RadioButton($IsOrNot, $template[pageHtml], 'template[pageHtml]')}>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label">
                            URL规则:
                        </label>
                        <div class="col-md-10">
                            <input type="text" name="template[pageUrl]" class="form-control"
                                   value="<{if $template[pageUrl]}><{$template[pageUrl]}><{else}>page-[id].htm<{/if}>" />
                            <p class="help-block">
                                涉及到参数有（取双引号里面的，不含双引号）：“[id]”是当前栏目的ID。
                            </p>
                        </div>
                    </div>
                </div>
            </div>
            <{/if}>
            <input type="hidden" name="id" value="<{$id}>"/>
            <input type="hidden" name="type" value="<{$type}>"/>
            <div class="form-actions">
                <button type="button" data-form="categoryLinkForm" class="btn btn-primary pull-left">
                    提交保存
                </button>
            </div>
        </form>
    </div>
</div>
<!-- /Row -->
