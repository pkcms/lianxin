<{admin_breadcrumb('栏目列表')}>

<!-- Title -->
<div class="page-header">
    <div class="page-title">
        <h3>栏目列表</h3>
    </div>
</div>
<!-- /Title -->
<!-- Row -->
<div class="row">
    <div class="col-md-12">
        <div class="widget">
            <div class="widget-content">
                <div class="btn-toolbar btn-toolbar-demo">
                    <button class="btn btn-success dropdown-toggle" type="button" id="dropdownMenu1"
                            data-toggle="dropdown">
                        <i class="icon-plus-sign"></i>&nbsp;添加栏目
                        <span class="caret"></span>
                    </button>
                    <ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu1">
                        <li role="presentation">
                            <a role="menuitem" tabindex="-1" data-ajax="true" data-ajax-begin="beginPaging"
                               data-ajax-failure="failurePaging" data-ajax-mode="replace"
                               data-ajax-success="successPaging" data-ajax-update="#main"
                               href="/index.php/content/AdminGetCategory?type=0">URL形式</a>
                        </li>
                        <li role="presentation">
                            <a role="menuitem" tabindex="-1" data-ajax="true" data-ajax-begin="beginPaging"
                               data-ajax-failure="failurePaging" data-ajax-mode="replace"
                               data-ajax-success="successPaging" data-ajax-update="#main"
                               href="/index.php/content/AdminGetCategory?type=1">单页形式</a>
                        </li>
                        <li role="presentation">
                            <a role="menuitem" tabindex="-1" data-ajax="true" data-ajax-begin="beginPaging"
                               data-ajax-failure="failurePaging" data-ajax-mode="replace"
                               data-ajax-success="successPaging" data-ajax-update="#main"
                               href="/index.php/content/AdminGetCategory?type=2">列表形式</a>
                        </li>
                    </ul>
                    <{if $parent[id]}>
                    <a class="btn btn-success" data-ajax="true" data-ajax-begin="beginPaging"
                       data-ajax-failure="failurePaging" data-ajax-mode="replace"
                       data-ajax-success="successPaging" data-ajax-update="#main"
                       href="/index.php/content/AdminGetCategory?id=<{$parent[id]}>">
                        <i class="icon-edit"></i>&nbsp;编辑栏目</a>
                    <{/if}>
                    <button class="btn btn-danger" data-action="del" data-table="categoryList">
                        <i class="icon-remove-circle"></i>
                        删除
                    </button>
                    <button class="btn btn-default" data-action="sort" data-table="categoryList">
                        排序
                    </button>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-12">
        <div class="widget box">
            <div class="widget-header">
                <h4>
                    <i class="icon-reorder"></i>
                    <{if $parent[name]}>【<{$parent[name]}>】的子<{/if}>栏目列表
                </h4>
            </div>
            <div class="widget-content">
                <table class="table table-bordered table-striped table-hover mb-0"
                       id="categoryList" data-action="content/AdminSetCategoryOther">
                    <thead class="thead-secondary">
                    <tr>
                        <th width="30">&nbsp;</th>
                        <th width="80">排序</th>
                        <th width="60">ID</th>
                        <th>栏目名称</th>
                        <th>静态文件</th>
                        <th width="180">操作选项</th>
                    </tr>
                    </thead>
                    <tbody>
                    <{$tableHtml}>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<!-- /Row -->
