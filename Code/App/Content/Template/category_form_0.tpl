<{admin_breadcrumb('栏目管理')}>

<!-- Title -->
<div class="page-header">
    <div class="page-title">
        <h3>URL形式-栏目编辑</h3>
    </div>
</div>
<!-- /Title -->
<!-- Row -->
<div class="row">
    <div class="col-md-12">
        <div class="widget box">
            <div class="widget-header">
                <h4>
                    <i class="icon-reorder"></i>
                    栏目详细编辑
                </h4>
            </div>
            <div class="widget-content">
                <form id="categoryLinkForm" class="form-horizontal row-border" action="content/AdminSetCategory">
                    <div class="form-group">
                        <label class="col-md-2 control-label">
                            上级栏目:
                        </label>
                        <div class="col-md-10">
                            <select id="parent" class="form-control" name="parentid" <{if $groupid == 3}>disabled="disabled"<{/if}>>
                            <option value="0">作为一级栏目</option>
                            <{$selectParentHtml}>
                            </select>
                            <input type="hidden" name="fromParentId" value="<{$parentid}>"/>
                            <{if $groupid == 3}>
                            <input type="hidden" name="parentid" value="<{$parentid}>"/>
                            <{/if}>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label">
                            栏目名称:
                        </label>
                        <div class="col-md-10">
                            <input type="text" id="name" class="form-control" name="name"
                                   value="<{$name}>" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label">
                            副栏目名称:
                        </label>
                        <div class="col-md-10">
                            <input type="text" id="subname" class="form-control" name="subname"
                                   value="<{$subname}>" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label">
                            栏目图片:
                        </label>
                        <div class="col-md-10">
                            <{UploadFile('image',$image,'image')}>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label">
                            链接地址:
                        </label>
                        <div class="col-md-10">
                            <input type="text" id="url" name="url" class="form-control" value="<{$url}>"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label">
                            栏目描述:
                        </label>
                        <div class="col-md-10">
                            <textarea name="seo[description]" class="form-control"><{$seo[description]}></textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label">
                            是否显示在导航栏:
                        </label>
                        <div class="col-md-10">
                            <{RadioButton($IsOrNot, $status, 'status')}>
                        </div>
                    </div>
                    <input type="hidden" name="id" value="<{$id}>"/>
                    <input type="hidden" name="type" value="<{$type}>"/>
                    <div class="form-actions">
                        <button type="button" data-form="categoryLinkForm" class="btn btn-primary pull-left">
                            提交保存</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- /Row -->
