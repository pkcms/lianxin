<{admin_breadcrumb('内容管理')}>

<!-- Title -->
<div class="page-header">
    <div class="page-title">
        <h3>综合搜索</h3>
    </div>
</div>
<!-- /Title -->
<!-- Row -->
<div class="row">
    <div class="col-md-12">
        <div class="widget">
            <div class="widget-content row">
                <form method="get" class="form-inline" action="/index.php/content/AdminSearch/Search"
                      data-ajax="true" data-ajax-mode="replace" data-ajax-update="#contentList">
                    <div class="form-group col-md-4 col-sm-4 col-xs-12">
                        <label class="control-label">
                            第1步：选择搜索的内容模型
                        </label>
                        <{optionBySelct('modelId', $modelList)}>
                    </div>
                    <div class="form-group col-md-4 col-sm-4 col-xs-12">
                        <label class="control-label">
                            第2步：输入要搜索标题的关键字
                        </label>
                        <input type="text" name="query" class="input-xs form-control col-sm-4" maxlength="10"
                               placeholder="请输入标题的搜索关键词"/>
                    </div>
                    <div class="form-group col-md-4 col-sm-4 col-xs-12">
                        <label class="control-label">
                            第3步：点【搜索】按钮
                        </label>
                        <div>
                            <button class="btn btn-info">
                                <i class="icon-search"></i>
                                搜索
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="widget box">
            <div class="widget-header">
                <h4>
                    <i class="icon-reorder"></i>
                    搜索结果
                </h4>
            </div>
            <div id="contentList" class="widget-content">
            </div>
        </div>
    </div>
</div>
<!-- /Row -->
