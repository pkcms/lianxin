<table class="table table-striped table-condensed table-hover table-bordered table-responsive">
    <thead>
    <tr>
        <th>标题</th>
        <th width="120">所属栏目</th>
    </tr>
    </thead>
    <tbody>
    <form>
        <{loop $lists $item}>
        <tr class="<{$item[category_id]}>">
            <td>
                <a data-ajax="true" data-ajax-begin="beginPaging"
                   data-ajax-failure="failurePaging"
                   data-ajax-mode="replace" data-ajax-success="successPaging" data-ajax-update="#main"
                   href="/index.php/content/AdminGetContent?catId=<{$item[catid]}>&id=<{$item[id]}>"
                   title="点击编辑内容">
                    <i class="icon-pencil"></i>
                    <{$item[title]}></a>
                <a class="f14 viewContent"
                   href="/index.php/content/content?catId=<{$item[catid]}>&id=<{$item[id]}>"
                   data-toggle="tooltip" title="点击预览内容" target="_blank"><i
                            class="icon-eye-open"></i></a>
            </td>
            <td>
                <{$categoryList[$item[catid]]}>
            </td>
        </tr>
        <{/loop}>
    </tbody>
</table>
<div class="row">
    <div class="dataTables_footer clearfix">
        <div class="col-md-6">
            <div class="dataTables_info" id="DataTables_Table_4_info">
                第&nbsp;<{$pageList['index']}>/<{$pageList['pageCount']}>&nbsp;页，
                总&nbsp;<{$pageList['dataCount']}>&nbsp;条。
            </div>
        </div>
        <div class="col-md-6">
            <div class="dataTables_paginate paging_bootstrap">
                <ul class="pagination">
                    <{loop $pageList $key $pageItem}>
                    <{if $key == 'pre'}>
                    <li class="prev<{if $pageList['index'] == 1}> disabled<{/if}>">
                        <a data-ajax="true" data-ajax-begin="beginPaging" data-ajax-failure="failurePaging"
                           data-ajax-mode="replace" data-ajax-success="successPaging" data-ajax-update="#contentList"
                           href="<{$pageItem}>">← Previous</a>
                    </li>
                    <{elseif $key == 'sizeList'}>
                    <{loop $pageItem $i $sizeUrl}>
                <li<{if $pageList['index'] == $i}> class="active"<{/if}>>
                    <a data-ajax="true" data-ajax-begin="beginPaging" data-ajax-failure="failurePaging"
                       data-ajax-mode="replace" data-ajax-success="successPaging" data-ajax-update="#contentList"
                       href="<{$sizeUrl}>"><{$i}></a>
                    </li>
                    <{/loop}>
                    <{elseif $key == 'next'}>
                    <li class="next<{if $pageList['index'] == $pageList['pageCount']}>  disabled<{/if}>">
                        <a data-ajax="true" data-ajax-begin="beginPaging" data-ajax-failure="failurePaging"
                           data-ajax-mode="replace" data-ajax-success="successPaging" data-ajax-update="#contentList"
                           href="<{$pageItem}>">Next → </a>
                    </li>
                    <{/if}>
                    <{/loop}>
                </ul>
            </div>
        </div>
    </div>
</div>
