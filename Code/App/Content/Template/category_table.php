<?php
function htmlForeachTable($lists, $level)
{
    if (is_array($lists)) {
        foreach ($lists as $item) {
            ?>
            <tr>
                <td>
                    <input type="checkbox" value="<?php echo $item['id'] ?>" name="set_id"/>
                </td>
                <td>
                    <input type="text" value="<?php echo $item['listsort'] ?>" name="sort[]"
                           class="form-control" data-id="<?php echo $item['id'] ?>" />
                </td>
                <td><?php echo $item['id'] ?></td>
                <td>
                    <?php for ($i = 0; $i < $level; $i++) {
                        echo '&nbsp&nbsp;&nbsp&nbsp;';
                    } ?>
                    <?php if ($item['type'] == 1) { ?>
                    <a class="f17" data-ajax="true" data-ajax-begin="beginPaging" data-ajax-failure="failurePaging"
                       data-ajax-mode="replace" data-ajax-success="successPaging" data-ajax-update="#main"
                       href="/index.php/content/AdminGetPage?id=<?php echo $item['id'] ?>"
                       target="main">
                        <?php } elseif ($item['type'] == 2) { ?>
                        <a data-ajax="true" data-ajax-begin="beginPaging" data-ajax-failure="failurePaging"
                           data-ajax-mode="replace" data-ajax-success="successPaging" data-ajax-update="#main"
                           <?php if (!(is_array($item['child']) && count($item['child']) > 0)) { ?>href="/index.php/content/AdminGetContentList?catId=<?php echo $item['id'] ?>"
                           <?php } else { ?>href="/index.php/content/AdminGetCategoryList?parentId=<?php echo $item['id'] ?>"<?php } ?>>
                            <?php } else { ?>
                            <a href="<?php echo $item['url'] ?>" target="_blank">
                                <?php } ?>
                                <?php echo $item['name'] ?>
                            </a>
                </td>
                <td>
                    <span class="bs-tooltip" data-placement="top" data-original-title="Tooltip at top">
                    <?php echo $item['url'] ?></span>
                </td>
                <td>
                    <a class="btn btn-sm btn-success" data-ajax="true" data-ajax-begin="beginPaging"
                       data-ajax-failure="failurePaging" data-ajax-mode="replace"
                       data-ajax-success="successPaging" data-ajax-update="#main"
                       href="/index.php/content/AdminGetCategory?parent=<?php echo $item['id'] ?>"
                       data-toggle="tooltip" data-original-title="点击添加子栏目">
                        <i class="icon-plus-sign"></i>
                    </a>
                    <a class="btn btn-sm btn-success" data-ajax="true" data-ajax-begin="beginPaging"
                       data-ajax-failure="failurePaging" data-ajax-mode="replace"
                       data-ajax-success="successPaging" data-ajax-update="#main"
                       href="/index.php/content/AdminGetCategory?id=<?php echo $item['id'] ?>"
                       data-toggle="tooltip" data-original-title="点击编辑栏目">
                        <i class="icon-edit"></i>
                    </a>

                    <a class="btn btn-sm btn-default" href="<?php if ($item['type'] == 2) {
                        echo "/index.php/content/category?siteId=" . $item['siteId'] . "&id=" . $item['id'];
                    } elseif ($item['type'] == 1) {
                        echo "/index.php/content/category?siteId=" . $item['siteId'] . "&id=" . $item['id'];
                    } else {
                        echo $item['url'];
                    } ?>" target="_blank" data-toggle="tooltip" data-original-title="点击预览内容">
                        <i class="icon-eye-open"></i>
                    </a>
                </td>
            </tr>
            <?php if (is_array($item['child']) && count($item['child']) > 0) {
                htmlForeachTable($item['child'], $level + 1);
            }
        }
    }
}

if (isset($lists)) {
    htmlForeachTable($lists, 0);
}
?>