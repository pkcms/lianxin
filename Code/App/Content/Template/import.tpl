<{admin_breadcrumb('导入数据')}>

<!-- Title -->
<div class="page-header">
    <div class="page-title">
        <h3>
            导入数据
        </h3>
    </div>
</div>
<!-- /Title -->
<!-- Row -->
<div class="row">
    <div class="col-md-12">
        <div class="widget box">
            <div class="widget-header">
                <h4 id="step">
                    <i class="icon-upload-alt"></i>
                    <span id="step1">第1步：上传要导入的execl文件</span>
                    <span id="step2" class="hidden">第2步：选择要导入的栏目</span>
                    <span id="step3" class="hidden">第3步：匹配数据模型对应的字段</span>
                    <span id="step4" class="hidden">第4步：执行导入</span>
                </h4>
            </div>
            <div class="widget-content">
                <div class="widget-title"></div>
                <div class="widget-deeper">
                    <div id="action1" class="action">
                        <form class="form-horizontal row-border">
                            <div class="form-group">
                                <label class="col-md-2 control-label">
                                    上传文件:
                                </label>
                                <div class="col-md-10">
                                    <div style="overflow: hidden">
                                        <input type="text" class="form-control" style="width: 60%;float: left;"
                                               id="excel"/>
                                        <input type="file" id="up_excel" style="display: none;"
                                               onchange="html5_uploadFile('up_excel','excel')"/>
                                        <input type="hidden" id="uploadType" value="excel"/>
                                        <input type='button' class='btn' value='浏览...'
                                               onclick="document.getElementById('up_excel').click()"/>
                                    </div>
                                    <div id="progressNumber"></div>
                                    <div id="tips"></div>
                                    <script type="text/javascript"
                                            src="/statics/pkcms/js/pkcms.upload.html5.js?_<?php echo time() ?>"></script>


                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-2 control-label">
                                    内容格式:
                                </label>
                                <div class="col-md-10">
                                    第1行为表头，写明每列的字段，<b style="color: red">数据列表的最后边一列写上【结束列】</b><br/>
                                    第2行开始为表数据，也是要被入库的数据。
                                </div>
                            </div>
                        </form>
                    </div>
                    <div id="action2" class="action">
                        <form class="form-horizontal row-border">
                            <div class="form-group">
                                <label class="col-md-2 control-label">
                                    选择栏目:
                                </label>
                                <div class="col-md-10">
                                    <select id="category" class="form-control">
                                        <{$selectParentHtml}>
                                    </select>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div id="action3" class="action">
                        <form class="form-horizontal row-border">
                            <div class="form-group">
                                <div class="col-md-6">
                                    <label class="control-label">
                                        EXCEL字段:
                                    </label>
                                    <select id="field_excel" class="form-control" multiple="multiple">
                                    </select>
                                </div>
                                <div class="col-md-6">
                                    <label class="control-label">
                                        数据模型字段:
                                    </label>
                                    <select id="field_model" class="form-control" multiple="multiple">
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="control-label col-md-2">
                                    <button type="button" onclick="selectField()" class="btn btn-success">
                                        <i class="icon-ok-sign"></i>
                                    </button>
                                    <button type="button" onclick="selectFieldRemove()" class="btn btn-danger">
                                        <i class="icon-remove-sign"></i>
                                    </button>
                                </label>
                                <div class="col-md-10">
                                    <label class="control-label">
                                        匹配的字段:
                                    </label>
                                    <select id="field_join" class="form-control" multiple="multiple">
                                    </select>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div id="action4" class="action">
                        <ul style="font-size: 18px">
                            <li>
                                <i id="import_field" class="icon-chevron-sign-left"></i>
                                解析数据结构
                            </li>
                            <li>
                                <i id="import_count" class="icon-chevron-sign-left"></i>
                                解析数据导入量
                            </li>
                            <li>
                                <i id="import_do" class="icon-chevron-sign-left"></i>
                                执行导入数据，进度: <b id="speed"></b> %
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="widget-chart-blue">
                    <button id="btn-step-back" class="btn btn-primary" onclick="step(step_now - 1)">
                        返回上一步
                        <i class="icon-step-backward"></i>
                    </button>
                    <button id="btn-step-next" class="btn btn-success" onclick="action(step_now + 1)">
                        <i class="icon-step-forward"></i>
                        继续下一步
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /Row -->
<script type="text/javascript">

    var step_now = 1;
    var btn_stepBack = $('#btn-step-back');
    var btn_stepNext = $('#btn-step-next');
    var input_excel = $('#excel');
    var select_category = $('#category');
    var select_field_model = $('#field_model');
    var select_field_excel = $('#field_excel');
    var select_field_join = $('#field_join');
    var speed = $('#speed');
    var data = {
        field_excel: [],
        field_model: [],
        field_model_base: [],
        field_model_data: [],
        table: '',
        category: '',
        model: ''
    };

    function step(index) {
        step_now = index;

        btn_stepBack.attr('disabled', (step_now <= 1));
        btn_stepNext.attr('disabled', (step_now >= 4));

        $('#step>span').attr('class', 'hidden');
        $('#step' + index).removeAttr('class');

        $('.action').hide();
        $('#action' + index).show();

        return false;
    }

    function selectOptionAppEnd(json_data, select_dom) {
        select_dom.empty();
        $.each(json_data, function (index, item) {
            var opt = $("<option>").text(item).val(index);
            select_dom.append(opt);
        });
    }

    function apiHelper(action, params, call) {
        $.post(
            '/index.php/Content/AdminImport/' + action, params,
            function (apiResult) {
                if (apiResult.isSuccess) {
                    call(apiResult.data);
                } else {
                    alert(apiResult.msg);
                }

            }
        );
    }
    /**
     * 计算百分比
     * @param   {number} num   分子
     * @param   {number} total 分母
     * @returns {number} 返回数百分比
     */
    function Percentage(num, total) {
        if (num == 0 || total == 0){
            return 0;
        }
        return (Math.round(num / total * 10000) / 100.00);// 小数点后两位百分比
    }

    function import_in(api_params) {
        console.log('step', api_params.step);
        $('#' + api_params.step).attr('class', 'icon-chevron-sign-right');
        apiHelper('DoImport', api_params, function (apiResult) {
            if (api_params.step === 'import_do') {
                if (apiResult.hasOwnProperty('do_index') && apiResult.hasOwnProperty('allRow')) {
                    if (apiResult.do_index >= apiResult.allRow) {
                        speed.text(100);
                        $('#' + api_params.step).attr('class', 'icon-ok-sign');
                    } else
                    {
                        speed.text(Percentage(apiResult.do_index, apiResult.allRow));
                        import_in(apiResult);
                    }
                }
            } else
            {
                $('#' + api_params.step).attr('class', 'icon-ok-sign');
                import_in(apiResult);
            }
        });
    }

    function action(index) {
        switch (index) {
            case 2:
                if (index > step_now) {
                    if (input_excel.val() === '') {
                        alert('请先上传！');
                    } else {
                        step(index);
                    }
                }
                break;
            case 3:
                apiHelper('GetModelField', {
                    catId: select_category.val()
                }, function (apiResult) {
                    data.field_model = apiResult.field;
                    data.field_model_base = apiResult.field_base;
                    data.field_model_data = apiResult.field_data;
                    data.table = apiResult.table;
                    data.category = apiResult.category;
                    data.model = apiResult.model;
                    selectOptionAppEnd(apiResult.field, select_field_model);
                });
                apiHelper('GetExcelField', {
                    file: input_excel.val()
                }, function (apiResult) {
                    data.field_excel = apiResult;
                    selectOptionAppEnd(apiResult, select_field_excel);
                });
                select_field_join.empty();
                step(index);
                break;
            case 4:
                var api_params = {
                    file: input_excel.val(),
                    step: 'import_field',
                    fieldJoin: [],
                    table: data.table,
                    category: data.category,
                    model: data.model,
                    field_model_base: data.field_model_base,
                    field_model_data: data.field_model_data
                };
                for (var i = 0; i < $("#field_join option").length; i++) {
                    api_params.fieldJoin.push(
                        $("#field_join option:eq(" + i + ")").attr('value')
                    );
                }
                console.log('api_params', api_params);
                step(index);
                import_in(api_params);
                break;
        }
    }

    function selectField() {
        var fieldVal_excel = select_field_excel.val();
        var fieldVal_model = select_field_model.val();
        console.log('fieldVal_excel', fieldVal_excel);
        if (fieldVal_model == null || fieldVal_excel == null) {
            return false;
        }
        var fieldName_excel = data.field_excel[fieldVal_excel];
        var fieldName_model = data.field_model[fieldVal_model];

        var opt = $("<option>").text(fieldName_excel + ' -> ' + fieldName_model)
            .val(fieldVal_excel + '==' + fieldVal_model);
        select_field_join.append(opt);

        $("#field_excel option[value='" + fieldVal_excel + "']").remove();
        $("#field_model option[value='" + fieldVal_model + "']").remove();
    }

    function selectFieldRemove() {
        var fieldVal_join = select_field_join.val();
        console.log('fieldVal_join', fieldVal_join);
        if (fieldVal_join == null) {
            return false;
        }
        var fieldName_join = select_field_join.find("option:selected").text();

        var val_arr = fieldVal_join[0].split('==');
        var name_arr = fieldName_join.split(' -> ');

        select_field_excel.append(
            $("<option>").text(name_arr[0]).val(val_arr[0])
        );
        select_field_model.append(
            $("<option>").text(name_arr[1]).val(val_arr[1])
        );

        $("#field_join option[value='" + fieldVal_join + "']").remove();
    }

    step(step_now);
</script>
