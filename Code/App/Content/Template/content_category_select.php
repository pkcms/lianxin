<?php
function htmlForeachTable($lists, $level = 0)
{
    if (is_array($lists)) {
        foreach ($lists as $item) {
            ?>
            <tr>
                <td>
                    <input type="checkbox" name="set_id"
                           value="<?php echo $item['id'] . '|' . $item['name'] ?>"<?php if ($item['type'] < 2 || $item['child']) { ?> disabled="disabled"<?php } ?> />
                </td>
                <td>
                    <?php for ($i = 0; $i < $level; $i++) {
                        echo '&nbsp&nbsp;&nbsp&nbsp;';
                    } ?>
                    <?php echo $item['name'] ?>
                </td>
            </tr>
            <?php if ($item['child']) {
                htmlForeachTable($item['child'], $level + 1);
            }
        }
    }
}

if (isset($lists)) {
    htmlForeachTable($lists);
}
?>