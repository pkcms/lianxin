<{admin_breadcrumb('栏目管理')}>

<!-- Title -->
<div class="page-header">
    <div class="page-title">
        <h3>列表形式-栏目编辑</h3>
    </div>
</div>
<!-- /Title -->
<!-- Row -->
<div class="row">
    <div class="col-md-12">
        <form id="categoryLinkForm" class="form-horizontal row-border" action="content/AdminSetCategory">
            <div class="widget box">
                <div class="widget-header">
                    <h4>
                        <i class="icon-reorder"></i>
                        基本设置
                    </h4>
                </div>
                <div class="widget-content">
                    <{if $groupid < 3}>
                    <div class="form-group">
                        <label class="col-md-2 control-label">
                            选择模型:
                        </label>
                        <div class="col-md-10">
                            <{optionBySelct('modelid', $modelList, $modelid)}>
                        </div>
                    </div>
                    <{/if}>
                    <div class="form-group">
                        <label class="col-md-2 control-label">
                            上级栏目:
                        </label>
                        <div class="col-md-10">
                            <select id="parent" class="form-control" name="parentid"
                            <{if $groupid == 3}>disabled="disabled"<{/if}>>
                            <option value="0">作为一级栏目</option>
                            <{$selectParentHtml}>
                            </select>
                            <input type="hidden" name="fromParentId" value="<{$parentid}>"/>
                            <{if $groupid == 3}>
                        <input type="hidden" name="parentid" value="<{$parentid}>"/>
                            <{/if}>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label">
                            栏目名称:
                        </label>
                        <div class="col-md-10">
                            <input type="text" id="name" class="form-control" name="name"
                                   value="<{$name}>"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label">
                            副栏目名称:
                        </label>
                        <div class="col-md-10">
                            <input type="text" id="subname" class="form-control" name="subname"
                                   value="<{$subname}>"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label">
                            栏目图片:
                        </label>
                        <div class="col-md-10">
                            <{UploadFile('image',$image,'image')}>
                        </div>
                    </div>
                </div>
            </div>
            <div class="widget box">
                <div class="widget-header">
                    <h4>
                        <i class="icon-reorder"></i>
                        SEO 设置
                    </h4>
                </div>
                <div class="widget-content">
                    <div class="form-group">
                        <label class="col-md-2 control-label">
                            SEO 标题:
                        </label>
                        <div class="col-md-10">
                            <input type="text" name="seo[title]" class="form-control" value="<{$seo[title]}>"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label">
                            SEO 关键词:
                        </label>
                        <div class="col-md-10">
                            <textarea name="seo[keyword]" class="form-control"><{$seo[keyword]}></textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label">
                            SEO 描述:
                        </label>
                        <div class="col-md-10">
                            <textarea name="seo[description]" class="form-control auto"><{$seo[description]}></textarea>
                        </div>
                    </div>
                </div>
            </div>
            <{if $groupid < 4}>
            <div class="widget box">
                <div class="widget-header">
                    <h4>
                        <i class="icon-reorder"></i>
                        模板设置
                    </h4>
                </div>
                <div class="widget-content">
                    <div class="form-group">
                        <label class="col-md-2 control-label">
                            栏目页模板:
                        </label>
                        <div class="col-md-10">
                            <{optionBySelct('template[categoryTpl]', $categoryTplList, $template[categoryTpl])}>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label">
                            内容页模板:
                        </label>
                        <div class="col-md-10">
                            <{optionBySelct('template[contentTpl]', $showTplList, $template[contentTpl])}>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label">
                            列表页显示条数:
                        </label>
                        <div class="col-md-10">
                            <input type="number" id="name" name="setting[listDataSize]" class="form-control"
                                   value="<{if $setting[listDataSize]}><{$setting[listDataSize]}><{else}>20<{/if}>"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label">
                            列表页的排序方式:
                        </label>
                        <div class="col-md-10">
                            <{optionBySelct('setting[orderList]', $category_content_orderList, $setting[orderList])}>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label">
                            是否显示在导航栏:
                        </label>
                        <div class="col-md-10">
                            <{RadioButton($IsOrNot, $status, 'status')}>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label">
                            是否启用伪静态:
                        </label>
                        <div class="col-md-10">
                            <{RadioButton($IsOrNot, $template[isRewrite], 'template[isRewrite]')}>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label">
                            栏目页是否生成静态:
                        </label>
                        <div class="col-md-10">
                            <{RadioButton($IsOrNot, $template[categoryHtml], 'template[categoryHtml]')}>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label">
                            内容页是否生成静态:
                        </label>
                        <div class="col-md-10">
                            <{RadioButton($IsOrNot, $template[contentHtml], 'template[contentHtml]')}>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label">
                            栏目页URL规则:
                        </label>
                        <div class="col-md-10">
                            <input type="text" id="category_url" name="template[categoryUrl]" class="form-control"
                                   value="<{if $template[categoryUrl]}><{$template[categoryUrl]}><{else}>category-[id]-[page].htm<{/if}>"/>
                            <p class="help-block">
                                涉及到参数有（取双引号里面的，不含双引号）：“[id]”是当前栏目的ID，“[page]”是页码。
                            </p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label">
                            内容页URL规则:
                        </label>
                        <div class="col-md-10">
                            <input type="text" id="show_url" name="template[contentUrl]" class="form-control"
                                   value="<{if $template[contentUrl]}><{$template[contentUrl]}><{else}>content-[catid]-[id].htm<{/if}>"/>
                            <p class="help-block">
                                涉及到参数有（取双引号里面的，不含双引号）：“[id]”是当前内容的ID，“[catid]”是内容所属栏目的ID。
                            </p>
                        </div>
                    </div>
                </div>
            </div>
            <{/if}>
            <{if $groupid < 3}>
            <div class="widget box">
                <div class="widget-header">
                    <h4>
                        <i class="icon-reorder"></i>
                        可选设置
                    </h4>
                </div>
                <div class="widget-content">
                    <div class="form-group">
                        <label class="col-md-2 control-label">
                            阅读权限:
                        </label>
                        <div class="col-md-10">
                            <{CheckBoxButton($UserGroup, $setting[PowerViewGroup], 'setting[PowerViewGroup][]')}>
                            <p class="help-block">
                                该设置只有在非静态化的状态下使用，否则这项的设置失效
                            </p>
                        </div>
                    </div>
                </div>
            </div>
            <{/if}>
            <input type="hidden" name="id" value="<{$id}>"/>
            <input type="hidden" name="type" value="<{$type}>"/>
            <{if $groupid > 2}>
        <input type="hidden" name="modelid" value="<{$modelid}>" />
            <{/if}>
            <div class="form-actions">
                <button type="button" data-form="categoryLinkForm" class="btn btn-primary pull-left">
                    提交保存
                </button>
            </div>
        </form>
    </div>
</div>
<!-- /Row -->
