<?php


namespace PKApp\Content;


use PKApp\Admin\Classes\AdminController;
use PKApp\Content\Classes\CategoryDataBase;
use PKApp\Content\Classes\ContentDataBase;
use PKCore\Formats;
use PKCore\Request;

class AdminSetCategoryOther extends AdminController
{

    public function Main()
    {
        $action = Request::post('action');
        if (method_exists($this, $action)) {
            $this->$action();
        }
    }

    protected function sort()
    {
        $sort = Request::post('selectData');
        if (Formats::isArray($sort)) {
            foreach ($sort as $i => $value) {
                $this->_db()->SetCategoryDetailById(
                    array('listsort' => Formats::isNumeric($value)),
                    Formats::isNumeric($i));
            }
        }
        \PKCore\msg('Data_Input_Success');
    }

    protected function del()
    {
        $idList = Request::post('selectData');
        if (empty($idList)) {
            \PKCore\alert('Category_idEmpty');
        }
        if (Formats::isArray($idList)) {
            foreach ($idList as $id) {
                $this->_db()->SetCategoryDetailById(array('status' => 4), Formats::isNumeric($id));
            }
        }
        \PKCore\msg('Data_Input_Success');
    }

    private function _db()
    {
        static $db;
        !empty($db) ?: $db = new CategoryDataBase();
        return $db;
    }

}