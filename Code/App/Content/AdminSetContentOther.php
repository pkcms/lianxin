<?php


namespace PKApp\Content;


use PKApp\Admin\Classes\AdminController;
use PKApp\Content\Classes\ContentDataBase;
use PKApp\Content\Classes\ContentExtend;
use PKApp\Content\Classes\ContentsDataBase;
use PKApp\Model\Classes\ModelExtend;
use PKCore\Converter;
use PKCore\Formats;
use PKCore\Request;

class AdminSetContentOther extends AdminController
{

    public function Main()
    {
        $action = Request::post('action');
        !method_exists($this, $action) ?: $this->$action();
        \PKCore\msg('Data_Input_Success');
    }

    protected function del()
    {
        $CategoryId = Formats::isNumeric(Request::post('catId'));
        $contentIdList = $this->_getContentList();
        $fromCategoryEntity = ContentExtend::GetCategory($CategoryId, array('modelid'));
        $db_content = new ContentsDataBase($fromCategoryEntity['modelid']);
        foreach ($contentIdList as $id) {
            $db_content->UpdateContent(array('status' => 4), $id);
        }

    }

    protected function sort()
    {
        $CategoryId = Formats::isNumeric(Request::post('catId'));
        $contentIdList = Request::post('selectData');
        Formats::isArray($contentIdList) ?: \PKCore\alert('Content_idEmpty');
        $fromCategoryEntity = ContentExtend::GetCategory($CategoryId, array('modelid'));
        $db_content = new ContentsDataBase($fromCategoryEntity['modelid']);
        foreach ($contentIdList as $id => $sortIndex) {
            $db_content->UpdateContent(array('listsort' => $sortIndex), $id);
        }
    }

    protected function attribute()
    {
        $attribute = Request::post('attribute');
        $attributeList = self::getDict('content_attribute');
        array_key_exists($attribute, $attributeList) ?: \PKCore\alert('attribute_error');
        $contentIdList = $this->_getContentList();
        $categoryId = Formats::isNumeric(Request::post('catId'));// 开始转移
        $fromCategoryEntity = ContentExtend::GetCategory($categoryId, array('modelid'));
        $db_content = new ContentsDataBase($fromCategoryEntity['modelid']);
        $contentListEntity = $db_content->GetListByNotData(array('id' => $contentIdList),
            array('id', $attribute));
        if (Formats::isArray($contentListEntity)) {
            $contentListEntity = Converter::arrayColumn($contentListEntity, strtolower($attribute), 'id');
            foreach ($contentIdList as $id) {
                $insert = array($attribute => ($contentListEntity[$id] ? 0 : 1));
                $db_content->UpdateContent($insert, $id);
            }
        }
    }

    protected function mobile()
    {
        $fromCategoryId = Formats::isNumeric(Request::post('catId'));
        $toCategoryId = Formats::isNumeric(Request::post('mobile'));
        !empty($toCategoryId) ?: \PKCore\alert('Category_idEmpty');
        $contentIdList = $this->_getContentList();
        // 检查两个栏目的信息
        $getCategoryField = array('modelid');
        $fromCategoryEntity = ContentExtend::GetCategory($fromCategoryId, $getCategoryField);
        $toCategoryEntity = ContentExtend::GetCategory($toCategoryId, $getCategoryField);
        $fromCategoryEntity['modelid'] == $toCategoryEntity['modelid'] ?: \PKCore\alert('Content_mobileCategory_Error');
        // 开始转移
        $db_content = new ContentsDataBase($fromCategoryEntity['modelid']);
        foreach ($contentIdList as $id) {
            $insert = array('catid' => $toCategoryId);
            $db_content->UpdateContent($insert, $id);
        }
    }

    private function _getContentList()
    {
        $contentIdList = Request::post('selectData');
        Formats::isArray($contentIdList) ?: \PKCore\alert('Content_idEmpty');
        return $contentIdList;
    }
}