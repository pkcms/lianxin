<?php
// 综合搜索

namespace PKApp\Content;


use PKApp\Admin\Classes\AdminController;
use PKApp\Content\Classes\CategoryDataBase;
use PKApp\Content\Classes\ContentDataBase;
use PKApp\Content\Classes\ContentsDataBase;
use PKApp\Model\Classes\ModelDatabase;
use PKApp\Model\Classes\ModelExtend;
use PKCore\Converter;
use PKCore\Request;
use PKCore\Route;
use PKCore\Statics;
use PKCore\Tpl;

class AdminSearch extends AdminController
{

    public function Main()
    {
        $db_model = new ModelDatabase();
        $modelList = $db_model->GetLists('content',null,array('name','id'));
        if (is_array($modelList)) {
            $modelList = Converter::arrayColumn($modelList,'name','id');
        } else {
            \PKCore\alert('Model_Empty');
        }
        $tpl = new Tpl();
        return $tpl->SetTplParam('modelList',$modelList)
            ->Display('content_search');
    }

    public function Search()
    {
        $modelId = Request::get('modelId');
        $query = Request::get('query');
        $tpl = new Tpl();
        if (empty($modelId) || empty($query)) {
            $tpl->Notice(Route\language('Search_Error'));
        }
        $db_category = new CategoryDataBase();
        $categoryList = $db_category->GetListByModelId($this->loginUser()->SiteId, $modelId);
        !is_array($categoryList) ?: $categoryList = Converter::arrayColumn($categoryList,'name','id');
        // 组合查询条件
        $where = array('title LIKE "%' . $query . '%"');
        $this->GetPages();
        $db_content = new ContentsDataBase($modelId);
        $contentEntityList = $db_content->GetList($where,
            array('id', 'title', 'catid'), self::$pageIndex, self::$pageSize);
        is_array($contentEntityList) ?: $contentEntityList = array('count' => 0, 'lists' => array());
        $pageList = Statics::pages($contentEntityList['count'], self::$pageSize, self::$pageIndex,
            '/index.php/content/AdminSearch/Search?modelId='.$modelId.'&query='.$query.'&pageIndex=[page]');
        $tpl->SetTplParam('pageList', $pageList)
            ->SetTplParam('categoryList', $categoryList)
            ->SetTplParam('modelId', $modelId)
            ->SetTplParam('query', urlencode($query))
            ->SetTplParamList($contentEntityList);
        return $tpl->Display('content_search_list');
    }
}