<?php


namespace PKApp\Content;


use PKApp\Admin\Classes\AdminController;
use PKApp\Admin\Classes\AdminDataBase;
use PKApp\Content\Classes\ContentDataBase;
use PKApp\Content\Classes\ContentExtend;
use PKApp\Content\Classes\ContentsDataBase;
use PKApp\Content\Classes\ContentUrlFormat;
use PKCore\Formats;
use PKCore\Request;
use PKCore\Statics;
use PKCore\Tpl;

class AdminGetContentList extends AdminController
{

    public function Main()
    {
        // 筛选方式
        $filter = Request::get('filter');
        $catId = Formats::isNumeric(Request::get('catId'));
        $siteEntity = AdminDataBase::GetSite(array('id' => $this->loginUser()->SiteId), array('Site_Path'));
        $categoryEntity = ContentExtend::GetCategory($catId,
            array('id', 'siteid', 'type', 'modelid', 'name', 'setting', 'template'));
        $templateConfig = $categoryEntity['template'];
        $categoryEntity['url'] = ContentUrlFormat::Category($categoryEntity, $templateConfig, $siteEntity['Site_Path']);
        // 后台界面渲染方式
        $pageMode = Request::get('mode');
        !empty($pageMode) ?: $pageMode = $categoryEntity['setting']['mode'];
        $attributeList = self::getDict('content_attribute');
        // 组合查询条件
        $where = array('catid' => $catId);
        $query = Request::get('query');
        $isSearch = false;
        switch ($filter) {
            case 'recovery':
                $categoryEntity['url'] .= '&filter=recovery';
                $where['status'] = 4;
                break;
            default:
                !array_key_exists($filter, $attributeList) ?: $where[$filter] = 1;
                break;
        }
        if (!empty($query)) {
            $where[] = 'title LIKE "%' . $query . '%"';
            $isSearch = true;
        }
        $this->GetPages();
        $db_content = new ContentsDataBase($categoryEntity['modelid']);
        $contentEntityList = $db_content->GetList($where,
            array('id', 'title', 'catid', 'thumb', 'listsort', 'headlines', 'posids', 'totop', 'url', 'status'),
            self::$pageIndex, self::$pageSize);
        if (is_array($contentEntityList)) {
            foreach ($contentEntityList['lists'] as $index => $item) {
                $item['url'] = ContentUrlFormat::Content($categoryEntity, $templateConfig, $item, $siteEntity['Site_Path']);
                $contentEntityList['lists'][$index] = $item;
            }
        } else {
            $contentEntityList = array('count' => 0, 'lists' => array());
        }
        $pageList = Statics::pages($contentEntityList['count'], self::$pageSize, self::$pageIndex, $categoryEntity['url']);
        $tpl = new Tpl();
        $tpl->SetTplParam('category', $categoryEntity)->SetTplParam('isSearch', $isSearch)
            ->SetTplParam('pageList', $pageList)->SetTplParam('attributeList', $attributeList)
            ->SetTplParam('mode', $pageMode)
            ->SetTplParamList($contentEntityList)
            ->SetTplParam('selectCategoryHtml', ContentExtend::GetCategoryOptionList($this->loginUser()->SiteId, $catId));
        return $tpl->Display('content_list');
    }
}