<?php


namespace PKApp\Content;

require_once PATH_PK . 'PlugIn' . DS . 'excel' . DS . 'PHPExcel.php';

use PKApp\Admin\Classes\AdminController;
use PKApp\Content\Classes\ContentExtend;
use PKApp\Model\Classes\FieldDateBase;
use PKApp\Model\Classes\ModelExtend;
use PKCore\DataBase;
use PKCore\Formats;
use PKCore\Request;
use PKCore\Tpl;
use PKCore\Route;

class AdminImport extends AdminController
{

    protected $excel_cls, $currentSheet, $allColumn, $allRow;
    protected $post;
    protected $batch_importSize = 10;

    public function Main()
    {
        $tpl = new Tpl();
        return $tpl
            ->SetTplParam('selectParentHtml', ContentExtend::GetCategoryOptionList($this->loginUser()->SiteId, null))
            ->Display('import');
    }

    public function GetModelField()
    {
        $catId = Formats::isNumeric(Request::post('catId'));
        $categoryEntity = ContentExtend::GetCategory($catId);
        !empty($categoryEntity['modelid']) ?: \PKCore\alert('Model_idEmpty');
        $params = array(
            'table' => ModelExtend::GetTableName($categoryEntity['modelid']),
            'category' => $catId,
            'model' => $categoryEntity['modelid'],
        );
        $db_field = new FieldDateBase();
        $db_field_list = $db_field->GetFieldLists(
            array('modelid' => $categoryEntity['modelid']),
            array('name', 'field', 'isindex'));
        $field_list_new = $field_list_base = $field_list_data = array();
        foreach ($db_field_list as $item) {
            $field_list_new[$item['field']] = $item['name'];
            if ($item['isindex']) {
                $field_list_base[] = $item['field'];
            } else {
                $field_list_data[] = $item['field'];
            }
        }
        $field_list_base[] = 'title';
        $field_list_base[] = 'thumb';
        $field_list_new['title'] = '标题';
        $field_list_new['thumb'] = '缩略图';
        $params['field'] = $field_list_new;
        $params['field_base'] = $field_list_base;
        $params['field_data'] = $field_list_data;
        $this->json($params);
    }

    public function GetExcelField()
    {
        $this->handlerFileExt();
        $this->GetTableHead();
    }

    public function DoImport()
    {
        $this->post = Request::post();
        $step = Request::post('step');
        switch ($step) {
            case 'import_field':
                // 数据结构的解析
                $this->import_field();
                break;
            case 'import_count':
                $this->handlerFileExt();
                $this->import_count();
                break;
            case 'import_do';
                $this->handlerFileExt();
                $this->import_do();
                break;
        }
    }

    protected function import_field()
    {
        $fieldJoin = Request::post('fieldJoin');
        $field_excel = $field_model = array();
        if (is_array($fieldJoin) && count($fieldJoin) > 0) {
            foreach ($fieldJoin as $item) {
                $tmp = explode('==', $item);
                $field_excel[] = $tmp[0];
                $field_model[] = $tmp[1];
            }
        } else {
            \PKCore\alert('Not received Field join Lists');
        }
        unset($this->post['fieldJoin']);
        $this->json(array_merge(
            $this->post,
            array(
                'field_excel' => $field_excel,
                'field_model' => $field_model,
                'step' => 'import_count',
            )
        ));
    }

    protected function import_count()
    {
        $params = array(
            'allRow' => $this->allRow,
            'doSize' => ceil($this->allRow / $this->batch_importSize),
            'step' => 'import_do',
        );
        $this->json(array_merge($this->post, $params));
    }

    protected function import_do()
    {
        $do_index = Formats::isNumeric(Request::post('do_index'));
        $doSize = Formats::isNumeric(Request::post('doSize'));
        $allRow = Formats::isNumeric(Request::post('allRow'));
        $table = Request::post('table');
        if ($do_index > $doSize) {
            $this->json(array_merge($this->post, array('page_index', $doSize)));
        }
        $doLine_start = $do_index == 0 ? 2 : $do_index;
        $doLine_stop = $do_index + $this->batch_importSize;
        // 如果结束行超过了，则以最大为准
        $doLine_stop < $allRow ?: $doLine_stop = $allRow;
        // 组织数据结构
        $field_excel = Request::post('field_excel');
        $field_model = Request::post('field_model');
        $field_model_base = Request::post('field_model_base');
        $field_model_data = Request::post('field_model_data');
        $data_index = $data_data = array();
        foreach ($field_excel as $index => $column_no) {
            $field_name = $field_model[$index];
            for ($row = $doLine_start; $row <= $doLine_stop; $row++) {
                $addr = $column_no . $row;
                $cell = (string)$this->currentSheet->getCell($addr)->getValue();
                //富文本转换字符串
                if ($cell instanceof \PHPExcel_RichText) {
                    $cell = $cell->__toString();
                }
                if (in_array($field_name, $field_model_base)) {
                    $data_index[$row][$field_name] = $cell;
                } elseif (in_array($field_name, $field_model_data)) {
                    $data_data[$row][$field_name] = $cell;
                }
            }
        }
        if (count($data_index) == 0) {
            $this->json('', Route\language('Import_fieldIndex_Empty'), false);
        }
        $db_base = array(
            'siteid' => $this->loginUser()->SiteId,
            'modelid' => Request::post('model'),
            'catid' => Request::post('category'),
            'status' => 1,
            'inputtime' => time(),
            'updatetime' => time());
        $db = new DataBase($table);
        $db_data = new DataBase($table . '_data');
        $db_count = new DataBase($table . '_count');
        foreach ($data_index as $row => $datum) {
            $id = $db->Insert(array_merge($db_base, $datum))->Exec();
            $insert_data = array_key_exists($row, $data_data) ? $data_data[$row] : array();
            $insert_data['id'] = $id;
            $db_data->Insert($insert_data)->Exec();
            $db_count->Insert(array('id' => $id))->Exec();
        }
        $this->json(array_merge($this->post, array('do_index' => $doLine_stop)));
    }

    protected function handlerFileExt()
    {
        $file = Request::post('file');
        $fileArr = explode('.', $file);
        $file_ext = $fileArr[count($fileArr) - 1];
        $file_path = PATH_ROOT . str_replace('/', DS, $file);
        $is_read = true;
        switch ($file_ext) {
            case 'xls':
                $this->excel_cls = new \PHPExcel_Reader_Excel5();
                break;
            case 'xlsx':
                //建立reader对象
                $this->excel_cls = new \PHPExcel_Reader_Excel2007();
                break;
            default:
                $is_read = false;
                break;
        }
        if ($is_read) {
            $this->GetReadExcel($file_path);
        } else {
            $this->json('', Route\language('Import_file_error'), false);
        }
    }

    protected function GetReadExcel($file_path)
    {
        try {
            if (!$this->excel_cls->canRead($file_path)) {
                throw new \Exception('no Excel!');
            }
            //建立excel对象
            $excel = $this->excel_cls->load($file_path);
            //**读取excel文件中的指定工作表*/
            $this->currentSheet = $excel->getSheet(0);
            //**取得最大的列号*/
            $this->allColumn = $this->currentSheet->getHighestColumn();
            //**取得一共有多少行*/
            $this->allRow = $this->currentSheet->getHighestRow();
        } catch (\PHPExcel_Reader_Exception $e) {
            \PKCore\alert($e->getMessage());
        } catch (\Exception $exception) {
            \PKCore\alert($exception->getMessage());
        }
    }

    protected function GetTableHead()
    {
        $data = array();
        $row = 1;
        $i = 0;
        // 取出excel第一行全部字段
        while (($column_no = $this->stringFromColumnIndex($i)) != $this->allColumn) {
            $addr = $column_no . $row;
            $cell = (string)$this->currentSheet->getCell($addr)->getValue();
            //富文本转换字符串
            if ($cell instanceof \PHPExcel_RichText) {
                $cell = $cell->__toString();
            }
            $data[$column_no] = $cell;
            $i++;
        }
        $this->json($data);
    }

    protected function stringFromColumnIndex($pColumnIndex = 0)
    {
        static $_indexCache = array();
        if (!isset($_indexCache[$pColumnIndex])) {
            if ($pColumnIndex < 26) {
                $_indexCache[$pColumnIndex] = chr(65 + $pColumnIndex);
            } elseif ($pColumnIndex < 702) {
                $_indexCache[$pColumnIndex] = chr(64 + ($pColumnIndex / 26)) . chr(65 + $pColumnIndex % 26);
            } else {
                $_indexCache[$pColumnIndex] = chr(64 + (($pColumnIndex - 26) / 676)) . chr(65 + ((($pColumnIndex - 26) % 676) / 26)) . chr(65 + $pColumnIndex % 26);
            }
        }
        return $_indexCache[$pColumnIndex];
    }
}