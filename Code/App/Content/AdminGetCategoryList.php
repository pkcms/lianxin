<?php


namespace PKApp\Content;


use PKApp\Admin\Classes\AdminController;
use PKApp\Content\Classes\CategoryDataBase;
use PKApp\Content\Classes\ContentDataBase;
use PKApp\Content\Classes\ContentExtend;
use PKApp\Model\Classes\ModelDatabase;
use PKCore\Converter;
use PKCore\Formats;
use PKCore\Request;
use PKCore\Tpl;

class AdminGetCategoryList extends AdminController
{

    public function Main()
    {
        $parentId = Formats::isNumeric(Request::get('parentId'));
        $tpl = new Tpl();
        $parentId = $parentId ? $parentId : null;
        if ($parentId > 0) {
            $db_category = new CategoryDataBase();
            $entity = $db_category->GetCategoryDetail($parentId);
            empty($entity['arrchildid']) ?: $viewIdLists = explode(',', $entity['arrchildid']);
            $tpl->SetTplParam('parent', $entity);
        }
        return $tpl->SetTplParam('tableHtml', $this->_getTableList($parentId, isset($viewIdLists) ? $viewIdLists : $parentId))
            ->SetTplParam('groupid', $this->loginUser()->GroupId)
            ->Display('category_lists');
    }

    public function selectCategory()
    {
        $tpl = new Tpl();
        return $tpl->SetTplParam('tplHtml', $this->_selectCategory())
            ->SetTplParam('showField', Request::get('showField'))
            ->Display('content_category_select');
    }

    private function _selectCategory()
    {
        $tpl = new Tpl();
        return $tpl->SetTplParam('lists', ContentExtend::AdminGetCategoryList($this->loginUser()->SiteId, null, true))
            ->PhpDisplay('content_category_select');
    }

    private function _getTableList($parentId, $viewIdLists)
    {
        $tpl = new Tpl();
        return $tpl->SetTplParam('lists', ContentExtend::AdminGetCategoryList($this->loginUser()->SiteId, $parentId, true, $viewIdLists))
            ->PhpDisplay('category_table');
    }

    public function Sidenav()
    {
        $tpl = new Tpl();
        return $tpl->SetTplParam('lists', ContentExtend::AdminGetCategoryList($this->loginUser()->SiteId, null, true))
            ->PhpDisplay('category_sidenav');
    }
}