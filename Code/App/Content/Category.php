<?php


namespace PKApp\Content;


use PKApp\Content\Classes\BaseController;
use PKApp\Content\Classes\ContentExtend;
use PKApp\Content\Classes\ContentPageDataBase;
use PKApp\Content\Classes\ContentTemplateExists;
use PKCore\Formats;
use PKCore\Request;
use PKCore\Route;

class Category extends BaseController
{
    private $_categoryEntity;

    public function Main()
    {
        $catId = Formats::isNumeric(Request::get('id'));
        $this->_categoryEntity = ContentExtend::GetCategory($catId, '*');
        $this->getSiteTemplates($this->_categoryEntity['siteId']);
        $this->tpl()->SetTplParam('categoryParent',
            ContentTemplateExists::GetParentIdList($this->_categoryEntity['siteId'],
                $this->_categoryEntity['arrparentid'], self::$siteEntity['Site_Path']));
        switch ($this->_categoryEntity['type']) {
            case 1:
                return $this->_typeByPage();
                break;
            case 2:
                return $this->_typeByList();
                break;
        }
        return '';
    }

    private function _typeByPage()
    {
        list($categoryEntity, $template) = ContentTemplateExists::GetAndHandlerCategory($this->_categoryEntity);
        $db_page = new ContentPageDataBase();
        $pageEntity = $db_page->GetPageDetail($categoryEntity['id']);
        is_array($pageEntity) ?: $this->tpl()->Notice(Route\language('Page_Empty'));
        $pageEntity = ContentExtend::CategoryByPageTypeHtmlDeCode($pageEntity);
        $tplFile = $template['pageTpl'];
        $this->tpl()->SetTplParam('category', $categoryEntity)->SetTplParamList($pageEntity);
        return $this->tplDisplay(substr($tplFile, 0, strlen($tplFile) - 4));
    }

    private function _typeByList()
    {
        list($categoryEntity, $template) = ContentTemplateExists::GetAndHandlerCategory($this->_categoryEntity);
//        $modelTypeLists = PKController::getDict('modelTypeLists');
//        $tableName = ModelExtend::GetTableName($categoryEntity['modelid'], $modelTypeLists);
        $tplFile = $template['categoryTpl'];
        $this->tpl()->SetTplParamList($categoryEntity);
        return $this->tplDisplay(substr($tplFile, 0, strlen($tplFile) - 4));
    }
}