<?php


namespace PKApp\Content;


use PKApp\Content\Classes\ContentDataBase;
use PKApp\Content\Classes\ContentDetail;
use PKApp\Content\Classes\ContentExtend;
use PKApp\Content\Classes\ContentsDataBase;
use PKApp\Content\Classes\ContentTemplateExists;
use PKApp\Content\Classes\ContentUrlFormat;
use PKApp\Member\Classes\MemberController;
use PKApp\Model\Classes\ModelExtend;
use PKApp\Model\Classes\ToOutData;
use PKCore\Converter;
use PKCore\Formats;
use PKCore\Request;

class Content extends MemberController
{
    private $_categoryEntity;

    public function Main()
    {
        $contentId = Formats::isNumeric(Request::get('id'));
        $this->getSiteTemplates();
        $contentDetail = new ContentDetail(
            self::$sitePath,
            array(),
            $this->tpl()
        );
        list($categoryEntity, $templateSetting) = $contentDetail->Category();
        $this->_categoryEntity = $categoryEntity;
        $contentEntity = $this->_db_content()->GetContentDetail($contentId);
        !empty($contentEntity) && is_array($contentEntity) ? $contentEntity = $contentDetail->GetContent($contentEntity) : \PKCore\alert('Content_Empty');
        // 当前内容的URL
        $this->tpl()->SetTplParam('url',
            ContentUrlFormat::Content($categoryEntity, $templateSetting, $contentEntity, self::$sitePath));
        //模板
        if (empty($contentEntity['template'])) {
            $tplFile = $templateSetting['contentTpl'];
        } else {
            $tplFile = $contentEntity['template'];
        }
        $contentDetail->GetUpOrNext($categoryEntity, $templateSetting, $contentEntity);
        return $this->tplDisplay(substr($tplFile, 0, strlen($tplFile) - 4));
    }

    //栏目浏览权限控制
//    private function _viewPower(array $setting)
//    {
//        if (!(array_key_exists('PowerViewGroup', $setting) && Formats::isArray($setting['PowerViewGroup']))) {
//            return false;
//        }
//        $powerViewGroup = $setting['PowerViewGroup'];
//        in_array($this->loginUser()->GroupId,$powerViewGroup) ?: \PKCore\alert('Content_ViewPower_Error');
//        return true;
//    }

    private function _db_content()
    {
        static $db;
        !empty($db) ?: $db = new ContentsDataBase($this->_categoryEntity['modelid']);
        return $db;
    }
}