<?php


namespace PKApp\Content;


use PKApp\Content\Classes\BaseController;
use PKApp\Content\Classes\ContentExtend;
use PKApp\Content\Classes\ContentsDataBase;
use PKCore\Formats;
use PKCore\Request;

class Asynchronous extends BaseController
{

    public function Main()
    {
        $actionType = Request::get('actionType');
        if (method_exists($this, $actionType)) {
            echo $this->$actionType();
        }
    }

    protected function hits()
    {
        // 检查 IP 是否正常访问
        $checkIP = BaseController::CheckIP(Request::module(), CONTENT_SHOW_PUT_TIME_CELL);
        $categoryId = Formats::isNumeric(Request::get('catid'));
        $contentId = Formats::isNumeric(Request::get('id'));
        $categoryEntity = ContentExtend::GetCategory($categoryId, array('modelid'));
        if (empty($categoryEntity)) {
            return 0;
        }
        $db_content = new ContentsDataBase($categoryEntity['modelid']);
        $entity = $db_content->GetContentCount($contentId, array('hits'));
        if (empty($entity)) {
            return 0;
        }
        if (!$checkIP) {
            return $entity['hits'];
        }
        $insert = array('hits' => $entity['hits'] + 1);
        $db_content->UpdateContentCount($insert, $contentId);
        return $insert['hits'];
    }

}