<?php


namespace PKApp\Content;


use PKApp\Content\Classes\BaseController;
use PKApp\Content\Classes\CategoryDataBase;
use PKApp\Content\Classes\ContentExtend;
use PKApp\Content\Classes\ContentTemplateExists;
use PKApp\Content\Classes\ContentUrlFormat;
use PKApp\Model\Classes\ModelDatabase;
use PKApp\Model\Classes\ModelExtend;
use PKCore\Converter;
use PKCore\Formats;
use PKCore\PKController;
use PKCore\Request;
use PKCore\Statics;
use PKCore\Tpl;

class Search extends BaseController
{

    private $_sqlOptions;
    private $_pageUrl;
    private $_siteId;

    private $_fieldEntity;
    private $_CategoryListEntity = array();

    private $_is_paramSearch = true;
    private $_is_paramSize = 0;

    public function Main()
    {
        $this->_siteId = Formats::isNumeric(Request::get('siteId'));
        $modelId = Formats::isNumeric(Request::get('modelId'));
        $categoryId = Formats::isNumeric(Request::get('catId'));
        $is_SearchSize = false;
        if (!empty($categoryId)) {
            $modelId = $this->_searchByCategoryId($categoryId);
            $this->tpl()->SetTplParam('catId', $categoryId);
        } elseif (!empty($modelId) && !empty($this->_siteId)) {
            $this->_searchByModelId($modelId);
        } elseif (!empty($this->_siteId)) {
            $is_SearchSize = true;
            return $this->_searchBySiteId();
        } else {
            !empty($categoryId) ?: \PKCore\alert('Category_idEmpty');
        }
        // 判断是否全站搜索
        if (!$is_SearchSize && isset($this->_fieldEntity)) {
            $query = $this->_checkParams_Query();
            $this->GetPages();
            // 返回行数
            if (empty(self::$siteEntity['setting']['searchItemSize'])) {
                $list_lineSize = self::$pageSize;
            } else {
                $list_lineSize = self::$siteEntity['setting']['searchItemSize'];
            }
            list($contentList, $contentTotal) = ContentTemplateExists::GetContentList($modelId,
                $this->_CategoryListEntity, $this->_sqlOptions, $list_lineSize, $this->_fieldEntity, self::$pageIndex);
            $pageNav = Statics::pages($contentTotal, $list_lineSize, PKController::$pageIndex,
                listParamsSearchUrl($this->_siteId, $categoryId, 'query', $query) . '&pageIndex=[page]');
            $this->tpl()->SetTplParam('searchData', $contentList)
                ->SetTplParam('pageNavList', $pageNav);
            return $this->tplDisplay('search');
        } else {
            return null;
        }
    }

    private function _searchBySiteId()
    {
        $db_model = new ModelDatabase();
        $modelList = $db_model->GetLists(0, null, array('id', 'name', 'tablename'));
        $this->getSiteTemplates($this->_siteId);
        $result = array();
        foreach ($modelList as $model) {
            $this->_sqlOptions = array('siteId' => $this->_siteId, 'status' => 1,);
            $this->_fieldEntity = ModelExtend::GetModelFieldEntity($model['id']);
            $this->_checkParams_SearchField();
            $query = $this->_checkParams_Query();
            list($contentList, $contentTotal) = ContentTemplateExists::GetContentList($model['id'],
                $this->_CategoryListEntity, $this->_sqlOptions, 999, $this->_fieldEntity, self::$pageIndex);
            $tmp_result = array(
                'name' => $model['name'],
                'count' => $contentTotal,
                'lists' => $contentList
            );
            $result[] = $tmp_result;
        }
        $this->tpl()->SetTplParam('searchData', $result)
            ->SetTplParam('isSearchAll', true);
        return $this->tplDisplay('search');
    }

    private function _searchByModelId($modelId)
    {
        // 根据模型去搜索
        $this->_fieldEntity = ModelExtend::GetModelFieldEntity($modelId);
        $this->_sqlOptions = array('siteId' => $this->_siteId, 'status' => 1,);
        $this->_checkParams_SearchField();
        $this->getSiteTemplates($this->_siteId);
    }

    private function _searchByCategoryId($categoryId)
    {
        // 内容栏目的搜索
        $categoryEntity = ContentExtend::GetCategory($categoryId,
            array('id', 'siteid', 'type', 'modelid', 'arrparentid', 'arrchildid', 'name', 'template'));
        $this->_siteId = $categoryEntity['siteId'];
        $this->getSiteTemplates($this->_siteId);
        $categoryList = $this->_getCategoryParent($categoryEntity);
        $categoryEntity['url'] = ContentUrlFormat::Category($categoryEntity, $categoryEntity['template'], self::$sitePath);
        // ============== 组建查询条件 start ==============
        $this->_sqlOptions = array('siteId' => $this->_siteId, 'status' => 1,
            'catid' => empty($categoryEntity['arrchildid']) ? $categoryId : $categoryEntity['arrchildid'],);
        $this->_fieldEntity = ModelExtend::GetModelFieldEntity($categoryEntity['modelid']);
        $this->_checkParams_SearchField();
        // 检查子栏目
        list($modelId, $this->_CategoryListEntity, $this->_sqlOptions['catid']) =
            ContentTemplateExists::GetTemplateCatId($this->_siteId,
                empty($categoryEntity['arrchildid']) ? $categoryEntity['id'] : $categoryEntity['arrchildid']);
        // ============== 组建查询条件 end ==============
        // 排序
        $order = $this->_sort($categoryEntity);
        // 父（子）栏目的组合
        $this->_CategoryListEntity[] = $categoryEntity;
        $this->tpl()->SetTplParam('categoryList', $categoryList)
            ->SetTplParam('catId', $categoryId);
        // 内容检索
        !empty($categoryEntity['modelid']) ?: \PKCore\alert('Search_Category_TypeError');
        $modelId = $categoryEntity['modelid'];
        return $modelId;
    }

    private function _checkParams_Query()
    {
        if (Request::has('query', 'get')) {
            $query = urldecode(Request::get('query'));
            !empty($query) ?: \PKCore\alert('SearchQuery_Empty');
            $this->_sqlOptions[] = 'title LIKE "%' . $query . '%"';
            $this->tpl()->SetTplParam('query', $query);
            return $query;
        } elseif (!$this->_is_paramSearch) {
            \PKCore\alert('Search_ParamEmpty');
        } else {
            $this->_is_paramSearch = false;
            return '';
        }
    }

    private function _checkParams_SearchField()
    {
        // 字段搜索
        $fieldList_search = self::GetArrayByKey('isSearch', $this->_fieldEntity);
        if (Formats::isArray($fieldList_search)) {
            $this->_is_paramSearch = true;
            $this->_is_paramSize += 1;
            $fieldList = self::GetArrayByKey('fieldList', $this->_fieldEntity);
            foreach ($fieldList_search as $fieldName) {
                $value = Request::get($fieldName);
                // 该字段名的相关属性
                $theFieldName_Info = $fieldList[$fieldName];
                if (!empty($value)) {
                    $setting = self::GetArrayByKey('setting', $theFieldName_Info);
                    $boxType = self::GetArrayByKey('boxtype', $setting);
                    if ($boxType == 'checkbox') {
                        $this->_sqlOptions[] = '' . $fieldName . ' like "%,' . $value . ',%"';
                    } else {
                        $this->_sqlOptions[$fieldName] = $value;
                    }
                }
            }
        } else {
            $this->_is_paramSize > 0 ?: $this->_is_paramSearch = false;
        }
    }

    private function _getCategoryParent($categoryEntity)
    {
        $parentIdList_str = self::GetArrayByKey('arrparentid', $categoryEntity);
        $categoryList = array();
        if (!empty($parentIdList_str)) {
            $categoryList = ContentTemplateExists::GetParentIdList($categoryEntity['siteId'], $parentIdList_str,
                self::$siteEntity['Site_Path']);
        }
        $categoryList[] = array('name' => $categoryEntity['name'],
            'url' => ContentUrlFormat::Category($categoryEntity, $categoryEntity['template'], self::$sitePath));
        return $categoryList;
    }

    private function _sort($categoryEntity)
    {
        // 排序
        $setting = self::GetArrayByKey('setting', $categoryEntity);
        $sortIndex = self::GetArrayByKey('orderList', $setting);
        $orderList = PKController::getDict('category_content_orderList');
        return $orderList[$sortIndex]['value'];
    }

}