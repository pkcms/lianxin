<?php


namespace PKApp\Content;


use PKApp\Admin\Classes\AdminController;
use PKApp\Content\Classes\ContentExtend;
use PKApp\Content\Classes\ContentPageDataBase;
use PKCore\Formats;
use PKCore\Request;
use PKCore\Tpl;

class AdminGetPage extends AdminController
{

    public function Main()
    {
        $catId = Formats::isNumeric(Request::get('id'));
        $db_page = new ContentPageDataBase();
        $pageEntity = $db_page->GetPageDetail($catId);
        $pageEntity = is_array($pageEntity) ? ContentExtend::CategoryByPageTypeHtmlDeCode($pageEntity) : array();
        $tpl = new Tpl();
        return $tpl->SetTplParamList($pageEntity)->SetTplParam('catId', $catId)
            ->Display('page_editor');
    }
}