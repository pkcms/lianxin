<?php


namespace PKApp\Content\Classes;


use PKCore\DataBase;

class ContentPageDataBase extends DataBase
{

    public function __construct()
    {
        parent::__construct('content_page');
    }

    public function GetPageDetail($id, $field = '*')
    {
        $where = array('id' => $id);
        return $this->Where($where)->Select($field)->First();
    }

    public function AddPage($data)
    {
        $newId = $this->Insert($data)->Exec();
        return $newId;
    }

    public function UpdatePage($data, $id)
    {
        $this->Where(array('id' => $id))->Update($data)->Exec();
    }

}