<?php


namespace PKApp\Content\Classes;


use PKApp\Model\Classes\ModelDatabase;
use PKCore\DataBase;
use PKCore\Formats;

class ContentDataBase
{
    private static $_content = 'content_';

    private static function _getPage()
    {
        return self::$_content . 'page';
    }

    public static function GetPageDetail($id, $field = '*')
    {
        try {
            $db = new DataBase(self::_getPage());
            $where = array('id' => $id);
            return $db->Where($where)->Select($field)->First();
        } catch (\Exception $exception) {
            \PKCore\handlerException($exception);
        }
        return '';
    }

}