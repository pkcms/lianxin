<?php


namespace PKApp\Content\Classes;


use PKApp\Model\Classes\ModelExtend;
use PKCore\DataBase;
use PKCore\Formats;

class ContentsDataBase extends DataBase
{
    private $_table;
    private $_tableData;
    private $_tableCount;
    private $_tablePage;

    public function __construct($modelId)
    {
        $tableName = $this->_getModelTableName($modelId);
        parent::__construct($tableName);
        $this->_table = $tableName;
        $this->_tableData = $tableName . '_data';
        $this->_tableCount = $tableName . '_count';
    }

    private function _getModelTableName($modelId)
    {
        return ModelExtend::GetTableName($modelId);
    }

    public function GetCountContent($option = array())
    {
        return $this->Where($option)->Count();
    }

    public function GetList($option = array(), $field = '*', $index = 0, $line = 0, $orderArr = null)
    {
        array_key_exists('status', $option) ?: $option['status'] = 1;
        $result = array('count' => $this->GetCountContent($option));
        if (is_null($orderArr)) {
            $this->OrderBy('id');
        } elseif (is_array($orderArr)) {
            foreach ($orderArr as $key => $item) {
                $this->OrderBy($key, $item);
            }
        }
        $result['lists'] = $this->Where($option)->Limit($line, $index)->Select($field)->ToList();
        return $result;
    }

    public function GetListByNotData($option = array(), $field = '*')
    {
        return $this->Where($option)->Select($field)->ToList();
    }

    // ======================== 单个查询 Start

    public function GetContentDetail($id, $field = '*', $status = 1)
    {
        $where = array($this->GetTableName($this->_table) . '.id' => $id);
        if (!is_null($status)) {
            $where['status'] = $status;
        }
        $this->setLimitEmpty();
        try {
            $result = $this->TableJoin($this->_tableData,
                $this->GetTableName($this->_table) . '.id=' . $this->GetTableName($this->_tableData) . '.id')
                ->Where($where)->Select($field)->First();
            $this->From($this->_table);
            return $result;
        } catch (\Exception $exception) {
            \PKCore\handlerException($exception);
        }
        return '';
    }

    public function GetContentDetailByNext($eachIndex = 0)
    {
        try {
            $where = array('status' => 1);
            $result = $this->TableJoin($this->_tableData,
                $this->GetTableName($this->_table) . '.id=' . $this->GetTableName($this->_tableData) . '.id')
                ->Where($where)->Limit(1, $eachIndex)->Select()->First();
            $this->From($this->_table);
            return $result;
        } catch (\Exception $exception) {
            \PKCore\handlerException($exception);
        }
        return '';
    }

    public function GetContentUpOrNext($options, $orderBy, $orderByList)
    {
        $where = array('status' => 1,);
        !Formats::isArray($options) ?: $where = array_merge($where, $options);
        $this->Where($where);

        if (Formats::isArray($orderBy)) {
            foreach ($orderBy as $key => $item) {
                $this->OrderBy($key, strtoupper($item));
            }
        }
        $orderBy_list = '';
        if (Formats::isArray($orderByList)) {
            foreach ($orderByList as $key => $item) {
                $orderBy_list .= (empty($orderBy_list) ? 'ORDER BY ' : ',') . $key . ' ' . strtoupper($item);
            }
        }

        $sql = <<<eof
SELECT id,title,url {$this->getFrom()} WHERE id IN (
    SELECT id {$this->getFrom()} {$this->getWhere()} {$this->getOrder()}
    ) {$orderBy_list} limit 1; 
eof;
        $this->Sql($sql);
        return $this->First();
    }

    public function GetContentCount($id, $field = '*')
    {
        return $this->From($this->_tableCount)->Where(array('id' => $id))->Select($field)->First();
    }

    // ======================== 单个查询 End

    public function AddContent($data)
    {
        if (is_array($data) && array_key_exists('id', $data)) {
            unset($data['id']);
        }
        $postData = $data['data'];
        unset($data['data']);
        $newId = $this->From($this->_table)->Insert($data)->Exec();
        $postData['id'] = $newId;
        $this->From($this->_tableData)->Insert($postData)->Exec();
        $count_data = array('id' => $newId);
        $this->From($this->_tableCount)->Insert($count_data)->Exec();
        return $newId;
    }

    public function UpdateContent($data, $id)
    {
        $this->Where(array('id' => $id))->Update($data)->Exec();
    }

    public function UpdateContentAndData($data, $id)
    {
        $postData = $data['data'];
        unset($data['data']);
        $this->UpdateContent($data, $id);
        if (Formats::isArray($postData)) {
            $this->From($this->_tableData)->Where(array('id' => $id))->Update($postData)->Exec();
        }
    }

    public function UpdateContentCount($data, $id)
    {
        $this->From($this->_tableCount)->Where(array('id' => $id))->Update($data)->Exec();
    }

}