<?php


namespace PKApp\Content\Classes;


use PKApp\Model\Classes\ToOutData;
use PKCore\Formats;
use PKCore\PKController;
use PKCore\Request;

class ContentDetail
{
    protected $categoryEntity;
    protected $tpl;
    protected $sitePath;

    public function __construct($sitePath, $categoryEntity, $tpl)
    {
        if (empty($categoryEntity)) {
            $categoryId = Formats::isNumeric(Request::get('catId'));
            $categoryEntity = ContentExtend::GetCategory($categoryId, '*');
        }
        $this->categoryEntity = $categoryEntity;
        $this->sitePath = $sitePath;
        $this->tpl = $tpl;

    }

    public function Category()
    {
        // 当当前栏目为隐蔽栏目时，取其父级栏目取代其本身
        if (!($this->categoryEntity['status'] || ($this->categoryEntity['parentid'] == 0))) {
            $categoryEntityByNow = ContentExtend::GetCategory($this->categoryEntity['parentid'], '*');
        } else {
            $categoryEntityByNow = $this->categoryEntity;
        }
        list($categoryEntityByNow, $template) = ContentTemplateExists::GetAndHandlerCategory($categoryEntityByNow);
        $this->tpl->SetTplParam('category', $categoryEntityByNow)
            ->SetTplParam('categoryParent',
                ContentTemplateExists::GetParentIdList($this->categoryEntity['siteId'],
                    $categoryEntityByNow['arrparentid'], $this->sitePath));
//        $this->_viewPower($this->categoryEntity['setting']);
        return array($this->categoryEntity, $this->categoryEntity['template']);
    }


    //上下篇
    public function GetUpOrNext($categoryEntity, $templateSetting, $contentEntity)
    {
        // 排序方式
        $orderList = PKController::getDict('category_content_orderList');
        $order_index = Formats::isNumeric($this->categoryEntity['setting']['orderList']);
        $order_info = $orderList[$order_index];
        $order_byList = $order_by = $order_info['value'];
        $order_keys = array_keys($order_by);
        $order_byList[$order_keys[0]] = ($order_info['by'] == 'desc' ? 'ASC' : 'DESC');
        // 条件
        $where = array('catid' => $categoryEntity['id'],
            'siteid' => $categoryEntity['siteId'], 'status' => 1);
        // 下一篇
        $where[0] = $order_keys[0] . ($order_info['by'] == 'desc' ? '<' : '>') . $contentEntity[$order_keys[0]];
        $db = new ContentsDataBase($this->categoryEntity['modelid']);
        $next = $db->GetContentUpOrNext($where, $order_by, $order_by);
        // 上一篇
        $where[0] = $order_keys[0] . ($order_info['by'] == 'desc' ? '>' : '<') . $contentEntity[$order_keys[0]];
        $db = new ContentsDataBase($this->categoryEntity['modelid']);
        $previous = $db->GetContentUpOrNext($where, $order_by, $order_byList);
        //上下篇的URL规则
        $category = array('title' => $categoryEntity['name'], 'url' => $categoryEntity['url']);
        if (Formats::isArray($previous)) {
            $previous['url'] = ContentUrlFormat::Content($categoryEntity, $templateSetting, $previous, $this->sitePath);
        }
        if (Formats::isArray($next)) {
            $next['url'] = ContentUrlFormat::Content($categoryEntity, $templateSetting, $next, $this->sitePath);
        }
        $this->tpl->SetTplParam('previous', Formats::isArray($previous) ? $previous : $category)
            ->SetTplParam('next', Formats::isArray($next) ? $next : $category);
    }

    public function GetContent(array $contentEntity)
    {
        $delFieldList = array('posids', 'status', 'readpoint', 'inputip');
        foreach ($delFieldList as $item) {
            if (array_key_exists($item, $contentEntity)) {
                unset($contentEntity[$item]);
            }
        }
        $toOut = new ToOutData();
        $contentEntity = $toOut->setIsFormOut(false)->Get($contentEntity, $contentEntity['modelid']);
        $contentEntity['url'] = ContentUrlFormat::Content($this->categoryEntity, $this->categoryEntity['template'], $contentEntity);
        $this->tpl->SetTplParamList($contentEntity);
        return $contentEntity;
    }

    /**
     * @param mixed $sitePath
     */
    public function setSitePath($sitePath)
    {
        $this->sitePath = $sitePath;
    }

}