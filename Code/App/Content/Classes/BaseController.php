<?php


namespace PKApp\Content\Classes;


use PKApp\Admin\Classes\AdminExtend;
use PKCore\Fileter;
use PKCore\Formats;
use PKCore\PKController;
use PKCore\Request;
use PKCore\TMPSave;
use PKCore\Tpl;
use PKCore\Route;

abstract class BaseController extends PKController
{
    public static $siteEntity;
    public static $sitePath;

    protected function getSiteTemplates($siteId = null)
    {
        if (empty(self::$siteEntity)) {
            $siteId = empty($siteId) ? Request::get('siteId') : $siteId;
            Request::param('site_id', $siteId);
            !empty($siteId) ?: \PKCore\alert('Site_idEmpty');
            self::$siteEntity = AdminExtend::GetSiteInfo($siteId);
            self::$siteEntity['Site_Id'] = self::$siteEntity['id'];
            $setting = self::GetArrayByKey('setting', self::$siteEntity);
            $isMobileStatics = self::GetArrayByKey('IsMobileStatics', $setting);
            $this->tpl()->is_Mobile = (bool)$isMobileStatics;
            if ((Request::isMobile() || Request::has('m', 'get')) && $isMobileStatics) {
                self::$sitePath = self::GetArrayByKey('MobileStaticsPath', $setting);
            } else {
                self::$sitePath = self::GetArrayByKey('Site_Path', self::$siteEntity);
            }
            unset(self::$siteEntity['id']);
        }
        $notSetTemplateTagKey = array('Site_statisticsCode', 'setting', 'id');
        if (Formats::isArray(self::$siteEntity)) {
            foreach (self::$siteEntity as $key => $value) {
                if ($key == 'Site_Contact') {
                    $siteContact = self::$siteEntity[$key];
                    foreach ($siteContact as $index => $item) {
                        $new_index = 'siteContact_' . strtolower($index);
                        $this->tpl()->SetTplParam($new_index,
                            (($index == 'qq') ? explode(',', $item) : $item));
                    }
                } elseif ($key == 'info') {
                    $siteInfo = self::$siteEntity[$key];
                    foreach ($siteInfo as $index => $item) {
                        $new_index = 'site_' . strtolower($index);
                        $this->tpl()->SetTplParam($new_index,
                            (($index == 'qq') ? explode(',', $item) : $item));
                    }
                } else {
                    in_array($key, $notSetTemplateTagKey) ?: $this->tpl()->SetTplParam(strtolower($key), $value);
                }
            }
        }
        $this->tpl()->SetTplThemes(self::$siteEntity['setting']['Site_Themes']);
    }

    protected function tplDisplay($tmpFile)
    {
        return $this->tpl()->Display(
            Request::isMobile() && $this->tpl()->is_Mobile ? $tmpFile . '_m' : $tmpFile);
    }

    protected function tpl()
    {
        static $tpl;
        if (empty($tpl)) {
            $tpl = new Tpl();
        }
        return $tpl;
    }

    public static function CheckIP($app, $timeCell = 0, $tips = '')
    {
        $ip = Request::ServerIP();
        $dirName = $app . 'IP';
        $fileName = date('Y-m-d') . '.json';
        $nowTime = time();
        $log_ip = TMPSave::get($fileName, $dirName);
        if (empty($log_ip) || !is_array($log_ip)) {
            $log_ip = array($ip => $nowTime);
        } elseif (is_array($log_ip)) {
            if (array_key_exists($ip, $log_ip)) {
                // 如果有过 IP 记录，则检查行动的时间间隔
                if (($nowTime - $log_ip[$ip]) < $timeCell) {
                    if (empty($tips)) {
                        return false;
                    } else {
                        \PKCore\alert(Route\language($tips, $app));
                    }
                }
            }
            $log_ip[$ip] = $nowTime;
        }
        TMPSave::set($log_ip, $fileName, $dirName);
        return true;
    }

}