<?php


namespace PKApp\Content\Classes;


use PKCore\Request;

class ContentUrlFormat
{

    /**
     * 栏目的URL转换
     * @param $category
     * @param $templateConfig
     * @param null $sitePath
     * @param bool $isPageNav
     * @param bool $isSearch
     * @return mixed|string
     */
    public static function Category($category, $templateConfig, $sitePath = NULL, $isPageNav = false)
    {
        $isRewrite = in_array($category['type'], array(1, 2)) &&
            (((array_key_exists('categoryHtml', $templateConfig) && $templateConfig['categoryHtml']) ||
                (array_key_exists('pageHtml', $templateConfig) && $templateConfig['pageHtml'])) ||
                $templateConfig['isRewrite']);
        switch ($category['type']) {
            case 1:
                if ($isRewrite) {
                    $url = str_replace('[id]', $category['id'], $templateConfig['pageUrl']);
                    return (empty($sitePath) ? DS . $url : rtrim($sitePath,DS) . DS . $url);
                } else {
                    return '/index.php/content/category?id=' . $category['id'];
                }
                break;
            case 2:
                $isAdminCategoryContentList = Request::controller() == 'AdminGetContentList';
                if ($isRewrite && !$isAdminCategoryContentList) {
                    $url = preg_replace('(\[id\]|\[catid\])', $category['id'], $templateConfig['categoryUrl']);
                    $isPageNav ?: $url = preg_replace('(-\[page\]|_\[page\]|/\[page\]|\[page\])', '', $url);
                    return '/' . (empty($sitePath) ? $url : $sitePath . '/' . $url);
                } elseif ($isAdminCategoryContentList) {
                    return '/index.php/content/AdminGetContentList?catId=' . $category['id'] . '&query='
                        . Request::get('query') . '&pageIndex=[page]';
                } else {
                    return '/index.php/content/category?id=' . $category['id']
                        . ($isPageNav ? '&pageIndex=[page]' : '');
                }
                break;
            case 0:
                return $category['url'];
                break;
        }
        return '';
    }

    /**
     * 内容页URL规则
     * @param $category
     * @param $templateConfig
     * @param $content
     * @param null $sitePath
     * @return string
     */
    public static function Content($category, $templateConfig, $content, $sitePath = NULL)
    {
        $isRewrite = $templateConfig['contentHtml'] || $templateConfig['isRewrite'];
        if ($isRewrite && !empty($content['url'])) {
            $url = ltrim($content['url'], '/');
        } elseif ($isRewrite && isset($templateConfig['contentHtml']) && !empty($templateConfig['contentHtml'])) {
            $url = str_replace('[id]', $content['id'], $templateConfig['contentUrl']);
            $url = str_replace('[modelid]', $category['modelid'], $url);
            $url = str_replace('[catid]', $category['id'], $url);
        } else {
            return '/index.php/content/content?siteId=' . $content['siteId'] . '&catId=' . $content['catid'] . '&id=' . $content['id'];
        }
        return (empty($sitePath) ? DS . $url : rtrim($sitePath,DS) . DS . $url);
    }
}
