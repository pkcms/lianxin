<?php


namespace PKApp\Content\Classes;


use PKCore\Converter;
use PKCore\DataBase;

class CategoryDataBase extends DataBase
{

    public function __construct()
    {
        parent::__construct('category');
    }

    public function GetList($siteId, $parentId = null, $isAll = false,
                            $lineNum = null, $index = 0)
    {
        $option = array('siteId' => $siteId);
        is_null($parentId) ?: $option['parentid'] = $parentId;
        $field = '*';
        if (!$isAll) {
            $option['status'] = 1;
            $field = array('id', 'parentid', 'type', 'arrchildid',
                'name', 'subname', 'image', 'url', 'seo', 'template', 'total', 'listsort');
        }
        return $this->_getList($option, $field, $lineNum, $index);
    }


    public function GetListByAdmin($siteId, $viewId = null, $noInId = null)
    {
        $option = array('siteId' => $siteId, 'status <> 4');
        if (!is_null($viewId) && is_array($viewId)) {
            $option['id'] = $viewId;
        } elseif (!is_null($viewId) && is_numeric($viewId)) {
            $option['parentid'] = $viewId;
        }
        if (!is_null($noInId) && is_array($noInId)) {
            $option[] = 'id NOT IN(' . implode(',', $noInId) . ')';
        }
        return $this->_getList($option, array('id', 'siteId', 'arrchildid', 'parentid', 'type', 'name', 'url',
            'template', 'total', 'listsort'));
    }

    public function GetListByModelId($siteId, $modelId)
    {
        return $this->_getList(
            array('siteId' => $siteId, 'modelid' => $modelId),
            array('id', 'name'));
    }

    public function GetListById($siteId, $idList = array())
    {
        $option = array('siteId' => $siteId, 'id' => $idList, 'status NOT IN(4)');
        return $this->Where($option)
            ->OrderBy('id', 'ASC')
            ->Select(array('id', 'parentid', 'modelid', 'type', 'name', 'url', 'template'))->ToList();
    }

    private function _getList(array $option, $field = '*', $lineNum = null, $index = 0)
    {
        array_key_exists('status', $option) ?: $option[] = 'status NOT IN(4)';
        if ($lineNum > 0) {
            $this->Limit($lineNum, $index);
        }
        return $this->Where($option)
            ->OrderBy('listsort', 'ASC')
            ->OrderBy('id', 'ASC')
            ->Select($field)->ToList();
    }

    public function GetCategoryByMakeHtml($siteId, $lineIndex = 0)
    {
        return $this->_getByLimit(array('siteId' => $siteId), $lineIndex);
    }

    public function GetCategoryByNext($eachIndex = 0)
    {
        return $this->_getByLimit(array('status' => 1), $eachIndex, array('id', 'name', 'seo'));
    }

    public function GetCategoryDetail($id, $field = '*')
    {
        $option = array('id' => $id, 'status <> 4');
        $entity = $this->Where($option)->Select($field)->First();
        !empty($entity) ?: \PKCore\alert('Category_Empty');
        if (is_array($entity)) {
            if (array_key_exists('seo', $entity)) {
                $entity['seo'] = Converter::Unserialize($entity['seo']);
            }
            if (array_key_exists('template', $entity)) {
                $entity['template'] = Converter::Unserialize($entity['template']);
            }
            if (array_key_exists('setting', $entity)) {
                $entity['setting'] = Converter::Unserialize($entity['setting']);
            }
        }
        return $entity;
    }

    private function _getByLimit(array $option, $lineIndex, $field = '*')
    {
        $option[] = 'status NOT IN(4)';
        return $this->Where($option)
            ->OrderBy('id', 'ASC')
            ->Limit(1, $lineIndex)
            ->Select($field)->First();
    }

    public function SetCategoryDetail($data)
    {
        $id = 0;
        if (is_array($data) && array_key_exists('id', $data)) {
            empty($data['id']) ?: $id = $data['id'];
            unset($data['id']);
        }
        empty($id) ? $id = $this->Insert($data)->Exec() : $this->SetCategoryDetailById($data, $id);
        return $id;
    }

    public function SetCategoryDetailById($param, $id)
    {
        $this->Where(array('id' => $id))->Update($param)->Exec();
    }

}