<?php


namespace PKApp\Content\Classes;


use PKApp\Admin\Classes\AdminDataBase;
use PKCore\Converter;
use PKCore\Files;
use PKCore\Fileter;
use PKCore\Formats;
use PKCore\Tpl;

class ContentExtend
{

    private static $_categoryIdChildList;

    public static function GetCategoryList($SiteId, $parentId = null, $isTree = false, $isAll = false, $lineNum = null, $index = 0)
    {
        $db_category = new CategoryDataBase();
        $entityList = $db_category->GetList($SiteId, $parentId, $isAll, $lineNum, $index);
        if ($isTree && Formats::isArray($entityList)) {
            $siteEntity = AdminDataBase::GetSite(array('id' => $SiteId), array('Site_Path'));
            $entityList = self::TreeCategoryList($entityList, $parentId, $siteEntity['Site_Path']);
        }
        return $entityList;
    }

    public static function AdminGetCategoryList($SiteId, $parentId = null, $isTree = false, $viewIdLists = null)
    {
        $db_category = new CategoryDataBase();
        $entityList = $db_category->GetListByAdmin($SiteId, is_null($viewIdLists) ? $parentId : $viewIdLists);
        if ($isTree && Formats::isArray($entityList)) {
            self::$_categoryIdChildList = array();
            $siteEntity = AdminDataBase::GetSite(array('id' => $SiteId), array('Site_Path'));
            $entityList = self::TreeCategoryList($entityList, empty($parentId) ? 0 : $parentId, $siteEntity['Site_Path']);
            if ($parentId == null) {
                Files::putContents(PATH_DATA, 'CategoryChildList_' . $SiteId . '.php',
                    Converter::arrayToSaveString(self::$_categoryIdChildList));
            }
        }
        return $entityList;
    }

    public static function TreeCategoryList($entityList, $parentId = null, $sitePath = null)
    {
        $result = array();
        $nowChild = array_filter($entityList, function ($entity) use ($parentId) {
            return $entity['parentid'] == $parentId ? $entity : array();
        });
        if (Formats::isArray($nowChild)) {
            foreach ($nowChild as $item) {
                empty($item['arrchildid']) ?: self::$_categoryIdChildList[$item['id']] = explode(',', $item['arrchildid']);
                if (is_array($item) && array_key_exists('template', $item)) {
                    empty($item['template']) ?: $item['template'] = Converter::Unserialize($item['template']);
                    $item['url'] = ContentUrlFormat::Category($item, $item['template'], $sitePath);
                }
                $item['child'] = self::TreeCategoryList($entityList, $item['id'], $sitePath);
                $result[] = $item;
            }
        }
        return $result;
    }

    /**
     * 栏目的下拉菜单选择器
     * @param $SiteId
     * @param $parentId
     * @return false|string
     */
    public static function GetCategoryOptionList($SiteId, $parentId)
    {
        $tpl = new Tpl();
        return $tpl->SetTplParam('lists', ContentExtend::AdminGetCategoryList($SiteId, null, true))
            ->SetTplParam('valueId', $parentId)
            ->PhpDisplay('category_option');
    }

    public static function GetCategory($id, $field = null)
    {
        if (is_null($field)) {
            $field = array('id', 'siteid', 'type', 'modelid', 'name', 'template');
        }
        $db_category = new CategoryDataBase();
        $categoryEntity = $db_category->GetCategoryDetail($id, $field);
        !empty($categoryEntity) ?: \PKCore\alert('Category_Empty');
        return $categoryEntity;
    }

    /**
     * 单页面形式的 HTML 信息反转义
     * @param $post
     * @return mixed
     */
    public static function CategoryByPageTypeHtmlDeCode($post)
    {
        $fieldArr = array('title', 'content', 'seotitle', 'seokeywords', 'seodescription');
        foreach ($post as $key => $value) {
            !in_array($key, $fieldArr) ?: $post[$key] = Fileter::htmlspecialchars_decode($value);
        }
        return $post;
    }
}