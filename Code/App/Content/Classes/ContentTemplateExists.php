<?php


namespace PKApp\Content\Classes;


use PKApp\Admin\Classes\AdminDataBase;
use PKApp\Model\Classes\FieldTypeByOptionsDataBase;
use PKApp\Model\Classes\ModelDatabase;
use PKCore\Converter;
use PKCore\Fileter;
use PKCore\Route;
use PKCore\Formats;

class ContentTemplateExists
{

    public static function GetSiteId($param)
    {
        return max((is_array($param) && array_key_exists('siteId', $param) ? $param['siteId'] : 0), 1);
    }

    /**
     * 内容列表在输出前的处理
     * @param $modelId
     * @param $categoryListEntity
     * @param $options
     * @param $lineNum
     * @param null $fieldEntity
     * @param int $lineIndex
     * @param null $orderArr
     * @return array
     */
    public static function GetContentList($modelId, $categoryListEntity, $options, $lineNum,
                                          $fieldEntity = null, $lineIndex = 0, $orderArr = null)
    {
        // 选项字段的处理 start
        $fieldList = $fieldEntity['fieldList'];
        $indexFieldList = $fieldEntity['isIndex'];
        $optionList = self::_getOptionList($indexFieldList, $fieldList);
        // 选项字段的处理 end
        $viewFieldList = array('id', 'title', 'catid', 'inputtime', 'thumb', 'url', 'seotitle', 'seokeywords', 'seodescription');
        is_null($indexFieldList) || !Formats::isArray($indexFieldList) ?: $viewFieldList = array_merge($viewFieldList, $indexFieldList);
        $db_content = new ContentsDataBase($modelId);
        $contentEntity = $db_content->GetList($options, $viewFieldList, $lineIndex, $lineNum, $orderArr);
        $contentListEntity = $contentEntity['lists'];
        if (count($categoryListEntity) == 0) {
            $catIds = array();
            foreach ($contentListEntity as $index => $item) {
                in_array($item['catid'], $catIds) ?: $catIds[] = $item['catid'];
            }
            list($modelId, $categoryListEntity) =
                ContentTemplateExists::GetTemplateCatId($options['siteId'], $catIds);
        }
        foreach ($contentListEntity as $index => $item) {
            // 检索模型列表字段
            if (Formats::isArray($indexFieldList)) {
                foreach ($indexFieldList as $field) {
                    if (array_key_exists($field, $item) && !empty($item[$field])) {
                        if (is_array($optionList) && array_key_exists($field, $optionList)) {
                            $tmp = trim($item[$field], ',');
                            $tmp_selectList = explode(',',$tmp);
                            $tmp_value = '';
                            foreach ($tmp_selectList as $select_item) {
                                $tmp_value .= (empty($tmp_value) ? '' : ',') . $optionList[$field][$select_item];
                            }
                            $item[$field] =$tmp_value;
                        } else {
                            $tmp_arr = Converter::Unserialize($item[$field]);
                            $item[$field] = !empty($tmp_arr) ? $tmp_arr : Fileter::htmlspecialchars_decode($item[$field]);
                        }
                    }
                }
            }
            $category = $categoryListEntity[$item['catid']];
            $item['url'] = ContentUrlFormat::Content($category, $category['template'], $item, BaseController::$sitePath);
            $contentListEntity[$index] = $item;
        }
        return array($contentListEntity, $contentEntity['count']);
    }

    private static function _getOptionList($indexFieldList, $fieldList)
    {
        $result = array();
        if (Formats::isArray($indexFieldList)) {
            foreach ($indexFieldList as $field) {
                if (array_key_exists($field, $fieldList) && is_array($fieldList[$field])
                    && $fieldList[$field]['formtype'] == 'option') {
                    $setting = $fieldList[$field]['setting'];
                    $db_options = new FieldTypeByOptionsDataBase();
                    $optionList = $db_options->GetOptionsLists($setting['option'], array('id', 'name'));
                    if (Formats::isArray($optionList)) {
                        $result[$field] = Converter::arrayColumn($optionList, 'name', 'id');
                    }
                }
            }
        }
        return $result;
    }

    public static function GetContentListByTemplateTag($modelId, $categoryListEntity, $param, $options, $lineNum,
                                                       $indexFieldList = null)
    {
        $assign = is_array($param) && array_key_exists('assign', $param) ? $param['assign'] : 'data';
        list($listEntity, $count) = self::GetContentList($modelId, $categoryListEntity, $options, $lineNum, $indexFieldList);
        return array($assign => $listEntity);
    }

    /**
     * 前台在输出栏目的相关模板变量信息前的处理
     * 去除栏目的模板及其它配置的信息，提取栏目的当前的URL
     * @param $categoryEntity
     * @return array
     */
    public static function GetAndHandlerCategory($categoryEntity)
    {
        $template = $categoryEntity['template'];
//        $setting = $categoryEntity['setting'];
        unset($categoryEntity['template'], $categoryEntity['setting']);
        $categoryEntity['url'] = ContentUrlFormat::Category($categoryEntity, $template, BaseController::$sitePath);
        return array($categoryEntity, $template);
    }

    /**
     * 组合栏目 ID
     * @param $siteId
     * @param $catId
     * @return array
     */
    public static function GetTemplateCatId($siteId, $catId)
    {
        empty($catId) ?: $catIdList = is_array($catId) ? $catId : explode(',', $catId);
        $cache_childList = Route\isLoadingFile(PATH_DATA . 'CategoryChildList_' . $siteId, true);
        $newCatIdList = array();
        if (isset($catIdList)) {
            $newCatIdList = $catIdList;
            foreach ($catIdList as $item) {
                !Formats::isArray($cache_childList[$item]) ?: $newCatIdList = array_merge($newCatIdList, $cache_childList[$item]);
            }
            $db_category = new CategoryDataBase();
            $categoryListEntity = $db_category->GetListById($siteId, $newCatIdList);
        }
        $content_categoryListEntity = array();
        $modelId = 0;
        if (isset($categoryListEntity) && Formats::isArray($categoryListEntity)) {
            foreach ($categoryListEntity as $categoryItem) {
                $categoryItem['template'] = Converter::Unserialize($categoryItem['template']);
                $content_categoryListEntity[$categoryItem['id']] = $categoryItem;
                if (empty($modelId)) {
                    $modelId = $categoryItem['modelid'];
                }
            }
        }
        return array($modelId, $content_categoryListEntity, $newCatIdList);
    }

    public static function GetParentIdList($siteId, $parentIds, $sitePath)
    {
        $parentIdList = explode(',', $parentIds);
        $db_category = new CategoryDataBase();
        $categoryListEntity = $db_category->GetListById($siteId, $parentIdList);
        $result = array();
        if (Formats::isArray($categoryListEntity)) {
            foreach ($categoryListEntity as $item) {
                $result[] = array('name' => $item['name'],
                    'url' => ContentUrlFormat::Category($item,
                        Converter::Unserialize($item['template']), $sitePath));
            }
        }
        return $result;
    }

}