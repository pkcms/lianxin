<?php

namespace PKApp\Content\Classes;

use PKApp\Admin\Classes\AdminDataBase;
use PKApp\Model\Classes\ModelExtend;
use PKCore\Converter;
use PKCore\Formats;
use PKCore\PKController;
use PKCore\Request;
use PKCore\Statics;

class ContentTemplateTag extends BaseController
{

    private $_lineNum, $_modelId, $_content_categoryListEntity;

    public function category($param)
    {
        $siteId = ContentTemplateExists::GetSiteId($param);
        $parentId = is_array($param) && array_key_exists('parentid', $param) ? $param['parentid'] : 0;
        $num = is_array($param) && array_key_exists('num', $param) ? Formats::isNumeric($param['num']) : null;
        $isPage = is_array($param) && array_key_exists('isPage', $param) ? Formats::isBool($param['isPage']) : false;
        $assign = is_array($param) && array_key_exists('assign', $param) ? $param['assign'] : 'categoryList';
        $this->getSiteTemplates($siteId);
        // 栏目形式的翻页
        $index = 0;
        if ($isPage) {
            $this->GetPages();
            $index = (self::$pageIndex - 1) * $num;
            $num = self::$pageIndex * $num;
        }
        $categoryListEntity = ContentExtend::GetCategoryList($siteId, $parentId, false, false, $num, $index);
        $categoryListCount = count($categoryListEntity);
        // 遍历返回栏目的列表
        foreach ($categoryListEntity as $index => $itemEntity) {
            $itemEntity['seo'] = empty($itemEntity['seo']) ? array() : Converter::Unserialize($itemEntity['seo']);
            $template = Converter::Unserialize($itemEntity['template']);
            unset($itemEntity['template']);
            $itemEntity['url'] = ContentUrlFormat::Category($itemEntity, $template, self::$sitePath);
            $itemEntity['seo']['description'] = htmlspecialchars_decode($itemEntity['seo']['description']);
            $categoryListEntity[$index] = $itemEntity;
        }
        $result = array($assign => $categoryListEntity);
        !isset($pageEntity) ?: $result['categoryPageNav'] = $pageEntity;
//        print_r($result);
//        exit();
        return $result;
    }

    private function _globalOptionsOrLimit($param)
    {
        $siteId = ContentTemplateExists::GetSiteId($param);
        $this->_lineNum = is_array($param) && array_key_exists('num', $param) ? Formats::isNumeric($param['num']) : 1;
        $option = array('status' => 1, 'siteid' => $siteId);
        list($this->_modelId, $this->_content_categoryListEntity, $option['catid']) =
            ContentTemplateExists::GetTemplateCatId($siteId, $param['catid']);
        return $option;
    }

    public function position($param)
    {
        $option = $this->_globalOptionsOrLimit($param);
        $positionTypeArray = array('headlines', 'totop', 'posids');
        if (in_array($param['position'], $positionTypeArray)) {
            $option[$param['position']] = true;
        }
        if (!Formats::isArray($option['catid'])) {
            return array();
        }
        $fieldEntity = ModelExtend::GetModelFieldEntity($this->_modelId);
        return ContentTemplateExists::GetContentListByTemplateTag($this->_modelId, $this->_content_categoryListEntity, $param, $option,
            $this->_lineNum, $fieldEntity);
    }

    function related($param)
    {
        ((strtolower(Request::module()) == 'content') && (strtolower(Request::controller()) == 'content')) || (Request::controller() == 'AdminDoMakeHtml') ?: \PKCore\alert(\PKCore\Route\language('TemplateTag_RelatedError'));
        $option = $this->_globalOptionsOrLimit($param);
        if (isset($param['keywords']) && !empty($param['keywords'])) {
            $keywords = explode(',', $param['keywords']);
            foreach ($keywords as $item) {
                $param[0] = 'title LIKE "%' . urldecode($item) . '%"';
                $param[1] = 'seotitle LIKE "%' . urldecode($item) . '%"';
                $param[2] = 'seokeywords LIKE "%' . urldecode($item) . '%"';
            }
        }
        $contentId = Formats::isNumeric(Request::get('id'));
        $option[] = 'id != ' . $contentId;
        $fieldEntity = ModelExtend::GetModelFieldEntity($this->_modelId);
        return ContentTemplateExists::GetContentListByTemplateTag($this->_modelId, $this->_content_categoryListEntity,
            $param, $option, $this->_lineNum, $fieldEntity);
    }

    public function lists($param)
    {
        $option = $this->_globalOptionsOrLimit($param);
        if (!Formats::isArray($option['catid'])) {
            return array();
        }
        $where_array = array(1 => 'headlines', 'totop', 'posids');
        if (array_key_exists($param['notPosids'], $where_array)) {
            $option[$where_array[$param['notPosids']]] = 0;
        }
        $fieldEntity = ModelExtend::GetModelFieldEntity($this->_modelId);
        return ContentTemplateExists::GetContentListByTemplateTag($this->_modelId, $this->_content_categoryListEntity,
            $param, $option, $this->_lineNum, $fieldEntity);
    }

    public function categoryContentList()
    {
        if (Request::controller() != 'AdminDoMakeHtml') {
            (strtolower(Request::module()) == 'content') && (strtolower(Request::controller()) == 'category') ?: \PKCore\alert('TemplateTag_ContentListError');
        }
        $catId = Formats::isNumeric(Request::get('id'));
        !empty($catId) ?: \PKCore\alert('Category_idEmpty');
        $categoryEntity = ContentExtend::GetCategory($catId, '*');
        $siteId = ContentTemplateExists::GetSiteId($categoryEntity);
        $option = array('status' => 1, 'siteid' => $siteId);
        // 检查子栏目
        list($modelId, $CategoryListEntity, $option['catid']) =
            ContentTemplateExists::GetTemplateCatId($siteId,
                empty($categoryEntity['arrchildid']) ? $categoryEntity['id'] : $categoryEntity['arrchildid']);
        // 父（子）栏目的组合
        $CategoryListEntity[] = $categoryEntity;
        $lineNum = $categoryEntity['setting']['listDataSize'];
        // 进入正体
        // 排序
        $orderIndex = $categoryEntity['setting']['orderList'];
        if (is_null($orderIndex) || !is_numeric($orderIndex) || ($orderIndex == '')) {
            $orderIndex = 0;
        }
        $orderList = PKController::getDict('category_content_orderList');
        $order = $orderList[$orderIndex]['value'];
        $fieldEntity = ModelExtend::GetModelFieldEntity($categoryEntity['modelid']);
        // 字段搜索
        $isSearch_list = $fieldEntity['isSearch'];
        if (Formats::isArray($isSearch_list)) {
            $get = Request::get();
            foreach ($get as $field =>$value) {
                if (in_array($field,$isSearch_list)) {
                    header('location:' . listParamsSearchUrl($siteId, $catId));
                    exit();
                }
            }
        }
        $this->GetPages();
        list($contentList, $contentTotal) = ContentTemplateExists::GetContentList(
            $categoryEntity['modelid'], $CategoryListEntity,
            $option, $lineNum, $fieldEntity, self::$pageIndex, $order);
        $pageNavUrl = ContentUrlFormat::Category($categoryEntity, $categoryEntity['template'], self::$sitePath, true);
        $pageNav = Statics::pages($contentTotal, $lineNum, PKController::$pageIndex, $pageNavUrl);
        return array('contentListData' => $contentList, 'pageNavList' => $pageNav);
    }

    public function Main()
    {
    }
}
