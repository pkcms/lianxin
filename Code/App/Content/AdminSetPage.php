<?php


namespace PKApp\Content;


use PKApp\Admin\Classes\AdminController;
use PKApp\Content\Classes\ContentPageDataBase;
use PKCore\Formats;
use PKCore\Request;

class AdminSetPage extends AdminController
{

    public function Main()
    {
        $this->checkPostFieldIsEmpty('title', 'Content_titleEmpty');
        $this->checkPostFieldIsEmpty('id', 'Category_idEmpty');
        $post = Request::post();
        $id = Request::post('id');
        $db_page = new ContentPageDataBase();
        $pageEntity = $db_page->GetPageDetail($id);
        if (empty($post['seotitle'])) {
            $post['seotitle'] = $post['title'];
        }
        if (array_key_exists('type', $post)) {
            unset($post['type']);
        }
        $post['siteid'] = $this->loginUser()->SiteId;
        if (Formats::isArray($pageEntity)) {
            $db_page->UpdatePage($post, $id);
        } else {
            $db_page->AddPage($post);
        }
        \PKCore\msg('Data_Input_Success');
    }
}