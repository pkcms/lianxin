<?php


namespace PKApp\Content;


use PKApp\Admin\Classes\AdminController;
use PKApp\Content\Classes\ContentDataBase;
use PKApp\Content\Classes\ContentExtend;
use PKApp\Content\Classes\ContentsDataBase;
use PKApp\Model\Classes\ModelExtend;
use PKApp\Model\Classes\ModelFormControl;
use PKApp\Template\Classes\TemplateExists;
use PKCore\Converter;
use PKCore\Formats;
use PKCore\Request;
use PKCore\Tpl;

class AdminGetContent extends AdminController
{

    public function Main()
    {
        $catId = Formats::isNumeric(Request::get('catId'));
        $categoryEntity = ContentExtend::GetCategory($catId);
        $contentEntity = $this->_getContent($categoryEntity['modelid']);
        $formObj = new ModelFormControl();
        $fieldList = $formObj->Get($contentEntity, $categoryEntity['modelid']);
        $tpl = new Tpl();
        $showTplList = TemplateExists::GetFileList($this->loginUser()->SiteId, 'content', 'show');
        $tpl->SetTplParam('category', $categoryEntity)
            ->SetTplParam('fieldList', $fieldList)->SetTplParamList($contentEntity)
            ->SetTplParam('showTplList', $this->getItemByArrayKey($showTplList))
            ->SetTplParam('contentAttributeList', self::getDict('content_attribute'));
        return $tpl->Display('content_editor');
    }

    private function _getContent($modelId)
    {
        $contentId = Formats::isNumeric(Request::get('id'));
        if (empty($contentId)) {
            return array();
        }
        $db_content = new ContentsDataBase($modelId);
        $contentEntity = $db_content->GetContentDetail($contentId, '*', null);
        $contentEntity['inputtime'] = Converter::date('Y-m-d', $contentEntity['inputtime']);
        return $contentEntity;
    }
}