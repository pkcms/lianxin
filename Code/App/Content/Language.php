<?php
/**
 * 模块语言包
 * User: Administrator
 * Date: 2019/5/27
 * Time: 9:28
 */

return array(
    'TemplateTag_ContentListError' => array(
        'zh-cn' => '请注意！该模板标签只能在栏目页中使用',
        'en' => ''
    ),
    'TemplateTag_RelatedError' => array(
        'zh-cn' => '请注意！该模板标签只能在内容页中使用',
        'en' => ''
    ),
    'Model_Empty' => array(
        'zh-cn' => '模型信息找不到或不存在',
        'en' => ''
    ),
    'Model_idEmpty' => array(
        'zh-cn' => '请选择模型',
        'en' => ''
    ),
    'Site_idEmpty' => array(
        'zh-cn' => '站点ID不能为空',
        'en' => ''
    ),
    'Category_Empty' => array(
        'zh-cn' => '栏目信息找不到或不存在',
        'en' => ''
    ),
    'Category_idEmpty' => array(
        'zh-cn' => '栏目ID不能为空',
        'en' => ''
    ),
    'Category_NameEmpty' => array(
        'zh-cn' => '栏目名称不能为空',
        'en' => ''
    ),
    'Content_idEmpty' => array(
        'zh-cn' => '内容ID不能为空',
        'en' => ''
    ),
    'Content_titleEmpty' => array(
        'zh-cn' => '内容标题不能为空',
        'en' => ''
    ),
    'Content_Empty' => array(
        'zh-cn' => '内容信息找不到或不存在',
        'en' => ''
    ),
    'Content_mobileCategory_Error' => array(
        'zh-cn' => '被转移内容的目标栏目的数据模型不相同，所以不能移动',
        'en' => ''
    ),
    'Content_attribute_Error' => array(
        'zh-cn' => '您操作的内容属性不在允许的属性列表中',
        'en' => ''
    ),
    'Content_ViewPower_Error' => array(
        'zh-cn' => '您当前没有查看该内容的权限，如想阅读请通过本站的联系方式同站长沟通。',
        'en' => ''
    ),
    'Page_Empty' => array(
        'zh-cn' => '单页信息找不到或不存在',
        'en' => ''
    ),
    'SearchQuery_Empty' => array(
        'zh-cn' => '请输入搜索关键字',
        'en' => ''
    ),
    'Search_ParamEmpty' => array(
        'zh-cn' => '没有检测到 query 请求参数，或者其他筛选参数',
        'en' => ''
    ),
    'Search_Error' => array(
        'zh-cn' => '请选择被搜索的数据模型，并输入搜索关键字',
        'en' => ''
    ),
    'Search_Category_TypeError' => array(
        'zh-cn' => '请选择被搜索的栏目的栏目类型必须是【内容列表】类型',
        'en' => ''
    ),
    'Import_file_error' => array(
        'zh-cn' => '导入数据的文件不支持',
        'en' => ''
    ),
    'Import_fieldIndex_Empty' => array(
        'zh-cn' => '导入数据中的主列表字段必须有一个',
        'en' => ''
    ),
);