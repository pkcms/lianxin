<?php


namespace PKApp\Template;


use PKApp\Admin\Classes\AdminController;
use PKApp\Template\Classes\TemplateExists;
use PKCore\Files;
use PKCore\Request;
use PKCore\Route;

class AdminSetMaterialDetail extends AdminController
{


    public function Main()
    {
    }

    public function Delete()
    {
        $fileId = Request::get('fileId');
        !empty($fileId) ?: \PKCore\alert('template_fileIdEmpty');
        $siteThemesDir = TemplateExists::GetTemplatePath($this->loginUser()->SiteId, 'style');
        $path = $siteThemesDir . DS . $fileId;
        file_exists($path) ?: \PKCore\msg(Route\language('naterial_PathNotExists') . $path);
        unlink($path);
        \PKCore\msg('naterial_Delete_Success', null);
    }

}