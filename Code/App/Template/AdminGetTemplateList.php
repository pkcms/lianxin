<?php


namespace PKApp\Template;


use PKApp\Admin\Classes\AdminController;
use PKApp\Admin\Classes\AdminExtend;
use PKApp\Template\Classes\TemplateExists;
use PKCore\Files;
use PKCore\Request;
use PKCore\Route;
use PKCore\Tpl;

class AdminGetTemplateList extends AdminController
{

    public function Main()
    {
        $siteThemesDir = $templateType = Request::get('type');
        $searchFile = '';
        switch ($templateType) {
            case 'style':
                $searchFile = '.css';
                break;
            case 'script':
                $searchFile = '.js';
                $siteThemesDir = 'style';
                break;
            default:
                $searchFile = '.tpl';
                break;
        }
        $tpl = new Tpl();
        $fileList = TemplateExists::GetFileList($this->loginUser()->SiteId, $siteThemesDir, $searchFile);
        $tpl->SetTplParam('lists', $fileList)->SetTplParam('templateType',$templateType);
        return $tpl->Display('template_list');
    }
}