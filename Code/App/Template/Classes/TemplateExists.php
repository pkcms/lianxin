<?php


namespace PKApp\Template\Classes;


use PKApp\Admin\Classes\AdminExtend;
use PKCore\Files;
use PKCore\Tpl;
use PKCore\Route;

class TemplateExists
{

    public static function GetTemplatePath($siteId, $templateType)
    {
        $siteEntity = AdminExtend::GetSiteInfo($siteId);
        $setting = $siteEntity['setting'];
        $siteThemesDir = PATH_ROOT . 'Templates' . DS . $setting['Site_Themes'] . DS . $templateType;
        return $siteThemesDir;
    }

    public static function GetFileList($siteId, $siteThemesDir, $searchFile)
    {
        $siteThemesDir = self::GetTemplatePath($siteId, $siteThemesDir);
        $tpl = new Tpl();
        file_exists($siteThemesDir) ?: \PKCore\alert(Route\language('template_PathNotExists', 'template'), $siteThemesDir);
        return Files::opendir($siteThemesDir, $searchFile);
    }

}