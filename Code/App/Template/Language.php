<?php
/**
 * 模块语言包
 * User: Administrator
 * Date: 2019/5/27
 * Time: 9:28
 */

return array(
    'template_PathNotExists' => array(
        'zh-cn' => '模板路径不存在',
        'en' => ''
    ),
    'template_fileIdEmpty' => array(
        'zh-cn' => '文件名不能为空',
        'en' => ''
    ),
    'template_codeEmpty' => array(
        'zh-cn' => '文件的代码不能为空',
        'en' => ''
    ),
    'template_typeEmpty' => array(
        'zh-cn' => '文件的类型不能为空',
        'en' => ''
    ),
    'naterial_PathNotExists' => array(
        'zh-cn' => '素材路径不存在，路径：',
        'en' => ''
    ),
    'naterial_Delete_Success' => array(
        'zh-cn' => '素材删除成功，请关闭此窗口后刷新列表',
        'en' => ''
    ),
    'template_file_delError' => array(
        'zh-cn' => '你要删除的文件已经不存在，或找不到',
        'en' => ''
    ),
);