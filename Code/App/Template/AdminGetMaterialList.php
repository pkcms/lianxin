<?php


namespace PKApp\Template;


use PKApp\Admin\Classes\AdminController;
use PKApp\Template\Classes\TemplateExists;
use PKCore\Files;
use PKCore\Request;
use PKCore\Tpl;
use PKCore\Route;

class AdminGetMaterialList extends AdminController
{

    public function Main()
    {
        $tpl = new Tpl();
        $siteThemesDir = TemplateExists::GetTemplatePath($this->loginUser()->SiteId, 'style');
        $siteThemesDirArray = explode(DS, $siteThemesDir);
        file_exists($siteThemesDir) ?: $tpl->Notice(Route\language('template_PathNotExists','template') . $siteThemesDir);
        $fileList = Files::opendir($siteThemesDir, array('jpg', 'git', 'png'));
        $tpl->SetTplParam('lists', $fileList)->SetTplParam('domain', Request::domain() . 'Templates/'
            . $siteThemesDirArray[count($siteThemesDirArray) - 2] . '/style/');
        return $tpl->Display('material_list');
    }

}