<?php


namespace PKApp\Template;


use PKApp\Admin\Classes\AdminController;
use PKApp\Template\Classes\TemplateExists;
use PKCore\Request;

class AdminSetTemplateOther extends AdminController
{

    public function Main()
    {
        $action = Request::post('action');
        if (method_exists($this,$action)) {
            $this->$action();
        }
        \PKCore\msg('Data_Input_Success');
    }

    protected function del()
    {
        $this->checkPostFieldIsEmpty('selectData','template_fileIdEmpty');
        $this->checkPostFieldIsEmpty('type','template_typeEmpty');
        $fileName = Request::post('selectData');
        $templateType = Request::post('type');
        switch ($templateType) {
            case 'style':
            case 'script':
                $siteThemesDir = 'style';
                break;
            default:
                $siteThemesDir = $templateType;
                break;
        }
        $siteThemesDir = TemplateExists::GetTemplatePath($this->loginUser()->SiteId, $siteThemesDir);
        $path = $siteThemesDir . DS . $fileName;
        file_exists($path) ? unlink($path) : \PKCore\alert('template_file_delError');
    }
}