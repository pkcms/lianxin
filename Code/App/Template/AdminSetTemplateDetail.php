<?php


namespace PKApp\Template;


use PKApp\Admin\Classes\AdminController;
use PKApp\Template\Classes\TemplateExists;
use PKCore\Files;
use PKCore\Fileter;
use PKCore\Request;
use PKCore\Tpl;

class AdminSetTemplateDetail extends AdminController
{

    public function Main()
    {
        $id = Request::get('id');
        $templateType = Request::get('type');
        switch ($templateType) {
            case 'style':
                $syntax = 'css';
                break;
            case 'script':
                $syntax = 'js';
                break;
            default:
                $syntax = 'html';
                break;
        }
        $tpl = new Tpl();
        if (!empty($id)) {
            $siteThemesDir = TemplateExists::GetTemplatePath($this->loginUser()->SiteId,
                $this->_handlerThemeDir(Request::get('type')));
            $tpl->SetTplParam('code', Fileter::htmlspecialchars(Files::getContents($siteThemesDir . DS . $id)), false);
        }
        $tpl->SetTplParam('fileId', $id)->SetTplParam('templateType', $templateType)->SetTplParam('syntax', $syntax);
        $tplFile = 'code_form';
        return empty($id) ? $tpl->Display($tplFile) : $tpl->PhpDisplay($tplFile);
    }

    public function Post()
    {
        $this->checkPostFieldIsEmpty('fileId', 'template_fileIdEmpty');
//        $this->checkPostFieldIsEmpty('code', 'template_codeEmpty');
        $this->checkPostFieldIsEmpty('templateType', 'template_typeEmpty');
        $post = Request::post();
        $siteThemesDir = TemplateExists::GetTemplatePath($this->loginUser()->SiteId,
            $this->_handlerThemeDir(Request::post('templateType')));
        if (!stristr($post['fileId'], '.')) {
            switch (Request::post('templateType')) {
                case 'style':
                    $post['fileId'] .= '.css';
                    break;
                case 'script':
                    $post['fileId'] .= '.js';
                    break;
                default:
                    $post['fileId'] .= '.tpl';
                    break;
            }
        }
//        $post['code'] = Fileter::htmlspecialchars_decode($post['code']);
        $post['code'] = htmlspecialchars_decode($post['code'], ENT_QUOTES);
        $post['code'] = stripslashes($post['code']);
        Files::putContents($siteThemesDir, $post['fileId'], $post['code']);
        \PKCore\msg('Data_Input_Success');
    }

    private function _handlerThemeDir($siteThemesDir)
    {
        $siteThemesDir != 'script' ?: $siteThemesDir = 'style';
        return $siteThemesDir;
    }
}