<{admin_breadcrumb('模板管理')}>

<!-- Title -->
<div class="page-header">
    <div class="page-title">
        <h3>
            图片素材
        </h3>
    </div>
</div>
<!-- /Title -->
<!-- Row -->
<div class="row">
    <div class="col-md-12">
        <div class="widget box">
            <div class="widget-header">
                <h4>
                    <i class="icon-reorder"></i>
                    图片素材上传
                </h4>
            </div>
            <div class="widget-content">
                <{UploadFile('image','','material')}>
            </div>
        </div>
    </div>
    <div class="col-md-12">
        <div class="widget box">
            <div class="widget-header">
                <h4>
                    <i class="icon-reorder"></i>
                    图片素材列表
                </h4>
            </div>
            <div class="widget-content">
                <ul class="row">
                    <{loop $lists $item}>
                    <li class="col-lg-3 col-md-4 col-sm-5 col-xs-6 thumbnail">
                        <img src="<{$domain}><{$item}>">
                        <div>
                            <a href="<{$domain}><{$item}>" target="_blank">
                                <i class="icon-eye-open"></i>&nbsp;<{$item}>
                            </a>
                            <a href="/index.php/Template/AdminSetMaterialDetail/Delete/?fileId=<{$item}>" target="_blank">
                                <i class="icon-remove"></i>&nbsp;
                            </a>
                        </div>
                    </li>
                    <{/loop}>
                </ul>
            </div>
        </div>
    </div>
</div>
<!-- /Row -->
