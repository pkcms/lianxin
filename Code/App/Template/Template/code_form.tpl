<{admin_breadcrumb('模板管理')}>

<!-- Title -->
<div class="page-header">
    <div class="page-title">
        <h3>
            代码编辑
        </h3>
    </div>
</div>
<!-- /Title -->
<!-- Row -->
<div class="row">
    <div class="col-md-12">
        <div class="widget box">
            <div class="widget-header">
                <h4>基本填写</h4>
            </div>
            <div class="widget-content">
                <form class="form-horizontal row-border" id="TemplateFileForm" action="Template/AdminSetTemplateDetail/Post">
                    <div class="form-group">
                        <label class="col-md-2 control-label">
                            文件名:
                        </label>
                        <div class="col-md-10">
                            <input type="text" class="form-control" name="fileId" value="<{$fileId}>" />
                            <p class="help-block">
                                无需输入文件的扩展名，
                            </p>
                        </div>
                    </div>
                    <div class="form-actions">
                        <input type="hidden" name="templateType" value="<{$templateType}>">
                        <button type="button" data-form="TemplateFileForm" class="btn btn-primary pull-left">
                            提交保存
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- /Row -->
