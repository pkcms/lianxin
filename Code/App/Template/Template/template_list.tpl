<{admin_breadcrumb('模板管理')}>

<!-- Title -->
<div class="page-header">
    <div class="page-title">
        <h3>
            模板文件列表
        </h3>
    </div>
</div>
<!-- /Title -->
<!-- Row -->
<div class="row">
    <div class="col-md-12">
        <div class="widget">
            <div class="widget-content">
                <div class="btn-toolbar btn-toolbar-demo">
                    <a class="btn btn-success" data-ajax="true" data-ajax-begin="beginPaging"
                       data-ajax-failure="failurePaging"
                       data-ajax-mode="replace" data-ajax-success="successPaging"
                       data-ajax-update="#main"
                       href="/index.php/Template/AdminSetTemplateDetail/?type=<{$templateType}>">
                        <i class="icon-plus-sign"></i>&nbsp;添加
                    </a>
                    <button class="btn btn-danger" data-action="del" data-table="templateListForm">
                        <i class="icon-remove-circle"></i>
                        删除
                    </button>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-12">
        <div class="widget box">
            <div class="widget-header">
                <h4>
                    <i class="icon-reorder"></i>
                    列表
                </h4>
            </div>
            <div class="widget-content" id="templateListForm" data-action="Template/AdminSetTemplateOther">
                <input type="hidden" name="type" value="<{$templateType}>"/>
                <table class="table table-bordered table-striped table-hover mb-0">
                    <thead class="thead-secondary">
                    <tr>
                        <th width="30"></th>
                        <th>文件名</th>
                    </tr>
                    </thead>
                    <tbody>
                    <{loop $lists $item}>
                        <tr>
                            <td><input type="radio" value="<{$item}>" name="set_id"/></td>
                            <td>
                                <a target="_blank"
                                   href="/index.php/Template/AdminSetTemplateDetail/?type=<{$templateType}>&id=<{$item}>">
                                    <i class="icon-edit"></i>&nbsp;<{$item}>
                                </a>
                            </td>
                        </tr>
                        <{/loop}>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<!-- /Row -->
