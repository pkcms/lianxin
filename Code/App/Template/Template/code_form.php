<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="fr">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <title>在线代码编辑</title>
    <script type="text/javaScript" src="/statics/jquery/jquery-1.8.3.min.js"></script>
    <script language="Javascript" type="text/javascript"
            src="/statics/edit_area/edit_area_full.js"></script>
    <script language="Javascript" type="text/javascript">
        // initialisation
        editAreaLoader.init({
            id: "example_1"	// id of the textarea to transform
            , start_highlight: true	// if start with highlight
            , allow_resize: "both"
            , allow_toggle: false
            , word_wrap: true
            , toolbar: "save,|, search, go_to_line, fullscreen, |, undo, redo, |, select_font"
            , save_callback: "my_save"
            , language: "zh"
            , syntax: '<?php if (isset($syntax)) {
                echo $syntax;
            } else {
                echo 'html';
            } ?>'
        });

        // callback functions
        function my_save(id, content) {
            $.ajax({
                type: "post",
                url: '/index.php/Template/AdminSetTemplateDetail/Post',
                data: {
                    "templateType": "<?php if (isset($templateType)) {
                        echo $templateType;
                    } ?>",
                    "fileId": "<?php if (isset($fileId)) {
                        echo $fileId;
                    } ?>",
                    "code": content
                },
                success: function (data) {
                    console.log(data);
                    alert(data.StateMsg);
                },
                error: function (xhr, status, error) {
                    console.log($.parseJSON(xhr.responseText));
                }
            });
            // alert("Here is the content of the EditArea '" + id + "' as received by the save callback function:\n" + content);
        }

    </script>
</head>
<body>
<fieldset>
    <legend>文件名：<?php if (isset($fileId)) {
            echo $fileId;
        } ?></legend>
    <textarea id="example_1" style="height: 550px; width: 100%;" name="test_1"><?php if (isset($code)) {
            echo $code;
        } ?></textarea>
</fieldset>
</body>
</html>
