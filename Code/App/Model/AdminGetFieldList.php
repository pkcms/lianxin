<?php


namespace PKApp\Model;

use PKApp\Admin\Classes\AdminController;
use PKApp\Model\Classes\FieldDateBase;
use PKApp\Model\Classes\ModelDatabase;
use PKCore\Request;
use PKCore\Tpl;

class AdminGetFieldList extends AdminController
{

    public function Main()
    {
        $modelId = Request::get('modelid');
        $db_model = new ModelDatabase();
        $modelEntity = $db_model->GetById($modelId);
        $tpl = new Tpl();
        $tpl->SetTplParam('modelid', $modelId);
        if (!empty($modelEntity) && is_array($modelEntity)) {
            $tpl->SetTplParam('modelname', $modelEntity['name']);
            $tpl->SetTplParam('type', $modelEntity['type']);
        }
        $db = new FieldDateBase();
        $lists = $db->GetFieldLists(array('modelid' => $modelId),
            array('id', 'modelid', 'formtype', 'field', 'name', 'isindex', 'issearch', 'isEmpty', 'isShow', 'listsort'));
        $tpl->SetTplParam('lists', $lists);
        return $tpl->Display('field_lists');
    }
}