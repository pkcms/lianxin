<?php


namespace PKApp\Model\Classes;


use PKCore\Converter;
use PKCore\Formats;
use PKCore\Tpl;

class ModelFormControl
{

    private $_editorIsLoadJs = true, $isNull, $datetimeJs;
    private $_fieldList, $_dataList;
    private $_modelId;

    public function Get($dataList, $modelId, $filterField = NULL)
    {
        $this->_modelId = $modelId;
        $fieldEntity = ModelExtend::GetModelFieldEntity($modelId);
        if (!Formats::isArray($fieldEntity)) {
            return array();
        }
        $this->_fieldList = $fieldEntity['fieldList'];
        $toOut = new ToOutData();
        $dataList = $toOut->Get($dataList, $modelId, $filterField);
        $filterFieldArray = !empty($filterField) ? explode(',', $filterField) : array();
        $result = array();
        foreach ($this->_fieldList as $field => $item) {
            if ($item['isShow'] == 0) {
                continue;
            }
            if (count($filterFieldArray) && !in_array($field, $filterFieldArray)) {
                continue;
            }
            $func = $item['formtype'];
            $value = (is_array($this->_dataList) && array_key_exists($field, $this->_dataList))
                ? $this->_dataList[$field] : '';

            $html = method_exists($this, $func) ? $this->$func($field, $dataList[$field]) : '';
//            print_r($item);
//            exit();
            $result[$field] = array('name' => $item['name'], 'field' => $field, 'htmlcode' => $html,
                'isNull'=>$item['IsEmpty']);
        }
        return $result;
    }

    protected function editor($field, $value)
    {
        $fieldEntity = $this->_fieldList[$field];
        return FormControl::editor($field, ($fieldEntity['isindex'] ? $field : 'data[' . $field . ']'),
            $value, 'width:80%;');
    }

    protected function textarea($field, $value)
    {
        $fieldEntity = $this->_fieldList[$field];
        // css
        $setting = $fieldEntity['setting'];
        $css = isset($setting['lenw']) ? 'width:' . $setting['lenw'] . '%;' : '';
        $css .= isset($setting['lenw']) ? 'height:' . $setting['lenw'] . 'px;' : '';
        $tpl = new Tpl();
        $tpl->is_Model_output = true;
        $tpl->SetTplParam('css', ' style="' . $css . '"')
            ->SetTplParam('id', $field)
            ->SetTplParam('field', $fieldEntity['isindex'] ? $field : 'data[' . $field . ']')
            ->SetTplParam('name', $fieldEntity['name'])
            ->SetTplParam('value', $value);
        return $tpl->SetTplDir('Model')->PhpDisplay('form_control_' . $fieldEntity['formtype']);
    }

    function frontend_code($field, $value) {
        $fieldEntity = $this->_fieldList[$field];;
        // css
        $setting = $fieldEntity['setting'];
        $css = isset($setting['lenw']) ? 'width:' . $setting['lenw'] . '%;' : '';
        $css .= isset($setting['lenw']) ? 'height:' . $setting['lenw'] . 'px;' : '';
        $tpl = new Tpl();
        $tpl->is_Model_output = true;
        $tpl->SetTplParam('css', ' style="' . $css . '"')
            ->SetTplParam('id', $field)
            ->SetTplParam('field', $fieldEntity['isindex'] ? $field : 'data[' . $field . ']')
            ->SetTplParam('name', $fieldEntity['name'])
            ->SetTplParam('value', $value);
        return $tpl->SetTplDir('Model')->PhpDisplay('form_control_textarea');
    }

    protected function text($field,$value)
    {
        $fieldEntity = $this->_fieldList[$field];
        $setting = $fieldEntity['setting'];
        $tpl = new Tpl();
        $tpl->is_Model_output = true;
        $tpl->SetTplParam('isLoadJs', $this->_editorIsLoadJs)
            ->SetTplParam('code_html', 'maxLength="' . $setting['len'] . '"')
            ->SetTplParam('id', $field)
            ->SetTplParam('field', $fieldEntity['isindex'] ? $field : 'data[' . $field . ']')
            ->SetTplParam('name', $fieldEntity['name'])
            ->SetTplParam('value', $value);
        return $tpl->SetTplDir('Model')->PhpDisplay('form_control_' . $fieldEntity['formtype']);
    }

    protected function upload_list($field, $value)
    {
        $fieldEntity = $this->_fieldList[$field];
        $tpl = new Tpl();
        $tpl->is_Model_output = true;
        $tpl->SetTplParam('liId', $field)
            ->SetTplParam('inputName', $fieldEntity['isindex'] ? $field : 'data[' . $field . ']')
            ->SetTplParam('modelId', $fieldEntity['modelid'])
            ->SetTplParam('valueList', $value);
        return $tpl->SetTplDir('Model')->PhpDisplay('form_control_uploadList');
    }

    protected function image_list($field, $value)
    {
        $fieldEntity = $this->_fieldList[$field];
        $tpl = new Tpl();
        $tpl->is_Model_output = true;
        $tpl->SetTplParam('id', $field)
            ->SetTplParam('inputName', $fieldEntity['isindex'] ? $field : 'data[' . $field . ']')
            ->SetTplParam('modelId', $fieldEntity['modelid'])
            ->SetTplParam('valueList', $value);
        return $tpl->SetTplDir('Model')->PhpDisplay('form_control_imageList');
    }

    protected function baidu_map($field, $value)
    {
        $fieldEntity = $this->_fieldList[$field];
        $tpl = new Tpl();
        $tpl->is_Model_output = true;
        $tpl->SetTplParam('id', $field)
            ->SetTplParam('field', $fieldEntity['isindex'] ? $field : 'data[' . $field . ']')
            ->SetTplParam('name', $fieldEntity['name'])
            ->SetTplParam('value', $value);
        return $tpl->SetTplDir('Model')->PhpDisplay('form_control_baidumap');
    }

    protected function option($field, $value)
    {
        $fieldEntity = $this->_fieldList[$field];
        $setting = $fieldEntity['setting'];
        if (is_array($setting) && array_key_exists('boxtype', $setting)
            && array_key_exists('option', $setting)) {
            //选项参数
            $db_options = new FieldTypeByOptionsDataBase();
            $optionList = $db_options->GetOptionsLists($setting['option'], array('id', 'name'));
            !Formats::isArray($optionList) ?: $optionList = Converter::arrayColumn($optionList, 'name', 'id');
            $name = $fieldEntity['isindex'] ? $field : 'data[' . $field . ']';
            //选项类型
            switch ($setting['boxtype']) {
                case 'radio':
                    return FormControl::OptionByRadio($field, $name, $optionList, $value);
                    break;
                case 'checkbox':
                    return FormControl::OptionByCheckBox($field, $name, $optionList, $value);
                    break;
                case 'select':
                    return FormControl::OptionBySelect($field, $name, $optionList, $value);
                    break;
            }
        }
        return '';
    }

}