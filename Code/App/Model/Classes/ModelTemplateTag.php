<?php


namespace PKApp\Model\Classes;


use PKCore\Converter;

class ModelTemplateTag
{

    public function fieldData($params) {
        $id = isset($params['id']) ? $params['id'] : 0;
        $result = array('fieldList'=>array());
        if (empty($id)) {
            return $result;
        }
        $db =new FieldTypeByOptionsDataBase();
        $lists = $db->GetOptionsLists($id);
        $result['fieldList'] = (Converter::arrayColumn($lists,'name','id'));
        return $result;
    }

}