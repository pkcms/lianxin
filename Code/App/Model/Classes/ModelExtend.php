<?php


namespace PKApp\Model\Classes;


use PKCore\Converter;
use PKCore\Files;
use PKCore\Formats;
use PKCore\PKController;
use PKCore\Request;
use PKCore\Route;

class ModelExtend
{

    public static function GetTypeId($typeList, $type = null)
    {
        $typeId = 0;
        !is_null($type) ?: $type = Request::get('type');
        !empty($type) ?: $type = 'content';
        if (Formats::isArray($typeList)) {
            $nowType = array_filter($typeList, function ($typeInfo) use ($type) {
                if (is_array($typeInfo) && array_key_exists(0, $typeInfo)) {
                    return $typeInfo[0] == $type ? $typeInfo : array();
                }
                return array();
            });
            !Formats::isArray($typeList) ?: $typeId = key($nowType);
        }
        return $typeId;
    }

    public static function GetModelFieldEntity($modelId)
    {
        static $entity;
        !empty($entity) && is_array($entity) ?: $entity = array();
        if (!array_key_exists($modelId, $entity)) {
            $path = PATH_CACHE . 'Data' . DS . 'Model_' . $modelId;
            $entity[$modelId] = Route\isLoadingFile($path, true);
        }
        return $entity[$modelId];
    }

    public static function GetModel($modelId)
    {
        static $modelList = array();
        if (array_key_exists($modelId, $modelList)) {
            return $modelList[$modelId];
        }
        $db_model = new ModelDatabase();
        $modelEntity = $db_model->GetById($modelId);
        !empty($modelEntity) && is_array($modelEntity) ?: \PKCore\alert(Route\language('Model_NoExists','Model'));
        $modelList[$modelId] = $modelEntity;
        return $modelEntity;
    }

    public static function GetTableName($modelId)
    {
        // 检查模型是否已经存在
        static $modelList = array();
        if (array_key_exists($modelId, $modelList)) {
            return $modelList[$modelId];
        }
        $modelTypeLists = PKController::getDict('modelTypeLists');
        $modelEntity = ModelExtend::GetModel($modelId);
        if (is_array($modelEntity) && array_key_exists('type', $modelEntity)
            && array_key_exists('tablename', $modelEntity)
            && is_array($modelTypeLists) && array_key_exists($modelEntity['type'], $modelTypeLists)) {
            $modelType = $modelTypeLists[$modelEntity['type']][0];
            $tableName = $modelType . '_' . $modelEntity['tablename'];
            $modelList[$modelId] = $tableName;
            return $tableName;
        }
        return '';
    }

    public static function SaveCache($modelId)
    {
        $db_field = new FieldDateBase();
        $fieldList = $db_field->GetFieldLists(array('modelid' => $modelId, 'isShow' => 1), '*');
        $search = $index = $notNull = array();
        $cache = array('fieldList' => array(), 'notNull' => array(), 'isIndex' => array(), 'isSearch' => array(),);
        foreach ($fieldList as $k => $v) {
            $v['setting'] = Converter::Unserialize($v['setting']);
            $cache['fieldList'][$v['field']] = $v;
            if ($v['issearch'])
                $search[] = $v['field'];
            if ($v['isindex'])
                $index[] = $v['field'];
            if ($v['isEmtpy'])
                $notNull[] = $v['field'];
        }
        $cache['notNull'] = $notNull;
        $cache['isIndex'] = $index;
        $cache['isSearch'] = $search;
        Files::putContents(PATH_DATA, 'Model_' . $modelId . '.php', Converter::arrayToSaveString($cache));
    }
}