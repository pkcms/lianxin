<?php


namespace PKApp\Model\Classes;


use PKCore\Request;
use PKCore\Tpl;
use PKCore\Route;

class FormControl
{

    public static function OptionBySelect($inputId, $inputName, $selectList, $value, $disabledKeyList = null)
    {
        if (is_string($value) && stristr($value, ',')) {
            $valueList = explode(',', $value);
        } elseif (is_string($value)) {
            $valueList = array($value);
        } elseif (is_array($value)) {
            $valueList = $value;
        } else {
            $valueList = array();
        }
        // TODO: 禁用选择项
        $tpl = new Tpl();
        $tpl->SetTplDir('Model')
            ->SetTplParam('disabledKeyList', is_array($disabledKeyList) ? $disabledKeyList : array())
            ->SetTplParam('inputId', $inputId)
            ->SetTplParam('inputName', $inputName)
            ->SetTplParam('select', Route\language('Select'))
            ->SetTplParam('selectList', $selectList)
            ->SetTplParam('valueList', $valueList);
        return $tpl->PhpDisplay('form_control_select');
    }

    public static function OptionByCheckBox($inputId, $inputName, $selectList, $valueList)
    {
        $tpl = new Tpl();
        $tpl->SetTplParam('inputId', $inputId)
            ->SetTplParam('inputName', $inputName)
            ->SetTplParam('selectList', $selectList)
            ->SetTplParam('valueList', $valueList);
        return $tpl->SetTplDir('Model')->PhpDisplay('form_control_checkbox');
    }

    public static function OptionByRadio($inputId, $inputName, $selectList, $value)
    {
        $tpl = new Tpl();
        $tpl->SetTplParam('inputId', $inputId)
            ->SetTplParam('inputName', $inputName)
            ->SetTplParam('selectList', $selectList)
            ->SetTplParam('value', $value);
        return $tpl->SetTplDir('Model')->PhpDisplay('form_control_radio');
    }

    public static function UploadFile($inputName, $uploadType, $value, $inputTextId = null, $inputFileId = 'fileToUpload')
    {
        $tpl = new Tpl();
        $tpl->SetTplParam('inputId', $inputTextId == null ? $inputName : $inputTextId)
            ->SetTplParam('inputName', $inputName)
            ->SetTplParam('inputFileId', $inputFileId)
            ->SetTplParam('value', $value)
            ->SetTplParam('uploadType', $uploadType);
        return $tpl->SetTplDir('Model')->PhpDisplay('form_control_html5_fileUpload');
    }

    public static function UploadFile_ImageList($legendName, $liId, $inputName, $value)
    {
        $tpl = new Tpl();
        $tpl->SetTplParam('liId', $liId)
            ->SetTplParam('legendName', $legendName)
            ->SetTplParam('inputName', $inputName)
            ->SetTplParam('uploadList', $value);
        return $tpl->SetTplDir('Model')->PhpDisplay('form_control_uploadList_imageEach');
    }

    public static function editor($inputId, $inputName, $value, $css = null)
    {
        $tpl = new Tpl();
        $tpl->SetTplParam('css', $css)
            ->SetTplParam('id', $inputId)
            ->SetTplParam('name', $inputName)
            ->SetTplParam('value', $value);
        $key = 'editorIsLoadJs';
        $tplFile = 'form_control_editor';
        $tpl->SetTplParam('isLoadJs', !Request::has($key, 'param'));
        Request::param($key, true);
        return $tpl->SetTplDir('Model')->PhpDisplay($tplFile);
    }

}