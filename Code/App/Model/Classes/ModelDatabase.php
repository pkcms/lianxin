<?php


namespace PKApp\Model\Classes;


use PKCore\DataBase;
use PKCore\Files;
use PKCore\Formats;

class ModelDatabase extends DataBase
{
    public function __construct()
    {
        parent::__construct('model');
    }

    public function GetLists($type, $where = array(), $field = '*')
    {
        $options = array('type' => Formats::isNumeric($type));
        if (Formats::isArray($where)) {
            $options = array_merge($options, $where);
        }
        return $this->Where($options)->Select($field)->ToList();
    }

    public function GetById($id, $field = '*')
    {
        $options = array('id' => Formats::isNumeric($id));
        return $this->Where($options)->Select($field)->First();
    }

    public function GetByTableName($name)
    {
        $options = array('tablename' => $name);
        return $this->Where($options)->Select()->First();
    }

    public function CheckTableNameBySQL($name)
    {
        return $this->CheckTableNameExists($name);
    }

    public function Add($data)
    {
        return $this->Insert($data)->Exec();
    }

    public function UpdateModel($data, $id)
    {
        return $this->Where(array('id' => $id))->Update($data)->Exec();
    }

    public function DelModel($id)
    {
        $this->Where(array('id' => $id))->Delete()->Exec();
    }

    public function doSQL($modelType, $replaceTableName)
    {
        $nowConfig = $this->GetNowConfig();
        if (is_array($nowConfig) && array_key_exists('type', $nowConfig)) {
            $path = __DIR__ . DS . '..' . DS . 'install' . DS;
            switch ($nowConfig['type']) {
                case 'sqlite':
                    $path .= $modelType . '.model.sqlite.sql';
                    break;
                case 'mysql':
                    $path .= $modelType . '.model.mysql.sql';
                    break;
            }
            if (isset($path) && array_key_exists('prefix', $nowConfig)) {
                $sql = Files::getContents($path);
                $sql = str_replace('[tablename]', $nowConfig['prefix'] . $replaceTableName, $sql);
                $this->Sql($sql)->Exec();
            } else {
                \PKCore\alert('SQLFile_Exists');
            }
        }
    }

}

