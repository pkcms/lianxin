<?php


namespace PKApp\Model\Classes;


use PKCore\DataBase;
use PKCore\Formats;

class FieldDateBase extends DataBase
{

    public function __construct()
    {
        parent::__construct('model_field');
    }

    public function GetFieldLists($options, $field)
    {
        return $this->Where($options)->OrderBy('listsort', 'asc')
            ->OrderBy('id', 'asc')->Select($field)->ToList();
    }

    public function GetFieldById($id)
    {
        $options = array('id' => Formats::isNumeric($id));
        return $this->GetField($options);
    }

    public function GetField($options)
    {
        return $this->Where($options)->Select()->First();
    }

    public function AddField($data)
    {
        return $this->Insert($data)->Exec();
    }

    public function UpdateField($data, $id)
    {
        $options = array('id' => Formats::isNumeric($id));
        return $this->Where($options)->Update($data)->Exec();
    }

    public function CheckFieldExists($tableName, $fieldName)
    {
        $tableName = $this->GetTableName($tableName);
        $sql = <<<Eof
select sql from sqlite_master where name="{$tableName}" and type='table';
Eof;
        $entity = $this->Sql($sql)->First();
        if (!empty($entity) && is_array($entity) && array_key_exists('sql', $entity)) {
            return stristr($entity['sql'], '[' . $fieldName . ']') ? TRUE : FALSE;
        }
        return false;
    }

    public function AddFieldChange($tableName, $fieldParam)
    {
        $tableName = $this->GetTableName($tableName);
        $type = resultSqlite3FieldType($fieldParam);
        $sql = <<<Eof
ALTER TABLE {$tableName} ADD COLUMN [{$fieldParam['field']}] {$type} NULL;
Eof;
        $this->Sql($sql)->Exec();
    }

}

function resultSqlite3FieldType(array $fieldParam)
{
    if (array_key_exists('formtype', $fieldParam)) {
        switch ($fieldParam['formtype']) {
            case 'editor':
            case 'text':
            case 'option':
            case 'upload_once':
            case 'linkage':
            case 'textarea':
            case 'choose':
            case 'upload_list':
            case 'image_list':
            case 'baidumap';
            case 'script_code':
            case 'line_link':
                return 'TEXT';
                break;
            case 'date':
            case 'datetime':
            case 'number':
                if (array_key_exists('setting', $fieldParam) && is_array($fieldParam['setting'])) {
                    if (array_key_exists('decimal', $fieldParam['setting'])) {
                        return 'REAL(0,' . $fieldParam['setting']['decimal'] . ")";
                    }
                }
                return 'INTEGER';
                break;
        }
    }
    return '';
}
