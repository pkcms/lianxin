<?php


namespace PKApp\Model\Classes;


use PKCore\DataBase;
use PKCore\Formats;

class FieldTypeByOptionsDataBase extends DataBase
{

    public function __construct()
    {
        parent::__construct('model_options');
    }

    public function GetOptionsLists($parentId, $field = '*')
    {
        $options = array('parentId' => Formats::isNumeric($parentId));
        return $this->Where($options)->Select($field)->ToList();
    }

    public function GetOptions($id, $field = '*')
    {
        $options = array('id' => Formats::isNumeric($id));
        return $this->Where($options)->Select($field)->First();
    }

    public function GetOptionsByIdList($idList, $field = '*')
    {
        $options = array('id' => $idList);
        return $this->Where($options)->Select($field)->ToList();
    }

    public function AddOptions($data)
    {
        return $this->Insert($data)->Exec();
    }

    public function UpdateOptions($data, $id)
    {
        return $this->Where(array('id' => $id))->Update($data)->Exec();
    }

    public function DelOptions($id)
    {
        $this->Where(array('id' => $id))->Delete()->Exec();
    }

}