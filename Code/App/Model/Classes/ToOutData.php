<?php


namespace PKApp\Model\Classes;


use PKCore\Converter;
use PKCore\Fileter;
use PKCore\Formats;
use PKCore\Request;
use PKCore\Tpl;

class ToOutData
{

    private $_editorIsLoadJs = true, $isNull, $datetimeJs;
    private $_fieldList, $_dataList;
    private $_modelId;
    private $_isFormOut = true;

    /**
     * 是否以表单格式输出
     * @param $isFormOut
     * @return $this
     */
    public function setIsFormOut($isFormOut)
    {
        $this->_isFormOut = $isFormOut;
        return $this;
    }

    public function Get($dataList, $modelId, $filterField = NULL)
    {
        $this->_modelId = $modelId;
        $fieldEntity = ModelExtend::GetModelFieldEntity($modelId);
        if (!Formats::isArray($fieldEntity)) {
            return array();
        }
        $this->_fieldList = $fieldEntity['fieldList'];
        $this->_dataList = $dataList;
        $filterFieldArray = !empty($filterField) ? explode(',', $filterField) : array();
        $result = array();
        foreach ($this->_fieldList as $field => $item) {
            if ($item['isShow'] == 0) {
                continue;
            }
            if (count($filterFieldArray) && !in_array($field, $filterFieldArray)) {
                continue;
            }
            $func = $item['formtype'];
            $value = (is_array($this->_dataList) && array_key_exists($field, $this->_dataList))
                ? $this->_dataList[$field] : '';

            $this->_dataList[$field] = method_exists($this, $func) ? $this->$func($field) : '';
        }
        return $this->_dataList;
    }

    protected function editor($field)
    {
        return Fileter::htmlspecialchars_decode($this->_dataList[$field]);
    }

    protected function textarea($field)
    {
        // value 回车处理
        $replace = array("\r\n", " ");
        $search = array("<br/>", '&nbsp;');
        return str_replace($search, $replace, $this->_dataList[$field]);
    }

    protected function frontend_code($field)
    {
        $value = $this->_dataList[$field];
        return htmlspecialchars_decode($value, ENT_QUOTES);
    }

    protected function text($field)
    {
        return $this->_dataList[$field];
    }

    protected function upload_list($field)
    {
        return Formats::isArray($this->_dataList) ? Converter::Unserialize($this->_dataList[$field]) : array();
    }

    protected function image_list($field)
    {
        return Formats::isArray($this->_dataList) ? Converter::Unserialize($this->_dataList[$field]) : array();
    }

    protected function baidu_map($field)
    {
        return $this->_dataList[$field];
    }

    protected function option($field)
    {
        $result = '';
        $fieldEntity = $this->_fieldList[$field];
        $setting = $fieldEntity['setting'];
        if (is_array($setting) && array_key_exists('boxtype', $setting)) {
            $db_options = new FieldTypeByOptionsDataBase();
            //选项类型
            switch ($setting['boxtype']) {
                case 'radio':
                case 'select':
                    $value = $this->_dataList[$field];
                    if (!$this->_isFormOut) {
                        $optionEntity = $db_options->GetOptions($value, array('name'));
                        !Formats::isArray($optionEntity) ?: $result = $optionEntity['name'];
                    } else {
                        $result = $value;
                    }
                    break;
                case 'checkbox':
                    $value = trim($this->_dataList[$field], ',');
                    $valueList = explode(',', $value);
                    if (!$this->_isFormOut) {
                        $optionEntity = $db_options->GetOptionsByIdList($valueList, array('name'));
                        if (is_array($optionEntity)) {
                            $optionEntity = Converter::arrayGetByKey($optionEntity, 'name');
                            $result = implode(',', $optionEntity);
                        }
                    } else {
                        $result = $valueList;
                    }
                    break;
            }
        }
        return $result;
    }

}