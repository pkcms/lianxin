<?php

namespace PKApp\Model\Classes;

use PKApp\Content\Classes\ContentExtend;
use PKCore\Fileter;
use PKCore\Formats;
use PKCore\Request;
use PKCore\Statics;
use PKCore\Route;

class ToInsertData
{

    private $_fieldList;
    private $_dataList;

    public function Get($dataList, $modelId)
    {
        $fieldListEntity = ModelExtend::GetModelFieldEntity($modelId);
        if (!Formats::isArray($fieldListEntity)) {
            return array();
        }
        $this->_fieldList = $fieldListEntity['fieldList'];
        $this->_dataList = $dataList;
        $this->_fastDataList($this->_dataList);
        return $this->_dataList;
    }

    private function _fastDataList($dataList, $key = null)
    {
        foreach ($dataList as $field => $value) {
            if (is_array($value)) {
                $this->_fastDataList($value, 'data');
            }
            $fieldEntity = '';
            if (array_key_exists($field, $this->_fieldList)) {
                $fieldEntity = $this->_fieldList[$field];
            }
            if (empty($fieldEntity) || !is_array($fieldEntity))
                continue;
            $func = $fieldEntity['formtype'];
            if ($fieldEntity['isEmpty'] && empty($value)) {
                \PKCore\alert(Route\language('field_postEmpty', 'model'), $field);
                break;
            }
            // TODO: 数据提交入库前的数据检验
//            if ($fieldEntity['preg']) {
//            }
            if (!is_null($key)) {
                $this->_dataList[$key][$field] = method_exists($this, $func) ? $this->$func($fieldEntity, $value) : '';
            } else {
                $this->_dataList[$field] = method_exists($this, $func) ? $this->$func($fieldEntity, $value) : '';
            }
        }
    }

    protected function text($field, $value)
    {
        return $value;
    }

    protected function textarea($field, $value)
    {
        return $value;
    }

    protected function frontend_code($field, $value)
    {
        return $value;
    }

    protected function editor($field, $value)
    {
        $setting = $field['setting'];
        $imgList = Statics::imgsinarticle(\htmlspecialchars_decode($value));
        $thumb = Request::post('thumb');
        if (Formats::isArray($imgList) && empty($thumb)) {
            !empty($thumb) ?: $this->_dataList['thumb'] = $imgList[0];
        }
        return $value;
    }

    protected function option($field, $value)
    {
        $setting = $field['setting'];
        if ($setting['boxtype'] == 'checkbox') {
            $result = ',' . implode(',', $value) . ',';
        } else {
            $result = $value;
        }
        return $result;
    }

    protected function upload_list($field, $value)
    {
        $setting = $field['setting'];
        if ($setting['isPass']) {
            $password = array();
            foreach ($value['fileurl'] as $key => $v) {
                $password[$key] = \PKCore\random(6);
            }
            $value['password'] = $password;
        }
        return serialize($value);
    }

    protected function image_list($field, $value)
    {
        $thumb = Request::post('thumb');
        !empty($thumb) || !Formats::isArray($value) ?: $this->_dataList['thumb'] = $value['fileurl'][0];
        return serialize($value);
    }

    protected function baidu_map($field, $value)
    {
        return $value;
    }

}
