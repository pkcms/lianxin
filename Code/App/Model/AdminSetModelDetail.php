<?php


namespace PKApp\Model;


use PKApp\Admin\Classes\AdminController;
use PKApp\Model\Classes\ModelDatabase;
use PKApp\Model\Classes\ModelExtend;
use PKCore\Converter;
use PKCore\Formats;
use PKCore\Request;
use PKCore\Tpl;

class AdminSetModelDetail extends AdminController
{

    public function Main()
    {
        $id = Formats::isNumeric(Request::get('id'));
        $typeId = 0;
        $isNew = true;
        $tpl = new Tpl();
        if (!empty($id)) {
            $entity = ModelExtend::GetModel($id);
            $isNew = false;
            if (is_array($entity)) {
                $typeId = $entity['type'];
                $tpl->SetTplParam('type', $typeId);
                $entity['setting'] = Converter::Unserialize($entity['setting']);
                $tpl->SetTplParamList($entity);
            }
        }
        if ($isNew) {
            $typeList = self::getDict('modelTypeLists');
            $typeId = ModelExtend::GetTypeId($typeList);
            $tpl->SetTplParam('type', $typeId);
        }
        if ($typeId == 3) {
            $tpl->SetTplParam('IsOrNot', self::getDict('IsOrNot'));
        }
        return $tpl->Display('model_form');
    }

    public function Post()
    {
        $typeList = self::getDict('modelTypeLists');
        $this->checkPostFieldIsEmpty('name', 'Model_NameEmpty');
        $post = Request::post();
        $modelType = $typeList[Formats::isNumeric($post['type'])][0];
        $this->_checkTableExists(Request::post('tablename'), $modelType);
        $isCreateDataBase = false;
        $db_model = new ModelDatabase();
        if (is_array($post)) {
            if (array_key_exists('setting', $post) && is_array($post['setting'])) {
                if (array_key_exists('database', $post['setting'])) {
                    $isCreateDataBase = Formats::isBool($post['setting']['database']);
                }
                $post['setting'] = serialize($post['setting']);
            }
            if (array_key_exists('id', $post) && empty($post['id'])
                && array_key_exists('tablename', $post) && !empty($post['tablename'])
                && array_key_exists('type', $post) && $isCreateDataBase) {
                $db_model->doSQL($modelType, $modelType . '_' . $post['tablename']);
            }
        }
        $id = Formats::isNumeric(Request::post('id'));
        if (empty($id)) {
            unset($post['id']);
            $db_model->Add($post);
        } else {
            $db_model->UpdateModel($post, $id);
        }
        \PKCore\msg('Data_Input_Success');
    }

    private function _checkTableExists($tableName, $modelType)
    {
        if (!empty($tableName)) {
            Formats::pregMatch('letter', $tableName) ?: \PKCore\alert('Model_NameError');
            $db_model = new ModelDatabase();
            $entity = $db_model->GetByTableName($tableName);
            if (empty($entity)) {
                $isExists = $db_model->CheckTableNameExists($modelType . '_' . $tableName);
                !$isExists ?: \PKCore\alert('Model_TableNameExists');
            } else {
                \PKCore\alert('Model_TableNameExists');
            }
        }
    }

}