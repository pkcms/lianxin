CREATE TABLE IF NOT EXISTS [tablename] (
  [id] INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
  [ip] INTEGER DEFAULT '0',
  [uid] INTEGER unsigned DEFAULT '0',
  [username] TEXT,
  [addtime] INTEGER unsigned DEFAULT '0',
  [lastuid] INTEGER unsigned DEFAULT '0',
  [lastusername] TEXT,
  [lasttime] INTEGER unsigned DEFAULT '0',
  [status] INTEGER unsigned DEFAULT '0'
);

CREATE TABLE IF NOT EXISTS [[tablename]_last] (
  [id] INTEGER unsigned DEFAULT NULL,
  [userid] INTEGER unsigned DEFAULT '0',
  [username] TEXTL,
  [addtime] INTEGER unsigned NOT NULL PRIMARY KEY,
  [ip] INTEGER DEFAULT '0',
  [reply] TEXT,
  [status] INTEGER unsigned DEFAULT '0'
);

