
CREATE TABLE [tablename] (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `siteid` smallint(5) unsigned NOT NULL DEFAULT '1',
  `modelid` smallint(5) unsigned DEFAULT '0',
  `catid` smallint(5) unsigned DEFAULT '0',
  `thumb` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `seotitle` varchar(80) COLLATE utf8_bin DEFAULT NULL,
  `seokeywords` text COLLATE utf8_bin,
  `seodescription` text COLLATE utf8_bin,
  `inputtime` int(11) unsigned DEFAULT '0',
  `updatetime` int(11) unsigned DEFAULT '0',
  `listsort` tinyint(3) unsigned DEFAULT '100',
  `template` varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `url` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `headlines` tinyint(1) unsigned DEFAULT '0',
  `totop` tinyint(1) unsigned DEFAULT '0',
  `posids` tinyint(1) unsigned DEFAULT '0',
  `status` tinyint(1) DEFAULT '0',
  `readpoint` smallint(5) unsigned DEFAULT '0',
  `userid` int(8) unsigned DEFAULT '0',
  `inputip` int(11) DEFAULT '0',
  `inputusername` varchar(30) COLLATE utf8_bin DEFAULT NULL,
  `checkusername` varchar(30) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `status` (`catid`,`listsort`,`status`),
  KEY `catid` (`id`,`catid`,`status`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

CREATE TABLE `[tablename]_data` (
  `id` mediumint(8) unsigned DEFAULT '0',
  KEY `id` (`id`)
) ENGINE=MYISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin ;

CREATE TABLE `[tablename]_count` (
  `id` mediumint(8) unsigned DEFAULT '0',
  `day` tinyint(3) unsigned DEFAULT '0',
  `week` smallint(5) unsigned DEFAULT '0',
  `year` smallint(5) unsigned DEFAULT '0',
  `hits` int(8) unsigned DEFAULT '0',
  `download` smallint(6) unsigned DEFAULT '0',
  `favorites` smallint(6) unsigned DEFAULT '0',
  `recommend` smallint(6) unsigned DEFAULT '0',
  KEY `id` (`id`)
) ENGINE=MYISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin ;
