CREATE TABLE [[tablename]] (
[id] INTEGER  NOT NULL PRIMARY KEY AUTOINCREMENT,
[siteid] INTEGER DEFAULT '1' NULL,
[modelid] INTEGER DEFAULT '0' NULL,
[catid] INTEGER DEFAULT '0' NULL,
[thumb] TEXT(100)  NULL,
[seotitle] TEXT(80)  NULL,
[seokeywords] TEXT  NULL,
[seodescription] TEXT  NULL,
[inputtime] INTEGER DEFAULT '0' NULL,
[updatetime] INTEGER DEFAULT '0' NULL,
[listsort] INTEGER DEFAULT '0' NULL,
[template] TEXT(20)  NULL,
[url] TEXT(100)  NULL,
[headlines] INTEGER DEFAULT '0' NULL,
[totop] INTEGER DEFAULT '0' NULL,
[posids] INTEGER DEFAULT '0' NULL,
[status] INTEGER DEFAULT '0' NULL,
[readpoint] INTEGER DEFAULT '0' NULL,
[userid] INTEGER DEFAULT '0' NULL,
[inputip] INTEGER DEFAULT '0' NULL,
[inputusername] TEXT(30)  NULL,
[checkusername] TEXT(30)  NULL,
[title] TEXT(80)  NULL
);

CREATE INDEX [IDX_[tablename]] ON [[tablename]](
[siteid]  ASC,
[modelid]  ASC,
[catid]  ASC
);

CREATE TABLE [[tablename]_data] (
[id] INTEGER  DEFAULT '0' NULL
);

CREATE INDEX [IDX_[tablename]_data] ON [[tablename]_data](
[id]  DESC
);

CREATE TABLE [[tablename]_count] (
[id] INTEGER  DEFAULT '0' NULL,
[day] INTEGER  DEFAULT '0' NULL,
[week] INTEGER  DEFAULT '0' NULL,
[year] INTEGER  DEFAULT '0' NULL,
[hits] INTEGER  DEFAULT '50' NULL,
[download] INTEGER  DEFAULT '0' NULL,
[favorites] INTEGER  DEFAULT '0' NULL,
[recommend] INTEGER  DEFAULT '0' NULL
);

CREATE INDEX [IDX_[tablename]_count] ON [[tablename]_count](
[id]  DESC
);
