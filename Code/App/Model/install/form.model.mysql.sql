CREATE TABLE [tablename] (
  `id` int(8) unsigned NOT NULL AUTO_INCREMENT,
  `ip` int(11) DEFAULT '0',
  `uid` int(8) unsigned DEFAULT '0',
  `username` varchar(70) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `addtime` int(11) unsigned DEFAULT '0',
  `lastuid` int(8) unsigned DEFAULT '0',
  `lastusername` varchar(30) COLLATE utf8_bin DEFAULT NULL,
  `lasttime` int(11) unsigned DEFAULT '0',
  `status` tinyint(1) unsigned DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

CREATE TABLE IF NOT EXISTS `[tablename]_last` (
  `id` int(10) unsigned DEFAULT NULL,
  `userid` int(10) unsigned DEFAULT '0',
  `username` varchar(20) COLLATE utf8_bin DEFAULT NULL,
  `addtime` int(10) unsigned NOT NULL,
  `ip` int(11) DEFAULT '0',
  `reply` text COLLATE utf8_bin,
  `status` tinyint(1) unsigned DEFAULT '0',
  PRIMARY KEY (`addtime`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

