<?php


namespace PKApp\Model;


use PKApp\Admin\Classes\AdminController;
use PKApp\Model\Classes\FieldDateBase;
use PKApp\Model\Classes\FieldTypeByOptionsDataBase;
use PKApp\Model\Classes\ModelDatabase;
use PKApp\Model\Classes\ModelExtend;
use PKCore\Converter;
use PKCore\Formats;
use PKCore\Request;
use PKCore\Tpl;

class AdminSetFieldOther extends AdminController
{

    public function Main()
    {
        $action = Request::post('action');
        if (method_exists($this, $action)) {
            return $this->$action();
        }
        return '';
    }

    public function selectFieldType()
    {
        $type = Request::get('type');
        if (empty($type)) {
            return null;
        }
        $id = Formats::isNumeric(Request::get('id'));
        if (!empty($id)) {
            $db_field = new FieldDateBase();
            $entity = $db_field->GetFieldById($id);
        }
        $tpl = new Tpl();
        switch ($type) {
            case 'option':
                $db_options = new FieldTypeByOptionsDataBase();
                $optionList = $db_options->GetOptionsLists(0, array('id', 'name'));
                !Formats::isArray($optionList) ?: $optionList = Converter::arrayColumn($optionList, 'name', 'id');
                $tpl->SetTplParam('optionList', $optionList);
                break;
            case 'editor':
                $tpl->SetTplParam('versionList', self::getDict('fieldType_editor_version'));
                $tpl->SetTplParam('version', 'min');
                break;
        }
        $tpl->SetTplDir(Request::module());
        return $tpl->Display('field_' . $type);
    }

    protected function sort()
    {
        $sortList = Request::post('selectData');
        $modelId = Request::post('modelid');
        if (Formats::isArray($sortList)) {
            $db_field = new FieldDateBase();
            foreach ($sortList as $key => $value) {
                $db_field->UpdateField(array('listsort' => Formats::isNumeric($value)),
                    Formats::isNumeric($key));
            }
            ModelExtend::SaveCache($modelId);
        }
        \PKCore\msg('Data_Input_Success');
    }

    protected function del()
    {
        // 由于 SQLite 数据库不支付用 Alter 命令来删除字段，本系统将不支付直接删除字段操作
//        $fieldId = Request::post('set_id');
//        $modelId = Request::post('id');
//        $modelEntity = ModelExtend::GetModel($modelId);
//        $fieldEntity = ModelDatabase::GetFieldById($fieldId);
//        $modelTypeLists = PKController::getDict('modelTypeLists');
//        $tableName = ModelExtend::GetTableName($modelId, $modelTypeLists);
//        if (is_array($fieldEntity) && array_key_exists('field', $fieldEntity)) {
//            $fieldName = $fieldEntity['field'];
//            $isExists = ModelDatabase::CheckFieldExists($tableName, $fieldName);
//            if (!$isExists && in_array($modelEntity['type'], array(0, 1))) {
//                $tableName .= '_data';
//                $isExists = ModelDatabase::CheckFieldExists($tableName, $fieldName);
//                !$isExists ?: ModelDatabase::DelFieldChange($tableName, $fieldName);
//            } else {
//                ModelDatabase::DelFieldChange($tableName, $fieldName);
//            }
//        }
//        ModelDatabase::DelField($fieldId);
//        \PKCore\msg('Data_Input_Success');
    }
}