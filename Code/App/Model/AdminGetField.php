<?php


namespace PKApp\Model;


use PKApp\Admin\Classes\AdminController;
use PKApp\Model\Classes\FieldDateBase;
use PKApp\Model\Classes\FieldTypeByOptionsDataBase;
use PKApp\Model\Classes\ModelDatabase;
use PKApp\Model\Classes\ModelExtend;
use PKCore\Converter;
use PKCore\Formats;
use PKCore\Request;
use PKCore\Tpl;

class AdminGetField extends AdminController
{

    public function Main()
    {
        $id = Formats::isNumeric(Request::get('id'));
        $isNew = true;
        $tpl = new Tpl();
        $formType = 'text';
        $setting = array();
        if (!empty($id)) {
            $db_field = new FieldDateBase();
            $entity = $db_field->GetFieldById($id);
            $isNew = false;
            if (is_array($entity)) {
                if (array_key_exists('setting', $entity)) {
                    $setting = Converter::Unserialize($entity['setting']);
                }
//                $tpl->SetTplParam('setting', $entity['setting']);
                $tpl->SetTplParamList($entity);
            }
            $modelId = $entity['modelid'];
            $formType = $entity['formtype'];
        } else {
            $modelId = Formats::isNumeric(Request::get('modelid'));
        }
        $modelEntity = ModelExtend::GetModel($modelId);
        $modelTypeId = $this->_getModelTypeId($modelEntity);
        if ($isNew) {
            $tpl->SetTplParam('modelid', $modelId)
                ->SetTplParam('formtype', $formType);
        }
        $tpl->SetTplParam('modelTypeId', $modelTypeId)
            ->SetTplParam('fieldTypeList', self::getDict('fieldTypeList'))
            ->SetTplParam('fieldRegularList', self::getDict('fieldRegularList'))
            ->SetTplParam('IsOrNot', self::getDict('IsOrNot'))
            ->SetTplParam('isNew', $isNew)
            ->SetTplParam('formTypeTpl', $this->_getFieldType($formType, $setting));
        return $tpl->Display('field_form');
    }

    private function _getFieldType($formType, $setting)
    {
        $tpl = new Tpl();
        switch ($formType) {
            case 'option':
                $db_option = new FieldTypeByOptionsDataBase();
                $optionList = $db_option->GetOptionsLists(0, array('id', 'name'));
                !Formats::isArray($optionList) ?: $optionList = Converter::arrayColumn($optionList, 'name', 'id');
                $tpl->SetTplParam('optionList', $optionList);
                break;
            case 'editor':
                $tpl->SetTplParam('isAntispam', true);
                $tpl->SetTplParam('versionList', self::getDict('fieldType_editor_version'));
                $tpl->SetTplParam('version', 'min');
                break;
        }
        return $tpl->SetTplParamList($setting)->SetTplDir('Model')
            ->Display('field_' . $formType);
    }

    private function _getModelTypeId($modelEntity)
    {
        $modelTypeId = 0;
        if (is_array($modelEntity) && array_key_exists('type', $modelEntity)) {
            $modelTypeId = $modelEntity['type'];
        }
        return $modelTypeId;
    }

}