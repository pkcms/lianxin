<{admin_breadcrumb('数据模型')}>

<!-- Title -->
<div class="page-header">
    <div class="page-title">
        <h3>
            <{$modelname}> - 模型字段管理
        </h3>
        <p style="color: red;font-weight: bold;">
            由于 SQLite 数据库本身不支付用 Alter 命令来删除字段，故本系统不支付直接删除字段操作。您可通过以下的解决方法来满足您的删除需求：<br>
            解决方法1：将字段设置为不显示（推荐这种方法，这种方法危害性较小）<br>
            解决方法2：通过数据库管理工具进入数据表进行手动删除操作。
        </p>
    </div>
</div>
<!-- /Title -->
<!-- Row -->
<div class="row">
    <div class="col-md-12">
        <div class="widget">
            <div class="widget-content">
                <div class="btn-toolbar btn-toolbar-demo">
                    <a class="btn btn-success" data-ajax="true" data-ajax-begin="beginPaging"
                       data-ajax-failure="failurePaging"
                       data-ajax-mode="replace" data-ajax-success="successPaging" data-ajax-update="#main"
                       href="/index.php/Model/AdminGetField?modelid=<{$modelid}>">
                        <i class="icon-plus-sign"></i>&nbsp;添加字段</a>
                    <button class="btn btn-default" data-action="sort" data-table="fieldList">
                        排序
                    </button>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-12">
        <div class="widget box">
            <div class="widget-header">
                <h4>
                    <i class="icon-reorder"></i>
                    列表
                </h4>
            </div>
            <div class="widget-content" id="fieldList" data-action="Model/AdminSetFieldOther">
                <input type="hidden" name="modelid" value="<{$modelid}>">
                <table class="table table-bordered table-striped table-hover mb-0">
                    <thead class="thead-secondary">
                    <tr>
                        <th width="80">排序</th>
                        <th>字段名</th>
                        <th>别名</th>
                        <th>类型</th>
                        <th>列表字段</th>
                        <th>搜索条件</th>
                        <th>必填</th>
                        <th>显示</th>
                    </tr>
                    </thead>
                    <tbody>
                    <{loop $lists $item}>
                        <tr class="<{$item[id]}>">
                            <td><input type="text" value="<{$item[listsort]}>" name="sort[]" data-id="<{$item[id]}>" class="form-control" /></td>
                            <td>
                                <a class="f14" data-ajax="true" data-ajax-begin="beginPaging"
                                   data-ajax-failure="failurePaging"
                                   data-ajax-mode="replace" data-ajax-success="successPaging" data-ajax-update="#main"
                                   href="/index.php/Model/AdminGetField?id=<{$item[id]}>" data-toggle="tooltip" title="点击编辑">
                                    <i class="icon-edit"></i>&nbsp;<{$item[name]}>
                                </a>
                            </td>
                            <td><{$item[field]}></td>
                            <td><{$item[formtype]}></td>
                            <td><{if $item[isindex]}> &radic; <{else}> &chi; <{/if}></td>
                            <td><{if $item[issearch]}> &radic; <{else}> &chi; <{/if}></td>
                            <td><{if $item[isEmpty]}> &radic; <{else}> &chi; <{/if}></td>
                            <td><{if $item[isShow]}> &radic; <{else}> &chi; <{/if}></td>
                        </tr>
                        <{/loop}>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<!-- /Row -->
