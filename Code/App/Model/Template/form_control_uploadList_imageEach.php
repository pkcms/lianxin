<?php $m_liId = (isset($liId) ? $liId : '') ?>
<div id="<?php echo $m_liId ?>">
    <?php
    if (isset($uploadList) && is_array($uploadList)) {
        foreach ($uploadList['fileurl'] as $index => $item) {
            echo UploadFileListEachByImageItem($index, $m_liId,
                isset($inputName) ? $inputName : '', $item, $uploadList['filename'][$index],
                $uploadList['link'][$index]);
        }
    }
    ?>
</div>
