<{admin_breadcrumb('数据模型')}>

<!-- Title -->
<div class="page-header">
    <div class="page-title">
        <h3>
            模型选项编辑
        </h3>
    </div>
</div>
<!-- /Title -->
<!-- Row -->
<div class="row">
    <div class="col-md-12">
        <div class="widget box">
            <div class="widget-header">
                <h4>基本填写</h4>
            </div>
            <div class="widget-content">
                <form id="OptionForm" class="form-horizontal row-border" action="Model/AdminSetOptionDetail">
                    <div class="form-group">
                        <label class="col-md-2 control-label">
                            选项名称(图片地址):
                        </label>
                        <div class="col-md-10">
                            <{UploadFile('image',$name,'name')}>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label">
                            选择顶级:
                        </label>
                        <div class="col-md-10">
                            <{optionBySelct('parentId', $parentList, $parentId)}>
                        </div>
                    </div>
                    <div class="form-actions">
                        <input type="hidden" name="id"  value="<{$id}>" />
                        <button type="button" data-form="OptionForm" class="btn btn-primary pull-left">
                            提交保存
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- /Row -->
