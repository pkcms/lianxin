<?php $m_id = isset($id) ? $id : '' ?>
<!-- 加载编辑器的容器 -->
<script id="form<?php echo $m_id ?>" name="<?php echo isset($name) ? $name : '' ?>" type="text/plain"><?php echo isset($value) ? $value : '' ?></script>
<?php
if (isset($isLoadJs) && $isLoadJs) {
    ?>
    <link href="/statics/ueditor/themes/default/css/ueditor.min.css" type="text/css" rel="stylesheet">
    <script type="text/javascript" charset="utf-8" src="/statics/ueditor/ueditor.config.js"></script>
    <script type="text/javascript" charset="utf-8" src="/statics/ueditor/ueditor.all.min.js"></script>
    <?php
}
?>
<!-- 实例化编辑器代码 -->
<script type="text/javascript">
    $(document).ready(function () {
        var editor_<?php echo $m_id ?> = UE.getEditor("form<?php echo $m_id ?>");
    });
</script>
