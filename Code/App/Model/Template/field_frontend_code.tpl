
<div class="form-group">
    <label class="col-md-2 control-label">
        文本域宽度:
    </label>
    <div class="col-md-10 input-group">
        <input type="number" name="setting[lenw]" value="<{if $lenw}><{$lenw}><{else}>100<{/if}>" class="form-control" />
        <span class="input-group-addon">
            %
        </span>
    </div>
</div>
<div class="form-group">
    <label class="col-md-2 control-label">
        文本域高度:
    </label>
    <div class="col-md-10 input-group">
        <input type="number" name="setting[lenh]" value="<{if $lenh}><{$lenh}><{else}>46<{/if}>" class="form-control" />
        <span class="input-group-addon">
            px
        </span>
    </div>
</div>
