<?php
if (isset($selectList) && is_array($selectList)) {
    foreach ($selectList AS $key => $item) {
        $checked = isset($value) && $key == $value ? 'checked' : null;
        ?>
        <label>
            <input type='radio' id="<?php echo isset($inputId) ? $inputId : '' ?>" <?php echo $checked ?>
                   name="<?php echo isset($inputName) ? $inputName : '' ?>" value="<?php echo $key ?>"/>
            &nbsp;<?php $search = '[.](jpg|gif|png|jpeg)$';
            if (preg_match($search, strtolower($item))) { ?>
                <img src="<?php echo $item; ?>" alt="">
            <?php } else {
                echo $item;
            } ?>
        </label>
        <?php
    }
}
?>