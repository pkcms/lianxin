<select name="<?php echo isset($inputName) ? $inputName : '' ?>" class="form-control"
        id="<?php echo isset($inputId) ? $inputId : '' ?>">
    <option value=""><?php echo isset($select) ? $select : '' ?></option>
    <?php
    if (isset($selectList) && is_array($selectList)) {
        foreach ($selectList as $key => $item) {
            $selected = (isset($valueList) && in_array($key, $valueList)) ? ' selected="selected"' : '';
            $disabled = (isset($disabledKeyList) && in_array($key, $disabledKeyList)) ? ' disabled="disabled"' : '';
            ?>
            <option value="<?php echo $key ?>"<?php echo $selected . $disabled ?>><?php echo $item ?></option>
            <?php
        }
    }
    ?>
</select>