<{admin_breadcrumb('数据模型')}>

<!-- Title -->
<div class="page-header">
    <div class="page-title">
        <h3>
            模型选项管理<{if $parentName}> - <{$parentName}> 的子级<{/if}>
        </h3>
    </div>
</div>
<!-- /Title -->
<!-- Row -->
<div class="row">
    <div class="col-md-12">
        <div class="widget">
            <div class="widget-content">
                <div class="btn-toolbar btn-toolbar-demo">
                    <a class="btn btn-success" data-ajax="true" data-ajax-begin="beginPaging"
                       data-ajax-failure="failurePaging"
                       data-ajax-mode="replace" data-ajax-success="successPaging" data-ajax-update="#main"
                       href="/index.php/Model/AdminGetOptionDetail?parentId=<{$parentId}>">
                        <i class="icon-plus-sign"></i>&nbsp;添加选项</a>
                    <button class="btn btn-danger" data-action="del" data-table="FieldTypeOptions">
                        <i class="icon-remove-circle"></i>
                        删除
                    </button>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-12">
        <div class="widget box">
            <div class="widget-header">
                <h4>
                    <i class="icon-reorder"></i>
                    列表
                </h4>
            </div>
            <div class="widget-content">
                <table id="FieldTypeOptions" data-action="Model/AdminSetOptionsOther"
                       class="table table-bordered table-striped table-hover mb-0">
                    <thead class="thead-secondary">
                    <tr>
                        <th width="30">选</th>
                        <th>id</th>
                        <th>选项名(图片地址)</th>
                        <th>所属父级</th>
                    </tr>
                    </thead>
                    <tbody>
                        <{loop $lists $item}>
                        <tr class="<{$item[id]}>">
                            <td><input type="radio" value="<{$item[id]}>" name="set_id" /></td>
                            <td><{$item[id]}></td>
                            <td>
                                <a class="f14" data-ajax="true" data-ajax-begin="beginPaging" data-ajax-failure="failurePaging"
                                   data-ajax-mode="replace" data-ajax-success="successPaging" data-ajax-update="#main"
                                   href="/index.php/Model/AdminGetOptionDetail?id=<{$item[id]}>" title="点击编辑">
                                    <i class="icon-edit"></i>&nbsp;<{$item[name]}>
                                </a>
                                <{if $item[parentId] == 0}>
                                <a data-ajax="true" data-ajax-begin="beginPaging" data-ajax-failure="failurePaging"
                                   data-ajax-mode="replace" data-ajax-success="successPaging" data-ajax-update="#main"
                                   href="/index.php/Model/AdminGetOptions?parentId=<{$item[id]}>"
                                   class="btn btn-sm btn-alt btn-default" data-toggle="tooltip" title="查看选项列">
                                    <i class="icon-list"></i>&nbsp;查看选项列
                                </a>
                                <{/if}>
                            </td>
                            <td><{if $parentName}><{$parentName}><{else}>顶级<{/if}></td>
                        </tr>
                        <{/loop}>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<!-- /Row -->
