<{admin_breadcrumb('数据模型')}>

<!-- Title -->
<div class="page-header">
    <div class="page-title">
        <h3>
            模型管理
        </h3>
    </div>
</div>
<!-- /Title -->
<!-- Row -->
<div class="row">
    <div class="col-md-12">
        <div class="widget">
            <div class="widget-content">
                <div class="btn-toolbar btn-toolbar-demo">
                    <a class="btn btn-success" data-ajax="true" data-ajax-begin="beginPaging"
                       data-ajax-failure="failurePaging"
                       data-ajax-mode="replace" data-ajax-success="successPaging" data-ajax-update="#main"
                       href="/index.php/Model/AdminSetModelDetail?type=<{$type}>">
                        <i class="icon-plus-sign"></i>&nbsp;添加新模型</a>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-12">
        <div class="widget box">
            <div class="widget-header">
                <h4>
                    <i class="icon-reorder"></i>
                    列表
                </h4>
            </div>
            <div class="widget-content">
                <table class="table table-bordered table-striped table-hover mb-0">
                    <thead class="thead-secondary">
                    <tr>
                        <th>模型ID</th>
                        <th>模型名称</th>
                        <th width="120">数据表</th>
                    </tr>
                    </thead>
                    <tbody>
                    <{loop $lists $item}>
                        <tr class="<{$item[id]}>">
                            <td><{$item[id]}></td>
                            <td>
                                <a class="f14" data-ajax="true" data-ajax-begin="beginPaging"
                                   data-ajax-failure="failurePaging"
                                   data-ajax-mode="replace" data-ajax-success="successPaging" data-ajax-update="#main"
                                   href="/index.php/Model/AdminSetModelDetail?id=<{$item[id]}>" title="点击编辑">
                                    <i class="icon-edit"></i>&nbsp;<{$item[name]}>
                                </a>
                                <{if $typeId != 2}>
                                <a data-ajax="true" data-ajax-begin="beginPaging"
                                   data-ajax-failure="failurePaging"
                                   data-ajax-mode="replace" data-ajax-success="successPaging" data-ajax-update="#main"
                                   href="/index.php/Model/AdminGetFieldList?modelid=<{$item[id]}>"
                                   class="btn btn-sm btn-alt btn-default" title="字段管理">
                                    <i class="icon-list"></i>&nbsp;字段管理</a>
                                <{/if}>
                                <{if $typeId == 3}>
                                <a data-ajax="true" data-ajax-begin="beginPaging"
                                   data-ajax-failure="failurePaging"
                                   data-ajax-mode="replace" data-ajax-success="successPaging"
                                   data-ajax-update="#main"
                                   href="/index.php/form/AdminGetDataList?modelId=<{$item[id]}>"
                                   class="btn btn-sm btn-alt btn-default" title="数据管理">
                                    <i class="icon-list"></i>&nbsp;数据管理</a>
                                <a data-ajax="true" data-ajax-begin="beginPaging" data-ajax-failure="failurePaging"
                                   data-ajax-mode="replace" data-ajax-success="successPaging" data-ajax-update="#main"
                                   href="/index.php/Admin/SetMenuDetail/byFormData?modelId=<{$item[id]}>" class="btn btn-sm btn-alt btn-default"
                                   title="添加到导航">
                                    <i class="icon-plus"></i>&nbsp;添加到导航</a>
                                <{/if}>
                            </td>
                            <td><{$item[tablename]}></td>
                        </tr>
                        <{/loop}>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<!-- /Row -->
