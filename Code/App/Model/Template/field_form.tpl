<{admin_breadcrumb('数据模型')}>

<!-- Title -->
<div class="page-header">
    <div class="page-title">
        <h3>
            字段编辑
        </h3>
    </div>
</div>
<!-- /Title -->
<!-- Row -->
<div class="row">
    <div class="col-md-12">
        <div class="widget box">
            <div class="widget-header">
                <h4>基本填写</h4>
            </div>
            <div class="widget-content">
                <form id="fieldForm" class="form-horizontal row-border" action="Model/AdminSetField">
                    <{if $isNew}>
                    <div class="form-group">
                        <label class="col-md-2 control-label">
                            字段类型:
                        </label>
                        <div class="col-md-10">
                            <{optionBySelct('formtype', $fieldTypeList, $formtype, 'field_type')}>
                        </div>
                    </div>
                    <{/if}>
                    <{if $isNew && $modelTypeId != 3}>
                    <div class="form-group">
                        <label class="col-md-2 control-label">
                            作为列表字段:
                        </label>
                        <div class="col-md-10">
                            <{RadioButton($IsOrNot, 0, 'isindex')}>
                        </div>
                    </div>
                    <{/if}>
                    <div class="form-group">
                        <label class="col-md-2 control-label">
                            英文名:
                        </label>
                        <div class="col-md-10">
                            <input type="text" id="field" name="field" class="form-control"
                                   value="<{$field}>" <{if $field}>disabled="disabled"<{/if}> />
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label">
                            中文名:
                        </label>
                        <div class="col-md-10">
                            <input type="text" id="name" name="name" class="form-control" value="<{$name}>" />
                        </div>
                    </div>
                    <{if $modelTypeId != 3}>
                    <div class="form-group">
                        <label class="col-md-2 control-label">
                            作为搜索条件:
                        </label>
                        <div class="col-md-10">
                            <{RadioButton($IsOrNot, $issearch, 'issearch')}>
                            <p class="help-block">
                                与列表字段搭配，作为前台的筛选条件
                            </p>
                        </div>
                    </div>
                    <{/if}>
                    <div class="form-group">
                        <label class="col-md-2 control-label">
                            是否必填:
                        </label>
                        <div class="col-md-10">
                            <{RadioButton($IsOrNot, $isEmpty, 'isEmpty')}>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label">
                            相关配置:
                        </label>
                        <div id="options" class="col-md-10">
                            <{$formTypeTpl}>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label">
                            数据检验正则:
                        </label>
                        <div class="col-md-10">
                            <{optionBySelct('fieldRegular', $fieldRegularList, $fieldRegular)}>
                            <p class="help-block">
                                系统将通过此正则检验表单提交的数据合法性，如果不想检验数据请留空
                            </p>
                        </div>
                    </div>
                    <{if $type==0}>
                    <div class="form-group">
                        <label class="col-md-2 control-label">
                            是否跟随订单:
                        </label>
                        <div class="col-md-10">
                            <{RadioButton($IsOrNot, $isorder, 'isorder')}>
                        </div>
                    </div>
                    <{/if}>
                    <div class="form-group">
                        <label class="col-md-2 control-label">
                            是否显示:
                        </label>
                        <div class="col-md-10">
                            <{RadioButton($IsOrNot, $isShow, 'isShow')}>
                        </div>
                    </div>
                    <div class="form-actions">
                        <input type="hidden" name="id"  value="<{$id}>" />
                        <input type="hidden" name="modelid"  value="<{$modelid}>" />
                        <button type="button" data-form="fieldForm" class="btn btn-primary pull-left">
                            提交保存
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- /Row -->

<script type="text/javascript">
$('#field_type').change(function(){
    console.log(location.href);
    var url = "/index.php/Model/AdminSetFieldOther/selectFieldType";
    url += '?id=' + $('input[name="id"]').val() + '&type=' + $(this).val();
    $('#options').load(url);
});
</script>