
<div class="form-group">
    <label class="col-md-2 control-label">
        选项类型:
    </label>
    <div class="col-md-10">
        <label>
            <input type="radio" name="setting[boxtype]" value="radio" <{if $boxtype=='radio'}> checked="checked" <{/if}> />
            单选按钮
        </label>
        <label>
            <input type="radio" name="setting[boxtype]" value="checkbox" <{if $boxtype=='checkbox'}> checked="checked" <{/if}> />
            复选框
        </label>
        <label>
            <input type="radio" name="setting[boxtype]" value="select" <{if $boxtype=='select'}> checked="checked" <{/if}> />
            下拉框
        </label>
    </div>
</div>
<div class="form-group">
    <label class="col-md-2 control-label">
        选项列表:
    </label>
    <div class="col-md-10">
        <{optionBySelct('setting[option]', $optionList, $option)}>
    </div>
</div>
