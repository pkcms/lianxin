<?php
$m_liId = isset($liId) ? $liId : '';
?>
<div id="<?php echo $m_liId ?>">
    <?php
    if (isset($valueList) && is_array($valueList)) {
        foreach ($valueList['fileurl'] as $key => $item) {
            echo UploadFileListEachByFileItem($key, $m_liId,
                isset($inputName) ? $inputName : '', $item, $valueList['filename'][$key]);
        }
    }
    ?>
</div>
<div class="bk10"></div>
<?php echo UploadFileListBtn($m_liId, isset($modelId) ? $modelId : '', isset($inputName) ? $inputName : '') ?>

