<?php
$m_liId = (isset($liId) ? $liId : '');
$m_index = (isset($index) ? $index : '');
$m_inputName = isset($inputName) ? $inputName : '';
$m_filePath = isset($filePath) ? $filePath : '';
?>

<div id="<?php echo $m_liId . $m_index ?>" class="widget box <?php echo $m_liId ?>">
    <div class="widget-header">
        <h4>
            <i class="icon-reorder"></i>
            文件:&nbsp;<?php echo $m_filePath ?>&nbsp;信息
        </h4>
        <div class="toolbar no-padding">
            <div class="btn-group">
                <span class="btn btn-xs" onclick="remove_id('<?php echo $m_liId . $m_index ?>');">
                      <i class="icon-remove-sign"></i>
                      移除
                </span>
            </div>
        </div>
    </div>
    <div class="widget-content">
        <div class="form-group">
            <label class="col-md-2 control-label">
                文件路径:
            </label>
            <div class="col-md-10">
                <input type="text" name="<?php echo $m_inputName ?>[fileurl][]" class="form-control"
                       value="<?php echo $m_filePath ?>"/>
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-2 control-label">
                描述:
            </label>
            <div class="col-md-10">
                <textarea name="<?php echo $m_inputName ?>[filename][]"
                          class="form-control"><?php echo isset($fileInfo) ? $fileInfo : '' ?></textarea>
            </div>
        </div>
    </div>
</div>