<{admin_breadcrumb('数据模型')}>

<!-- Title -->
<div class="page-header">
    <div class="page-title">
        <h3>
            模型编辑
        </h3>
    </div>
</div>
<!-- /Title -->
<!-- Row -->
<div class="row">
    <div class="col-md-12">
        <div class="widget box">
            <div class="widget-header">
                <h4>基本填写</h4>
            </div>
            <div class="widget-content">
                <form id="modelForm" class="form-horizontal row-border" action="Model/AdminSetModelDetail/Post">
                    <div class="form-group">
                        <label class="col-md-2 control-label">
                            模型名称:
                        </label>
                        <div class="col-md-10">
                            <input type="text" id="name" name="name" value="<{$name}>" class="form-control" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label">
                            模型表键名:
                        </label>
                        <div class="col-md-10">
                            <input type="text" id="tablename" name="tablename" class="form-control" <{if $tablename}>disabled="disabled"<{/if}> value="<{$tablename}>" />
                        </div>
                    </div>
                    <{if $type == 3}>
                    <div class="form-group">
                        <label class="col-md-2 control-label">
                            是否保存到数据库:
                        </label>
                        <div class="col-md-10">
                            <{if !$id}>
                            <{RadioButton($IsOrNot, $setting[database], 'setting[database]')}>
                            <{else}>
                            <{if $setting[database]}>是<{else}>否<{/if}>
                            <input type="hidden" name="setting[database]" value="<{$setting[database]}>" />
                            <{/if}>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label">
                            接收提交的邮箱地址:
                        </label>
                        <div class="col-md-10">
                            <input type="text" name="setting[email_address]" class="form-control" value="<{$setting[email_address]}>" />
                        </div>
                    </div>
                    <{/if}>
                    <div class="form-actions">
                        <{if $type != 3}>
                        <input type="hidden" name="setting[database]" value="1" />
                        <{/if}>
                        <input type="hidden" name="id"  value="<{$id}>" />
                        <input type="hidden" name="type"  value="<{$type}>" />
                        <button type="button" data-form="modelForm" class="btn btn-primary pull-left">
                            提交保存
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- /Row -->
