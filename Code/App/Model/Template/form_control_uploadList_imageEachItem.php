<?php
$m_liId = (isset($liId) ? $liId : '');
$m_index = (isset($index) ? $index : '');
$m_inputName = isset($inputName) ? $inputName : '';
$m_imagePath = isset($imagePath) ? $imagePath : '';
?>
<div id="<?php echo $m_liId . $m_index ?>" class="widget box <?php echo $m_liId ?>">
    <div class="widget-header">
        <h4>
            <i class="icon-reorder"></i>
            图片： <?php echo $m_imagePath ?> 信息
        </h4>
        <div class="toolbar no-padding">
            <div class="btn-group">
                <span class="btn btn-xs btn-edit widget-collapse arrow_<?php echo $m_liId ?> goUp">
                    <i class="icon-circle-arrow-up"></i>
                </span>
                <span class="btn btn-xs btn-edit arrow_<?php echo $m_liId ?> goDown">
                    <i class="icon-circle-arrow-down"></i>
                </span>
                <span class="btn btn-xs" onclick="remove_id('<?php echo $m_liId . $m_index ?>');">
                      <i class="icon-remove-sign"></i>
                      移除
                </span>
            </div>
        </div>
    </div>
    <div class="widget-content">
        <div class="row">
            <div class="col-md-2 col-sm-3 col-xs-12" style="height: 196px">
                <img class="thumbnail" src="<?php echo $m_imagePath ?>" style="max-width: 150px;max-height: :150px;">
            </div>
            <div class="col-md-10 col-sm-9 col-xs-12">
                <div class="form-group">
                    <label class="col-md-2 control-label">
                        文件路径:
                    </label>
                    <div class="col-md-10">
                        <input type="text" name="<?php echo $m_inputName ?>[fileurl][]" class="form-control"
                               value="<?php echo $m_imagePath ?>"/>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-2 control-label">
                        图片描述:
                    </label>
                    <div class="col-md-10">
                        <textarea name="<?php echo $m_inputName ?>[filename][]" class="form-control"><?php echo isset($imageInfo) ? $imageInfo : '' ?></textarea>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-2 control-label">
                        链接地址:
                    </label>
                    <div class="col-md-10">
                        <input type="text" name="<?php echo $m_inputName ?>[link][]" class="form-control"
                               value="<?php echo isset($imageLink) ? $imageLink : '' ?>">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
