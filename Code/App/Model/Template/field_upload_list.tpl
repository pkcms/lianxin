
<div class="form-group">
    <label class="col-md-4 control-label">
        允许上传的文件类型:
    </label>
    <div class="col-md-8">
        <input type="text" name="setting[upload_allowExt]" class="form-control"
               value="<{if $upload_allowExt}><{$upload_allowExt}><{else}>zip|pdf<{/if}>" />
    </div>
</div>
<div class="form-group">
    <label class="col-md-4 control-label">
        是否通过密码检验下载:
    </label>
    <div class="col-md-8">
        <label>
            <input type="radio" name="setting[isPass]" value="1" <{if $isPass==1}> checked="checked" <{/if}> />
            是
        </label>
        <label>
            <input type="radio" name="setting[isPass]" value="0" <{if $isPass==0}> checked="checked" <{/if}> />
            否
        </label>
    </div>
</div>