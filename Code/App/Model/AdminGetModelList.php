<?php


namespace PKApp\Model;


use PKApp\Admin\Classes\AdminController;
use PKApp\Model\Classes\ModelDatabase;
use PKApp\Model\Classes\ModelExtend;
use PKCore\Request;
use PKCore\Tpl;

class AdminGetModelList extends AdminController
{

    public function Main()
    {
        $typeList = self::getDict('modelTypeLists');
        $typeId = ModelExtend::GetTypeId($typeList);
        $tpl = new Tpl();
        if (isset($typeId)) {
            $db_model = new ModelDatabase();
            $tpl->SetTplParam('lists', $db_model->GetLists($typeId));
            $tpl->SetTplParam('type', Request::get('type'));
            $tpl->SetTplParam('typeId', $typeId);
        }
        return $tpl->Display('model_lists');
    }
}