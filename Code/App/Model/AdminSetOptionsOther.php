<?php


namespace PKApp\Model;


use PKApp\Admin\Classes\AdminController;
use PKApp\Model\Classes\FieldTypeByOptionsDataBase;
use PKCore\Request;

class AdminSetOptionsOther extends AdminController
{

    public function Main()
    {
        $action = Request::post('action');
        if (method_exists($this, $action)) {
            $this->$action();
        }
        \PKCore\msg('Data_Input_Success');
    }

    protected function del()
    {
        $del_id = Request::post('selectData');
        $db_options = new FieldTypeByOptionsDataBase();
        $res = $db_options->GetOptionsLists($del_id);
        if (empty($res)) {
            $db_options->DelOptions($del_id);
        } else {
            \PKCore\alert('Options_DelError');
        }
    }
}