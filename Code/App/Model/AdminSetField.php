<?php


namespace PKApp\Model;

use PKApp\Admin\Classes\AdminController;
use PKApp\Model\Classes\FieldDateBase;
use PKApp\Model\Classes\ModelDatabase;
use PKApp\Model\Classes\ModelExtend;
use PKCore\Converter;
use PKCore\Formats;
use PKCore\Request;

class AdminSetField extends AdminController
{

    public function Main()
    {
        $post = Request::post();
        $fieldId = Formats::isNumeric(Request::post('id'));
        if (empty($fieldId)) {
            $this->checkPostFieldIsEmpty('field', 'ModelField_NameEmpty');
            $this->checkPostFieldIsEmpty('name', 'ModelField_CnNameEmpty');
            $this->checkPostFieldIsEmpty('modelid', 'Model_IdEmpty');
            $this->checkPostFieldIsEmpty('formtype', 'ModelField_FormTypeEmpty');
        }
        $modelId = Request::post('modelid');
        $modelEntity = ModelExtend::GetModel($modelId);
        $db_field = new FieldDateBase();
        if (empty($fieldId)) {
            $this->_checkField($db_field, $modelEntity, $modelId, $fieldId);
            // 创建实体表字段
            $tableName = ModelExtend::GetTableName($modelId);
            if (in_array($modelEntity['type'], array(0, 1))) {
                Request::post('isindex') ?: $tableName .= '_data';
            }
            $modelTypeId = $this->_getModelTypeId($modelEntity);
            if ($modelTypeId == 3) {
                // 如果是表单模型，则判断是否要求入库
                $setting = Converter::Unserialize($modelEntity['setting']);
                if (array_key_exists('database', $setting) && $setting['database']) {
                    $db_field->AddFieldChange($tableName, $post);
                }
            } elseif ($modelTypeId != 3) {
                // 非表单模型
                $db_field->AddFieldChange($tableName, $post);
            }
        }
        if (is_array($post) && is_array($modelEntity) && array_key_exists('setting', $modelEntity)) {
            $post['setting'] = serialize($post['setting']);
        }
        if (empty($fieldId)) {
            unset($post['id']);
            $db_field->AddField($post);
        } else {
            $db_field->UpdateField($post, $fieldId);
        }
        ModelExtend::SaveCache($modelId);
        \PKCore\msg('Data_Input_Success');
    }


    private function _checkField(FieldDateBase $db_field, $modelEntity, $modelId, $fieldId)
    {
        $field = Request::post('field');
        Formats::pregMatch('letter_', $field) ?: \PKCore\alert('ModelField_NameError');
        $field = strtolower($field);
        // 检查字段是否已经存在
        $where = array('modelid' => $modelId, 'field' => $field);
        empty($fieldId) ?: $where[] = " id <> " . Formats::isNumeric($fieldId);
        $fieldEntity = $db_field->GetField($where);
        empty($fieldEntity) ?: \PKCore\alert('ModelField_NameExists');
        // 检查字段在实体表中是否已经存在
        $tableName = ModelExtend::GetTableName($modelId);
        $isExists = $db_field->CheckFieldExists($tableName, $field);
        if (!$isExists && in_array($modelEntity['type'], array(0, 1))) {
            $tableName .= '_data';
            $isExists = $db_field->CheckFieldExists($tableName, $field);
        }
        !$isExists ?: \PKCore\alert('ModelField_NameExists');
    }

    private function _getModelTypeId($modelEntity)
    {
        $modelTypeId = 0;
        if (is_array($modelEntity) && array_key_exists('type', $modelEntity)) {
            $modelTypeId = $modelEntity['type'];
        }
        return $modelTypeId;
    }

}