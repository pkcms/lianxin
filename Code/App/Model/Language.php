<?php
/**
 * 模块语言包
 * User: Administrator
 * Date: 2019/5/27
 * Time: 9:28
 */

return array(
    'Model_NameEmpty' => array(
        'zh-cn' => '模型名称不能为空',
        'en' => ''
    ),
    'Model_NameError' => array(
        'zh-cn' => '模型名称只能为英文',
        'en' => ''
    ),
    'Model_TableNameExists' => array(
        'zh-cn' => '数据表名已经存在',
        'en' => ''
    ),
    'Model_IdEmpty' => array(
        'zh-cn' => '数据表模型ID不能为空',
        'en' => ''
    ),
    'Model_NoExists' => array(
        'zh-cn' => '数据表模型不存在',
        'en' => ''
    ),
    'SQLFile_Exists' => array(
        'zh-cn' => 'SQL文件不存在',
        'en' => ''
    ),
    'ModelField_NameEmpty' => array(
        'zh-cn' => '字段英文名称不能为空',
        'en' => ''
    ),
    'ModelField_NameError' => array(
        'zh-cn' => '字段英文名称只能为英文，或加下划线',
        'en' => ''
    ),
    'ModelField_NameExists' => array(
        'zh-cn' => '字段英文名称在数据表中已经存在',
        'en' => ''
    ),
    'ModelField_CnNameEmpty' => array(
        'zh-cn' => '字段中文名称不能为空',
        'en' => ''
    ),
    'ModelField_FormTypeEmpty' => array(
        'zh-cn' => '字段的类型不能为空',
        'en' => ''
    ),
    'field_postEmpty' => array(
        'zh-cn' => '您提交的信息中有存在空的情况，请检查信息：',
        'en' => ''
    ),
    'Options_Empty' => array(
        'zh-cn' => '该选项信息找不到，或不存在',
        'en' => ''
    ),
    'Options_ParentError' => array(
        'zh-cn' => '请检查父级的选择',
        'en' => ''
    ),
    'Options_DelError' => array(
        'zh-cn' => '该选项信息下有子信息的存在，不可删除！',
        'en' => ''
    ),
);