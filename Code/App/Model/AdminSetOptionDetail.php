<?php


namespace PKApp\Model;


use PKApp\Admin\Classes\AdminController;
use PKApp\Model\Classes\FieldTypeByOptionsDataBase;
use PKApp\Model\Classes\ModelDatabase;
use PKCore\Formats;
use PKCore\Request;

class AdminSetOptionDetail extends AdminController
{

    public function Main()
    {
        $post = Request::post();
        $post['parentId'] = Formats::isNumeric($post['parentId']);
        $post['id'] = Formats::isNumeric($post['id']);
        $db_options = new FieldTypeByOptionsDataBase();
        if (empty($post['id'])) {
            unset($post['id']);
            $db_options->AddOptions($post);
        } else {
            $post['parentId'] != $post['id'] ?: \PKCore\alert('Options_ParentError');
            $db_options->UpdateOptions($post, $post['id']);
        }
        \PKCore\msg('Data_Input_Success');
    }
}