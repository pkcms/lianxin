<?php


namespace PKApp\Model;


use PKApp\Admin\Classes\AdminController;
use PKApp\Model\Classes\FieldTypeByOptionsDataBase;
use PKApp\Model\Classes\ModelDatabase;
use PKCore\Converter;
use PKCore\Formats;
use PKCore\Request;
use PKCore\Tpl;

class AdminGetOptionDetail extends AdminController
{

    public function Main()
    {
        $id = Request::get('id');
        $tpl = new Tpl();
        $db_options = new FieldTypeByOptionsDataBase();
        if (!empty($id)) {
            $optionsEntity = $db_options->GetOptions($id);
            is_array($optionsEntity) ?: \PKCore\alert('Options_Empty');
            $tpl->SetTplParamList($optionsEntity);
        } else {
            $tpl->SetTplParam('parentId', Formats::isNumeric(Request::get('parentId')));
        }
        $optionsList = $db_options->GetOptionsLists(0);
        !Formats::isArray($optionsList) ?: $optionsList = Converter::arrayColumn($optionsList, 'name', 'id');
        return $tpl->SetTplParam('parentList', $optionsList)
            ->Display('options_form');
    }
}