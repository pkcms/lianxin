<?php


namespace PKApp\Model;


use PKApp\Admin\Classes\AdminController;
use PKApp\Model\Classes\FieldTypeByOptionsDataBase;
use PKApp\Model\Classes\ModelDatabase;
use PKCore\Request;
use PKCore\Tpl;

class AdminGetOptions extends AdminController
{

    public function Main()
    {
        $parentId = Request::get('parentId');
        $tpl = new Tpl();
        $db_options = new FieldTypeByOptionsDataBase();
        if (!empty($parentId)) {
            $parentEntity = $db_options->GetOptions($parentId, array('name'));
            $tpl->SetTplParam('parentName', $parentEntity['name']);
        }
        return $tpl->SetTplParam('lists', $db_options->GetOptionsLists($parentId))
            ->SetTplParam('parentId', $parentId)
            ->Display('options_list');
    }
}