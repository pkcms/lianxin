<?php
/**
 * Created by PhpStorm.
 * User: wz_zh
 * Date: 2018/3/12
 * Time: 9:38
 */

namespace PKApp\Attachment;

use PKCore\Files;
use PKCore\Formats;
use PKCore\Request;
use PKCore\Statics;
use PKCore\Tpl;

class AdminPicture
{
    private $_dir = 'upload';
    private $_fileExtArr = array('jpg', 'jpeg', 'png', 'gif');

    public function Main()
    {
        $tpl = new Tpl();
        Statics::resultWebPageModel($tpl->Display('picture'));
    }

    public function getJsonList()
    {
        $pathLists = $this->_getPathList();
        $dirCount = count($pathLists);
        $result = array('dirStart' => Formats::isNumeric(Request::get('dirStart')),
            'index' => 0, 'pictureList' => array(), 'count' => 0);
        while (($dirCount > $result['dirStart']) && (count($result['pictureList']) < 4)) {
            $index = $dirCount - $result['dirStart'] - 1;
            $result['index'] = $index;
            $result['pictureList'] = $this->_getPictureList($pathLists[$index]);
            if (count($result['pictureList']) < 4) {
                $result['dirStart'] += 1;
                $newPic = $this->_getPictureList($pathLists[$index]);
                $result['pictureList'] = array_merge($result['pictureList'], $newPic);
            }
        }
        $result['count'] = count($result['pictureList']);
        return $result;
    }

    private function _getPictureList($path = '')
    {
        $list = $this->_getPathList($path);;
        if (Formats::isArray($list)) {
            foreach ($list as $index => $item) {
                $item = $this->_dir . (empty($path) ? '' : '/' . $path) . '/' . $item;
                $list[$index] = $item;
            }
        }
        return $list;
    }

    private function _getPathList($path = '')
    {
        $openPath = PATH_ROOT . $this->_dir . (empty($path) ? '' : DS . $path);
        return Files::opendir($openPath,$this->_fileExtArr);
    }

}