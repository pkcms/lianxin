<?php

namespace PKApp\Attachment\Classes;

use Grafika\Grafika;

class Thumb
{

    private $fromImgFile, $newFile;

    public function __construct($fromImgFile, $newFile, $newWidth, $newHeight, $narrow)
    {
        $fromImgPath = PATH_ROOT . str_replace('/', DS, ltrim($fromImgFile, '/'));
        if (file_exists($fromImgPath)) {
            $this->fromImgFile = $fromImgPath;
            list($width, $height) = getimagesize($fromImgPath);
            $this->newFile = $newFile;
            if (($narrow == 1) || (($width == $height) && ($newWidth == $newHeight))) {
                $this->narrow_thumb($newWidth, $newHeight);
            } else {
                if ($width < $height) {
                    $this->maxheight_thumb($newHeight);
                } else {
                    $this->maxwidth_thumb($newWidth);
                }
            }
        }
    }

    /**
     * 等比例缩小性截图
     * @param $new_width
     * @param $new_height
     */
    public function narrow_thumb($new_width, $new_height)
    {
        try {
            $editor = Grafika::createEditor();
            $editor->open($image, $this->fromImgFile);
            $editor->resizeFit($image, $new_width, $new_height);
            $editor->save($image, $this->newFile);
        } catch (\Exception $exception) {
            \PKCore\handlerException($exception);
        }
    }

    /**
     * 按最大高度的区域性截图
     * @param $new_height
     */
    public function maxheight_thumb($new_height)
    {
        try {
            $editor = Grafika::createEditor();
            $editor->open($image, $this->fromImgFile);
            $editor->resizeExactHeight($image, $new_height);
            $editor->save($image, $this->newFile);
        } catch (\Exception $exception) {
            \PKCore\handlerException($exception);
        }
    }

    /**
     * 按最大宽度的区域性截图
     * @param $new_width
     */
    public function maxwidth_thumb($new_width)
    {
        try {
            $editor = Grafika::createEditor();
            $editor->open($image, $this->fromImgFile);
            $editor->resizeExactWidth($image, $new_width);
            $editor->save($image, $this->newFile);
        } catch (\Exception $exception) {
            \PKCore\handlerException($exception);
        }
    }

}
