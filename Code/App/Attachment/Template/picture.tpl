<{template 'header_ajax','admin'}>
<div class="container">
    <ol class="row">
    </ol>
    <button class="btn btn-lg btn-default">
        继续加载
    </button>
</div>
<script type="text/javascript">
    var dirStart = 0;
    var btn = $('button');
    btn.hide().die().on('click', function () {
        $(this).text('正在读取...').attr('disabled', true);
        dirStart += 1;
        getPics();
    });
    function getPics() {
        var html = '';
        $.get('/index.php/attachment/AdminPicture/getJsonList',{
            dirStart:dirStart}, function(data){
            // if (data.match("^\{(.+:.+,*){1,}\}$")) {
            //     data = $.parseJSON(data);
                btn.show();
                dirStart = data.dirStart;
                if (data.count > 0) {
                    for (var i=0; i<data.count; i++) {
                        var item = data.pictureList[i];
                        html += '<li class="col-xs-6 thumbnail" style="height: 240px; padding: 0;margin: 0">\n' +
                            '            <img onclick="imgClick($(this));" src="/'+item+'">\n' +
                            '        </li>';
                    }
                    $('ol').append(html);
                    btn.text('继续加载').attr('disabled', false);
                } else {
                    btn.text('没有可加载的了').attr('disabled', true);
                }
                console.log(data);
            // } else {
            //     console.log(data);
            // }
        });
    }
    function imgClick(obj) {
        var src = obj.attr('src');
        $('body',parent.document).find('#thumb').val(src);
        $('body',parent.document).find('.thumb').attr('src',src);
        window.top.art.dialog({id: 'upload'}).close();
    }
    getPics();
</script>
</body>
</html>