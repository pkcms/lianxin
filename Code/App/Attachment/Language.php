<?php
/**
 * 模块语言包
 * User: Administrator
 * Date: 2019/5/27
 * Time: 9:28
 */

return array(
    'tmp_nameEmpty' => array(
        'zh-cn' => '找不致临时缓存文件',
        'en' => ''
    ),
    'tmp_extensionError' => array(
        'zh-cn' => '请检查上传文件的类型',
        'en' => ''
    ),
    'tmp_maxSizeError' => array(
        'zh-cn' => '上传文件的大小不能超过服务器要求的大小',
        'en' => ''
    ),
    'upload_fileType_empty' => array(
        'zh-cn' => '找不到允许上传文件的类型列表',
        'en' => ''
    ),
);