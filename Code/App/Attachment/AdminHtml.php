<?php


namespace PKApp\Attachment;


use PKApp\Admin\Classes\AdminController;
use PKApp\Model\Classes\ModelExtend;
use PKCore\Request;
use PKCore\Statics;
use PKCore\Route;
Route\extendCore('TemplateTagFunction');

class AdminHtml extends AdminController
{

    public function Main()
    {
    }

    public function ImgList()
    {
        Statics::resultWebPageModel($this->_display('image'));
    }

    public function UploadList()
    {
        Statics::resultWebPageModel($this->_display('file'));
    }

    private function _display($type)
    {
        $modelId = urldecode(Request::post('modelId'));
        $liId = urldecode(Request::post('liId'));
        $inputName = urldecode(Request::post('inputName'));
        $filePath = urldecode(Request::post('filePath'));
        $fileName = urldecode(Request::post('fileName'));
        if (!empty($modelId)) {
            $fieldEntityList = ModelExtend::GetModelFieldEntity($modelId);
            $fieldEntity = array_key_exists($liId, $fieldEntityList['fieldList']) ? $fieldEntityList['fieldList'][$liId] : arraY();
            $liId = $fieldEntity['field'];
            $inputName = $fieldEntity['isindex'] ? $liId : 'data[' . $liId . ']';
        }
        $key = \PKCore\random(6);
        if ($type == 'image') {
            $html = \UploadFileListEachByImageItem($key, $liId, $inputName, $filePath, $fileName);
        } else {
            $html = \UploadFileListEachByFileItem($key, $liId, $inputName, $filePath, $fileName);
        }
        Statics::resultWebPageModel($html);
    }
}