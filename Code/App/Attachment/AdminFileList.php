<?php


namespace PKApp\Attachment;


use PKApp\Admin\Classes\AdminController;
use PKApp\Model\Classes\FieldDateBase;
use PKCore\Converter;
use PKCore\Files;
use PKCore\Request;
use PKCore\Statics;
use PKCore\Tpl;

class AdminFileList extends AdminController
{
    private $_session_key = 'fileList';

    public function Main()
    {
        $fileList = Request::file('fileList');
        if (empty($fileList)) {
            $get = Request::get();
            Request::session($this->_session_key, array(
                'showHtmlId' => self::GetArrayByKey('showHtmlId', $get),
                'inputName' => self::GetArrayByKey('inputName', $get),
                'modelId' => self::GetArrayByKey('modelId', $get)));
            $tpl = new Tpl();
            return $tpl->SetTplDir('Attachment')->PhpDisplay('html5_fileListUpload');
        } else {
            $this->_doUpload($fileList);
        }
    }

    private function _doUpload($up)
    {
        is_uploaded_file($up['tmp_name']) ?: $this->_result('tmp_nameEmpty');
        $nameArr = explode(".", $up['name']);
        $extension = strtolower($nameArr[count($nameArr) - 1]);
        $res_session = Request::session($this->_session_key);
        $modelId = self::GetArrayByKey('modelId', $res_session);
        $fieldName = self::GetArrayByKey('showHtmlId', $res_session);

        $upload_allowExt = self::getDict('upload_allowExt');
        if (!empty($modelId)) {
            $db_field = new FieldDateBase();
            $fieldEntity = $db_field->GetField(array('modelid' => $modelId, 'field' => $fieldName));
            switch (self::GetArrayByKey('formtype', $fieldEntity)) {
                case 'image_list':
                    $upload_allowExt = self::GetArrayByKey('image', $upload_allowExt);
                    break;
                case 'upload_list':
                    $setting = Converter::Unserialize(self::GetArrayByKey('setting', $fieldEntity));
                    $upload_allowExt = explode('|',self::GetArrayByKey('upload_allowExt',$setting));
                    break;
                default:
                    $this->_result('upload_fileType_empty');
                    break;
            }
        } else {
            $upload_allowExt = self::GetArrayByKey('image', $upload_allowExt);
        }
        in_array($extension, $upload_allowExt) ?: $this->_result('tmp_extensionError');
        // 比较大小
        $upload_max_filesize = Converter::unitToBytes(get_cfg_var("upload_max_filesize"), 'mb');
        $up['size'] <= $upload_max_filesize ?: \PKCore\alert('tmp_maxSizeError');
        $dir = PATH_ROOT . 'upload' . DS . date("Ymd") . DS;
        $path = '/upload/' . date("Ymd");
        $fileName = date("His") . \PKCore\random(6) . '.' . $extension;
        Files::uploaded($up['tmp_name'], $dir, $fileName);
        // 组合
        $this->_result(null, array(
            'path' => $path . '/' . $fileName, 'extension' => $extension,
            'modelId' => $modelId,
            'liId' => $fieldName,
            'inputName' => self::GetArrayByKey('inputName', $res_session),));
    }

    private function _result($error, $body = null)
    {
        Statics::resultJsonModel(array('error' => \PKCore\Route\language($error), 'body' => $body));
    }
}