<?php


namespace PKApp\Attachment;


use PKApp\Admin\Classes\AdminController;
use PKApp\Admin\Classes\AdminDataBase;
use PKCore\Converter;
use PKCore\Files;
use PKCore\Request;
use PKCore\Statics;

class AdminHtml5Upload extends AdminController
{

    public function Main()
    {
        $uploadType = Request::post('uploadType');
        $showInputId = Request::post('showInputId');
        $upload_allowExt = self::getDict('upload_allowExt');
        if (is_array($upload_allowExt) && array_key_exists($uploadType, $upload_allowExt)) {
            $upload_allowExt = $upload_allowExt[$uploadType];
        } else {
            \PKCore\alert('upload_fileType_empty');
        }
        $fileToUpload = $_FILES['fileToUpload'];
//        print_r($fileToUpload);
//        print_r($upload_allowExt);
        is_uploaded_file($_FILES['fileToUpload']['tmp_name']) ?: \PKCore\alert('tmp_nameEmpty');

        $up = $_FILES["fileToUpload"];
        $nameArr = explode(".", $up['name']);
        $extension = strtolower($nameArr[count($nameArr) - 1]);
        in_array($extension, $upload_allowExt) ?: \PKCore\alert('tmp_extensionError');
        // 比较大小
        $upload_max_filesize = Converter::unitToBytes(get_cfg_var("upload_max_filesize"), 'mb');
        $up['size'] <= $upload_max_filesize ?: \PKCore\alert('tmp_maxSizeError');
        // 判断是否为模板的素材
        if ($showInputId == 'material') {
            $siteEntity = AdminDataBase::GetSite(array('id' => $this->loginUser()->SiteId), array('setting'));
            $setting = Converter::Unserialize($siteEntity['setting']);
            $dir = PATH_ROOT . 'templates' . DS . $setting['Site_Themes'] . DS . 'style' . DS;
            $path = '/templates/' . $setting['Site_Themes'] . '/style/';
        } else {
            $dir = PATH_ROOT . 'upload' . DS . date("Ymd") . DS;
            $path = '/upload/' . date("Ymd");
        }
        $fileName = date("His") . \PKCore\random(6) . '.' . $extension;
        Files::uploaded($up['tmp_name'], $dir, $fileName);
        $result = array(
            'path' => $path . '/' . $fileName, 'showFieldId' => $showInputId, 'uploadType' => $uploadType
        );
        Statics::resultJsonModel($result);
    }
}