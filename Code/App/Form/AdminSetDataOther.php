<?php


namespace PKApp\Form;


use PKApp\Admin\Classes\AdminController;
use PKApp\Form\Classes\FormDataBase;
use PKApp\Form\Classes\FormDataBase1;
use PKCore\Formats;
use PKCore\Request;

class AdminSetDataOther extends AdminController
{

    public function Main()
    {
        $action = Request::get('action');
        if (method_exists($this,$action)) {
            $this->$action();
        }
    }

    protected function del()
    {
        $modelId = Formats::isNumeric(Request::post('modelId'));
        $id = Formats::isNumeric(Request::post('set_id'));
        !empty($modelId) ?: \PKCore\alert('Model_IdEmpty');
        if (empty($id)) {
            \PKCore\alert('Data_idEmpty');
        }
        $db = new FormDataBase($modelId);
        $db->Where(array('id' => $id))->Update(array('status' => 4))->Exec();
        \PKCore\msg('Data_Input_Success');
    }

}