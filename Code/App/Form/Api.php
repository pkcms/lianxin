<?php


namespace PKApp\Form;


use PKApp\Form\Classes\FormDataBase;
use PKApp\Model\Classes\FieldTypeByOptionsDataBase;
use PKApp\Model\Classes\ModelExtend;
use PKCore\Converter;
use PKCore\Formats;
use PKCore\PKController;
use PKCore\Request;
use PKCore\Statics;

class Api extends PKController
{

    private $formId;

    public function Main()
    {
        // TODO: Implement Main() method.
    }

    public function getGrade()
    {
        $this->formId = Formats::isNumeric(Request::get('formId'));
        $fieldName = Request::get('name');
        $cache_field = ModelExtend::GetModelFieldEntity($this->formId);
        $field_list = array();
        if (is_array($cache_field) && array_key_exists('fieldList',$cache_field)) {
            if (array_key_exists($fieldName,$cache_field['fieldList'])) {
                $field = $cache_field['fieldList'][$fieldName];
                if ($field['formtype'] == 'option') {
                    $db = new FormDataBase($this->formId);
                    $db_res = $db->GetGradeCount($fieldName);
                    $grade_data = Converter::arrayColumn($db_res, 'count',$fieldName);
                    $total = 0;
                    foreach ($grade_data as $item) {
                        $total += $item[$fieldName];
                    }
                    $optionId = $field['setting']['option'];
                    // 获取选项数据
                    $db_options = new FieldTypeByOptionsDataBase();
                    $parentEntity = $db_options->GetOptionsLists($optionId, array('id', 'name'));
                    foreach ($parentEntity as $index =>$item) {
                        $item['size'] = array_key_exists($item['id'],$grade_data) ? $grade_data[$item['id']] : 0;
                        $item['ratio'] = round($item['size']/$total*100,2);
                        $parentEntity[$index] = $item;
                    }
                    $this->json(array('lists'=>$parentEntity));
                } else {
                    $this->json(null,'这个字段不符合评级要求', false);
                }
            }
        }
        $this->json(null,'没有找到该表单的字段', false);
    }

    public function getList()
    {
        $this->formId = Formats::isNumeric(Request::get('formId'));
        $field_list = $this->getField();
        if (is_array($field_list) && count($field_list)) {
            $this->GetPages();
            $db = new FormDataBase($this->formId);
            $db_res = $db->GetMainDataList($field_list,self::$pageIndex,self::$pageSize,
                array('status <> 4'));
            if (is_array($db_res) && array_key_exists('lists',$db_res)) {
                foreach ($db_res['lists'] as $index => $db_re) {
                    $db_re['addtime'] = date('Y-m-d H:i',$db_re['addtime']);
                    $db_res['lists'][$index] = $db_re;
                }
            }
            $this->json($db_res);
        } else {
            $this->json(null,'没有找到该表单的字段', false);
        }
    }

    private function getField() {
        $cache_field = ModelExtend::GetModelFieldEntity($this->formId);
        $field_list = array();
        if (is_array($cache_field) && array_key_exists('fieldList',$cache_field)) {
            foreach ($cache_field['fieldList'] as $key => $item) {
                $field_list[] = $key;
            }
        }
        if (is_array($field_list) && count($field_list)) {
            $field_list[] = 'id';
            $field_list[] = 'ip';
            $field_list[] = 'addtime';
        }
        return $field_list;
    }
}