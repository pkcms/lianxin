<?php


namespace PKApp\Form;


use PKApp\Admin\Classes\AdminController;
use PKApp\Form\Classes\FormDataBase;
use PKApp\Form\Classes\FormDataBase1;
use PKApp\Model\Classes\ModelExtend;
use PKCore\Formats;
use PKCore\Request;
use PKCore\Statics;
use PKCore\Tpl;

class AdminGetDataList extends AdminController
{

    public function Main()
    {
        $modelId = Formats::isNumeric(Request::get('modelId'));
        $options = array();
        if (Request::has('status', 'get')) {
            $options['status'] = Formats::isNumeric(Request::get('status'));
        }
        $modelEntity = ModelExtend::GetModel($modelId);
        $tpl = new Tpl();
        $tpl->SetTplParam('formName', $modelEntity['name']);
        $this->GetPages();
        $db = new FormDataBase($modelId);
        $dataList = $db->GetDataList(self::$pageIndex, self::$pageSize, $options);
        $pageList = Statics::pages($dataList['count'], self::$pageSize, self::$pageIndex,
            '/index.php/form/AdminGetDataList?modelId=' . $modelId . '&pageIndex=[page]');
        $tpl->SetTplParam('pageList', $pageList)->SetTplParam('modelId', $modelId)
            ->SetTplParam('status', self::getDict('form_dataStatusList'))
            ->SetTplParamList(is_array($dataList) ? $dataList : array());
        return $tpl->Display('index');
    }

}