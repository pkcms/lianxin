<?php
/**
 * 模块语言包
 * User: Administrator
 * Date: 2019/5/27
 * Time: 9:28
 */

return array(
    'App_Name' => array(
        'zh-cn' => '表单',
        'en' => ''
    ),
    'Model_IdEmpty' => array(
        'zh-cn' => '表单ID不能为空',
        'en' => ''
    ),
    'Model_NoExists' => array(
        'zh-cn' => '表单模型不存在',
        'en' => ''
    ),
    'fieldList_Empty' => array(
        'zh-cn' => '表单模型里面的字段找不到，或者不存在',
        'en' => ''
    ),
    'Guest' => array(
        'zh-cn' => '访客',
        'en' => ''
    ),
    'AuthCode_Expire' => array(
        'zh-cn' => '表单验证码已经过期，请重新刷新验证码后再提交。',
        'en' => ''
    ),
    'AuthCode_Error' => array(
        'zh-cn' => '您提交的验证码验证失败。',
        'en' => ''
    ),
    'PostError' => array(
        'zh-cn' => '您当前的提交频率太过勤快，请休息一下！',
        'en' => ''
    ),
    'PostOk' => array(
        'zh-cn' => '提交成功',
        'en' => ''
    ),
    'field_postEmpty' => array(
        'zh-cn' => '您提交的信息中有存在空的情况，请检查信息：',
        'en' => ''
    ),
    'Data_idEmpty' => array(
        'zh-cn' => '数据的ID编号不能为空，请检查信息：',
        'en' => ''
    ),
    'Data_statusEmpty' => array(
        'zh-cn' => '数据的操作状态不能为空',
        'en' => ''
    ),
);