<?php


namespace PKApp\Form;


use PKApp\Admin\Classes\AdminController;
use PKApp\Form\Classes\FormDataBase;
use PKApp\Form\Classes\FormDataBase1;
use PKApp\Model\Classes\FieldDateBase;
use PKApp\Model\Classes\ModelDatabase;
use PKApp\Model\Classes\ModelExtend;
use PKCore\Converter;
use PKCore\Formats;
use PKCore\Request;
use PKCore\Tpl;

class AdminGetData extends AdminController
{

    public function Main()
    {
        $modelId = Formats::isNumeric(Request::get('modelId'));
        $dataId = Formats::isNumeric(Request::get('id'));
        $modelEntity = ModelExtend::GetModel($modelId);
        $db_field = new FieldDateBase();
        $fieldListEntity = $db_field->GetFieldLists(array('modelid' => $modelId), array('field', 'name'));
        !Formats::isArray($fieldListEntity) ? \PKCore\alert('fieldList_Empty')
            : $fieldListEntity = Converter::arrayColumn($fieldListEntity, 'name', 'field');
        $db = new FormDataBase($modelId);
        $dataEntity = $db->Where(array('id' => $dataId))->Select()->First();
        $tpl = new Tpl();
        $tpl->SetTplParam('fieldListEntity', $fieldListEntity)
            ->SetTplParam('modelId',$modelId)
            ->SetTplParam('status',self::getDict('form_dataStatusList'))
            ->SetTplParam('data',$dataEntity);
        return $tpl->Display('form');
    }
}