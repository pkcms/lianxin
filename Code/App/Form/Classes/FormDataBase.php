<?php


namespace PKApp\Form\Classes;


use PKApp\Model\Classes\ModelExtend;
use PKCore\DataBase;
use PKCore\Formats;
use PKCore\Route;
use PKCore\Tpl;

class FormDataBase extends DataBase
{

    public function __construct($modelId)
    {
        $tableName = $this->_getModelTableName($modelId);
        parent::__construct($tableName);
        $isExists = $this->CheckTableNameExists();
        $tpl = new Tpl();
        $isExists ?: $tpl->Notice(Route\language('App_Name') . $tableName . Route\language('App_Error'));
    }

    private function _getModelTableName($modelId)
    {
        return ModelExtend::GetTableName($modelId);
    }

    // 评级字段的统计（星级）
    public function GetGradeCount($fieldName)
    {
        $sql = 'SELECT '.$fieldName.', count(id) AS count '.$this->getFrom()
            .' GROUP BY '.$fieldName;
        $db_res = $this->Sql($sql)->ToList();
        return $db_res;
    }

    public function GetMainDataList($fieldList = array(), $index, $lineNum, $options = array())
    {
        !Formats::isArray($options) ?: $this->Where($options);
        $result = array('count' => $this->Count());
        if (Formats::isArray($options)) {
            $this->Where($options);
        } else {
            $this->Where(array('status <> 4'));
        }
        $result['lists'] = $this->OrderBy('id', 'DESC')->Limit($lineNum, $index)->Select($fieldList)->ToList();
        return $result;
    }

    public function GetDataList($index, $lineNum, $options = array())
    {
        !Formats::isArray($options) ?: $this->Where($options);
        $result = array('count' => $this->Count());
        if (Formats::isArray($options)) {
            $this->Where($options);
        } else {
            $this->Where(array('status <> 4'));
        }
        $result['lists'] = $this->OrderBy('id', 'DESC')->Limit($lineNum, $index)->Select(
            array('id', 'ip', 'username', 'lastusername', 'lasttime', 'addtime', 'status')
        )->ToList();
        return $result;
    }

}