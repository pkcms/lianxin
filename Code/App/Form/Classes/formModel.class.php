<?php

defined('PK_PATH') or exit('No direct script access allowed');

class formModel extends Model {

    function table_lists($tablename, $field = '*') {
        $other = array('orderby' => 'id desc', 'display' => 20);
        if ($field == '*') {
            $newfield = "*";
        } else {
            $newfield = "id,uid,username,addtime,lastuid,lastusername,lasttime,status";
            foreach ($field as $v) {
                $newfield .= ", " . $v . '';
            }
        }
        $where = isset($_GET['filter']) ? array('status' => isNumber($_GET['filter'])) : NULL;
        $total = $this->total('form_' . $tablename);
        $list = $this->db->data_select('form_' . $tablename, $where, $other, $newfield);
        return array($list, $total);
    }

    function table_view($tablename, $where) {
        return $this->db->data_select('form_' . $tablename, $where);
    }

    function table_last_view($tablename, $where) {
        return $this->db->data_select('form_' . $tablename . '_last', $where, array('orderby' => 'addtime ASC'));
    }

    function dataDelete($tableName, $id) {
        $table_arr = array('form_' . $tableName, 'form_' . $tableName . '_last');
        $dateBaseConfig = Route::cache("database", "config", 'default');
        if (strstr($dateBaseConfig['DB_Type'], "sqlite")) {
            foreach ($table_arr as $table) {
                $this->db->delete($table, array('id' => $id));
            }
        } else {
            $this->db->delete($table_arr, array('id' => $id));
        }
    }

}

?>