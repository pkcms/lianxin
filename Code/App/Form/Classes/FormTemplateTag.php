<?php


namespace PKApp\Form\Classes;


use PKApp\Model\Classes\ModelFormControl;
use PKCore\Formats;
use PKCore\Route;

class FormTemplateTag
{

    public function Main($param)
    {
        $modelId = Formats::isNumeric($param['id']);
        !empty($modelId) ?: \PKCore\alert(Route\language('Model_IdEmpty', 'Form'));
        $formObj = new ModelFormControl();
        $fieldList = $formObj->Get(array(), $modelId);
        return array('formFieldList' => $fieldList, 'formId' => $modelId);
    }

}