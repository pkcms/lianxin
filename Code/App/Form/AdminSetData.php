<?php


namespace PKApp\Form;


use PKApp\Admin\Classes\AdminController;
use PKApp\Form\Classes\FormDataBase;
use PKApp\Form\Classes\FormDataBase1;
use PKCore\Formats;
use PKCore\Request;

class AdminSetData extends AdminController
{

    public function Main()
    {
        $id = Formats::isNumeric(Request::post('id'));
        $modelId = Formats::isNumeric(Request::post('modelId'));
        $status = Formats::isNumeric(Request::post('status'));
        !empty($id) ?: \PKCore\alert('Data_idEmpty');
        !empty($modelId) ?: \PKCore\alert('Model_IdEmpty');
        !empty($status) ?: \PKCore\alert('Data_statusEmpty');
        $db = new FormDataBase($modelId);
        $db->Where(array('id' => $id))->Update(array('status' => $status))->Exec();
        \PKCore\msg('Data_Input_Success');
    }
}