<{admin_breadcrumb('表单')}>

<!-- Title -->
<div class="page-header">
    <div class="page-title">
        <h3>
            表单 - <{$formName}> 数据列表
        </h3>
    </div>
</div>
<!-- /Title -->
<!-- Row -->
<div class="row">
    <div class="col-md-12">
        <div class="widget">
            <div class="widget-content">
                <div class="btn-toolbar btn-toolbar-demo">
                    <button class="btn btn-danger" data-action="del" data-table="formDataList">
                        <i class="icon-remove-circle"></i>
                        删除
                    </button>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-12">
        <div class="widget box">
            <div class="widget-header">
                <h4>
                    <i class="icon-reorder"></i>
                    列表
                </h4>
                <div class="toolbar no-padding">
                    <div class="btn-group">
                        <!-列表筛选方式 start-->
                        <span class="btn btn-xs dropdown-toggle" data-toggle="dropdown">
                          筛选
                          <i class="icon-angle-down"></i>
                        </span>
                        <ul class="dropdown-menu pull-right">
                            <li>
                                <a data-ajax="true" data-ajax-begin="beginPaging"
                                   data-ajax-failure="failurePaging"
                                   data-ajax-mode="replace" data-ajax-success="successPaging"
                                   data-ajax-update="#main"
                                   href="/index.php/form/AdminGetDataList?modelId=<{$modelId}>">全部</a>
                            </li>
                            <{loop $status $key $item}>
                            <li>
                                <a data-ajax="true" data-ajax-begin="beginPaging"
                                   data-ajax-failure="failurePaging"
                                   data-ajax-mode="replace" data-ajax-success="successPaging"
                                   data-ajax-update="#main"
                                   href="/index.php/form/AdminGetDataList?modelId=<{$modelId}>&status=<{$key}>"><{$item}></a>
                            </li>
                            <{/loop}>
                        </ul>
                        <!-列表筛选方式 end-->
                    </div>
                </div>
            </div>
            <div class="widget-content" id="formDataList" data-action="form/AdminSetDataOther">
                <input type="hidden" name="modelId" value="<{$modelId}>">
                <table class="table table-bordered table-striped table-hover mb-0">
                    <thead class="thead-secondary">
                    <tr>
                        <th width="35">&nbsp;</th>
                        <th width="70">操作</th>
                        <th>提交者信息</th>
                        <th>跟进者信息</th>
                        <th width="70">状态</th>
                    </tr>
                    </thead>
                    <tbody>
                    <{loop $lists $item}>
                        <tr class="<{$item[id]}>">
                            <td><input type="radio" value="<{$item[id]}>" name="set_id"/></td>
                            <td>
                                <a data-ajax="true" data-ajax-begin="beginPaging"
                                   data-ajax-failure="failurePaging"
                                   data-ajax-mode="replace" data-ajax-success="successPaging"
                                   data-ajax-update="#main"
                                   href="/index.php/form/AdminGetData?modelId=<{$modelId}>&id=<{$item[id]}>">查看</a>
                            </td>
                            <td>
                                <{$item[username]}>&nbsp;于&nbsp;
                                <{date('Y-m-d H:i', $item[addtime])}>&nbsp;提交
                            </td>
                            <td>
                                <{if $item[lasttime]}>
                                <{$item[lastusername]}>&nbsp;于&nbsp;
                                <{date('Y-m-d H:i', $item[lasttime])}>&nbsp;处理
                                <{/if}>
                                &nbsp;
                            </td>
                            <td><{$status[$item[status]]}></td>
                        </tr>
                        <{/loop}>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<!-- /Row -->
