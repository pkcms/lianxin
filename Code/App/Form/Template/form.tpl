<{admin_breadcrumb('表单')}>

<!-- Title -->
<div class="page-header">
    <div class="page-title">
        <h3>
            表单 - <{$formName}> 数据详情
        </h3>
    </div>
</div>
<!-- /Title -->
<!-- Row -->
<div class="row">
    <div class="col-md-12">
        <div class="widget box">
            <div class="widget-header">
                <h4>访客的提交</h4>
            </div>
            <div class="widget-content">
                <form id="formDataForm" class="form-horizontal row-border" action="form/AdminSetData">
                    <div class="form-group">
                        <label class="col-md-2 control-label">
                            录入时间:
                        </label>
                        <div class="col-md-10">
                            <{date('Y-m-d H:i', $data[addtime])}>
                            <input type="hidden" name="id"  value="<{$data[id]}>" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label">
                            状态:
                        </label>
                        <div class="col-md-10">
                            <{optionBySelct('status', $status, $data[status])}>
                        </div>
                    </div>
                    <{if $data[uid]}>
                    <div class="form-group">
                        <label class="col-md-2 control-label">
                            提问人:
                        </label>
                        <div class="col-md-10">
                            <{$data[username]}> （<{$info[email]}>，<{$info[mobile]}>）
                        </div>
                    </div>
                    <{/if}>
                    <{loop $fieldListEntity $key $item}>
                    <div class="form-group">
                        <label class="col-md-2 control-label">
                            <{$item}>【<{$key}>】:
                        </label>
                        <div class="col-md-10">
                            <{$data[$key]}>
                        </div>
                    </div>
                    <{/loop}>
                    <div class="form-group">
                        <label class="col-md-2 control-label">
                            是否已经解决:
                        </label>
                        <div class="col-md-10">
                            <input type="checkbox" name="status" value="2" />
                        </div>
                    </div>
                    <div class="form-actions">
                        <input type="hidden" name="modelId" value="<{$modelId}>" />
                        <button type="button" data-form="formDataForm" class="btn btn-primary pull-left">
                            提交保存
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- /Row -->
