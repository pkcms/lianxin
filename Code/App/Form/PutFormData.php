<?php


namespace PKApp\Form;


use PKApp\Content\Classes\BaseController;
use PKApp\Form\Classes\FormDataBase;
use PKApp\Form\Classes\FormDataBase1;
use PKApp\Model\Classes\FieldDateBase;
use PKApp\Model\Classes\ModelDatabase;
use PKApp\Model\Classes\ModelExtend;
use PKApp\Model\Classes\ToInsertData;
use PKCore\Converter;
use PKCore\Curl;
use PKCore\Fileter;
use PKCore\Formats;
use PKCore\Request;
use PKCore\Route;
use PKCore\TMPSave;

class PutFormData extends BaseController
{

    public function Main()
    {
        $post = Request::post();
        $isAuthCodeType = Request::session('authCodeType');
        if ($isAuthCodeType == 'form') {
            $authCode = Request::cookie('authCode');
            !empty($authCode) ?: \PKCore\alert('AuthCode_Expire');
            ($authCode == $post['authCode']) ?: \PKCore\alert('AuthCode_Error');
        }
        BaseController::CheckIP(Request::module(), FORM_PUT_TIME_CELL, 'PostError');
        $this->checkPostFieldIsEmpty('formId', 'Model_IdEmpty');
        $formId = Formats::isNumeric($post['formId']);
        $insertHandler = new ToInsertData();
        $input = $insertHandler->Get($post['data'], $formId);
        $moduleEntity = ModelExtend::GetModel($formId);
        if (Formats::isArray($moduleEntity)) {
            $setting = Converter::Unserialize($moduleEntity['setting']);
            !Formats::isBool($setting['email_address']) ?: $this->_sendEmail($formId, $input,
                $setting['email_address'], $setting['email_title']);
            !Formats::isBool($setting['database']) ?: $this->_insetDB($formId, $input);
        }
        return Route\language('PostOk');
    }

    /**
     * 将提交的数据录入到数据库中
     * @param $formId
     * @param $input
     */
    private function _insetDB($formId, $input)
    {
        $input['uid'] = Formats::isNumeric(Request::cookie('uid'));
        $ip = Request::GuestIP();
        if ($input['uid']) {
//            $post['username'] = $user['username'];
        } else {
            $input['username'] = Route\language('Guest') . substr(md5($ip), 0, 8);
        }
        $input['addtime'] = time();
        $input['ip'] = $ip;
        $db = new FormDataBase($formId);
        $db->Insert($input)->Exec();
    }

    /**
     * 发送邮件
     * @param $formId
     * @param $input
     * @param $toEmail
     * @param $emailTitle
     */
    private function _sendEmail($formId, $input, $toEmail, $emailTitle)
    {
        $db_field = new FieldDateBase();
        $fieldListEntity = $db_field->GetFieldLists(array('modelid' => $formId), array('field', 'name'));
        !Formats::isArray($fieldListEntity) ? \PKCore\alert('fieldList_Empty')
            : $fieldListEntity = Converter::arrayColumn($fieldListEntity, 'name', 'field');
        $html = '';
        foreach ($input as $key => $value) {
            $html .= $fieldListEntity[$key] . ':' . htmlspecialchars_decode($value) . "\r\n";
        }
        $this->_mailBox($toEmail, $emailTitle, $html);
    }

    /**
     * 邮件发送
     * @param $toEmail
     * @param $title
     * @param $body
     */
    private function _mailBox($toEmail, $title, $body)
    {
        $serverInfo = $_SERVER['COMPUTERNAME'] . '|' . $_SERVER['SERVER_NAME'] . ':' . $_SERVER['SERVER_PORT']
            . '|' . $_SERVER['PATH_TRANSLATED'];
        $postArray = array('toMail' => $toEmail, 'mailBody' => $body,
            'fromDomain' => Request::domain(),
            'key' => Fileter::base64Encode($serverInfo, true, 'PKCMSCODE'));
        $curl = new Curl('https://func.zj11.net/mail', 'json');
        $result = $curl->Param($postArray)->Request();
//        print_r($result);
    }

}