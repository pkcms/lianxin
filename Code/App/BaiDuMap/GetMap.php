<?php


namespace PKApp\BaiDuMap;


use PKApp\Content\Classes\BaseController;
use PKCore\Request;
use PKCore\Tpl;

class GetMap extends BaseController
{

    public function Main()
    {
        $referer = Request::server('HTTP_REFERER');
        (!empty($referer) && stristr($referer, Request::host())) ?: \PKCore\alert('referer_error');
        $tpl = new Tpl();
        return $tpl->SetTplParam('baidumap',Request::get('baidumap'))
            ->SetTplParam('title',Request::get('title'))
            ->SetTplParam('info',Request::get('info'))->PhpDisplay('baidu_map');
    }
}