<?php


namespace PKApp\BaiDuMap;


use PKApp\Admin\Classes\AdminController;
use PKCore\Request;
use PKCore\Tpl;

class AdminGetMaps extends AdminController
{

    public function Main()
    {
        $baiduMap = Request::get('baidumap');
        !empty($baiduMap) ?: $baiduMap = '120.69, 27.99';
        $search = Request::get('search');
        $tpl = new Tpl();
        return $tpl->SetTplParam('baidumap',$baiduMap)
            ->SetTplParam('search',$search)->PhpDisplay('init');
    }

    public function Set() {
        $baiduMap = Request::get('baidumap');
        !empty($baiduMap) ?: $baiduMap = '120.69, 27.99';
        $search = Request::get('search');
        $tpl = new Tpl();
        return $tpl->SetTplParam('baidumap',$baiduMap)
            ->SetTplParam('search',$search)->PhpDisplay('init_set');
    }
}