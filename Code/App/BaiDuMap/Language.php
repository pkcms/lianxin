<?php
/**
 * 模块语言包
 * User: Administrator
 * Date: 2019/5/27
 * Time: 9:28
 */

return array(
    'Parame_BaiduMapError' => array(
        'zh-cn' => '请提供百度地图的坐标，不能为空',
        'en' => ''
    ),
    'Parame_titleMapError' => array(
        'zh-cn' => '请提供坐标的描述标题，不能为空',
        'en' => ''
    ),
    'referer_error' => array(
        'zh-cn' => '该服务拒绝非本站的资源链接',
        'en' => ''
    ),
);