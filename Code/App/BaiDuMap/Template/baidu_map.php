<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<!-- 包含 js_css 脚本样式引用模板文件 -->
		<style>
			body {height:100%; overflow:hidden; margin:0;padding:0;}
			#allmap {height:100%; background:#ff0000; position:absolute; width:100%;} 
        </style>
    </head>
    <body>
		<div id="allmap"></div>
<!-- 内容区域 结束 -->
<script type="text/javascript" src="http://api.map.baidu.com/api?v=2.0&ak=<?php echo BAIDU_MAP_KEY ?>"></script>
<script type="text/javascript">

// 百度地图API功能
var map = new BMap.Map("allmap");
var point = new BMap.Point(<?php if (isset($baidumap)) {echo $baidumap;} ?>);
map.centerAndZoom(point, 13);
map.enableScrollWheelZoom();                   
map.addControl(new BMap.NavigationControl());
map.addControl(new BMap.MapTypeControl());
map.addControl(new BMap.ScaleControl());
map.addControl(new BMap.OverviewMapControl());
var marker = new BMap.Marker(point);        // 创建标注    
map.addOverlay(marker);                     // 将标注添加到地图中  
var opts = {
  width : 260,     // 信息窗口宽度
  height: 100,     // 信息窗口高度
  title : "<?php if (isset($title)) {echo $title;} ?>" , // 信息窗口标题
  enableMessage:false,//设置允许信息窗发送短息
  message:""
}
var infoWindow = new BMap.InfoWindow("<?php if (isset($info)) {echo $info;} ?>", opts);  // 创建信息窗口对象
marker.openInfoWindow(infoWindow,point); //开启信息窗口

</script>

	</body>
</html>