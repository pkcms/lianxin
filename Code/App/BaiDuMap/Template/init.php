<style type="text/css">
    #allmap {
        width: 100%;
        height: 320px;
        overflow: hidden;
        margin: 0;
    }
</style>

<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                &times;
            </button>
            <h4 class="modal-title">
                百度地图
            </h4>
        </div>
        <div class="modal-body">
            <iframe style="width: 100%;height: 330px;" frameborder=”no”
                    src="/index.php/BaiDuMap/AdminGetMaps/Set?baidumap=<?php echo $baidumap; ?>&search=<?php echo $search; ?>"></iframe>
        </div>
    </div>
</div>

