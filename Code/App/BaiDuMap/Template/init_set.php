<script type="text/javascript" src="http://api.map.baidu.com/api?v=2.0&ak=<?php echo BAIDU_MAP_KEY ?>"></script>

<div id="allmap" style="width: 100%;height: 300px;"></div>

<script type="text/javascript">
    // 百度地图API功能
    var map = new BMap.Map("allmap");            // 创建Map实例
    var point = new BMap.Point(<?php if (isset($baidumap) && !empty($baidumap)) {
        echo $baidumap;
    } else {
        echo '120.69, 27.99';
    } ?>);    // 创建点坐标
    map.centerAndZoom(point, 12);                     // 初始化地图,设置中心点坐标和地图级别。
    map.enableScrollWheelZoom();                            //启用滚轮放大缩小
    map.addControl(new BMap.NavigationControl());
    map.addControl(new BMap.MapTypeControl());
    map.addControl(new BMap.ScaleControl());
    map.addControl(new BMap.OverviewMapControl());

    var marker = new BMap.Marker(point);
    map.addOverlay(marker);
    marker.enableDragging();
    marker.addEventListener("dragend", function (e) {
        map.panTo(new BMap.Point(e.point.lng, e.point.lat));
        window.parent.document.getElementById('baidumap').value = e.point.lng + ',' + e.point.lat;
    });
    map.addEventListener("click", function (e) {
        window.parent.document.getElementById('baidumap').value = e.point.lng + ',' + e.point.lat;
    });
</script>
