<?php


namespace PKApp\BaiDuMap\Classes;

use PKCore\PKController;

class BaiDuMapTemplateTag
{

    public function Main($param)
    {
        $baiDuMap = PKController::GetArrayByKey('baidumap', $param, 'Parame_BaiduMapError', 'BaiDuMap');
        $title = PKController::GetArrayByKey('title', $param, 'Parame_titleMapError', 'BaiDuMap');
        $info = PKController::GetArrayByKey('info',$param);
        $height = PKController::GetArrayByKey('height',$param);
        !empty($height) ?: $height = 260;
        return <<<EOF
<iframe style="width: 100%;height: {$height}px;" frameborder="0" scrolling="no"
 src="/index.php/BaiDuMap/GetMap?baidumap={$baiDuMap}&title={$title}&info={$info}"></iframe>
EOF;

    }

}