<?php


namespace PKApp\LianChat;

use PKApp\Admin\Classes\AdminController;
use PKApp\Admin\Classes\AdminExtend;
use PKApp\LianChat\Classes\LianChatDataBase;
use PKCore\Files;
use PKCore\Tpl;
use PKCore\Route;

class Admin extends AdminController
{

    public function Main()
    {
        $this->_checkInstall();
        $site_entity = AdminExtend::GetSiteInfo($this->loginUser()->SiteId);
        $urlArr = parse_url($site_entity['Site_Domain']);
        $lastTime = $this->_getLastTime();
        $nowTime = time();
        $tpl = new Tpl();
        $tpl->SetTplParam('domain', $urlArr['host'])
            ->SetTplParam('lastTime',$lastTime)
            ->SetTplParam('isUpdate',($nowTime - $lastTime) > (24 * 3600) ? 'run' : 'stop');
        return $tpl->Display('admin_init');
    }

    private function _getLastTime()
    {
        $path = PATH_CACHE . 'lianChat.log';
        if (file_exists($path)) {
            $lastTime = file_get_contents($path);
        } else {
            $row = $this->_db()->getLastData();
            $lastTime = empty($row) ? 0 : $row['add_time'];
        }
        return $lastTime;
    }

    private function _checkInstall()
    {
        $is = $this->_db()->checkAppInstall();
        if (!$is) {
            $tpl = new Tpl();
            $tpl->Notice(
                Route\language('App_Name') . Route\language('App_Error'),
                '/index.php/LianChat/Install'
            );
        }
    }

    private function _db()
    {
        static $db;
        !empty($db) ?: $db = new LianChatDataBase();
        return $db;
    }

}