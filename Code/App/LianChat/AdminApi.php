<?php


namespace PKApp\LianChat;


use PKApp\Admin\Classes\AdminController;
use PKApp\LianChat\Classes\LianChatDataBase;
use PKCore\Files;
use PKCore\Formats;
use PKCore\Request;

class AdminApi extends AdminController
{

    public function Main()
    {
        $post_data = Request::post('data');
        $arr_addTime = array();
        if (Formats::isArray($post_data)) {
            foreach ($post_data as $key => $item) {
                $arr_addTime[] = $item['add_time'];
                $post_data[$key] = $this->_modelByImport($item);
            }
            $lastTime = max($arr_addTime);
            $db = new LianChatDataBase();
            $db->importData($post_data);
            Files::putContents(PATH_CACHE, 'lianChat.log', $lastTime);
            $this->json('', 'ok');
        } else {
            Files::putContents(PATH_CACHE, 'lianChat.log', time());
            $this->json('', \PKCore\Route\language('latest_data'), false);
        }
    }

    private function _modelByImport($data)
    {
        $arr = array(
            'id', 'username', 'phone', 'content', 'from_domain',
            'page_title', 'page_url', 'ip', 'ipArea', 'add_time', 'status'
        );
        $result = array();
        foreach ($arr as $item) {
            !array_key_exists($item, $data) ?: $result[$item] = $data[$item];
        }
        return $result;
    }

}