<?php


namespace PKApp\LianChat;


use PKApp\Admin\Classes\AdminController;
use PKApp\LianChat\Classes\LianChatDataBase;
use PKCore\Statics;
use PKCore\Tpl;

class AdminHtml extends AdminController
{

    public function Main()
    {
        $this->GetPages();
        $db = new LianChatDataBase();
        $db_count = $db->Count('count','id');
        $list = $db->getList(self::$pageIndex, self::$pageSize);
        $pageNav = Statics::pages($db_count, self::$pageSize, self::$pageIndex,
            '/index.php/LianChat/AdminHtml?pageIndex=[page]');
        $tpl = new Tpl();
        $tpl->SetTplParam('lists',$list)
            ->SetTplParam('pageList', $pageNav);
        return $tpl->Display('admin_list');
    }
}