<{admin_breadcrumb('在线客服留言列表')}>

<!-- Title -->
<div class="page-header">
    <div class="page-title">
        <h3>在线客服留言列表</h3>
        <p style="color: red;font-weight: bold;">
            与云端数据的更新频率为1个小时
        </p>
    </div>
</div>
<!-- /Title -->
<!-- Row -->
<div class="row">
    <div id="step_update" class="col-md-12">
        <div class="widget box">
            <div class="widget-header">
                <h4>
                    <i class="icon-circle-arrow-right"></i>
                    检查最新数据
                </h4>
            </div>
            <div class="widget-content">
                <div class="row">
                    <div class="col-md-3 col-sm-6 col-xs-12">
                        <i id="step_getToken" class="icon-circle-arrow-right"></i>&nbsp;建立通信
                    </div>
                    <div class="col-md-3 col-sm-6 col-xs-12">
                        <i id="step_getList" class="icon-circle-arrow-right upgrade_color_blue"></i>&nbsp;获取最新
                    </div>
                    <div class="col-md-3 col-sm-6 col-xs-12">
                        <i id="step_toDB" class="icon-circle-arrow-right upgrade_color_green"></i>&nbsp;录入本地库
                    </div>
                    <div class="col-md-3 col-sm-6 col-xs-12">
                        <i id="step_done" class="icon-circle-arrow-right upgrade_color_red"></i> 已完成
                    </div>
                </div>
                <div id="tips_update" class="alert alert-danger"></div>
            </div>
        </div>
    </div>
    <div id="step_list" class="col-md-12" style="display: none;">
        <div class="widget box">
            <div class="widget-header">
                <h4>
                    <i class="icon-circle-arrow-right"></i>留言列表
                </h4>
                <a id="refresh" style="display: none" data-ajax="true" data-ajax-begin="beginPaging" data-ajax-failure="failurePaging"
                   data-ajax-mode="replace" data-ajax-success="successPaging" data-ajax-update="#msgList"
                   href="/index.php/LianChat/AdminHtml">刷新数据</a>
            </div>
            <div class="widget-content">
                <div id="msgList"></div>
            </div>
        </div>
    </div>
</div>
<!-- /Row -->
<script type="text/javascript">
    var domain = '<{$domain}>';
    var lastTime = <{$lastTime}>;
    var token = '';
    var isUpdate = '<{$isUpdate}>';
    console.log('isUpdate',isUpdate);
    var apiUrl = 'https://lian.zj11.net/index/CMSApi/';

    function getToken() {
        var id_step = 'step_getToken';
        $.get(
            apiUrl + 'getToken?domain=' + encodeURI(domain),
            function (data) {
                console.log(typeof data);
                if (typeof data == "object") {
                    if (data.code == 0) {
                        token = data.data;
                        step_done(id_step);
                        getList();
                    } else {
                        step_fail(id_step);
                        $('#tips_update').text(data.msg);
                    }
                } else {
                    step_fail(id_step);
                }
            }
        );
    }

    function getList() {
        var id_step = 'step_getList';
        $.get(
            apiUrl + 'getLeaveMsg?domain=' + encodeURI(domain) + '&token=' + token + '&lastTime=' + lastTime,
            function (data) {
                if (typeof data == "object") {
                    if (data.code == 0) {
                        step_done(id_step);
                        toDB(data.data);
                    } else {
                        step_fail(id_step);
                        $('#tips_update').text(data.msg);
                    }
                } else {
                    step_fail(id_step);
                }
            }
        );
    }

    function toDB(data) {
        var id_step = 'step_toDB';
        $.post(
            '/index.php/LianChat/AdminApi',
                {data:data},
            function (data) {
                if (typeof data == "object") {
                    step_done(id_step);
                    $('#tips_update').text(data.msg);
                } else {
                    step_fail(id_step);
                }
                step_done('step_done');
                showList();
            }
        );
    }


    function step_done(id) {
        $('#' + id).removeClass('icon-circle-arrow-right').addClass('icon-ok-sign');
    }

    function step_fail(id) {
        $('#' + id).removeClass('icon-circle-arrow-right').addClass('icon-remove-sign');
    }

    function showList() {
        $('#step_update').hide();
        $('#step_list').show();
        $('#refresh').click();
    }

    getToken();
    // if (isUpdate === 'rum') {
    //     getToken();
    // } else {
    //     showList();
    // }
</script>