<{loop $lists $item}>

<div class="widget box">
    <div class="widget-header">
        <h4>
            <i class="icon-time"></i>
            留言时间:<{date('Y-m-d H:i:s',$item[add_time])}>
        </h4>
    </div>
    <div class="widget-content overflow-hidden">
        <div class="row">
            <div class="row">
                <ol class="col-lg-6 col-md-6 col-sm-12 col-xs-12 row">
                    <dt class="col-md-3 col-xs-3 col-sm-3 align-right">访客称呼：</dt>
                    <dd class="col-md-9 col-xs-9 col-sm-9"><{$item[username]}></dd>
                </ol>
                <ol class="col-lg-6 col-md-6 col-sm-12 col-xs-12 row">
                    <dt class="col-md-3 col-xs-3 col-sm-3 align-right">联系方式：</dt>
                    <dd class="col-md-9 col-xs-9 col-sm-9"><{$item[phone]}></dd>
                </ol>
            </div>
            <div class="row">
                <ol class="col-lg-6 col-md-6 col-sm-12 col-xs-12 row">
                    <dt class="col-md-3 col-xs-3 col-sm-3 align-right">留言内容：</dt>
                    <dd class="col-md-9 col-xs-9 col-sm-9"><{$item[content]}></dd>
                </ol>
                <ol class="col-lg-6 col-md-6 col-sm-12 col-xs-12 row">
                    <dt class="col-md-3 col-xs-3 col-sm-3 align-right">访问页面：</dt>
                    <dd class="col-md-9 col-xs-9 col-sm-9">
                        <a href="<{$item[page_url]}>"><{$item[page_title]}></a>
                    </dd>
                </ol>
            </div>
            <div class="row">
                <ol class="col-lg-6 col-md-6 col-sm-12 col-xs-12 row">
                    <dt class="col-md-3 col-xs-3 col-sm-3 align-right">访问域名：</dt>
                    <dd class="col-md-9 col-xs-9 col-sm-9"><{$item[from_domain]}></dd>
                </ol>
                <ol class="col-lg-6 col-md-6 col-sm-12 col-xs-12 row">
                    <dt class="col-md-3 col-xs-3 col-sm-3 align-right">信息来源：</dt>
                    <dd class="col-md-9 col-xs-9 col-sm-9"><{$item[ip]}>（<{$item[ipArea]}>）</dd>
                </ol>
            </div>
        </div>
    </div>
</div>

<{/loop}>

<div class="row">
    <div class="dataTables_footer clearfix">
        <div class="col-md-6">
            <div class="dataTables_info" id="DataTables_Table_4_info">
                第&nbsp;<{$pageList['index']}>/<{$pageList['pageCount']}>&nbsp;页，
                总&nbsp;<{$pageList['dataCount']}>&nbsp;条。
            </div>
        </div>
        <div class="col-md-6">
            <div class="dataTables_paginate paging_bootstrap">
                <ul class="pagination">
                    <{loop $pageList $key $pageItem}>
                    <{if $key == 'pre'}>
                    <li class="prev<{if $pageList['index'] == 1}> disabled<{/if}>">
                        <a data-ajax="true" data-ajax-begin="beginPaging" data-ajax-failure="failurePaging"
                           data-ajax-mode="replace" data-ajax-success="successPaging" data-ajax-update="#main"
                           href="<{$pageItem}>">← Previous</a>
                    </li>
                    <{elseif $key == 'sizeList'}>
                    <{loop $pageItem $i $sizeUrl}>
                <li<{if $pageList['index'] == $i}> class="active"<{/if}>>
                    <a data-ajax="true" data-ajax-begin="beginPaging" data-ajax-failure="failurePaging"
                       data-ajax-mode="replace" data-ajax-success="successPaging" data-ajax-update="#main"
                       href="<{$sizeUrl}>"><{$i}></a>
                    </li>
                    <{/loop}>
                    <{elseif $key == 'next'}>
                    <li class="next<{if $pageList['index'] == $pageList['pageCount']}>  disabled<{/if}>">
                        <a data-ajax="true" data-ajax-begin="beginPaging" data-ajax-failure="failurePaging"
                           data-ajax-mode="replace" data-ajax-success="successPaging" data-ajax-update="#main"
                           href="<{$pageItem}>">Next → </a>
                    </li>
                    <{/if}>
                    <{/loop}>
                </ul>
            </div>
        </div>
    </div>
</div>