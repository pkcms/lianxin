<?php


namespace PKApp\LianChat\Classes;


use PKCore\DataBase;
use PKCore\Route;

class LianChatDataBase extends DataBase
{

    public function __construct()
    {
        parent::__construct('lian_chat');
    }

    private function _checkInstall()
    {
        try {
            $isExists = $this->checkAppInstall();
            if (!$isExists) {
                throw new \Exception(Route\language('App_Name') . Route\language('App_Error'));
            }
        } catch (\Exception $exception) {
            \PKCore\handlerException($exception);
        }
    }

    public function checkAppInstall()
    {
        return $this->CheckTableNameExists();
    }

    public function importData($data)
    {
        $this->_checkInstall();
        $this->Insert($data, true)->Exec();
    }

    public function getLastData()
    {
        $this->_checkInstall();
        return $this->OrderBy('add_time', 'DESC')->Select()->First();
    }

    public function getList($index, $line)
    {
        $this->_checkInstall();
        return $this->OrderBy('add_time', 'DESC')->Limit($line, $index)
            ->Select()->ToList();
    }

}