<?php


namespace PKApp\Member;


use PKApp\Admin\Classes\AdminController;
use PKApp\Member\Classes\MemberDataBase;
use PKApp\Member\Classes\MemberExtend;
use PKCore\Formats;
use PKCore\Request;

class AdminSetUserDetail extends AdminController
{

    public function Main()
    {
        switch (Request::get('actionType')) {
            case 'info':
            case 'add':
                $this->_byInfo();
                break;
            case 'pass':
                $this->_byPassWord();
                break;
        }
        \PKCore\msg('Data_Input_Success');
    }

    private function _byInfo()
    {
        $id = Formats::isNumeric(Request::get('id'));
        $post = Request::post();
        if (empty($id)) {
            $this->checkPostFieldIsEmpty('username', 'UserName_Empty');
            $post['addtime'] = time();
            MemberDataBase::AddUserInfo($post);
        } else {
            MemberDataBase::UpdateUserInfo($id, $post);
        }
        return $id;
    }

    private function _byPassWord()
    {
        $id = Formats::isNumeric(Request::get('id'));
        !empty($id) ?: \PKCore\alert('User_IdEmpty');
        $post = Request::post();
        if (empty($post['password'])) {
            \PKCore\alert('User_PassEmpty');
        } elseif (strlen($post['password']) < 6) {
            \PKCore\alert('Password_Length_Error');
        } elseif ($post['password'] != $post['pwdconfirm']) {
            \PKCore\alert('Password_confirmation_Error');
        } else {
            $insert = MemberExtend::PasswordMD5(Request::post('password'));
            MemberDataBase::UpdateUserInfo($id, $insert);
        }
    }
}