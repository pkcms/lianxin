<?php
/**
 * 模块语言包
 * User: Administrator
 * Date: 2019/5/27
 * Time: 9:28
 */

return array(
    'Group_Empty' => array(
        'zh-cn' => '角色信息找不到或不存在',
        'en' => '',
    ),
    'Group_IdEmpty' => array(
        'zh-cn' => '角色信息找不到或不存在',
        'en' => '',
    ),
    'User_EditPower_Error' => array(
        'zh-cn' => '您当前没有用户编辑的权限操作',
        'en' => '',
    ),
    'User_Empty' => array(
        'zh-cn' => '用户信息找不到或不存在',
        'en' => '',
    ),
    'User_Login' => array(
        'zh-cn' => '用户登陆成功',
        'en' => '',
    ),
    'User_OnlineError' => array(
        'zh-cn' => '用户不在线，请重新登陆',
        'en' => '',
    ),
    'User_LoginOut' => array(
        'zh-cn' => '用户已成功退出',
        'en' => '',
    ),
    'User_LoginError' => array(
        'zh-cn' => '用户登陆验证失败',
        'en' => '',
    ),
    'User_IdEmpty' => array(
        'zh-cn' => '用户ID不能为空',
        'en' => '',
    ),
    'User_NameEmpty' => array(
        'zh-cn' => '用户名不能为空',
        'en' => '',
    ),
    'User_PassEmpty' => array(
        'zh-cn' => '用户密码不能为空',
        'en' => '',
    ),
    'Password_Length_Error' => array(
        'zh-cn' => '用户密码长度不能小于6位',
        'en' => '',
    ),
    'Password_confirmation_Error' => array(
        'zh-cn' => '两次输入的密码不一致',
        'en' => '',
    ),
    'PublicLogin_Error' => array(
        'zh-cn' => '该用户不是前台用户，请重新输入',
        'en' => '',
    ),
);