<?php
/**
 * 前台登录
 * User: Administrator
 * Date: 2019/5/24
 * Time: 11:48
 */

namespace PKApp\Member;

use PKApp\Content\Classes\BaseController;
use PKApp\Member\Classes\MemberExtend;
use PKCore\Fileter;
use PKCore\Request;
use PKCore\Tpl;
use PKCore\Route;

class PublicLogin extends BaseController
{

    public function Main()
    {
        $this->_checkPost();
        $dbUserInfo = MemberExtend::CheckLogin(Request::post('login'), Request::post('password'));
        $dbUserInfo['modelid'] == 2 ?: \PKCore\alert('PublicLogin_Error');
        $this->_cookie($dbUserInfo);
//        \PKCore\msg(Route\language('User_Login'));
    }

    public function Out()
    {
        Request::cookie(COOKIE_UserKey, '');
        $tpl = new Tpl();
        $tpl->Notice(Route\language('User_LoginOut'), Request::server('HTTP_REFERER'));
    }

    private function _checkPost()
    {
        $this->checkPostFieldIsEmpty('login', 'User_NameEmpty');
        $this->checkPostFieldIsEmpty('password', 'User_PassEmpty');
    }

    private function _cookie($userInfo)
    {
        $expiry = 3600 * 24;
        echo Request::cookie(COOKIE_UserKey, $userInfo['id'], $expiry, true);
    }
}