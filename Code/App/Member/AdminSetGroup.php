<?php


namespace PKApp\Member;


use PKApp\Admin\Classes\AdminController;
use PKApp\Member\Classes\MemberDataBase;
use PKCore\Formats;
use PKCore\Request;

class AdminSetGroup extends AdminController
{

    public function Main()
    {
        $post = Request::post();
        $id = Request::post('id');
        if (empty($id)) {
            unset($post['id']);
            MemberDataBase::AddGroup($post);
        } else {
            MemberDataBase::UpdateGroup($id, $post);
        }
        \PKCore\msg('Data_Input_Success');
    }
}