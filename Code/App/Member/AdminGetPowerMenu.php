<?php


namespace PKApp\Member;


use PKApp\Admin\Classes\AdminController;
use PKApp\Admin\Classes\AdminExtend;
use PKApp\Member\Classes\MemberDataBase;
use PKCore\Formats;
use PKCore\Request;
use PKCore\Tpl;

class AdminGetPowerMenu extends AdminController
{

    public function Main()
    {
        $id = Formats::isNumeric(Request::get('id'));
        !empty($id) ?: \PKCore\alert('Group_IdEmpty');
        $tpl = new Tpl();
        $groupEntity = MemberDataBase::GetGroup($id);
        is_array($groupEntity) ?: \PKCore\alert('Group_Empty');
        $tpl->SetTplParamList($groupEntity);
        $menuRole = explode(',', $groupEntity['menuRole']);
        return $tpl->SetTplParam('menuListTpl', $this->_getMenu($groupEntity['modelid'] == 1, $menuRole))
            ->Display('power_menu_form');
    }

    private function _getMenu($isSystem, $menuRole)
    {
        $tpl = new Tpl();
        return $tpl->SetTplParam('menuList', AdminExtend::GetMenu($isSystem ? array('system', 'admin') : 'member'))
            ->SetTplParam('menuRole', $menuRole)
            ->PhpDisplay('group_role');
    }
}