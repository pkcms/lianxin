<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=7"/>
    <title>后台登陆</title>
    <link href="/statics/bootstrap-3.3.7-dist/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="/statics/bootstrap-3.3.7-dist/css/bootstrap-theme.min.css" rel="stylesheet" type="text/css" />
    <link href="/statics/pkcms/pkcms_login.css" rel="stylesheet" type="text/css"/>
    <script type="text/javaScript" src="/statics/jquery/jquery-1.8.3.min.js"></script>
    <script src="/statics/messenger/js/messenger.min.js"></script>
    <script src="/statics/messenger/js/messenger-theme-future.js"></script>
    <link href="/statics/messenger/css/messenger.css" rel="stylesheet" type="text/css" />
    <link href="/statics/messenger/css/messenger-theme-future.css" rel="stylesheet" type="text/css" />
    <script type="text/javaScript" src="/statics/pkcms/js/pkcms.js"></script>
    <script type="text/javaScript" src="/statics/pkcms/js/pkcms-admin-func.js"></script>
    <script type="text/javaScript" src="/statics/pkcms/js/pkcms.bootstrap.modal.js"></script>
    <script type="text/javaScript" src="/statics/pkcms/js/pkcms.lang.js"></script>

    <script type="text/javaScript" src="/statics/pkcms/js/admin-action.js"></script>
</head>

<body>
<div class="body_login text-middle">
    <div class="newContainer1">
        <form class="form-horizontal" action="member/AdminPublicLogin" role="form" id="loginForm" style="width: 100%;margin-top: 25px">
            <div class="form-group form-inline">
                <div class="input-group-addon">
                    <i class="glyphicon glyphicon-user"></i>
                    <input type="text" class="form-control" id="login" name="login" placeholder="请输入您的账号">
                </div>
            </div>
            <div class="form-group form-inline">
                <div class="input-group-addon">
                    <i class="glyphicon glyphicon-lock"></i>
                    <input type="password" class="form-control" id="password" name="password" placeholder="请输入您的账号">
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-offset-5 col-sm-7">
                    <button type="button" class="btn btn-default" onclick="btnAction($(this));" data-form="loginForm"
                            style="padding:8px 15px;font-size: 21px;background: rgb(238,118,17);color:rgb(251,226,213);">
                        <i class="glyphicon glyphicon-arrow-right"></i>
                    </button>
                </div>
            </div>
        </form>

    </div>
</div>
</body>
</html>