<{admin_breadcrumb('成员管理')}>

<!-- Title -->
<div class="page-header">
    <div class="page-title">
        <h3>
            用户信息编辑
        </h3>
    </div>
</div>
<!-- /Title -->
<!-- Row -->
<div class="row">
    <div class="col-md-12">
        <div class="widget box">
            <div class="widget-header">
                <h4>基本填写</h4>
            </div>
            <div class="widget-content">
                <form id="userInfoForm" class="form-horizontal row-border" action="member/AdminSetUserDetail?actionType=<{$actionType}>&id=<{$id}>">

                    <{if $actionType == 'info' || $actionType == 'add'}>
                    <div class="form-group">
                        <label class="col-md-2 control-label">
                            模型选择:
                        </label>
                        <div class="col-md-10">
                            <select class="form-control" name="modelid" <{if $modelid}>disabled="disabled"<{/if}>>
                            <{loop $modelList $key $item}>
                                <option value="<{$key}>"<{if $key==$modelid}> selected="selected"<{/if}>><{$item}></option>
                            <{/loop}>
                            </select>
                            <{if $modelid}>
                            <input type="hidden" name="modelid" value="<{$modelid}>">
                            <{/if}>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label">
                            角色选择:
                        </label>
                        <div class="col-md-10">
                            <select class="form-control" name="groupid" <{if $id == 1}>disabled="disabled"<{/if}>>
                            <{loop $groupList $key $item}>
                                <option value="<{$key}>"<{if $key==$groupid}> selected="selected"<{/if}>
                            <{if $key == 1}> disabled="disabled"<{/if}>><{$item}></option>
                            <{/loop}>
                            </select>
                        </div>
                    </div>
                    <{if $groupid > 1}>
                    <div class="form-group">
                        <label class="col-md-2 control-label">
                            站点选择:
                        </label>
                        <div class="col-md-10">
                            <select class="form-control" name="siteId">
                                <{loop $siteList $key $item}>
                                <option value="<{$key}>" <{if $key==$siteId}>selected="selected"<{/if}>><{$item}></option>
                                <{/loop}>
                            </select>
                        </div>
                    </div>
                    <{/if}>
                    <{/if}>
                    <div class="form-group">
                        <label class="col-md-2 control-label">
                            会员名:
                        </label>
                        <div class="col-md-10">
                            <input type="text" name="username" id="username" class="form-control" value="<{$username}>" <{if $username}>disabled="disabled"<{/if}> />
                        </div>
                    </div>
                    <{if $actionType == 'info' || $actionType == 'add'}>
                    <div class="form-group">
                        <label class="col-md-2 control-label">
                            邮箱:
                        </label>
                        <div class="col-md-10">
                            <input type="email" name="email" id="email" class="form-control" value="<{$email}>" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label">
                            手机:
                        </label>
                        <div class="col-md-10">
                            <input type="text" name="mobile" class="form-control" value="<{$mobile}>" />
                        </div>
                    </div>
                    <{loop $form_info $name $item}>
                    <div class="form-group">
                        <label class="col-md-2 control-label">
                            <{$item[name]}>:
                        </label>
                        <div class="col-md-10">
                            <{$item[htmlcode]}>
                        </div>
                    </div>
                    <{/loop}>
                    <{/if}>
                    <{if $actionType == 'pass'}>
                    <div class="form-group">
                        <label class="col-md-2 control-label">
                            登录密码:
                        </label>
                        <div class="col-md-10">
                            <input type="password" id="password" name="password" class="form-control" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label">
                            确认密码:
                        </label>
                        <div class="col-md-10">
                            <input type="password" id="pwdconfirm" name="pwdconfirm" class="form-control" />
                        </div>
                    </div>
                    <{/if}>
                    <div class="form-actions">
                        <input type="hidden" name="id" value="<{$id}>"/>
                        <button type="button" data-form="userInfoForm" class="btn btn-primary pull-left">
                            提交保存
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- /Row -->
