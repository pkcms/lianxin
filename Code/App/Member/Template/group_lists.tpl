<{admin_breadcrumb('成员管理')}>

<!-- Title -->
<div class="page-header">
    <div class="page-title">
        <h3>
            成员角色列表
        </h3>
    </div>
</div>
<!-- /Title -->
<!-- Row -->
<div class="row">
    <div class="col-md-12">
        <div class="widget">
            <div class="widget-content">
                <div class="btn-toolbar btn-toolbar-demo">
                    <a class="btn btn-success"  data-ajax="true" data-ajax-begin="beginPaging"
                       data-ajax-failure="failurePaging"
                       data-ajax-mode="replace" data-ajax-success="successPaging" data-ajax-update="#main"
                       href="/index.php/member/AdminGetGroup">
                        <i class="icon-plus-sign"></i>&nbsp;
                        添加用户组
                    </a>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-12">
        <div class="widget box">
            <div class="widget-header">
                <h4>
                    <i class="icon-reorder"></i>
                    列表
                </h4>
            </div>
            <div class="widget-content">
                <table class="table table-bordered table-striped table-hover mb-0">
                    <thead class="thead-secondary">
                    <tr>
                        <th>操作</th>
                        <th>用户组名</th>
                        <th>是否禁用</th>
                        <th>是否系统内置</th>
                    </tr>
                    </thead>
                    <tbody>
                    <{loop $groupList $item}>
                        <tr>
                            <td>
                                <a class="f14"  data-ajax="true" data-ajax-begin="beginPaging"
                                   data-ajax-failure="failurePaging"
                                   data-ajax-mode="replace" data-ajax-success="successPaging" data-ajax-update="#main"
                                   href="/index.php/member/AdminGetGroup?id=<{$item[id]}>" title="编辑角色">
                                    <i class="icon-edit"></i>&nbsp;编辑角色
                                </a>
                                <a class="f14"  data-ajax="true" data-ajax-begin="beginPaging"
                                   data-ajax-failure="failurePaging"
                                   data-ajax-mode="replace" data-ajax-success="successPaging" data-ajax-update="#main"
                                   href="/index.php/member/AdminGetPowerMenu?id=<{$item[id]}>" title="权限菜单">
                                    <i class="icon-lock"></i>&nbsp;权限菜单
                                </a>
                            </td>
                            <td><{$item[name]}></td>
                            <td><{if $item[disabled]}>是<{else}>否<{/if}></td>
                            <td><{if $item[issystem]}>是<{else}>否<{/if}></td>
                        </tr>
                        <{/loop}>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<!-- /Row -->
