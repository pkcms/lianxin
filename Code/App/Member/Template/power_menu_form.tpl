<{admin_breadcrumb('成员管理')}>

<!-- Title -->
<div class="page-header">
    <div class="page-title">
        <h3>
            用户角色 - 权限菜单编辑
        </h3>
    </div>
</div>
<!-- /Title -->
<!-- Row -->
<div class="row">
    <div class="col-md-12">
        <div class="widget box">
            <div class="widget-header">
                <h4>基本填写</h4>
            </div>
            <div class="widget-content">
                <form id="userRoleMenuForm" class="form-horizontal row-border" action="member/AdminSetPowerMenu">
                    <div class="form-group">
                        <label class="col-md-2 control-label">
                            角色名:
                        </label>
                        <div class="col-md-10">
                            <input type="text" id="name" name="name" class="form-control" value="<{$name}>" disabled="disabled" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label">
                            角色权限:
                        </label>
                        <div class="col-md-10">
                            <{$menuListTpl}>
                        </div>
                    </div>
                    <div class="form-actions">
                        <input type="hidden" name="id" value="<{$id}>" />
                        <button type="button" data-form="userRoleMenuForm" class="btn btn-primary pull-left">
                            提交保存
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- /Row -->
