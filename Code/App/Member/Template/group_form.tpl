<{admin_breadcrumb('成员管理')}>

<!-- Title -->
<div class="page-header">
    <div class="page-title">
        <h3>
            用户角色编辑
        </h3>
    </div>
</div>
<!-- /Title -->
<!-- Row -->
<div class="row">
    <div class="col-md-12">
        <div class="widget box">
            <div class="widget-header">
                <h4>基本填写</h4>
            </div>
            <div class="widget-content">
                <form id="roleForm" class="form-horizontal row-border" action="member/AdminSetGroup">
                    <div class="form-group">
                        <label class="col-md-2 control-label">
                            用户模型:
                        </label>
                        <div class="col-md-10">
                            <{if $issystem}>
                            <{$modelList[$modelid]}>
                            <{else}>
                            <{optionBySelct('modelid', $modelList, $modelid, '', $disabledKeyList)}>
                            <{/if}>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label">
                            角色名:
                        </label>
                        <div class="col-md-10">
                            <input type="text" id="name" name="name" class="form-control" value="<{$name}>"
                            <{if $issystem}> disabled="disabled"<{/if}> />
                        </div>
                    </div>
                    <{if !$issystem}>
                    <div class="form-actions">
                        <input type="hidden" name="id" value="<{$id}>"/>
                        <button type="button" data-form="roleForm" class="btn btn-primary pull-left">
                            提交保存
                        </button>
                    </div>
                    <{/if}>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- /Row -->
