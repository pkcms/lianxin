<?php
if (isset($menuList) && is_array($menuList)) {
    foreach ($menuList as $item) {
        ?>
        <div class="panel panel-default">
            <div class="panel-heading">
                <label>
                    <input type="checkbox" id="nav_<?php echo $item['id'] ?>" name="menuRole[]"
                        <?php if (isset($menuRole) && in_array($item['id'], $menuRole)) { ?> checked="checked"<?php } ?>
                           value="<?php echo $item['id'] ?>"/>
                    <?php echo $item['name'] ?>
                </label>
            </div>
            <?php
            if (array_key_exists('children', $item) && !empty($item['children'])) {
                ?>
                <div class="panel-body row">
                    <?php
                    foreach ($item['children'] as $i => $value) {
                        ?>
                        <label class="col-sm-4">
                            <input type="checkbox" name="menuRole[]"
                                <?php if (isset($menuRole) && in_array($value['id'], $menuRole)) { ?> checked="checked"<?php } ?>
                                   value="<?php echo $value['id'] ?>"/>
                            <?php echo $value['name'] ?>
                        </label>
                        <?php
                    }
                    ?>
                </div>
                <?php
            }
            ?>
        </div>
        <?php
    }
}

?>
