<{admin_breadcrumb('成员管理')}>

<!-- Title -->
<div class="page-header">
    <div class="page-title">
        <h3>
            用户名列表
        </h3>
    </div>
</div>
<!-- /Title -->
<!-- Row -->
<div class="row">
    <div class="col-md-12">
        <div class="widget">
            <div class="widget-content">
                <div class="btn-toolbar btn-toolbar-demo">
                    <a class="btn btn-success" data-ajax="true" data-ajax-begin="beginPaging"
                       data-ajax-failure="failurePaging"
                       data-ajax-mode="replace" data-ajax-success="successPaging" data-ajax-update="#main"
                       href="/index.php/member/adminGetUserDetail?type=add&modelId=<{$modelId}>">
                        <i class="icon-plus-sign"></i>&nbsp;添加用户</a>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-12">
        <div class="widget box">
            <div class="widget-header">
                <h4>
                    <i class="icon-reorder"></i>
                    列表
                </h4>
            </div>
            <div class="widget-content">
                <table class="table table-bordered table-striped table-hover mb-0">
                    <thead class="thead-secondary">
                    <tr>
                        <th width="240">操作</th>
                        <th>会员名</th>
                        <th width="120">成员组别</th>
                        <th width="120">注册时间</th>
                        <th width="120">最新活动</th>
                        <th width="120">活动IP</th>
                    </tr>
                    </thead>
                    <tbody>
                    <{loop $userList $item}>
                        <tr>
                            <td>
                                <a class="btn btn-success" data-ajax="true" data-ajax-begin="beginPaging"
                                   data-ajax-failure="failurePaging"
                                   data-ajax-mode="replace" data-ajax-success="successPaging" data-ajax-update="#main"
                                   href="/index.php/member/adminGetUserDetail?id=<{$item[id]}>&type=info">
                                    <i class="icon-edit"></i>&nbsp;修改信息
                                </a>
                                <a class="btn btn-success" data-ajax="true" data-ajax-begin="beginPaging"
                                   data-ajax-failure="failurePaging"
                                   data-ajax-mode="replace" data-ajax-success="successPaging" data-ajax-update="#main"
                                   href="/index.php/member/adminGetUserDetail?id=<{$item[id]}>&type=pass">
                                    <i class="icon-edit"></i>&nbsp;修改密码
                                </a>
                            </td>
                            <td><{$item[username]}></td>
                            <td><{$groupList[$item[groupid]]}>&nbsp;</td>
                            <td><{date('Y-m-d H:i',$item[addtime])}></td>
                            <td><{date('Y-m-d H:i',$item[lasttime])}></td>
                            <td><{$item[lastip]}></td>
                        </tr>
                        <{/loop}>
                    </tbody>
                </table>
                <div class="row">
                    <div class="dataTables_footer clearfix">
                        <div class="col-md-6">
                            <div class="dataTables_info" id="DataTables_Table_4_info">
                                第&nbsp;<{$pageList['index']}>/<{$pageList['pageCount']}>&nbsp;页，
                                总&nbsp;<{$pageList['dataCount']}>&nbsp;条。
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="dataTables_paginate paging_bootstrap">
                                <ul class="pagination">
                                    <{loop $pageList $key $pageItem}>
                                    <{if $key == 'pre'}>
                                    <li class="prev<{if $pageList['index'] == 1}> disabled<{/if}>">
                                        <a data-ajax="true" data-ajax-begin="beginPaging" data-ajax-failure="failurePaging"
                                           data-ajax-mode="replace" data-ajax-success="successPaging" data-ajax-update="#main"
                                           href="<{$pageItem}>">← Previous</a>
                                    </li>
                                    <{elseif $key == 'sizeList'}>
                                    <{loop $pageItem $i $sizeUrl}>
                                <li<{if $pageList['index'] == $i}> class="active"<{/if}>>
                                    <a data-ajax="true" data-ajax-begin="beginPaging" data-ajax-failure="failurePaging"
                                       data-ajax-mode="replace" data-ajax-success="successPaging" data-ajax-update="#main"
                                       href="<{$sizeUrl}>"><{$i}></a>
                                    </li>
                                    <{/loop}>
                                    <{elseif $key == 'next'}>
                                    <li class="next<{if $pageList['index'] == $pageList['pageCount']}>  disabled<{/if}>">
                                        <a data-ajax="true" data-ajax-begin="beginPaging" data-ajax-failure="failurePaging"
                                           data-ajax-mode="replace" data-ajax-success="successPaging" data-ajax-update="#main"
                                           href="<{$pageItem}>">Next → </a>
                                    </li>
                                    <{/if}>
                                    <{/loop}>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /Row -->
