<?php


namespace PKApp\Member;


use PKApp\Member\Classes\MemberController;

class GetUserLoginInfo extends MemberController
{

    public function Main()
    {
        return $this->loginUser()->UserName;
    }
}