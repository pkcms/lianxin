<?php


namespace PKApp\Member;


use PKApp\Admin\Classes\AdminController;
use PKApp\Admin\Classes\AdminExtend;
use PKApp\Member\Classes\MemberDataBase;
use PKApp\Model\Classes\ModelDatabase;
use PKApp\Model\Classes\ModelExtend;
use PKCore\Converter;
use PKCore\Formats;
use PKCore\Request;
use PKCore\Tpl;

class AdminGetGroup extends AdminController
{

    public function Main()
    {
        $id = Formats::isNumeric(Request::get('id'));
        $tpl = new Tpl();
        $menuRole = array();
        if (!empty($id)) {
            $groupEntity = MemberDataBase::GetGroup($id);
            is_array($groupEntity) ?: \PKCore\alert('Group_Empty');
            $tpl->SetTplParamList($groupEntity);
            $menuRole = explode(',',$groupEntity['menuRole']);
        }
        $typeId = ModelExtend::GetTypeId(self::getDict('modelTypeLists'), 'member');
        $db_model = new ModelDatabase();
        $modelList = $db_model->GetLists($typeId, null, array('id', 'name'));
        !Formats::isArray($modelList) ?: $modelList = Converter::arrayColumn($modelList, 'name', 'id');
        return $tpl ->SetTplParam('disabledKeyList', array(1))
            ->SetTplParam('modelList', $modelList)
            ->Display('group_form');
    }
}