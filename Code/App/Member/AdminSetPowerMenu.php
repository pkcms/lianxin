<?php


namespace PKApp\Member;


use PKApp\Admin\Classes\AdminController;
use PKApp\Member\Classes\MemberDataBase;
use PKCore\Formats;
use PKCore\Request;

class AdminSetPowerMenu extends AdminController
{

    public function Main()
    {
        $post = Request::post();
        if (Formats::isArray($post['menuRole'])) {
            $post['menuRole'] = implode(',', $post['menuRole']);
        }
        $id = Request::post('id');
        MemberDataBase::UpdateGroup($id, $post);
        \PKCore\msg('Data_Input_Success');
    }
}