<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2019/5/27
 * Time: 17:30
 */

namespace PKApp\Member\Classes;


use PKCore\Formats;
use PKCore\Request;

class MemberExtend
{

    /**
     * 对用户的密码进行加密
     * @param $password
     * @param $encrypt //传入加密串，在修改密码时做认证
     * @return array/password
     */
    public static function PasswordMD5($password, $encrypt = NULL)
    {
        !is_null($encrypt) ?: $newEncrypt = \PKCore\randstr();
        return !isset($newEncrypt) ? md5(md5(trim($password)) . $encrypt) : array(
            'password' => md5(md5(trim($password)) . $newEncrypt), 'encrypt' => $newEncrypt
        );
    }


    public static function CheckLogin($login, $password)
    {
        $param = array('username' => $login);
        $dbUserInfo = MemberDataBase::CheckAndGetUserInfo($param);
        if (empty($dbUserInfo) || !Formats::isArray($dbUserInfo)) {
            \PKCore\alert('User_Empty');
        }
        if (is_array($dbUserInfo) && array_key_exists('password', $dbUserInfo)
            && array_key_exists('encrypt', $dbUserInfo)) {
            $password = self::PasswordMD5($password, $dbUserInfo['encrypt']);
            if ($password == $dbUserInfo['password']) {
                $data = array('lasttime' => time(), 'lastip' => Request::GuestIP());
                MemberDataBase::UpdateUserInfo($dbUserInfo['id'], $data);
            } else {
                \PKCore\alert('User_LoginError');
            }
        }
        return $dbUserInfo;
    }

}