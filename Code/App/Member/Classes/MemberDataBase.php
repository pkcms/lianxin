<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2019/5/27
 * Time: 10:37
 */

namespace PKApp\Member\Classes;


use PKCore\DataBase;

class MemberDataBase
{

    private static $_table_user = 'member_user';
    private static $_group = 'member_group';

    public static function CheckAndGetUserInfo($option = array(), $field = '*')
    {
        $db = new DataBase(self::$_table_user);
        return $db->Where($option)->Select($field)->First();
    }

    public static function GetUserLists($options = null, $fields = '*', $lineNum = 0, $index = 0)
    {
        $db = new DataBase(self::$_table_user);
        is_null($options) ?: $db->Where($options);
        if ($lineNum > 0) {
            $db->Limit($lineNum, $index);
        }
        return $db->Select($fields)->ToList();
    }

    public static function GetUserCount($options = null)
    {
        $db = new DataBase(self::$_table_user);
        (is_null($options) || !is_array($options)) ?: $db->Where($options);
        return $db->Count();
    }

    public static function AddUserInfo($data = array())
    {
        if (isset($data['id'])) {
            unset($data['id']);
        }
        $db = new DataBase(self::$_table_user);
        return $db->Insert($data)->Exec();
    }

    public static function UpdateUserInfo($id, $data = array())
    {
        $db = new DataBase(self::$_table_user);
        return $db->Where(array('id' => $id))->Update($data)->Exec();
    }

    public static function GetGroupLists($fields, $options = null)
    {
        $db = new DataBase(self::$_group);
        if (!is_null($options) && is_array($options)) {
            $db->Where($options);
        }
        return $db->Select($fields)->ToList();
    }

    public static function GetGroup($id)
    {
        $db = new DataBase(self::$_group);
        return $db->Where(array('id' => $id))->Select()->First();
    }

    public static function AddGroup($data = array())
    {
        $db = new DataBase(self::$_group);
        return $db->Insert($data)->Exec();
    }

    public static function UpdateGroup($id, $data = array())
    {
        $db = new DataBase(self::$_group);
        return $db->Where(array('id' => $id))->Update($data)->Exec();
    }

}