<?php


namespace PKApp\Member\Classes;


use PKApp\Content\Classes\BaseController;
use PKApp\Member\Model\LoginUserInfo;
use PKCore\Request;
use PKCore\Route;

abstract class MemberController extends BaseController
{

    protected function loginUser()
    {
        static $m;
        if (empty($m)) {
            $userId = Request::cookie(COOKIE_UserKey, null, 0, true);
            !empty($userId) ?: \PKCore\alert(Route\language('User_OnlineError','member'), '', 401);
            $userEntity = MemberDataBase::CheckAndGetUserInfo(array('id' => $userId),
                array('username', 'groupid'));
            $m = new LoginUserInfo();
            $m->Id = $userId;
            $m->UserName = $userEntity['username'];
            $m->GroupId = $userEntity['groupid'];
        }
        return $m;
    }

}