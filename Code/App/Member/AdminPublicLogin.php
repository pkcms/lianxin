<?php
/**
 * 后台登录
 * User: Administrator
 * Date: 2019/5/24
 * Time: 11:48
 */

namespace PKApp\Member;

use PKApp\Admin\Classes\AdminController;
use PKApp\Member\Classes\MemberDataBase;
use PKApp\Member\Classes\MemberExtend;
use PKCore\Formats;
use PKCore\Request;
use PKCore\Tpl;
use PKCore\Route;

class AdminPublicLogin extends AdminController
{

    public function Main()
    {
        if ($this->checkPostFieldIsExists('login') && $this->checkPostFieldIsExists('password')) {
            $this->_checkPost();
            $dbUserInfo = MemberExtend::CheckLogin(Request::post('login'),Request::post('password'));
            $this->_session($dbUserInfo);
            \PKCore\msg(Route\language('User_Login'), self::ToMeUrl('Admin/Index'), 'openWin');
        }
        $tpl = new Tpl();
        return $tpl->SetTplParam('toUrl', Request::domain() . 'index.php/admin/index')
            ->Display('admin_login');
    }

    public function Out()
    {
        Request::session(SESSION_AdminKey, null, true);
        $tpl = new Tpl();
        $tpl->Notice(Route\language('User_LoginOut'),self::ToMeUrl('member/adminPublicLogin'));
    }

    private function _checkPost()
    {
        $this->checkPostFieldIsEmpty('login', 'User_NameEmpty');
        $this->checkPostFieldIsEmpty('password', 'User_PassEmpty');
    }

    private function _session($userInfo)
    {
        $admin = array('siteId' => $userInfo['siteId'], 'uid' => $userInfo ['id'],
            'username' => $userInfo ['username'], 'groupid' => $userInfo ['groupid']);
        Request::session(SESSION_AdminKey, $admin);
    }
}