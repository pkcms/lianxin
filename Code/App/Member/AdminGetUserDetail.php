<?php


namespace PKApp\Member;


use PKApp\Admin\Classes\AdminController;
use PKApp\Admin\Classes\AdminDataBase;
use PKApp\Member\Classes\MemberDataBase;
use PKApp\Model\Classes\ModelDatabase;
use PKApp\Model\Classes\ModelExtend;
use PKCore\Converter;
use PKCore\Formats;
use PKCore\Request;
use PKCore\Tpl;

class AdminGetUserDetail extends AdminController
{

    public function Main()
    {
        $type = Request::get('type');
        $this->_tpl()->SetTplParam('actionType', $type);
        $userId = Formats::isNumeric(Request::get('id'));
        switch ($type) {
            case 'add':
            case 'info':
                $this->_byInfo($userId);
                break;
            case 'pass':
                $this->_byPassWord($userId);
                break;
        }
        return $this->_tpl()->Display('admin_form');
    }

    private function _byInfo($userId)
    {
        if (!empty($userId)) {
            $userEntity = MemberDataBase::CheckAndGetUserInfo(array('id' => $userId));
            is_array($userEntity) ?: \PKCore\alert('User_Empty');
            $modelid = $userEntity['modelid'];
            $this->_tpl()->SetTplParamList($userEntity);
        } else {
            $modelid = Request::get('modelId');
            $this->_tpl()->SetTplParam('modelid', $modelid);
        }
        $typeId = ModelExtend::GetTypeId(self::getDict('modelTypeLists'), 'member');
        $db_model = new ModelDatabase();
        $modelList = $db_model->GetLists($typeId, null, array('id', 'name'));
        !Formats::isArray($modelList) ?: $modelList = Converter::arrayColumn($modelList, 'name', 'id');
        $this->_tpl()->SetTplParam('modelList', $modelList);
        $groupList = MemberDataBase::GetGroupLists(array('id', 'name'), array('modelid' => $modelid));
        !Formats::isArray($groupList) ?: $groupList = Converter::arrayColumn($groupList, 'name', 'id');
        $this->_tpl()->SetTplParam('groupList', $groupList);
        $siteList = AdminDataBase::GetSiteList(array('id', 'Site_Name'));
        !Formats::isArray($siteList) ?: $siteList = Converter::arrayColumn($siteList, 'Site_Name', 'id');
        $this->_tpl()->SetTplParam('siteList', $siteList);
    }

    private function _byPassWord($userId)
    {
        if (!empty($userId)) {
            $userEntity = MemberDataBase::CheckAndGetUserInfo(array('id' => $userId));
            is_array($userEntity) ?: \PKCore\alert('User_Empty');
            $this->_tpl()->SetTplParamList($userEntity);
        }
    }

    private function _tpl()
    {
        static $tpl;
        !empty($tpl) ?: $tpl = new Tpl();
        return $tpl;
    }
}