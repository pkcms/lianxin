<?php


namespace PKApp\Member;


use PKApp\Admin\Classes\AdminController;
use PKApp\Member\Classes\MemberDataBase;
use PKCore\Converter;
use PKCore\Formats;
use PKCore\Request;
use PKCore\Statics;
use PKCore\Tpl;

class AdminGetUserList extends AdminController
{

    public function Main()
    {
        switch (Request::get('type')) {
            case 'admin':
                $this->loginUser()->GroupId == 1 ?: \PKCore\alert('User_EditPower_Error');
                $modelId = 1;
                break;
            default:
                $modelId = 2;
                break;
        }
        $options = array('modelid' => $modelId);
        $this->GetPages();
        $userCount = MemberDataBase::GetUserCount($options);
        $userList = MemberDataBase::GetUserLists($options,
            array('id', 'username', 'email', 'mobile', 'addtime', 'lasttime', 'lastip', 'modelid', 'groupid'),
            self::$pageSize, self::$pageIndex);
        $pageList = Statics::pages($userCount, self::$pageSize, self::$pageIndex,
            '/index.php/member/AdminGetUserList?pageIndex=[page]');
        $groupList = MemberDataBase::GetGroupLists(array('id', 'name'));
        !Formats::isArray($groupList) ?: $groupList = Converter::arrayColumn($groupList, 'name', 'id');
        $tpl = new Tpl();
        return $tpl->SetTplParam('pageList', $pageList)
            ->SetTplParam('userList', $userList)
            ->SetTplParam('modelId', $modelId)
            ->SetTplParam('groupList', $groupList)
            ->Display('admin_lists');
    }
}