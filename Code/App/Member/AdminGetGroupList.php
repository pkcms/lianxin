<?php


namespace PKApp\Member;


use PKApp\Admin\Classes\AdminController;
use PKApp\Member\Classes\MemberDataBase;
use PKCore\Tpl;

class AdminGetGroupList extends AdminController
{

    public function Main()
    {
        $groupList = MemberDataBase::GetGroupLists(array('id', 'name', 'disabled', 'issystem'));
        $tpl = new Tpl();
        return $tpl->SetTplParam('groupList', $groupList)
            ->Display('group_lists');
    }
}