<?php
/**
 * 自动加载
 * User: Administrator
 * Date: 2019/5/24
 * Time: 9:53
 */


/**
 * @param $class
 */
function autoLoad($class)
{
    $root_name = strstr($class, '\\', true);
//    print_r($nameArr);
    if (stristr($root_name, 'PK')) {
        $class = str_replace('\\', DS, ltrim($class, 'PK'));
        switch ($root_name) {
            case 'PKCore':
            case 'PKDriver':
                PKCore\Route\isLoadingFile(PATH_LIB . $class);
                break;
            case 'PKApp':
                $class = substr($class, strlen('App' . DS));
                PKCore\Route\moduleClasses($class);
                break;
        }
    }
}

spl_autoload_register('autoLoad');
require_once dirname(__FILE__) . DS . 'Config.php';
require_once PATH_LIB . 'Core' . DS . 'Func.php';
require_once PATH_LIB . 'Core' . DS . 'Route.php';
