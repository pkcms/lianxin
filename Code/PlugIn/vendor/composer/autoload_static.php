<?php

// autoload_static.php @generated by Composer

namespace Composer\Autoload;

class ComposerStaticInit79a12757d0f1f64bef943078f8bdb8fb
{
    public static $prefixLengthsPsr4 = array (
        'P' => 
        array (
            'Psr\\Log\\' => 8,
        ),
        'M' => 
        array (
            'Monolog\\' => 8,
        ),
        'G' => 
        array (
            'Grafika\\' => 8,
        ),
    );

    public static $prefixDirsPsr4 = array (
        'Psr\\Log\\' => 
        array (
            0 => __DIR__ . '/..' . '/psr/log/Psr/Log',
        ),
        'Monolog\\' => 
        array (
            0 => __DIR__ . '/..' . '/monolog/monolog/src/Monolog',
        ),
        'Grafika\\' => 
        array (
            0 => __DIR__ . '/..' . '/kosinix/grafika/src/Grafika',
        ),
    );

    public static function getInitializer(ClassLoader $loader)
    {
        return \Closure::bind(function () use ($loader) {
            $loader->prefixLengthsPsr4 = ComposerStaticInit79a12757d0f1f64bef943078f8bdb8fb::$prefixLengthsPsr4;
            $loader->prefixDirsPsr4 = ComposerStaticInit79a12757d0f1f64bef943078f8bdb8fb::$prefixDirsPsr4;

        }, null, ClassLoader::class);
    }
}
