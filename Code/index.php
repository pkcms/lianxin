<?php

// +----------------------------------------------------------------------
// | Pkcms [ Pk Content Management System ]
// +----------------------------------------------------------------------
// | Copyright (c) 2012-now http://pkcms.cn All rights reserved.
// +----------------------------------------------------------------------
// | Author: zhangh <zhangh@jishuzai.com>
// +----------------------------------------------------------------------
// Pk Framework 入口文件

define('DS', DIRECTORY_SEPARATOR);
define('ASK_PHP_VER', '5.3.0');
// 判断PHP版本号
if (!version_compare(PHP_VERSION, ASK_PHP_VER, '>')) {
    exit('Your current PHP version (' . PHP_VERSION . ') is less than the minimum requirement (' . ASK_PHP_VER . ')');
}
require_once dirname(__FILE__) . DS . 'AutoLoad.php';

//Session保存路径
//$sessSavePath = PATH_TMP . "sessions" . DS;
//file_exists(PATH_TMP) || mkdir(PATH_TMP);
//file_exists($sessSavePath) || mkdir($sessSavePath);
//if (is_writeable($sessSavePath) && is_readable($sessSavePath)) {
//    session_save_path($sessSavePath);
//}

eval('PKCore\Route\gotoApps();');
