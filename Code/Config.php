<?php
/**
 * 底层配置
 * User: wz_zh
 * Date: 2017/8/11
 * Time: 9:32
 */

error_reporting(E_ALL & ~E_NOTICE);
ini_set('display_errors', 1);
ini_set('memory_limit', '128M');
ini_set("output_buffering", 1);
header("Access-Control-Allow-Credentials:true");
header("Connection: keep-alive");
header('Access-Control-Max-Age: 86400');
// 响应类型
header('Access-Control-Allow-Methods:get,post,put');
date_default_timezone_set('PRC');
define('SYS_TIME', microtime(TRUE));
define('CHARSET', 'utf-8');

define('TIMESTAMP', time());
define('PATH_PK', dirname(__FILE__) . DS);
define('PATH_ROOT', dirname(PATH_PK) . DS);
define('PATH_TMP', PATH_ROOT . 'Tmp' . DS);
define('PATH_APP', PATH_PK . 'App' . DS);
define('PATH_LIB', PATH_PK . 'Lib' . DS);
define('PATH_CACHE', PATH_ROOT . 'Cache' . DS);
define('PATH_DATA', PATH_CACHE . 'Data' . DS);

define('SESSION_AdminKey', 'adminID');
define('COOKIE_UserKey', 'Login');
